<?php
/**
 * WordPress Cron Implementation for retailer autobid price check
 * @package WordPress
 */

ignore_user_abort(true);

/**
 * Tell WordPress we are doing the CRON task.
 *
 * @var bool
 */

if ( !defined('ABSPATH') ) {
	/** Set up WordPress environment */
	require_once( dirname( __FILE__ ) . '/wp-load.php' );
}

global $wpdb;
$tableJobs = $wpdb->prefix . 'jobs';
$tableQuotation = $wpdb->prefix . 'jobquotation';
$table_retailFeed = $wpdb->prefix ."feedRetalProducts";							 

$sql = "SELECT *, NOW() as corTstmp FROM $tableJobs WHERE `status` = '1'";
$results = $wpdb->get_results($sql);

foreach($results as $result){
	

	$quotationSendDate = strtotime($result->quotationSendDate);	
	$corTstmp = strtotime($result->corTstmp + "5 minutes");
	
	$productId = $result->productId;
	$loadProduct = wc_get_product( $productId );
	$productSku = $loadProduct->get_sku();
	
	if($corTstmp >= $quotationSendDate){
					  
	   $sqlQuotations = "SELECT * FROM `$tableQuotation` WHERE `jobId` = '".$result->id."' AND `quotationAmount` > 0 AND status = 1 AND autoBidStatus = 1";
	   $quotationResults = $wpdb->get_results($sqlQuotations);
	   
	   if(count($quotationResults)>0){
		 foreach($quotationResults as $quotationResult){
			$quotationAmount = $quotationResult->quotationAmount;
			$autoBidAmount   = $quotationResult->autoBidAmount;
			if($quotationAmount == $autoBidAmount){
				
				 $feedProductSql = "SELECT * FROM $table_retailFeed WHERE retailerId = '".$quotationResult->retailerId."' AND 	sku = '$productSku'";
				 $feedProduct = $wpdb->get_row($feedProductSql);
				 
				 $price = $feedProduct->price;
				 if($quotationAmount != $price){
					 $update_val = array('quotationAmount' => $price,'autoBidAmount' => $price);
				     $where = array('id' => $quotationResult->id);
				     $wpdb->update($tableQuotation,$update_val,$where);
				 }
				 
			}
		 }
	   }
	}
}
?>