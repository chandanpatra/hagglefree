<?php

/*********************************************************************************
* LiveZilla config.inc.php
* 
* Copyright 2013 LiveZilla GmbH
* All rights reserved.
* LiveZilla is a registered trademark.
* 
* Improper changes in this file may cause critical errors.
* To modify LiveZilla Server settings it is strongly recommended to use 
* LiveZilla Server Admin application and desist from editing this file directly.
* 
********************************************************************************/ 

// ALL VALUES ARE BASE64 ENCODED (encoder/decoder can be found on the net)

$_CONFIG["gl_lzid"] = "ZWE2NGQ2MGQ=";
$_CONFIG["gl_lzst"] = "MA==";
$_CONFIG["gl_pr_csp"] = "YWVkZWQ5NDhhNzM2N2Q5ZTZiMDQ0N2NmNjRkNzBhOWQ=";
$_CONFIG["gl_pr_nbl"] = "N2EyYzVkZWE2ZTEyNTFmOTY0Yzk4MmZiMDk3YThhYmI=";
$_CONFIG["gl_pr_ngl"] = "ZmQxNGIyMmU1MzA1OWQ4ZTU5ZDU1MWIwMmUzN2JlYjk=";
$_CONFIG["gl_pr_str"] = "Y2NmMzMzNTI4NmZmMmU4YTYyZmI2NDIzNmZjODY0ODc=";
$_CONFIG["gl_pr_st"] = "MF8wXzA=";
$_CONFIG["gl_pr_cr"] = "MTQ3MDQ5NTExMw==";
$_CONFIG["gl_crc3"] = "TVRRM01EUTVOVEV4TXl3d0xEQXNNQ3d3TERBc1ptUXhOR0l5TW1VMU16QTFPV1E0WlRVNVpEVTFNV0l3TW1Vek4ySmxZams9";
$_CONFIG["gl_licl"][0] = "WVRveU9udHBPakE3Y3pvME5Eb2lXa2RSZUUxcVp6Uk9WRVV4VG5wak1FMXFWbXRQUkZadFdsZFdhRTFFYkdsYVZHeHJUbnBCZWsxVVJUMGlPMms2TVR0ek9qZzZJbFpHU2twUlZYYzlJanQ5";

// Database settings hagglefree.com.au
$_CONFIG[0]["gl_db_host"] = "bG9jYWxob3N0";
$_CONFIG[0]["gl_db_user"] = "aGFnZ2xlZnJfbGl2ZQ==";
$_CONFIG[0]["gl_db_ext"] = "bXlzcWw=";
$_CONFIG[0]["gl_db_eng"] = "SW5ub0RC";
$_CONFIG[0]["gl_db_pass"] = "MXFhejJ3c3gzZWRj";
$_CONFIG[0]["gl_db_name"] = "aGFnZ2xlZnJfbGl2ZQ==";
$_CONFIG[0]["gl_db_prefix"] = "bGl2ZXppbGxhXw==";
$_CONFIG[0]["gl_host"] = "aGFnZ2xlZnJlZS5jb20uYXU=";
$_CONFIG[0]["gl_root"] = "MQ==";
$_CONFIG[0]["gl_datprov"] = "MQ==";

// Installer login hagglefree.com.au
$_CONFIG[0]["gl_insu"] = "YWRtaW5pc3RyYXRvcg==";
$_CONFIG[0]["gl_insp"] = "ODdmZDg4NDk5N2I1OTkyYjZhNzYzZmZkZWQwNjYwMGY=";


?>