<?php

/**
 * WordPress Cron Implementation for retailer
 * @package WordPress
 */
ignore_user_abort(true);

/**
 * Tell WordPress we are doing the CRON task.
 *
 * @var bool
 */
ini_set('max_execution_time', 3650);

if ( !defined('ABSPATH') ) {
	/** Set up WordPress environment */
	require_once( "../wp-load.php" );
}

global $wpdb;

/*$tableJobs = $wpdb->prefix . 'jobs';
$sql = "SELECT *, NOW() as corTstmp FROM $tableJobs WHERE `status` = '1'";
$results = $wpdb->get_results($sql);*/
$tableFeed = $wpdb->prefix . 'feedRetalProducts';

// get retailers feed url


       $feed_url = "http://www.hagglefree.com.au/hfeed/feeddata.txt";
		
		//$homepage = curl_get_contents($feed_url);
		$homepage = file_get_contents($feed_url);
		$data = explode("\n",$homepage);
		$retailerId = 257;
		foreach($data as $rec){
			if(trim($rec)!="")
			{
				$fsku = "";
				$price = "";
				$stockQ = "";
				$condition = "";
				$url = "";
								
				$pdata = explode("|",$rec);
				
				if(trim($pdata[0])!=""){
					$fsku = trim($pdata[0]);
				}
				$price = (float)trim($pdata[3]);
				$stockQ = trim($pdata[4]);
				$condition = trim($pdata[5]);
				$url = trim($pdata[6]);
				
				 $sql = "SELECT * FROM $tableFeed WHERE `retailerId` = '$retailerId' AND `sku` = '$fsku'";
				 $result = $wpdb->get_row($sql);
				 if($result){
					$sqlupdate = "UPDATE $tableFeed SET `price` = '$price',`qty`='$stockQ', `productCondition` = '$condition',`productUrl` = '$url',`status` = '2' WHERE `retailerId` = '$retailerId' AND `sku` = '$fsku'"; 
					$wpdb->query($sqlupdate);
				 }else{
					$sqlInsert = "INSERT INTO $tableFeed SET `price` = '$price',`qty`='$stockQ',`retailerId` = '$retailerId',`sku` = '$fsku', `productCondition` = '$condition',`productUrl` = '$url',`status` = '2'"; 
					$wpdb->query($sqlInsert);
				 }
				
			}
			   unset($pdata);
		}
		$sqlDelete = "DELETE FROM $tableFeed WHERE `retailerId` = '$retailerId' AND `status` = '1'";
		$wpdb->query($sqlDelete);
		
		$sqlUpdate = "UPDATE $tableFeed SET `status` = '1' WHERE `retailerId` = '$retailerId' AND `status` = '2'";
		$wpdb->query($sqlUpdate);
	
	?>