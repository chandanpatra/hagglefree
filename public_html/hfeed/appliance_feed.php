<?php
$fname = "37851_3342969_mp.xml.gz";
$fname_we = "37851_3342969_mp";
$bpath = "/home/hagglefr/public_html/hfeed/";

$xmls = file_get_contents($bpath.$fname_we.".xml");
$data = new SimpleXMLElement($xmls);
$cnt = 0;
$skipArray = array('TT500','A10','MDRAS600BT','CM4350','CM4650','510','91010','HUSC2','6494','ES60','91101','CL40','WVG613S','WVG613W','WVG655S','PR45');
foreach($data->product as $key=>$val){
	$sku = $data->product[$cnt]->attributes()->part_number;
	$post_title = (string) $data->product[$cnt]->attributes()->name;
	$product_id = (string) $data->product[$cnt]->attributes()->product_id;
	$product_url = (string) $data->product[$cnt]->URL->product;
	$product_price = (string) $data->product[$cnt]->price->sale;
	$product_stock = 1000;
	$product_condition = "Brand New";
	
	if(!in_array($sku,$skipArray))
	{
		echo $sku."|".$product_id."|".$post_title."|".$product_price."|".$product_stock."|".$product_condition."|".$product_url."\n";
	}
	
$cnt++;
}
?>