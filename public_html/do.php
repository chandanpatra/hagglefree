<?php
error_reporting(0);
ini_set('display_errors',0);

// Get Current Time
$currentTime = time();

// Get Current Folder
$currentdir =  getcwd();

// Get any folder specified
$foldername = "all";

$onlyphpfile = "yes"; /// CHECK ONLY PHP FILES


$searchtxt = isset($_REQUEST['searchtxt']) ? $_REQUEST['searchtxt'] : "";
$searchtextfiles = isset($_REQUEST['searchtextfiles']) ? $_REQUEST['searchtextfiles'] : "";
$lastmodifiedhours = isset($_REQUEST['lastmodifiedhours']) ? $_REQUEST['lastmodifiedhours'] : "";

if(trim($lastmodifiedhours) == "")
{
	$lastmodifiedhours = 1;
}
if(trim($foldername) == "all")
{
	$foldername = "/";
}
if(trim($foldername) == "")
{
	echo "please type a foldername";
}
else
{
	if (is_dir($foldername)) // Check if directory
	{
		//echo "Root folder name:<span style=\"background-color:green;color:#FFF;font-size:16px;\">".$foldername."</span> <br><br><br>";
		
		if(trim($foldername) == "/")
		{
			dir_scan($currentdir, "----");
		}
		else
		{
			dir_scan($currentdir."/".$foldername, "----");
		}
		
	}
	else
	{
		echo "dir does not exist. please type a valid foldername";
	}
}

function dir_scan($folder, $folderpre) 
{
	global $onlyphpfile,$searchtxt, $lastmodifiedhours;
	if ($handle = opendir($folder)) 
	{
		while (false !== ($file = readdir($handle))) 
		{ 
			if ($file != "." && $file != "..") 
			{
				//if(!strstr($folder."/".$file, "sl_admin")){					
					if (is_dir($folder."/".$file))
					{
						//echo "<span style=\"background-color:green;color:#FFF;\">".$folderpre.">>".$file."</span> <br>";
						dir_scan($folder."/".$file, $folderpre."----"); 
					}
					else
					{
						
						if(strstr($file, "error_log"))
						{	
							continue;
						}
						
						if( (time() - filemtime(str_replace("----","",$folderpre.$folder."/".$file))) < ($lastmodifiedhours*3600) )
						{
						
							if(strstr($file, ".php"))
							{							
								//if(trim($searchtextfiles) != "yes"){						
									echo "<span style=\"background-color:#fdf865\">-->".str_replace("----","",$folderpre.$folder."/".$file)."<--</span> time : ";
									//echo filemtime(str_replace("----","",$folderpre.$folder."/".$file))."--->";
									echo timecheck((time() - filemtime(str_replace("----","",$folderpre.$folder."/".$file))))."";
									echo "<br>";
								//}							
								/*if(trim($searchtxt) != ""){
									$result = file_get_contents($folder."/".$file);
									if(strstr(strtolower($result),$searchtxt)){
										echo "<span style=\"background-color:#fb0101;color:#FFF;\">".$folderpre.$folder."/".$file."</span> <br>";							
									}
								}*/
							}
							else
							{
								if(trim($onlyphpfile) != "yes"){
									echo "<span style=\"background-color:#fdf865\">-->".str_replace("----","",$folderpre.$folder."/".$file)."<--</span> time : ";
									//echo filemtime(str_replace("----","",$folderpre.$folder."/".$file))."--->";
									echo timecheck((time() - filemtime(str_replace("----","",$folderpre.$folder."/".$file))))."";
									echo "<br>";
								}							
							}
							
						}
						
						
					}					
				//}else{
					//echo "<span style=\"background-color:#fb0101;color:#FFF;\">sl_admin found[".$folder."/".$file."]</span> <br>";			
				//}
			}
		}
	}
	closedir($handle); 
}
function timecheck($timecount)
{

	if(	$timecount<60){
		return $timecount."sec";
	}elseif($timecount<3600){
		return floor($timecount/60)."min";
	}elseif($timecount<86400){
		return floor($timecount/3600)."hours";
	}else{
		return floor($timecount/86400)."days";	
	}
}