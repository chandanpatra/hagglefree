<?php
/**
 * WordPress Cron Implementation for retailer
 * @package WordPress
 */

ignore_user_abort(true);

/**
 * Tell WordPress we are doing the CRON task.
 *
 * @var bool
 
============Status of Jobs================
 0 ====> New job account not verified.
 1 ====> New job
 2 ====> Quotation send to cutomer 
 
 10 ====> Quotation not available
 
 
 ============Status of Quotations================
 0 ====> Request for Quotation
 1 ====> Quotation submited
 2 ====> Quotations selected to send to cutomer

 
*/


if ( !defined('ABSPATH') ) {
	/** Set up WordPress environment */
	require_once( dirname( __FILE__ ) . '/wp-load.php' );
}

global $wpdb;
$tableJobs = $wpdb->prefix . 'jobs';
$sql = "SELECT * FROM $tableJobs WHERE `status`=0";

$job_results = $wpdb->get_results($sql);

foreach($job_results as $job_result){
	$userId = $job_result->userId;
	$job_id = $job_result->id;
	$user_status = get_user_meta( $userId, 'cr_status', true );
	$social_user = get_user_meta( $userId, 'wsl_current_provider', true );
	if(($user_status == "active") || ($social_user!="") ||(get_option('hold_job_on_verification')=="1")){
		
				//assign job to retailer
				
				$job_no_of_quotes = get_option('job_no_of_quotes');
				$job_retailer_tobe_informed = get_option('job_retailer_tobe_informed');
				$job_increments_radius = get_option('job_increments_radius');
				$job = get_job("",$job_id);
				$email_job_details = email_job_details($job_id);
				$retailers = get_retailer_list($job_id);
				$userId = $job->userId;

				$lat = $job->lat;
				$long = $job->long;
				$i=0;
				foreach($retailers as $retailer){
					$retailers[$i++]['dist'] = distance($lat,$long,$retailer['lat'],$retailer['long'],"K");
				}
				
				$retailers = record_sort($retailers,'dist');
				//print_r($retailers);
				$count = 0;
				$table_jobquotation = $wpdb->prefix . 'jobquotation';	
				foreach($retailers as $retailer){
					

					if($count>$job_retailer_tobe_informed){
						break;
					}else{
						
						$insert_val = array(
						  'jobId'           => $job_id,
						  'retailerId'     => $retailer['user_id'],
						  'assignedDate'   => date("Y-m-d H:i:s")
						 );
						 
						 $insert = $wpdb->insert( $table_jobquotation, $insert_val);
						 $quote_id =  $wpdb->insert_id;
						 //==========Calculate lead fee and insert in database===============//
			
							$userDiscount = (float)get_user_meta($retailer['user_id'], 'cr_lead_fee', true);
							if($userDiscount > 0){			
							   $calLeadFee = ($rrp*$leadFee)/100;
							   $calLeadFee = ($calLeadFee - (($calLeadFee*$userDiscount)/100));
							}
							else{
							   $calLeadFee = ($rrp*$leadFee)/100;
							}
							if($calLeadFee > 0){
								$leadFeeText = "<b> $".round($calLeadFee,2)."</b><br />";
							}else{
								$leadFeeText = "<b>No Lead fee applicable.</b><br />";
							}
				  //print_r($insert_val);
				  if($insert){			  
					  $headers[] = 'Content-Type: text/html; charset=UTF-8';
					  $headers[] = 'From: '.get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'> ' . "\r\n";
					  $template_name = "quotation_request_for_new_job";					 
						
					  $media_url = get_stylesheet_directory_uri()."/img";
			          //Codes: {site_url}, {site_tag}, {media_url}, {email_heading}, {site_name}, {retailer_name}, {post_date}, {quotation_url}, {product_details}, {customer_requirements}, {admin_email}, {site_abn}, {site_phone}, {fb_url} 
					  $template_detail = get_email_template($template_name);
					  $product = wc_get_product( $job->productId );	
					   $company_name = get_user_meta($retailer['user_id'],'cr_company',true);
					  if($company_name == ""){
					  	$company_name = $retailer['display_name'];
					  }
					  $product_details ="
	   					<ul style=\"width:100%; float:left; margin:0 0 15px 0; padding:0\">
							<li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#ff8e2a; border-radius:4px 4px 0 0; text-align:left; color:#fff; font-weight:bold; font-size:16px\">
								".esc_html( $product->get_title() )."
							</li>
							<li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#fff; border-radius:0 0 4px 4px; text-align:left; color:#000; font-weight:normal; font-size:14px\">
								Job id: <span style=\"color: #ff5400;\">".$job->id."</span><br />
								SKU: <span style=\"color: #ff5400;\">".$product->get_sku()."</span><br />
								Quantity: <span style=\"color: #ff5400;\">".$job->quantity."</span>
							</li>
						</ul>";
					  $post_date = "<span style=\"color: #ff5400;\">".date("Y-m-d",strtotime($job->jobDate))."</span> <span style=\"color: #787878;\">".date("H:i:s",strtotime($job->jobDate))."</span>";
					  $customer_requirements = "<ul style=\"width:100%; float:left; margin:0 0 15px 0; padding:0\">
									<li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#000; border-radius:4px 4px 0 0; text-align:left; color:#fff; font-weight:normal; font-size:14px\">Customer's Requirement(s):</li>
									<li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#fff; border-radius:0 0 4px 4px; text-align:left; color:#000; font-weight:normal; font-size:14px\">";
						
						///get dynamic attributes ////
						$das = get_dynamic_job_attributes($job->id);
						if(count($das)>0){
							$cr_da ="";
							foreach($das['key'] as $key => $value){
								if($value == "wccpf_when_do_you_need_the_product")
									$cr_da .= $das['label'][$value]." : <span style=\"color: #ff5400;\">".product_delivery_switch($das['customer_choice'][$value])."</span><br />";
								else
									$cr_da .= $das['label'][$value]." : <span style=\"color: #ff5400;\">".ucfirst($das['customer_choice'][$value])."</span><br />";
							}
							$customer_requirements .=$cr_da;
						}
						
						$customer_requirements .="
										Usage type: <span style=\"color: #ff5400;\">".($job->is_commercial=='1'?'Commercial':'Private')."</span>
									</li>
								</ul>";	
								$buyer_details = "<ul style=\"width:100%; float:left; margin:0 0 15px 0; padding:0\">
								<li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#000; border-radius:4px 4px 0 0; text-align:left; color:#fff; font-weight:normal; font-size:14px\">Buyer Details:</li>
								<li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#fff; border-radius:0 0 4px 4px; text-align:left; color:#000; font-weight:normal; font-size:14px\">
									<div style=\"width:100%;\">Customer: <span style=\"color: #ff5400;\">".$job->firstname." ".$job->lastname."</span></div>									
									<div style=\"width:100%;\">Postcode: <span style=\"color: #ff5400;\">".$job->Postcode."</span></div>";
				/*if(get_option('show_phone_register_form')=="")
					$buyer_details .= "<div style=\"width:100%;\">Phone: <span style=\"color: #ff5400;\">".$job->telephone."</span></div>";*/
				$buyer_details .="
								</li>
							</ul>";
					  //token replacement	
					  /*
						{site_name},{site_url},{media_url},{email_heading},{company_name},{post_date},{product_details},{customer_requirements}
						{quotation_url},{lead_fee},{admin_email},{site_abn},{site_phone},{fb_url}
					  */	
					  $mail_content = str_replace("{site_name}",get_bloginfo( 'name' ),stripslashes($template_detail['email_template']));
					  
					   //======== Token Replacement =================//
						$mail_content = str_replace("{site_url}",home_url(),$mail_content);
						$mail_content = str_replace("{site_tag}",get_bloginfo( 'description' ),$mail_content);
						$mail_content = str_replace("{media_url}",$media_url,$mail_content);	
						$mail_content = str_replace("{email_heading}",stripslashes($template_detail['email_heading']),$mail_content);
						
					
						$mail_content = str_replace("{company_name}",$company_name,$mail_content);
						$mail_content = str_replace("{post_date}",$post_date,$mail_content);
						$mail_content = str_replace("{product_details}",$product_details,$mail_content);
						$mail_content = str_replace("{buyer_details}",$buyer_details,$mail_content);
						$mail_content = str_replace("{customer_requirements}",$customer_requirements,$mail_content);
						$mail_content = str_replace("{quotation_url}",get_site_url()."/retailer-job-detail/?quote_id=".$quote_id,$mail_content);
						$mail_content = str_replace("{lead_fee}",$leadFeeText,$mail_content);	
					
			
						$mail_content = str_replace("{admin_email}",get_option( 'admin_email' ),$mail_content);
						$mail_content = str_replace("{site_abn}",get_option( 'site_abn' ),$mail_content);
						$mail_content = str_replace("{site_phone}",get_option( 'site_phone' ),$mail_content);
						$mail_content = str_replace("{fb_url}",get_option( 'fb_url' ),$mail_content);
						
						$subject = $template_detail['subject'];
						$subject = str_replace("{job_id}",$job->id,$subject);
						$subject = str_replace("{sku}",$product->get_sku(),$subject);
						
						//======== Token Replacement =================//
					  
					  wp_mail( $retailer['email'], $subject, $mail_content,$headers);			
					  
				  }
					  $count++;
					}
				}// end for
				
					// update job status
					$update_val = array('status' => 1);
					$where = array('id' => $job_id);	
					$wpdb->update($tableJobs,$update_val,$where);
	}
}