<?php 

/*-Needs to search on date, name, email and request number
-show date, time, job number, name, postcode, 
# of merchants invited, # of quotes, lowest price (product only), 
Status (this can be time left, closed, purchased, deleted etc) option to close the job here
-once you click on a job then the request details along will all invites, all quotes received and flag which quotes were successful, 
show lead fee, quote numbers, request numbers etc etc this is the only place we can use for support
*/

function lead_fee_stats(){
	global $wpdb, $pagenum, $limit, $start;
	$tableJobQuotation = $wpdb->prefix ."jobquotation";
		if(trim(get_option('job_limit'))){
			$limit = get_option('job_limit');
		}else{
	 		$limit = 10; // number of jobs in page
		}
	$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;
	$start =  ($pagenum - 1) * $limit;
?>
<div class="wrap">
<div id="icon-users" class="icon32"><br/></div>
<h2>Lead Fee Stats</h2>
<?php
 $startN    = new DateTime($wpdb->get_var("SELECT MIN( quotationDate ) as minJobDate  FROM  $tableJobQuotation"));
 $endN = new DateTime( $wpdb->get_var("SELECT MAX( quotationDate ) as maxJobDate  FROM  $tableJobQuotation"));
 $periodN = new DatePeriod($startN->modify('first day of this month'), DateInterval::createFromDateString('1 month'),$endN->modify('last day of this month'));
 /*echo "<pre>";
 //print_r($startN);
 //print_r($endN);
 print_r($periodN);
 echo "</pre>";*/
?>
<form action="<?php echo admin_url('admin.php?page=lead_fee_stats'); ?>" id="searchleadsfrm" name="searchleadsfrm" method="get">
<input type="hidden" name="page" value="lead_fee_stats" />
<p class="search-box">
	
	<select name="leaddaterange" id="leaddaterange">
    	<option value="">Select date</option>
        <?php foreach ($periodN as $dtN) { ?>
        <option value="<?php echo $dtN->format("Y-m");?>" <?php if($_REQUEST['leaddaterange'] == $dtN->format("Y-m")){?> selected="selected" <?php } ?>><?php echo $dtN->format("M-Y");?></option>
        <?php } ?>
    </select>
    <input type="submit" value="Search Leads" class="button" id="search-submit">
    
</p>
</form>
<table class="widefat" width="100%" border="0" cellspacing="5" cellpadding="5">
	<thead>
      <tr>
        <th><strong>Product Name</strong></th>
        <th><strong>Retailer Name</strong></th>
        <th><strong>Quotation Date</strong></th>        
        <th><strong>Lead Fee</strong></th>
        <th><strong>Lead Discount</strong></th>
        <th><strong>Total Lead Fee</strong></th>        
      </tr>
  </thead>
  <tfoot>
      <tr>
        <th><strong>Product Name</strong></th>
        <th><strong>Retailer Name</strong></th>
        <th><strong>Quotation Date</strong></th>        
        <th><strong>Lead Fee</strong></th>
        <th><strong>Lead Discount</strong></th>
        <th><strong>Total Lead Fee</strong></th> 
      </tr>
  </tfoot>
  <tbody>
	 <?php
	 	
		$datefilterString = $_REQUEST['leaddaterange'];
		$offset = ( $pagenum - 1 ) * $limit;
		
		$leadFeeQuery = "select `id`,`jobId`,`retailerId`, `quotationDate`,`leadFee`,`leadDiscount`,`totalLeadFee` from `$tableJobQuotation` where 1=1 ";
	    $condition = ''; 
		if($datefilterString){
			$condition .= " AND date_format(`quotationDate`, '%Y-%m')='$datefilterString'";
		}
		
	
		$leadFeeQuery .= $condition;
				
		$leadFeers = $wpdb->get_results($leadFeeQuery);
		
		
		$total = count($leadFeers);
		$num_of_pages = ceil( $total / $limit );
		
		/*echo "<pre>";
		print_r($leadFeers);
		echo "</pre>";*/
        // Array of WP_User objects.
        foreach ( $leadFeers as $leedfeeData ) {
            //////////////////////
		$job = get_job("",$leedfeeData->jobId); 	
        $product = wc_get_product( $job->productId );
            
        $user_info = get_userdata($leedfeeData->retailerId);
        $first_name = $user_info->first_name;
        $last_name = $user_info->last_name; 
        
            ?>
			<tr>
            <td><span><a href="<?php echo admin_url('admin.php?page=lead_details_on_dashboard&quote_id='.$leedfeeData->id); ?>"><?php echo esc_html( $product->get_title() ) ; ?></a></span></td>
            <td nowrap><span><?php echo esc_html( $first_name." ".$last_name ) ; ?></span></td>
            <td nowrap><span><?php echo date("F j, Y, g:i a",strtotime($leedfeeData->quotationDate)) ; ?></span></td>			
			<td><?php echo $leedfeeData->leadFee; ?></td>
            <td><?php echo $leedfeeData->leadDiscount; ?></td>
            <td><?php echo $leedfeeData->totalLeadFee; ?></td>		
            
			</tr>
        <?php
            
        }
		$page_links = paginate_links( array(
			'base' => add_query_arg( 'pagenum', '%#%' ),
			'format' => '',
			'prev_text' => __( '&laquo;', 'text-domain' ),
			'next_text' => __( '&raquo;', 'text-domain' ),
			'total' => $num_of_pages,
			'current' => $pagenum
		) );
		
		if ( $page_links ) {
			echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $page_links . '</div></div>';
		}
    ?>
   </tbody>
</table>
 </div>
<?php	
}
function lead_details_on_dashboard(){
	global $wpdb;
?>

<h2>Lead Details</h2>
<hr/>
<?php
$id= $_GET['quote_id'];

$leadFeeQuery = "select `id`,`jobId`,`retailerId`, `quotationDate`,`leadFee`,`leadDiscount`,`totalLeadFee` from `$tableJobQuotation` where id= $id ";
$leadFeers = $wpdb->get_results($leadFeeQuery);
$job = get_job("",$leadFeers->jobId);
//print_r($job);
$product = wc_get_product($job->productId);
//echo $p->get_title();
//echo $job->productId;
//$jobs = $jobs['results'];
//print_r($jobs);
//foreach ( $jobs as $job ) {
            //////////////////////
        //$product = wc_get_product( $job->productId );
            
        $user_info = get_userdata($job->userId);
		if($job->retailerId !='0'){
			$retailer_info = get_userdata($job->retailerId);
		}
        $first_name = $user_info->first_name;
        $last_name = $user_info->last_name; 
		$address = $job->Postcode;
        $status = "";
        switch($job->status){
            case 1:
            $status = "New Job";
            break;
            case 2:
            $status = "Retailer Assigned";
            break;
            case 3:
            $status = "Accepted Quote";
            break;
        }
		?>
        <div class="admin-job-detail">
        	<fieldset class="job-fieldset">
    			<legend>Job detail</legend>	
                <span><strong>Job Title</strong> : </span> <?=esc_html( $product->get_title() )?><br />
                <span><strong>Posted Date</strong> : </span> <?=$job->jobDate?><br />
<!--              <span><strong>Retailer</strong> : </span> <?=($job->retailerId=='0'?'No retailer assigned yet':$retailer_info->first_name)?><br />
               <span><strong>Quotation Accept Date</strong> : </span> <?=($job->qacceptDate=='0000-00-00 00:00:00'?'No quotation yet':date("l, F j, Y",strtotime($job->qacceptDate)))?><br />
                <span><strong>Quotation Amount</strong> : </span> <?=($job->quotationAmount=='0'?'No quotation yet':$job->quotationAmount)?><br />
-->            </fieldset>
            <fieldset class="job-fieldset">
    			<legend>Job additional information</legend>
                <span><strong>Delivery/Pickup</strong> : </span> <?=get_job_meta($job->id,'wccpf_deliverypickup')?><br />
                <?php
                	$work_start = product_delivery_switch(get_job_meta($job->id,'wccpf_when_do_you_need_the_product'));					
				?>
                <span><strong>When do you need the work to start</strong> : </span> <?=$work_start?><br /> 
                <?php if(get_job_meta($job->id,'wccpf_removal_of_old_appliance')!=''){?>               
                <span><strong>Removal of old appliance ? </strong> : </span> <?=get_job_meta($job->id,'wccpf_removal_of_old_appliance')?><br /> 
                <?php }?>
                <?php if(get_job_meta($job->id,'wccpf_oven_type')!=''){?> 
                <span><strong>Oven type  </strong> : </span> <?=get_job_meta($job->id,'wccpf_oven_type')?><br />  
                <?php }?>
                <?php if(get_job_meta($job->id,'wccpf_installation')!=''){?> 
                <span><strong>Installation  </strong> : </span> <?=ucfirst(get_job_meta($job->id,'wccpf_installation'))?><br />  
                <?php }?>
                <span><strong>Is Commercial ? </strong> : </span> <?=($job->is_commercial=='1'?'Yes':'No')?><br />
                <?php if($job->is_commercial==1){?>
				<span><strong>Buisness Name </strong> : </span> <?=$job->businessName?><br />
                <span><strong>ABN no </strong> : </span> <?=$job->abn_no?><br />
				<?php }?>
                <span><strong>Quantity </strong> : </span> <?=$job->quantity?><br />  
           </fieldset>
           <fieldset class="job-fieldset">
    			<legend>Address</legend>
                <span><strong>Buyer Name</strong> : </span> <?=$first_name.' '.$last_name?><br />
                <span><strong>Email</strong> : </span> <?=$job->email?><br />
                <span><strong>Postcode</strong> : </span> <?=$address?><br />
                <span><strong>Telephone</strong> : </span> <?=$job->telephone?><br />
           </fieldset>
        </div>
        
        <div class="wrap">
		<h2>Quotation List</h2>
        <table class="widefat" width="100%" border="0" cellspacing="5" cellpadding="5">
            <thead>
              <tr>
                <td><strong>Job Id</strong></th>
                <th><strong>Retailer</strong></th>
                <th><strong>Quotation Amount</strong></th>
                <th><strong>Comments</strong></th>
                <th><strong>Product warranty</strong></th>
                <th><strong>Quotation Date</strong></th>
                <th><strong>Assigned Date</strong></th>
                <th><strong>Status</strong></th>
              </tr>
          </thead>
          
          <tbody>

	 <?php
	 	$sqlQuotation = "SELECT * FROM  `". $wpdb->prefix ."jobquotation` where jobId='".$id."' ";
		$resultsQuotation = $wpdb->get_results($sqlQuotation);
        foreach ( $resultsQuotation as $quotation ) {
			
			$sqlRetailer = "SELECT * FROM  `". $wpdb->prefix ."users` where ID='".$quotation->retailerId."' ";
			$resultsRetailer = $wpdb->get_results($sqlRetailer);
			foreach ( $resultsRetailer as $retailer ) {
				$retailerName = $retailer->display_name;
				$retailerEmail = $retailer->user_email;
			}		
			?>
            <tr>
                <td><?=$quotation->jobId?></th>
                <td><?=$retailerName?>[<?=$retailerEmail?>]</th>
                <td><?=$quotation->quotationAmount?></th>
                <td><?=$quotation->comments?></th>
                <td><?=$quotation->product_warranty?></th>
                <td><?=$quotation->quotationDate?></th>
                <td><?=$quotation->assignedDate?></th>
                <td><?=$quotation->status?></th>
            </tr>
      		<?php
        }
		
    ?>
   </tbody>
</table>
 </div>
        
        
        
        <div class="haggle-back-to"><a href="<?php menu_page_url('jobs_on_dashboard')?>" class="haggle-link">Back to list</a></div>
        <?php            
  //      }
}
?>