<?php
function jobs_configuration(){
		$das = get_all_dynamic_attributes();
		if($_POST['save_job_option']){
			$table_option  			  		= 		$wpdb->prefix . 'options';			
			$job_no_of_quotes 			    = 		$_POST['job_no_of_quotes'];
			$job_retailer_tobe_informed	  = 		$_POST['job_retailer_tobe_informed'];
			$job_increments_radius 		   = 		$_POST['job_increments_radius'];
			$job_limit 				       = 		$_POST['job_limit'];
			$job_limit_retailer 			  = 		$_POST['job_limit_retailer'];
			$admin_monitor_email 	   		 =    	$_POST['admin_monitor_email'];
			$lead_fee 	   			  		=    	$_POST['lead_fee'];
			$show_phone_register_form  		=    	$_POST['show_phone_register_form'];
			$show_phone_job_form  			 =    	$_POST['show_phone_job_form'];
			$hold_job_on_verification 		= 		$_POST['hold_job_on_verification'];
			$number_of_hours 		= 		$_POST['number_of_hours'];
			//comments on quotation
			
			$comment_quoted_price  	  		=    	$_POST['comment_quoted_price'];
			$comment_warranty  	  	  		=    	$_POST['comment_warranty'];
			
			$postArray = array();
			foreach($das['key'] as $key => $value){												
					$smallipop_comment = "smallipop_".$value;
					$postArray[$smallipop_comment]=$_POST[$smallipop_comment];
			}			
			
			
			//comments on job post form
			$comment_jobpost_name  	  	   =    $_POST['comment_jobpost_name'];
			$comment_jobpost_email  	  	  =    $_POST['comment_jobpost_email'];
			$comment_jobpost_postcode  	   =    $_POST['comment_jobpost_postcode'];
			$comment_jobpost_phone  	  	  =    $_POST['comment_jobpost_phone'];
			$comment_trade_price			=	$_POST['comment_trade_price'];
			
			//job cutoff time
			$cutoff_time  	  	  =    $_POST['cutoff_time'];
				
				update_option( 'job_no_of_quotes', $job_no_of_quotes );
							
				update_option( 'job_retailer_tobe_informed', $job_retailer_tobe_informed );
				
				update_option( 'job_increments_radius', $job_increments_radius );
							
				update_option( 'job_limit', $job_limit );
				
				update_option( 'job_limit_retailer', $job_limit_retailer );
				
				update_option( 'admin_monitor_email', $admin_monitor_email );
				
				update_option( 'lead_fee', $lead_fee );
				
				update_option( 'show_phone_job_form', $show_phone_job_form );
				
				update_option( 'show_phone_register_form', $show_phone_register_form );
				
				update_option( 'hold_job_on_verification', $hold_job_on_verification );
				
				update_option( 'number_of_hours', $number_of_hours );
			
			//comment on quotation
			
				update_option( 'comment_quoted_price', $comment_quoted_price );
			
				update_option( 'comment_warranty', $comment_warranty );
				
				foreach($postArray as $key=> $postData){
					update_option($key,$postData);
				}
				
			
			//comment on job post form
			
			
				update_option( 'comment_jobpost_name', $comment_jobpost_name );
			
				update_option( 'comment_jobpost_email', $comment_jobpost_email );			
			
				update_option( 'comment_jobpost_postcode', $comment_jobpost_postcode );			
			
				update_option( 'comment_jobpost_phone', $comment_jobpost_phone );
				
				update_option( 'comment_trade_price', $comment_trade_price );
			//Job cutoff time
				
				
				update_option( 'cutoff_time', $cutoff_time );
			
		}
		?>
        <h2>Job Options</h2>
        <hr/>
        <form method="post" action="">
        <table class="widefat" width="100%" border="0" cellspacing="5" cellpadding="5">            
              <tr>
                <td><strong>Number of quotes provided to customer(default : 3)</strong></td><td><input type="text" name="job_no_of_quotes" value="<?=get_option('job_no_of_quotes')?>"/></td>
              </tr>
              <tr>
                <td><strong>Number of retailers required to be informed of the job (before radius extends)</strong></td><td><input type="text" name="job_retailer_tobe_informed" value="<?=get_option('job_retailer_tobe_informed')?>"/></td>
              </tr>
              <tr>
                <td><strong>Increments of radius extension(in KM)</strong></td><td><input type="text" name="job_increments_radius" value="<?=get_option('job_increments_radius')?>"/></td>
              </tr>   
              <tr>
                <td><strong>No of job limits per page</strong></td><td><input type="text" name="job_limit" value="<?=get_option('job_limit')?>"/></td>
              </tr> 
               <tr>
                <td><strong>No of job limits per page[Retailer Account]</strong></td><td><input type="text" name="job_limit_retailer" value="<?=get_option('job_limit_retailer')?>"/></td>
              </tr>
              <tr>
                <td><strong>Admin monitor email</strong></td><td><input type="text" size="25" name="admin_monitor_email" value="<?=get_option('admin_monitor_email')?>"/></td>
              </tr>
              <tr>
                <td><strong>Lead Fee</strong></td><td><input type="text" size="25" name="lead_fee" value="<?=get_option('lead_fee')?>"/>%</td>
              </tr>  
              <tr>
                <td><strong>Remove phone no from job form</strong></td><td><input type="checkbox" name="show_phone_job_form" <?=(get_option('show_phone_job_form')=="1"?"checked":"")?> value="1"/></td>
              </tr> 
              <tr>
                <td><strong>Remove phone no from registration form</strong></td><td><input type="checkbox" name="show_phone_register_form" <?=(get_option('show_phone_register_form')=="1"?"checked":"")?> value="1"/></td>
              </tr>
              <tr>
                <td><strong>Not to hold job on email verification</strong></td><td><input type="checkbox" name="hold_job_on_verification" <?=(get_option('hold_job_on_verification')=="1"?"checked":"")?> value="1"/></td>
              </tr> 
               <tr>
                <td><strong>Number of hours for offer expires</strong></td><td><input type="text" name="number_of_hours"  value="<?=get_option('number_of_hours')?>"/>Hrs</td>
              </tr>       
        </table>
        
        <h2>Quotation Hints</h2>
        <hr/>
        <table class="widefat" width="100%" border="0" cellspacing="5" cellpadding="5">            
              <tr>
                <td><strong>Comment on the quoted price</strong></td><td><textarea name="comment_quoted_price" rows="4" cols="50"><?=get_option('comment_quoted_price')?></textarea></td>
              </tr>
              <tr>
                <td><strong>Condition and Warranty if not Brand New</strong></td><td><textarea name="comment_warranty" rows="5" cols="50"><?=get_option('comment_warranty')?></textarea></td>
              </tr>
              
               <!--dynamic attributes start-->
              <?php
			  	
				$dynamic_attribute_html = "";
				foreach($das['key'] as $key => $value){
					$smallipop_comment = "smallipop_".$value;
					if($smallipop_comment == "smallipop_wccpf_when_do_you_need_the_product")
						$dynamic_attribute_html .="
						  <tr>
							<td><strong>Comments on Timeframe</strong></td>
							<td><textarea name=\"$smallipop_comment\" rows=\"4\" cols=\"50\">".get_option($smallipop_comment)."</textarea></td>
						  </tr>   ";
					 else
						$dynamic_attribute_html .="
						  <tr>
							<td><strong>Comments on ".$das['label'][$value]."</strong></td>
							<td><textarea name=\"$smallipop_comment\" rows=\"4\" cols=\"50\">".get_option($smallipop_comment)."</textarea></td>
						  </tr>   ";
				}
				
				echo $dynamic_attribute_html;
			  ?>
              
              
                    
        </table>
        <hr/>	
        <h2>Job Post Hints</h2>
        <hr/>
        <table class="widefat" width="100%" border="0" cellspacing="5" cellpadding="5">              
        	   <tr>
                <td><strong>Comment on trade price</strong></td><td><textarea name="comment_trade_price" rows="4" cols="50"><?=get_option('comment_trade_price')?></textarea></td>
              </tr>      
              <tr>
                <td><strong>Comment on name</strong></td><td><textarea name="comment_jobpost_name" rows="4" cols="50"><?=get_option('comment_jobpost_name')?></textarea></td>
              </tr>
              <tr>
                <td><strong>Comment on email</strong></td><td><textarea name="comment_jobpost_email" rows="4" cols="50"><?=get_option('comment_jobpost_email')?></textarea></td>
              </tr>
              <tr>
                <td><strong>Comments on Postcode</strong></td><td><textarea name="comment_jobpost_postcode" rows="4" cols="50"><?=get_option('comment_jobpost_postcode')?></textarea></td>
              </tr>   
              <tr>
                <td><strong>Comment on Best no to contact</strong></td><td><textarea name="comment_jobpost_phone" rows="4" cols="50"><?=get_option('comment_jobpost_phone')?></textarea></td>
              </tr>       
        </table>
         <hr/>	
        <h2>Job cut off time</h2>
        <hr/>
        <table class="widefat" width="100%" border="0" cellspacing="5" cellpadding="5">            
              <tr>
                <td><strong>Cutoff time(Hrs)</strong></td><td><input type="text" name="cutoff_time" value="<?=get_option('cutoff_time')?>" /></td>
              </tr>                
        </table>
        <input type="submit" value="Save Options" name="save_job_option" class="button button-primary"/>
        </form>
        <?php
}
?>