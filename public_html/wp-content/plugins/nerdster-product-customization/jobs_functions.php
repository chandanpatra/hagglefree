<?php
// Save jobs 

/*
============Status of Jobs================
 0 ====> Not verified user job
 1 ====> New job
 2 ====> Quotation send to cutomer
 3 ====> Job closed
 
 10 ====> Quotation not available
 
 
 ============Status of Quotations================
 1 ====> New job Quotation
 2 ====> Quotations selected to send to cutomer
 3 ====> cutomer selected quotation
 
*/

function save_job_data($cart_item_data,$productId){
	global $wpdb,$quote_date;
	$table_job			   = 	$wpdb->prefix . 'jobs';
	$table_jobmeta		   =	$wpdb->prefix . 'jobmeta';
	//user data
	$first_name	   		  =	$_POST['first_name'];
	$email				   =	$_POST['email'];
	$cr_pcode		 		=	$_POST['cr_pcode'];
	$cr_phone		 		=	$_POST['cr_phone'];
	$is_extended_warranty 	= 	$_POST['is_extended_warranty'];
	
	
	// check wether customer posting repeated job
	$job_date_sql = "SELECT count(*) FROM $table_job WHERE `email`= \"$email\" AND `productId` =\"$productId\" AND `jobDate` LIKE '".date("Y-m-d")."%'";
	$job_date_result = $wpdb->get_var($job_date_sql);
	if($job_date_result>0)
		return 0;
		
	// check wether the user is logged in or not
	if ( is_user_logged_in() ) {		
		$userId = get_current_user_id();
	}else{
		
		//check wether user with same email id exists
		$exists = email_exists($email);		
		
		 if( $exists) {
			 // check account verification			
			 $userId = $exists;
			  verify_buyer_on_job_post($userId,$email);
		 }else{
			// register user on initial job form fillup
			$userId = register_buyer_on_job_post($first_name,$email,$cr_pcode,$cr_phone);		
		 }
	}
	
		//check wether the account status is inactive/active or not for buyer account
		$account_verified = get_user_meta( $userId, 'cr_status', true );
		$social_user = get_user_meta( $userId, 'wsl_current_provider', true );
	
		if($userId!= '0'){			
		$job_date = "SELECT NOW() AS times, (NOW() + INTERVAL 1 HOUR) as daylightTstmp";
		$result1 = $wpdb->get_row($job_date);
		$job_time = $result1->times;

 //==================Daylight saving start date and end date====================================//
		
	$dlsStartDate = strtotime(date('Y-m-d 00:00:00', strtotime('First Sunday Of October '.date('Y'))));
	$dlsEndDate = strtotime(date('Y-m-d 00:00:00', strtotime('First Sunday Of April '.(date('Y')+1))));	 
		
// ==================== Daylight Saving period=================================//


if(strtotime($job_time) > $dlsStartDate && strtotime($job_time) < $dlsEndDate){
		  
		switch($cr_pcode){
			// ==== South Australia ================//			
			case $cr_pcode>=5000 && $cr_pcode<=5799:
			case $cr_pcode>=5800 && $cr_pcode<=5999:
				$job_time = date('Y-m-d H:i:s',strtotime($result1->daylightTstmp.'+30 minutes'));
				break;
				// ==== NSW ================//
			case $cr_pcode>=1000 && $cr_pcode<=1999:
			case $cr_pcode>=2000 && $cr_pcode<=2599: 
			case $cr_pcode>=2620 && $cr_pcode<=2899:
			case $cr_pcode>=2921 && $cr_pcode<=2999: 
			    // ==== ACT ================//
			case $cr_pcode>=200 && $cr_pcode<=299:
			case $cr_pcode>=2600 && $cr_pcode<=2619:
			case $cr_pcode>=2900 && $cr_pcode<=2920:
			
			  // ==== VIC ================//
			
			case $cr_pcode>=3000 && $cr_pcode<=3999:  
			case $cr_pcode>=8000 && $cr_pcode<=8999: 
			  // ==== Tasmania ================//
			case $cr_pcode>=7000 && $cr_pcode<=7799: 
			case $cr_pcode>=7800 && $cr_pcode<=7999: 
			    $job_time = $result1->daylightTstmp;
				break;
		}
		  
	}		
		
		
		
		$status = "1";
		if($account_verified=="inactive" && (get_option('hold_job_on_verification')=="")){
			$status = "0";
		}
		//geo code lat/long calculation				
		 $addr = $_POST['cr_pcode'];
		 $addr = str_replace(" ", "+", $addr);
		 $geocode = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$addr&components=country:AU&sensor=false");
		 $output = json_decode($geocode);
		 $result = (array) json_decode($geocode);
		 if( !empty($output->results) && $output->status == 'OK' ) {
			$lat 	= $output->results[0]->geometry->location->lat;
			$long   = $output->results[0]->geometry->location->lng;
		}
		// geo calculation ends 		
		//save job quotation data start
		$quote_date = getbestQuotationDate($job_time,$cr_pcode);
		//save job quotation data end	

		//insert the job data to job table
		$job_form_val = array(
							'userId' 	  		    => $userId,
							'productId'  			 => $productId,
							'jobDate'    		   => $job_time,
							'lat' 		 		   => $lat,
							'long' 		          => $long,
							'is_commercial'         => $_POST['is_commercial'],
							'businessName' 		  => $_POST['businessName'],
							'abn_no'		        => $_POST['abn_no'],
							'quantity'	          => $_POST['quantity'],
							'firstname'   		     => $first_name,								
							'email'   	   			 => $email,
							'Postcode'   			  => $cr_pcode,								
							'telephone'   			 => $cr_phone,
							'is_extended_warranty'  => $is_extended_warranty,
							'quotationSendDate'	 => $quote_date['date'],
							'offerExp'			  => $quote_date['offerExp'],
							'status'				=> $status
						);	
			/*echo '<pre>';
			print_r($job_form_val);
			echo '</pre>';	*/
		if($wpdb->insert( $table_job, $job_form_val)){
			//update_user_meta($userId,'cr_pcode',$cr_pcode);	
			//update_user_meta($userId,'cr_phone',$cr_phone);
			$job_id =  $wpdb->insert_id;	
			
			foreach($cart_item_data as $meta_key => $meta_value){				
				$wpdb->insert( $table_jobmeta, array(
							'job_id'=>$job_id,
							'meta_key'=>$meta_key,
							'meta_value'=>$meta_value
				));				
			}
			if(($account_verified=="active") || ($social_user!="") ||(get_option('hold_job_on_verification')=="1")){
				//assign job to retailer
				$job_no_of_quotes = get_option('job_no_of_quotes');
				$job_retailer_tobe_informed = get_option('job_retailer_tobe_informed');
				$job_increments_radius = get_option('job_increments_radius');
				$job = get_job("",$job_id);
				$email_job_details = email_job_details($job_id);
				$retailers = get_retailer_list($job_id);
				$userId = $job->userId;
				$lat = $job->lat;
				$long = $job->long;
				$i=0;
				if(count($retailers)>0){
				$finalRetailers = array();
				foreach($retailers as $retailer){
					if($retailer['region']=="National")
					  $distance = 0;
					else if($retailer['region']=="State"){
					  if(getCustomerState($cr_pcode)==$retailer['state'])
					  $distance = 1;
					  else
					  continue;
					}
					else {
						if($retailer['region']=="Region"){
							$distance = round(distance($lat,$long,$retailer['lat'],$retailer['long'],"K"),2);
							if($distance > $job_increments_radius){
							 continue;
							}
						}
					}
					$retailer['dist'] = $distance;
					$finalRetailers[]=$retailer;
				}
				
				
				$retailers = record_sort($finalRetailers,'dist',true);
				$count = 1;
				
				$table_jobquotation = $wpdb->prefix . 'jobquotation';
				$table_jobquotation_meta = $wpdb->prefix . 'quotationmeta';
					
				$leadFee = (float)get_option('lead_fee');
				$rrp = (float)get_post_meta($productId,'_price', true);
				
				//========================== Get product price and root category id ===========================
				
				    $loadProduct = wc_get_product( $productId );
 					$productPrice = $loadProduct->get_price();
					$productSku = $loadProduct->get_sku();
					 
					$prod_terms = get_the_terms( $productId, 'product_cat' );
					
					$prod_terms_brand = get_the_terms( $productId, 'product_brand' );
					if($prod_terms_brand[0])
					$prod_brand = $prod_terms_brand[0]->term_id;
					else
					$prod_brand = 0;
					
						foreach ($prod_terms as $prod_term) {						
							// gets product cat id
							$product_cat_id = $prod_term->term_id;						
							// gets an array of all parent category levels
							$product_parent_categories_all_hierachy = get_ancestors( $product_cat_id, 'product_cat' );	
							// This cuts the array and extracts the last set in the array
							$last_parent_cat = array_slice($product_parent_categories_all_hierachy, -1, 1, true);
							foreach($last_parent_cat as $last_parent_cat_value){
								// $last_parent_cat_value is the id of the most top level category, can be use whichever one like
								//echo '<strong>' . $last_parent_cat_value . '</strong>';
							}
						}
				     $root_cat = $last_parent_cat_value; 			 
				
				//====================================   End   =============================================== 
				
				foreach($retailers as $retailer){
					if($count>$job_retailer_tobe_informed){
						break;
					}else{
						
						$insert_val = array(
						  'jobId'           => $job_id,
						  'retailerId'     => $retailer['user_id'],
						  'assignedDate'   => $job_time
						 );
						 
//======================= Check Auto bidding ================================//
			
			
				//============= check pickup radious if product shipping is pickup===============
					
					        $deliverypickup = get_job_meta($job_id,'wccpf_deliverypickup');
							$deliverypickupCondition = "true";
							if($deliverypickup == "pickup"){
								$pickup_radious_range = (int)get_user_meta($retailer['user_id'], 'pickup_radius_auto_quote', true);
								if($pickup_radious_range > 0 && $pickup_radious_range < (int)$retailer['dist']){
									$deliverypickupCondition = "false";
								}
							}
							
				//============================End of  check pickup radious ===========================//	
				
				//============ Check Radious Range ====================//					
					 $radious_range = (int)get_user_meta($retailer['user_id'], 'radius_range', true);				
					 $radiousRangeCondition = "true";
					 if($radious_range > 0 && $radious_range < (int)$retailer['dist']){						
						if($retailer['region'] == "National" )						
						$radiousRangeCondition = "true";
						else
						$radiousRangeCondition = "false";
					 }
				//============================End of  check pickup radious ===========================//	
				
				//========================== check retailers feed table ==============================//
				
				 $productInFeed = "false";
				 
				 $table_retailFeed = $wpdb->prefix ."feedRetalProducts";							 
				 $feedProductSql = "SELECT * FROM $table_retailFeed WHERE retailerId = '".$retailer['user_id']."' AND 	sku = '$productSku'";
				 $feedProduct = $wpdb->get_row($feedProductSql);
				
				 if($feedProduct){
					 $productInFeed = "true";
					 $productPrice = round($feedProduct->price,2);
					 $productMarketPrice = round($feedProduct->mprice,2);
					 $product_condition = $feedProduct->productCondition;
					 if($product_condition == ""){
						$product_condition = "bn_in_box"; 
					 }
					 else if($product_condition == "Brand New"){
						$product_condition = "bn_in_box"; 
					 }
					 else if($product_condition == "Factory Second (Carton Damaged)"){
						$product_condition = "fc_carton_damaged"; 
					 }
					 else if($product_condition == "Factory Second"){
						$product_condition = "fc_second"; 
					 }
					 else if($product_condition == "Factory Second (Small Ding)"){
						$product_condition = "fc_scratch_dent"; 
					 }
					 else if($product_condition == "Manufacturer Refurbished"){
						$product_condition = "fc_refurbished"; 
					 }
					 
					 if($feedProduct->productUrl != "")
					 $productUrlComments = $feedProduct->productUrl."?utm_source=HAGGLEFREE&utm_medium=quote";
					 else
					 $productUrlComments = "";
				 }
				
				//================================ end check feed table =========================//			
			
                $autobid = get_user_meta($retailer['user_id'], 'auto_quote', true);
				$finalQuotationAmount = 0;
				
				$table_rabc = $wpdb->prefix ."retailer_autobid_cats";
				$autoBidCatSql = "SELECT * FROM $table_rabc WHERE user_id = '".$retailer['user_id']."' AND cat_id = '$root_cat'";
				$autoBidCat = $wpdb->get_row($autoBidCatSql);
				
				$fileAutoBid = fopen("autobid.txt","a");
				fwrite($fileAutoBid,"\n$autoBidCatSql\n$feedProductSql\nAutobid: $autobid ============Radious Condition:$radiousRangeCondition ===========Delivary Codition:$deliverypickupCondition ========Product in Feed:$productInFeed\n");
				fclose($fileAutoBid);
				if($autobid && $autobid == "yes" && $autoBidCat && $radiousRangeCondition == "true" && $deliverypickupCondition == "true" && $productInFeed == "true"){
					
				  //================== Check Product Cat and Brand for auto bit ========================
							 
						 
						 $table_rabb = $wpdb->prefix ."retailer_autobid_brands";	 
						 $autoBidBrandSql = "SELECT * FROM $table_rabb WHERE user_id = '".$retailer['user_id']."' AND brand_id = '$prod_brand'";
						 $autoBidBrand = $wpdb->get_row($autoBidBrandSql);
						 
						 $contact_details_auto_quote = get_user_meta($retailer['user_id'],'contact_details_auto_quote', true);
        				 $contact_person_auto_quote  = get_user_meta($retailer['user_id'],'contact_person_auto_quote', true);
						 
						 if($autoBidCat && $autoBidBrand){
							 $discount = (int)$autoBidCat->discount;
						 }else{
							 $discount = 0;
						 }
						 $fileAutoBid = fopen("autobid.txt","a");
						 fwrite($fileAutoBid,"\n Discount : $discount");
						 fclose($fileAutoBid);
							 $bidAmount = ($productPrice - (($productPrice*$discount)/100));
							 
							 $insert_val['quotationAmount'] 	= $bidAmount;
							 $insert_val['autoBidAmount']   	  = $bidAmount;
							 
							 if($discount<0)
							 $insert_val['ticketAmount']   	   = $bidAmount;
							 else
							 $insert_val['ticketAmount']   	   = $productPrice;
							 
							 if($productMarketPrice && ($productMarketPrice > 0)){
								$insert_val['ticketAmount']    = $productMarketPrice; 
							 }
							 
							 $insert_val['autoBidStatus']      = 1;
							 $insert_val['status']             = 1;
							 $insert_val['quotationDate']      = $job_time;
							 $insert_val['contact_details']    = $contact_details_auto_quote;
        					 $insert_val['contact_person']     = $contact_person_auto_quote;
							 $insert_val['product_condition']  = $product_condition;
							 $insert_val['comments'] 		   = $productUrlComments;
							 
							 $finalQuotationAmount += $bidAmount*$_POST['quantity'];
							 //=======================Get other Auto bid data=========================================
							 
								$delivery_price_auto_quote 		= get_user_meta($retailer['user_id'], 'delivery_price_auto_quote', true);
								$delivery_comment_auto_quote  	  = get_user_meta($retailer['user_id'], 'delivery_comment_auto_quote', true);
								$pickup_comment_auto_quote  	    = get_user_meta($retailer['user_id'], 'pickup_comment', true);
								$offer_pickup 					 = get_user_meta($retailer['user_id'], 'offer_pickup', true);
								$rooa 							 = get_user_meta($retailer['user_id'], 'rooa', true);
								$rooa_price 				  	   = get_user_meta($retailer['user_id'], 'rooa_price', true);
								$rooa_comment 				 	 = get_user_meta($retailer['user_id'], 'rooa_comment', true);
								
								$installation_comment_auto_quote  = get_user_meta($retailer['user_id'], 'installation_comment_auto_quote', true);
								//$installation_price_auto_quote  = get_user_meta($retailer['user_id'], 'installation_price_auto_quote', true);			
								
							 //=================================End=======================================================
					}
			//======================= End Auto bidding ================================//						

						//================Insert Quotation initial data====================//
						 	$insert = $wpdb->insert( $table_jobquotation, $insert_val);
						 	$quote_id =  $wpdb->insert_id;
						 //====================== End ==============================//
						 
						 //================= Insert auto bid quotation meta data===========//
				
				if($autobid && $autobid == "yes" && $autoBidCat && $radiousRangeCondition == "true" && $deliverypickupCondition == "true" && $productInFeed == "true"){
							 
				 $jobAttributes = get_dynamic_job_attributes($job_id);
				 $autoQuoteMetaArray = array();
				 
				  if(count($jobAttributes)>0){
				   $dynamic_quote ="";
				   foreach($jobAttributes['key'] as $key => $value){
					   
						   $price_requirement = "price_".$value;
					
							   if(($value == "wccpf_deliverypickup") && ($jobAttributes['customer_choice'][$value] == "delivery")){
								  $autoQuoteMetaArray[$price_requirement]=$delivery_price_auto_quote;
								  $finalQuotationAmount += $delivery_price_auto_quote*$_POST['quantity'];
								  $dynamic_quote .= $jobAttributes['label'][$value]." <span style=\"color: #ff5400;\">".ucfirst($jobAttributes['customer_choice'][$value])."</span><br />";									
								  $dynamic_quote .= "Delivery Price <span style=\"color: #ff5400;\">$".$delivery_price_auto_quote." per item</span><br />";
							   }
							   
							   /*if(($value == "wccpf_installation") && ($jobAttributes['customer_choice'] == "yes")){
								  $autoQuoteMetaArray[$price_requirement]=$installation_price_auto_quote; 
							   }*/
							   
							   $meet_requirement = "meet_".$value; 
							   if($value == "wccpf_removal_of_old_appliance"){					  						  
										$autoQuoteMetaArray[$meet_requirement] = $rooa;		
										if(($rooa == "yes") && ($jobAttributes['customer_choice'][$value] == "yes")){
											$autoQuoteMetaArray[$price_requirement]=$rooa_price;
											$finalQuotationAmount += $rooa_price*$_POST['quantity'];
										
										    $dynamic_quote .= "<br>Can you meet ".$jobAttributes['label'][$value]."? <span style=\"color: #ff5400;\">$rooa</span><br />";       					  $dynamic_quote .= $jobAttributes['label'][$value]." Price <span style=\"color: #ff5400;\">$".$rooa_price." per item</span><br />";                                }
										
										if(($rooa == "no") && ($jobAttributes['customer_choice'][$value] == "yes")){																				
										    $dynamic_quote .= "<br>Can you meet ".$jobAttributes['label'][$value]."? <span style=\"color: #ff5400;\">$rooa</span><br />";       					}
							   }
							   
							   if($value == "wccpf_installation"){
								 if($jobAttributes['customer_choice'][$value] == "yes") { 
								  $autoQuoteMetaArray[$meet_requirement]="no"; 
								  $dynamic_quote .= "<br>Can you meet ".$jobAttributes['label'][$value]."? <span style=\"color: #ff5400;\">No</span><br />";
								 }
							   }
							   
							   if($value == "wccpf_installation_required"){
								 if($jobAttributes['customer_choice'][$value] == "yes") { 
								  $autoQuoteMetaArray[$meet_requirement]="no"; 
								  $dynamic_quote .= "<br>Can you meet ".$jobAttributes['label'][$value]."? <span style=\"color: #ff5400;\">No</span><br />";
								 }
							   }
					
							   $comment_requirement = "comment_".$value;
												
								if(($value == "wccpf_deliverypickup") && ($jobAttributes['customer_choice'][$value] == "delivery")){
								  $autoQuoteMetaArray[$comment_requirement]=$delivery_comment_auto_quote;	
								  $dynamic_quote .= $jobAttributes['label'][$value]." Comment  <span style=\"color: #ff5400;\">".$delivery_comment_auto_quote."</span><br />";
								}
								
								if(($value == "wccpf_deliverypickup") && ($jobAttributes['customer_choice'][$value] == "pickup")){
								  $autoQuoteMetaArray[$meet_requirement] = $offer_pickup;	
								  $autoQuoteMetaArray[$comment_requirement]= $pickup_comment_auto_quote; 
								  								  
								  $dynamic_quote .= $jobAttributes['label'][$value]." <span style=\"color: #ff5400;\">".ucfirst($jobAttributes['customer_choice'][$value])."</span><br />";		  
								  $dynamic_quote .= "<br>Can you meet ".$jobAttributes['label'][$value]."? <span style=\"color: #ff5400;\">".ucfirst($offer_pickup)."</span><br />";
								  $dynamic_quote .= $jobAttributes['label'][$value]." Comment  <span style=\"color: #ff5400;\">".$pickup_comment_auto_quote."</span><br />";						
								 
								}
								
								if($value == "wccpf_removal_of_old_appliance"){
									if(($rooa == "yes") && ($jobAttributes['customer_choice'][$value] == "yes")){
										$autoQuoteMetaArray[$comment_requirement]=$rooa_comment;
										$dynamic_quote .= $jobAttributes['label'][$value]." Comment  <span style=\"color: #ff5400;\">".$rooa_comment."</span><br />";						
									}
									if(($rooa == "no") && ($jobAttributes['customer_choice'][$value] == "yes")){
										$autoQuoteMetaArray[$comment_requirement]=$rooa_comment;
										$dynamic_quote .= $jobAttributes['label'][$value]." Comment  <span style=\"color: #ff5400;\">".$rooa_comment."</span><br />";						
									}
								}
							   
								if($value == "wccpf_installation"){
								  if($jobAttributes['customer_choice'][$value] == "yes") {
								   $autoQuoteMetaArray[$comment_requirement]=$installation_comment_auto_quote; 
								   $dynamic_quote .= $jobAttributes['label'][$value]." Comment  <span style=\"color: #ff5400;\">".$installation_comment_auto_quote."</span><br />";				  }			  
							   }
							   
							   if($value == "wccpf_installation_required"){
								  if($jobAttributes['customer_choice'][$value] == "yes") {
								   $autoQuoteMetaArray[$comment_requirement]=$installation_comment_auto_quote; 
								   $dynamic_quote .= $jobAttributes['label'][$value]." Comment  <span style=\"color: #ff5400;\">".$installation_comment_auto_quote."</span><br />";				  }			  
							   }
							  
							  
								if($value == "wccpf_when_do_you_need_the_product"){
									
									if($feedProduct && $feedProduct->qty > 0){									 
							         $instock_timeframe_comment_auto_quote = get_user_meta($retailer['user_id'],'instock_timeframe_comment_auto_quote', true);					                                     $autoQuoteMetaArray[$meet_requirement] = "yes";
									 $autoQuoteMetaArray[$comment_requirement]=$instock_timeframe_comment_auto_quote; 
									 
									 $dynamic_quote .= "<br>Can you meet ".product_delivery_switch($jobAttributes['customer_choice'][$value])." Timeframe? <span style=\"color: #ff5400;\">Yes</span><br />";
									$dynamic_quote .= "Timeframe Comment  <span style=\"color: #ff5400;\">$instock_timeframe_comment_auto_quote</span><br />";
									 
								   }else{
							         $outstock_timeframe_comment_auto_quote = get_user_meta($retailer['user_id'],'outstock_timeframe_comment_auto_quote', true);
									 //if product is outstock and timframe is 5-8days[next_week] and i am flexible
									 if($jobAttributes['customer_choice'][$value] =="next_week" || $jobAttributes['customer_choice'][$value] =="I_m_flexible"){
									  $autoQuoteMetaArray[$meet_requirement] = "yes";
									  $dynamic_quote .= "<br>Can you meet ".product_delivery_switch($jobAttributes['customer_choice'][$value])." Timeframe? <span style=\"color: #ff5400;\">Yes</span><br />"; 									 
									 }
									 //if product is outstock and timframe is today/tomorrow[asap] and 2-4days[next_few_days]
									 if($jobAttributes['customer_choice'][$value] =="asap" || $jobAttributes['customer_choice'][$value] =="next_few_days"){
									   $autoQuoteMetaArray[$meet_requirement] = "no";
									   $dynamic_quote .= "<br>Can you meet ".product_delivery_switch($jobAttributes['customer_choice'][$value])." Timeframe? <span style=\"color: #ff5400;\">No</span><br />"; 
									 }
									 $autoQuoteMetaArray[$comment_requirement]=$outstock_timeframe_comment_auto_quote;							 
									 $dynamic_quote .= "Timeframe Comment  <span style=\"color: #ff5400;\">$outstock_timeframe_comment_auto_quote</span><br />";
						           }
							}
					   }
				 }
				
				 //================= Auto quote data insert ======================================
			  
				   $wpdb->query( "DELETE FROM $table_jobquotation_meta WHERE `quote_id` = '$quote_id'" );
				 
				   if(count($autoQuoteMetaArray) > 0){
					   foreach($autoQuoteMetaArray as $key=> $autoQuoteMetaData){
							if(trim($autoQuoteMetaData) !=""){                
								$autoQuoteMetaSql ="INSERT INTO $table_jobquotation_meta VALUES('','$quote_id','$key','$autoQuoteMetaData')";
								$wpdb->query( $autoQuoteMetaSql ); 
							}
						}
				   }
				   
				  //================= End of Auto quote data insert ======================================
			}
					//========================================= End =============================================================//						 
						 //==========Calculate lead fee and insert in database===============//
			
							$userDiscount = (float)get_user_meta($retailer['user_id'], 'cr_lead_fee', true);
							if($userDiscount > 0){			
							   $calLeadFee = ($rrp*$leadFee)/100;
							   $calLeadFee = ($calLeadFee - (($calLeadFee*$userDiscount)/100));
							}
							else{
							   $calLeadFee = ($rrp*$leadFee)/100;
							}
							if($calLeadFee > 0){
								$leadFeeText = "<b> $".round($calLeadFee,2)."</b><br />";
							}else{
								$leadFeeText = "<b>No Lead fee applicable.</b><br />";
							}
				  //print_r($insert_val);
				  if($insert){			
				  	  //// Send mail to retailer for new job quotation starts ////  
					  $headers[] = 'Content-Type: text/html; charset=UTF-8';
					  $headers[] = 'From: '.get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'> ' . "\r\n";
					  $template_name = "quotation_request_for_new_job";
					  $media_url = get_stylesheet_directory_uri()."/img";
			          //Codes: {site_url}, {site_tag}, {media_url}, {email_heading}, {site_name}, {retailer_name}, {post_date}, {quotation_url}, {product_details}, {customer_requirements}, {admin_email}, {site_abn}, {site_phone}, {fb_url} 
					  $template_detail = get_email_template($template_name);
					  $product = wc_get_product( $job->productId );
					  $company_name = get_user_meta($retailer['user_id'],'cr_company',true);
					  if($company_name == ""){
					  	$company_name = $retailer['display_name'];
					  }
					  $usage_type = ($job->is_commercial==1?'Commercial':'Private');
					  $product_details ="
	   					<ul style=\"width:100%; float:left; margin:0 0 15px 0; padding:0\">
							<li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#ff8e2a; border-radius:4px 4px 0 0; text-align:left; color:#fff; font-weight:bold; font-size:16px\">
								".esc_html( $product->get_title() )."
							</li>
							<li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#fff; border-radius:0 0 4px 4px; text-align:left; color:#000; font-weight:normal; font-size:14px\">
								Job id: <span style=\"color: #ff5400;\">".$job->id."</span><br />
								SKU: <span style=\"color: #ff5400;\">".$product->get_sku()."</span><br />
								Quantity: <span style=\"color: #ff5400;\">".$job->quantity."</span><br />
								Usage Type: <span style=\"color: #ff5400;\">".$usage_type."</span>
							</li>
						</ul>";
					  $post_date = "<span style=\"color: #ff5400;\">".date("Y-m-d",strtotime($job->jobDate))."</span> <span style=\"color: #787878;\">".date("H:i:s",strtotime($job->jobDate))."</span>";
					  
					  
					  $customer_requirements = "<ul style=\"width:100%; float:left; margin:0 0 15px 0; padding:0\">
													<li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#000; border-radius:4px 4px 0 0; text-align:left; color:#fff; font-weight:normal; font-size:14px\">Customer's Requirement(s):</li>
													<li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#fff; border-radius:0 0 4px 4px; text-align:left; color:#000; font-weight:normal; font-size:14px\">";
						
						///get dynamic attributes ////
						$das = get_dynamic_job_attributes($job_id);
						if(count($das)>0){
							$cr_da ="";
							foreach($das['key'] as $key => $value){
								if($value == "wccpf_when_do_you_need_the_product")
									$cr_da .= $das['label'][$value]." : <span style=\"color: #ff5400;\">".product_delivery_switch($das['customer_choice'][$value])."</span><br />";
								else
									$cr_da .= $das['label'][$value]." : <span style=\"color: #ff5400;\">".ucfirst($das['customer_choice'][$value])."</span><br />";
							}
							$customer_requirements .=$cr_da;
						}
						
						$customer_requirements .="
										Usage type: <span style=\"color: #ff5400;\">".($job->is_commercial=='1'?'Commercial':'Private')."</span>
									</li>
								</ul>";
					  //token replacement
					  					  
					   $buyer_details = "<ul style=\"width:100%; float:left; margin:0 0 15px 0; padding:0\">
                        <li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#000; border-radius:4px 4px 0 0; text-align:left; color:#fff; font-weight:normal; font-size:14px\">Buyer Details:</li>
                        <li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#fff; border-radius:0 0 4px 4px; text-align:left; color:#000; font-weight:normal; font-size:14px\">
                            <div style=\"width:100%;\">Customer: <span style=\"color: #ff5400;\">".$job->firstname." ".$job->lastname."</span></div>							
							<div style=\"width:100%;\">Postcode: <span style=\"color: #ff5400;\">".$job->Postcode."</span></div>";
		/*if(get_option('show_phone_register_form')=="")
			$buyer_details .= "<div style=\"width:100%;\">Phone: <span style=\"color: #ff5400;\">".$job->telephone."</span></div>";*/
		$buyer_details .="
                        </li>
                    </ul>";
					  /*
						{site_name},{site_tag},{media_url},{email_heading},{company_name},{post_date},{product_details},{customer_requirements},
						{quotation_url},{lead_fee},{admin_email},{site_abn},{site_phone},{fb_url}
					  */	
					$mail_content = str_replace("{site_name}",get_bloginfo( 'name' ),stripslashes($template_detail['email_template']));
					  
					  //======== Token Replacement =================//
					$mail_content = str_replace("{site_url}",home_url(),$mail_content);
					$mail_content = str_replace("{site_tag}",get_bloginfo( 'description' ),$mail_content);
					$mail_content = str_replace("{media_url}",$media_url,$mail_content);	
					$mail_content = str_replace("{email_heading}",stripslashes($template_detail['email_heading']),$mail_content);
					
					$mail_content = str_replace("{company_name}",$company_name,$mail_content);
					$mail_content = str_replace("{post_date}",$post_date,$mail_content);
					$mail_content = str_replace("{product_details}",$product_details,$mail_content);
					$mail_content = str_replace("{buyer_details}",$buyer_details,$mail_content);
					$mail_content = str_replace("{customer_requirements}",$customer_requirements,$mail_content);
					$mail_content = str_replace("{quotation_url}",get_site_url()."/retailer-job-detail/?quote_id=".$quote_id,$mail_content);
					$mail_content = str_replace("{lead_fee}",$leadFeeText,$mail_content);	
			
					$mail_content = str_replace("{admin_email}",get_option( 'admin_email' ),$mail_content);
					$mail_content = str_replace("{site_abn}",get_option( 'site_abn' ),$mail_content);
					$mail_content = str_replace("{site_phone}",get_option( 'site_phone' ),$mail_content);
					$mail_content = str_replace("{fb_url}",get_option( 'fb_url' ),$mail_content);
					
					$subject = $template_detail['subject'];
					$subject = str_replace("{job_id}",$job->id,$subject);
					$subject = str_replace("{sku}",$product->get_sku(),$subject);
					$subject = str_replace("{postcode}",$job->Postcode,$subject);
					$subject = str_replace("{qty}",$job->quantity,$subject);
					$subject = str_replace("{job_type}",($job->is_commercial=='1'?'Commercial':'Private'),$subject);
					//======== Token Replacement =================//
					
	if($autobid && $autobid == "yes" && $autoBidCat && $radiousRangeCondition == "true" && $deliverypickupCondition == "true" && $productInFeed == "true"){						
							
							//======================== Quotation Details ============================
							
					$quotation_details ="<ul style=\"width:100%; float:left; margin:0 0 15px 0; padding:0\">
            <li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#000; border-radius:4px 4px 0 0; text-align:left; color:#fff; font-weight:normal; font-size:14px\">Your Quotation Details:</li>
                    <li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#fff; border-radius:0 0 4px 4px; text-align:left; color:#000; font-weight:normal; font-size:14px\">
                        Quotation Amount: <span style=\"color: #ff5400;\">$".$bidAmount." per item</span><br />
						".($productUrlComments!=""?"Comments: <span style=\"color: #ff5400;\">".$productUrlComments."</span><br />":"")."
						Quotation Date: <span style=\"color: #ff5400;\">".date("l, F j, Y",strtotime($job_time))."</span><br />";					
						
					$quotation_details .= $dynamic_quote;			
					$quotation_details .="<br>Contact Person: <span style=\"color: #ff5400;\">".$contact_person_auto_quote."</span><br />
					Contact Details: <span style=\"color: #ff5400;\">".$contact_details_auto_quote."</span><br />						
					Product Condition: <span style=\"color: #ff5400;\">".product_condition($product_condition)."</span><br />";
					$quotation_details .="Grand Quotation Total: <span style=\"color: #ff5400;\">$".$finalQuotationAmount."</span><br />";
                    $quotation_details .="</li></ul>";
							
							
					$subject = str_replace("Quotation request","Auto bid posted",$subject);
					$subject = $subject."/$".$bidAmount;
					$mail_content = str_replace("Quotation request","Auto bid posted",$mail_content);
					$mail_content = str_replace("Auto bid posted for a New Job","Auto bid posted for a New Job($".$bidAmount.")",$mail_content);
					$mail_content = str_replace("{quotation_details}",$quotation_details,$mail_content);
					$mail_content = str_replace("post-quotation.jpg","edit-quotation.jpg",$mail_content);
					$mail_content = str_replace("Please click here to post","Please click here to edit",$mail_content);
					}else{
						$mail_content = str_replace("{quotation_details}","",$mail_content);
					}
					
					wp_mail( $retailer['email'], $subject, $mail_content,$headers);					
					//// Send mail to retailer for new job quotation ends //// 		
				    $count++;
				     }
				   }
				}// end for
			  }
			}
			//echo "===============================I am here upto job post============================";
		}// end if job insert
		else{
			//$wpdb->print_error(); 
			echo "Unable to post job";
		}
}
		

}
// job details for email templte/////
function email_job_details($jobId,$show_commercial = "no"){
	global $wpdb;
	$job = get_job("",$jobId);
	$product = wc_get_product( $job->productId );
	
	$work_start = product_delivery_switch(get_job_meta($job->id,'wccpf_when_do_you_need_the_product'));
				
	
	$output = "<p>SKU: <strong>".$product->get_sku()."</strong></p><p><strong>".esc_html( $product->get_title() )."</strong><br />Quantity : <strong>".$job->quantity."</strong><br />Customer's postcode: <strong>{postcode}</strong><br /></p><p><strong>Customer's Requirement(s):</strong><br />Delivery/Pickup : <strong>".ucfirst(get_job_meta($job->id,'wccpf_deliverypickup'))."</strong><br />When do you need the product : <strong>".$work_start."</strong><br />";
				
	$removal_of_old_appliance = get_job_meta($job->id,'wccpf_removal_of_old_appliance');
	if($removal_of_old_appliance!=''){
		$output .= "Removal of old appliance ? : <strong>".ucfirst($removal_of_old_appliance)."</strong><br>";
	}
	$oven_type = get_job_meta($job->jobId,'wccpf_oven_type');
	if($oven_type!=''){
		$output .= "Oven Type ? : <strong>".$oven_type."</strong><br>";
	}
						
	if($job->is_commercial==1){
			$output .= 'Is Commercial ?  : <strong> '.($job->is_commercial=='1'?'Yes':'No').'</strong><br>';
			if($show_commercial=="yes"){
			$output .='Buisness Name : <strong> '.ucfirst($job->businessName).'</strong><br>
					   ABN no : <strong> '.$job->abn_no.'</strong><br>';
			}
	}else{
	 $output .='Usage type :  <strong>Private</strong>';
	}
	
	$output .= "</p>";

	return($output);
}

// Quotation details for email template

function email_quotation_details($quotationId,$quantity){
	global $wpdb;
	$table_jobquotation = $wpdb->prefix . 'jobquotation';
	$quotation_response_sql = "SELECT * FROM $table_jobquotation WHERE `id`= \"$quotationId\"";
	$quotation_response_result = $wpdb->get_row($quotation_response_sql);
				
	$quotationTotal = 0;
	$user_info = get_userdata($quotation_response_result->retailerId);
	
	$output .= "<div class=\"quotaion-details\"><div class=\"fieldset-content\">";
	if($quotation_response_result->quotationAmount!=""){
	$output .="<div class=\"qd-row\"><label>Quotation Amount : </label>$".$quotation_response_result->quotationAmount."<span> per item</span></div>";
	$quotationTotal += $quotation_response_result->quotationAmount*$quantity;
	}
	
	if($quotation_response_result->comments!="")
	$output .="<div class=\"qd-row\"><label>Quotation Comment : </label>".$quotation_response_result->comments."</div>";
	
	if($quotation_response_result->quotationDate!="")
	$output .="<div class=\"qd-row\"><label>Quotation Date : </label>".date("l, F j, Y",strtotime($quotation_response_result->quotationDate))."</div>";
	
	if($quotation_response_result->warranty_price!=""){
	$output .="<div class=\"qd-row\"><label>Extended Warranty Price : </label>$".$quotation_response_result->warranty_price."<span> per item</span></div>";
	$quotationTotal += $quotation_response_result->warranty_price*$quantity;
	}
	
	if($quotation_response_result->delivery_price!=""){
	$output .="<div class=\"qd-row\"><label>Delivery Price : </label>$".$quotation_response_result->delivery_price."<span> per item</span></div>";
	$quotationTotal += $quotation_response_result->delivery_price*$quantity;
	}
	
	if($quotation_response_result->delivery_comment!="")
	$output .="<div class=\"qd-row\"><label>Delivery Comment : </label>".$quotation_response_result->delivery_comment."</div>";
	
	if($quotation_response_result->installation_price!=""){
	$output .="<div class=\"qd-row\"><label>Installation Price : </label>$".$quotation_response_result->installation_price."<span> per item</span></div>";
	$quotationTotal += $quotation_response_result->installation_price*$quantity;
	}
	
	if($quotation_response_result->installation_comment!="")
	$output .="<div class=\"qd-row\"><label>Installation Comment : </label>".$quotation_response_result->installation_comment."</div>";
	
	if($quotation_response_result->timeframe_comments!="")
	$output .="<div class=\"qd-row\"><label>Timeframe Comment : </label>".$quotation_response_result->timeframe_comments."</div>";
	
	if($quotation_response_result->terms_and_conditions!="")
	$output .="<div class=\"qd-row\"><label>Terms & Condition : </label>".$quotation_response_result->terms_and_conditions."</div>";
	
	if($quotation_response_result->contact_person!="")
	$output .="<div class=\"qd-row\"><label>Contact Person : </label>".$quotation_response_result->contact_person."</div>";
	
	if($quotation_response_result->contact_details!="")
	$output .="<div class=\"qd-row\"><label>Contact Details : </label>".$quotation_response_result->contact_details."</div>";
	
	if($quotation_response_result->meet_timeframe!="")
	$output .="<div class=\"qd-row\"><label>Can you meet the customer's timeframe? : </label>".$quotation_response_result->meet_timeframe."</div>";
	
	if($quotation_response_result->product_condition!=""){	
	$product_condition = product_condition($quotation_response_result->product_condition);					
	$output .="<div class=\"qd-row\"><label>Product Condition : </label>".$product_condition."</div>";
	}
	
	if($quotation_response_result->product_comment!="")
	$output .="<div class=\"qd-row\"><label>Product Comment : </label>".$quotation_response_result->product_comment."</div>";
	
	$output .="<div class=\"qd-row\"><label>Grand Quotation Total : </label>$".$quotationTotal."</div>";	
		
	$output .="</div></div>";
	return $output;
	
	}




// job details for report on customer selected quotations/////
function job_details_for_report($jobId){
	global $wpdb;
	$job = get_job("",$jobId);
	$product = wc_get_product( $job->productId );	
	$work_start = product_delivery_switch(get_job_meta($job->id,'wccpf_when_do_you_need_the_product'));
	$is_extended_warranty = $job->is_extended_warranty;
	$installation_option = get_job_meta($job->id,'wccpf_installation_required');					
	
	$output = "<div class=\"product-image\">".$product->get_image()."</div>
		<div class=\"product-details\"><div class=\"product-title\">".esc_html( $product->get_title() )."</div>
		<div class=\"product-sku\"><span>SKU:</span>".$product->get_sku()."</div>
		<div class=\"product-rrp\"><span>RRP:</span>".$product->get_price_html()."</div>
		<div class=\"product-brand\"><span>Brand:</span>".get_the_term_list( $job->productId, 'product_brand')."</div>
	</div>
	<div class=\"jobCustomerDetails\">
	<div class=\"product-quantity\">Quantity : <strong>".$job->quantity."</strong></div>
	<div class=\"product-pickup\">Delivery/Pickup : <strong>".ucfirst(get_job_meta($job->id,'wccpf_deliverypickup'))."</strong></div>
	<div class=\"product-when-get\">When do you need the product : <strong>".$work_start."</strong></div>";
					
	$removal_of_old_appliance = get_job_meta($job->id,'wccpf_removal_of_old_appliance');	
	if($removal_of_old_appliance!=''){
		$output .= "<div class=\"product-remove\">Removal of old appliance ? : <strong>".ucfirst($removal_of_old_appliance)."</strong></div>";
	}
	
	$oven_type = get_job_meta($job->id,'wccpf_oven_type');	
	if($oven_type!=''){
		$output .= "<div class=\"product-remove\">Removal of old appliance ? : <strong>".$oven_type."</strong></div>";
	}
	
	if($is_extended_warranty!=''){
	    $output.="<div class=\"product-extended\"><span><strong>Extended Warranty :</strong>   $is_extended_warranty</span></div>";
	}	
	if($installation_option!=''){
		 $output.="<div class=\"product-installation\"><span><strong>Installation :</strong>   ".ucfirst($installation_option)."</span></div>";
	 }
	 
	if($job->is_commercial==1){
			$output .= '<div class=\"product-commercial\">Is Commercial ?  : <strong> '.($job->is_commercial=='1'?'Yes':'No').'</strong></div>';
			$output .='<div class=\"product-bn\">Buisness Name : <strong> '.ucfirst($job->businessName).'</strong><br>
					   ABN no : <strong> '.$job->abn_no.'</strong></div>';
	}else{
	 $output .='<div class=\"product-quantity\">Usage type : <strong>Private</strong></div>';
	}
	
	$output .= "</div>";

	return($output);
}

// job meta queries
function get_job_meta($job_id,$meta_key=""){
	global $wpdb;	
	$table_jobmeta = $wpdb->prefix . 'jobmeta';
	$sql = "select * from `$table_jobmeta` where 1=1";
	if($job_id){
		$sql .= " AND `job_id` = '$job_id'";
	}
	if($meta_key){
		$sql .= " AND `meta_key` = '$meta_key'";
	}
	if($job_id && $meta_key){
		$result = $wpdb->get_row($sql);
		return $result->meta_value;
	}else{
		$result = $wpdb->get_results($sql);	
		return $result;	
	}
}
// get all jobs
function get_jobs($user_id="",$job_id="",$status="",$orderby="desc", $filterstring="", $datefilterString=""){
	global $wpdb,$pagenum, $limit, $start;	
	$table_job = $wpdb->prefix . 'jobs';
	$sql = "select * from `$table_job` where 1=1 ";
	$condition = '';
	if($user_id){
		$condition .= " AND `userID` = '$user_id'";
	}
	if($job_id){
		$condition .= " AND `id` = '$job_id'";
	}
	if($status){
		$condition .= " AND `status` = '$status'";
	}
	if($filterstring){
		$condition .= " AND (`firstname` LIKE '%$filterstring%' OR `lastname` LIKE '%$filterstring%' OR `email` LIKE '%$filterstring%')";
	}
	if($datefilterString){
		$condition .= " AND date_format(`jobDate`, '%Y-%m')='$datefilterString'";
	}
	$sql .= $condition;
	if($orderby)
	$sql .= " order by `jobDate` $orderby ";
	else
	$sql .= " order by `id` desc ";
	
	$data = array();
	
	$totalResults = $wpdb->get_var("SELECT COUNT(*) FROM $table_job where 1=1 ".$condition);
	$data['totalData'] = $totalResults;
	$sql .= " LIMIT $start, $limit";
	
	$results = $wpdb->get_results($sql);
    $data['results'] = $results;
	return $data;
}

// get single job
function get_job($user_id="",$job_id=""){
	global $wpdb;	
	$table_job = $wpdb->prefix . 'jobs';
	$sql = "select * from `$table_job` where 1=1";
	if($user_id)
		$sql .= " AND `userID` = '$user_id'";
	if($job_id)
		$sql .= " AND `id` = '$job_id'";
		
    $sql .= " order by `jobDate` $orderby";
	$result = $wpdb->get_row($sql);
	if($result)	
	return $result;
	else
	return 0;
}


// function to show browse by category on header section
function get_product_categorylist($menu_class="",$hidden_default=""){
	  $taxonomy     = 'product_cat';
	  $orderby      = 'term_group';  
	  $show_count   = 0;      // 1 for yes, 0 for no
	  $pad_counts   = 0;      // 1 for yes, 0 for no
	  $hierarchical = 1;      // 1 for yes, 0 for no  
	  $title        = '';  
	  $empty        = 0;
	  if($hidden_default=="yes"){
	  	$style = "style=\"display:none\"";
	  }else{
	  	$style = "";
	  }
	  $html 		 = "<ul class=\"$menu_class\" $style>";
 	  $args = array(
			 'taxonomy'     => $taxonomy,
			 'orderby'      => $orderby,
			 'show_count'   => $show_count,
			 'pad_counts'   => $pad_counts,
			 'hierarchical' => $hierarchical,
			 'title_li'     => $title,
			 'hide_empty'   => $empty
	  );
	 $all_categories = get_categories( $args );
	 foreach ($all_categories as $cat) {
		if($cat->category_parent == 0) {
			$category_id = $cat->term_id;
			$children = get_term_children($category_id, $taxonomy);
			if (!empty($children)) {
			 	$sub_cat_class = "genericon-rightarrow";//has childeren		
				$span_html = "<span>&nbsp;</span>";		
			}     
			else{
				$sub_cat_class = "genericon-noarrow";
				$span_html = "";
			}
			// first level
			$html .= "<li class='$sub_cat_class' cat='$category_id'><a href='". get_term_link($cat->slug, 'product_cat') ."'>". $cat->name ."</a>".$span_html;
	
			$args2 = array(
					'taxonomy'     => $taxonomy,
					'child_of'     => 0,
					'parent'       => $category_id,
					'orderby'      => $orderby,
					'show_count'   => $show_count,
					'pad_counts'   => $pad_counts,
					'hierarchical' => $hierarchical,
					'title_li'     => $title,
					'hide_empty'   => $empty
			);
			$sub_cats = get_categories( $args2 );
			//print_r($sub_cats);
			if($sub_cats) {
				$html .= "<ul class='sub-category'>";
				foreach($sub_cats as $sub_category) {
					$sub_category_id = $sub_category->term_id; 
					$sub_children = get_term_children($sub_category_id, $taxonomy);
					if (!empty($sub_children)) {
						$sub_cat_class = "genericon-rightarrow has-children";//has childeren	
						$span_html = "<span>&nbsp;</span>";			
					}     
					else{
						$sub_cat_class = "genericon-noarrow";
						$span_html = "";
					}
					// second level
					$html .=  "<li class='$sub_cat_class' cat='$sub_category_id'><a href='".get_category_link( $sub_category_id )."'>".$sub_category->name."</a>".$span_html ;
					$args3 = array(
							'taxonomy'     => $taxonomy,
							'child_of'     => 0,
							'parent'       => $sub_category_id,
							'orderby'      => $orderby,
							'show_count'   => $show_count,
							'pad_counts'   => $pad_counts,
							'hierarchical' => $hierarchical,
							'title_li'     => $title,
							'hide_empty'   => $empty
					);
					$sub_sub_cats = get_categories( $args3 );
					
					if($sub_sub_cats) {
						$html .= "<ul class='sub-sub-category'>";
						foreach($sub_sub_cats as $sub_sub_category) {
							$sub_sub_category_id = $sub_sub_category->term_id; 
							
							//third level
							$html .=  "<li cat='$sub_sub_category_id'><a href='".get_category_link( $sub_sub_category_id )."'>".$sub_sub_category->name."</a></li>" ;
						}
						$html .= "</ul>";
					}
					$html .= "</li>";
				}   
				$html .= "</ul><!-- sub-category -->";
			}
			$html .="</li>";
			
		}       
	}
	$html .= "</ul><!-- category-menu  -->";
	if($menu_class=="category-menu"){
	$html .= "<script type='text/javascript'>
					jQuery(document).ready(function(){
					  jQuery('span.genericon-menu').click(function(){
						jQuery('ul.$menu_class').toggle();
						jQuery(this).toggleClass('genericon-close');
					  });
					  <!-- for mobile devices -->
					  if(jQuery(window).width() <= 767){
					  	jQuery('.browse-category ul li.genericon-rightarrow > span').click(function() {
							jQuery(this).parent().siblings().find('ul').slideUp(300);
							jQuery(this).next('ul').stop(true, false, true).slideToggle(300);
							jQuery(this).toggleClass('genericon-downarrow');
							jQuery(this).parent().toggleClass('open');							
						});
					  }
					  var mouse_is_inside;
					  jQuery('.browse-category').hover(function(){ 
							mouse_is_inside=true; 
						}, function(){ 
							mouse_is_inside=false; 
						});
						
						jQuery(\"body\").mouseup(function(){ 
						  if(! mouse_is_inside) {
							jQuery('.$menu_class').hide();
							jQuery('.browse-category span').removeClass('genericon-close');
						  }
						});
					});
			 </script>";
	}
	return $html;
}

// check wether retailer has done payment on account links
function check_retailer_payment_status($user_id="",$status="",$orderby="desc"){
	global $wpdb;	
	$table_pmpro_member = $wpdb->prefix . 'pmpro_membership_orders';
	$sql = "select * from `$table_pmpro_member` where 1=1";
	if($user_id){
		$sql .= " AND `user_id` = '$user_id'";
	}
	if($status){
		$sql .= " AND `status` = '$status'";
	}
	if($orderby)
	$sql .= " order by `timestamp` $orderby LIMIT 0,1";
	$result = $wpdb->get_results($sql);	
	return $result;
}

//check retailer payment
function check_retailer_payment($user_id=""){
	
		//checking wether the user is retailer or not
		global $wpdb;
		$table = $wpdb->prefix . 'pmpro_membership_orders';		
		$user = new WP_User( $user_id );
		$role = $user->roles[0];	
		$url_to_redirect ="";	
		if($role != "buyer-account"){
			$region = get_user_meta( $user_id, 'cr_region', 'true' );
			
			$result = $wpdb->get_row('select status,timestamp,gateway from '.$table.' where user_id='.$user_id.' order by timestamp desc');
			if(count($result)>0){				
				$status = $result->status;
				$ts = $result->timestamp;
				$curr_date = "SELECT TIMESTAMPDIFF(MONTH,'$ts',CURRENT_TIMESTAMP()) as times";
				$result1 = $wpdb->get_row($curr_date);
				$cur_time = $result1->times;
					
				if($result->gateway!="free" && ($status!='success' || $cur_time>12)){
					//redirect to membership level wise
					$url_to_redirect = home_url("retailer-checkout/?level=$region");
					//wp_redirect( home_url("retailer-checkout/?level=$region"), 301 ); exit;		
				}
			}elseif($result->gateway=="free"){
				$url_to_redirect = home_url("my-account");
			}else{
				$url_to_redirect = home_url("retailer-checkout/?level=$region");
				//wp_redirect( home_url("retailer-checkout/?level=$region"), 301 ); exit;
			}
		}
		if($url_to_redirect!=""){	
			return $url_to_redirect;
		}else{
			return 0;
		}
}

//Register user on job form post
function register_buyer_on_job_post($first_name,$email,$cr_pcode,$cr_phone){
	
	$mail_verify = rand(100000,999999);		
	$headers[] = 'Content-Type: text/html; charset=UTF-8';
	$headers[] = 'From: '.get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'> ' . "\r\n";
	$media_url = get_stylesheet_directory_uri()."/img";

	$userdata = array(		
            'user_login' => esc_attr($email),
            'user_email' => esc_attr($email), 
			'first_name' => esc_attr($first_name) 
    );
	$register_user = wp_insert_user($userdata);
	if (!is_wp_error($register_user)) {
		wp_update_user( array ('ID' => $register_user, 'role' => 'buyer-account' ) ) ;
		update_user_meta($register_user, 'cr_verify', $mail_verify);
		update_user_meta($register_user, 'cr_pcode', $cr_pcode);		
		update_user_meta($register_user, 'cr_phone', $cr_phone);				
		update_user_meta($register_user, 'cr_status', 'inactive');
		////mail content
		  $template_name = "account_creation_for_buyer";
		  $template_detail = get_email_template($template_name);	
		  //token replacement	
		  /*
			{site_name},{stylesheet_uri},{verify_url}
		  */
		   $verify_url = home_url()."/verify-email/?verify-email=$mail_verify";
 
			//======== Token Replacement =================//
				$mail_content = str_replace("{site_name}",get_bloginfo( 'name' ),stripslashes($template_detail['email_template']));				
				$mail_content = str_replace("{site_url}",home_url(),$mail_content);
				$mail_content = str_replace("{site_tag}",get_bloginfo( 'description' ),$mail_content);
				$mail_content = str_replace("{media_url}",$media_url,$mail_content);	
				$mail_content = str_replace("{email_heading}",stripslashes($template_detail['email_heading']),$mail_content);
				
				$mail_content = str_replace("{verify_url}",$verify_url,$mail_content);

				$mail_content = str_replace("{admin_email}",get_option( 'admin_email' ),$mail_content);
				$mail_content = str_replace("{site_abn}",get_option( 'site_abn' ),$mail_content);
				$mail_content = str_replace("{site_phone}",get_option( 'site_phone' ),$mail_content);
				$mail_content = str_replace("{fb_url}",get_option( 'fb_url' ),$mail_content);
				//======== Token Replacement =================//
					
			
		wp_mail( $email, $template_detail['subject'], $mail_content,$headers);
		return $register_user;
	}
	else{
		 $error_string = $register_user->get_error_message();		 
		return 0;
	}	
}

//Verify user on job form post
function verify_buyer_on_job_post($userId,$email){
	
	$mail_verify = get_user_meta($userId,'cr_verify',true);
	$user_status = get_user_meta($userId,'cr_status',true);
			
	$headers[] = 'Content-Type: text/html; charset=UTF-8';
	$headers[] = 'From: '.get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'> ' . "\r\n";
	$media_url = get_stylesheet_directory_uri()."/img";
	if($mail_verify !="")
	{	
		 //update_user_meta($userId, 'cr_verify', $mail_verify);		
		 ////mail content
		  $template_name = "account_creation_for_buyer";
		  $template_detail = get_email_template($template_name);	
		  //token replacement	
		  /*
			{site_name},{stylesheet_uri},{verify_url}
		  */
		   $verify_url = home_url()."/verify-email/?verify-email=$mail_verify";
 
			//======== Token Replacement =================//
				$mail_content = str_replace("{site_name}",get_bloginfo( 'name' ),stripslashes($template_detail['email_template']));				
				$mail_content = str_replace("{site_url}",home_url(),$mail_content);
				$mail_content = str_replace("{site_tag}",get_bloginfo( 'description' ),$mail_content);
				$mail_content = str_replace("{media_url}",$media_url,$mail_content);	
				$mail_content = str_replace("{email_heading}",stripslashes($template_detail['email_heading']),$mail_content);
				
				$mail_content = str_replace("{verify_url}",$verify_url,$mail_content);

				$mail_content = str_replace("{admin_email}",get_option( 'admin_email' ),$mail_content);
				$mail_content = str_replace("{site_abn}",get_option( 'site_abn' ),$mail_content);
				$mail_content = str_replace("{site_phone}",get_option( 'site_phone' ),$mail_content);
				$mail_content = str_replace("{fb_url}",get_option( 'fb_url' ),$mail_content);
				//======== Token Replacement =================//
					
			
		wp_mail( $email, $template_detail['subject'], $mail_content,$headers);
	}
		
}


//Retreive retailer associated with products brands/category
function get_retailer_list($jod_id){
	global $wpdb;
	$table = $wpdb->prefix . 'jobs';
	$table_brand = $wpdb->prefix . 'retailer_brands'; 
	$table_cat = $wpdb->prefix . 'retailer_cats';
	
	$sql = "select `userId`,`productId`,`is_commercial` from `$table` where `id`=".$jod_id;
	$result = $wpdb->get_row($sql);
	$productId = $result->productId;
	$is_commercial = $result->is_commercial;
	
	$output = array();
	$cat_taxonomy = 'product_cat';
	$brand_taxonomy = 'product_brand';
    // Get the category IDs assigned to product.
    $post_terms = wp_get_object_terms( $productId, $cat_taxonomy, array( 'fields' => 'ids' ) );

    if ( !empty( $post_terms ) && !is_wp_error( $post_terms ) ) {		
		// categories assigned to the product
        $term_ids = implode( ',' , $post_terms );        
    }
	
	// Get the brand IDs assigned to product.
    $post_terms1 = wp_get_object_terms( $productId, $brand_taxonomy, array( 'fields' => 'ids' ) );

    if ( !empty( $post_terms1 ) && !is_wp_error( $post_terms1 ) ) {		
		// categories assigned to the product
        $term_ids1 = implode( ',' , $post_terms1 );       
    }
	//retrieve retailer respect to productid
	
	$sql = "SELECT DISTINCT c.`user_id` FROM $table_brand as b, $table_cat as c WHERE b.user_id = c.user_id AND c.`cat_id` IN($term_ids) AND b.`brand_id` IN($term_ids1)";
	$retailersResult = $wpdb->get_results($sql);
	
	//print_r($retailersResult);
	
	
	if(count($retailersResult)>0){
		$i=0;
		foreach($retailersResult as $rs){
			
			$retailer_type = get_the_author_meta( 'cr_retailer_type', $rs->user_id );
			if($retailer_type!=""){
			 //commercial,retail,both
			 if($is_commercial == 1 && $retailer_type == "retail"){
				 continue;
			 }
			 if($is_commercial == 0 && $retailer_type == "commercial"){
				 continue;
			 }
			}
			//check inactive retailer
			$retailer_status = get_user_meta( $rs->user_id, 'cr_status', true );
			if($retailer_status == "inactive")
				continue;			
			$output[$i]['user_id'] = $rs->user_id;
			$output[$i]['lat'] = get_user_meta($rs->user_id, 'cr_lat', true );
  			$output[$i]['long'] = get_user_meta($rs->user_id, 'cr_long', true );
			$region = pmpro_level_name(get_user_meta($rs->user_id, 'cr_region', true ));
			$output[$i]['region'] = $region;
			$output[$i]['state'] = get_user_meta($rs->user_id, 'cr_state', true );	
			$user_info = get_userdata($rs->user_id);
			$output[$i]['email'] = $user_info->user_email;
			$output[$i]['display_name'] = $user_info->display_name;		
			$i++;
		}
	}
	//print_r($output);
	return $output;
}
/*function record_sort_old($records, $field, $reverse=false)
{
   
    $hash = array();
   
    foreach($records as $record)
    {
        $hash[$record[$field]] = $record;
    }
   
    ($reverse)? krsort($hash) : ksort($hash);
   
    $records = array();
   
    foreach($hash as $record)
    {
        $records []= $record;
    }
   
    return $records;
}
*/
function record_sort(&$array, $subkey="id", $sort_ascending=true) {

    if (count($array))
        $temp_array[key($array)] = array_shift($array);

    foreach($array as $key => $val){
        $offset = 0;
        $found = false;
        foreach($temp_array as $tmp_key => $tmp_val)
        {
            if(!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey]))
            {
                $temp_array = array_merge(    (array)array_slice($temp_array,0,$offset),
                                            array($key => $val),
                                            array_slice($temp_array,$offset)
                                          );
                $found = true;
            }
            $offset++;
        }
        if(!$found) $temp_array = array_merge($temp_array, array($key => $val));
    }

    if ($sort_ascending) $array = array_reverse($temp_array);

    else $array = $temp_array;
	return $array;
}
	

// get the time when the quotation will be received by buyer
function getbestQuotationDate_old($jobDate){
 global $wpdb;
 $cutoff_time = (int)get_option('cutoff_time');
 if($jobDate=="" || $jobDate==0)
    return;
 $jobDateTime = $jobDate;
 
 $jobDateTimeSec = strtotime($jobDateTime);
 $jobDateTimeExp = explode(" ",$jobDateTime);
 
 $jobDate = $jobDateTimeExp[0];
 $jobTime = $jobDateTimeExp[1];
 
 $jobStartDateTime = strtotime($jobDate." 09:00:00");
 
 $jobStartDateTimeOneHourlater = strtotime($jobDate." ".(9+$cutoff_time).":00:00");
 
 $jobEndDateTime = strtotime($jobDate." 17:00:00");
 $sql = "SELECT DATE_ADD('$jobDateTime',INTERVAL $cutoff_time HOUR) AS nextHour";
 $resultNH = $wpdb->get_row($sql);
 $nextHour = strtotime($resultNH->nextHour);
 
 $sql = "SELECT DATE_SUB('$jobDateTime',INTERVAL $cutoff_time HOUR) AS preHour";
 $resultPH = $wpdb->get_row($sql);
 $preHour = strtotime($resultPH->preHour);
 
 $sql = "SELECT DATE_ADD('$jobDate',INTERVAL 1 DAY) AS nextDay";
 $resultND = $wpdb->get_row($sql);
 $nextDay = $resultND->nextDay." ".(9+$cutoff_time).":00:00";
 $finalDate = array();
 if($jobEndDateTime > $nextHour && $nextHour > $jobStartDateTimeOneHourlater){
  $finalDate['date'] = $resultNH->nextHour;
  $format = "within ".$cutoff_time.($cutoff_time>1?" hours":" hour");
 }
 else{
  if($jobStartDateTime > $jobDateTimeSec){
    $finalDate['date'] = $jobDate." ".(9+$cutoff_time).":00:00";
	$format = "by ".(9+$cutoff_time)."am";
  }
  else{
  $finalDate['date'] = $resultND->nextDay." ".(9+$cutoff_time).":00:00";
  $format = "by ".(9+$cutoff_time)."am Tomorrow";
  }
 }
 
    //$sql = "SELECT DATE_FORMAT('".$finalDate['date']."', '%h.%i%p') as dateFormat";
    //$finalFormat = $wpdb->get_row($sql);
 
 //$finalDate['format'] = strtolower($finalFormat->dateFormat);
 $finalDate['format'] = $format;
 
 return $finalDate;
}

function getbestQuotationDate($jobDate,$cr_pcode){
	 global $wpdb;
	 $cr_pcode = (int)$cr_pcode;
	 $cutoff_time = (int)get_option('cutoff_time');
	 if($jobDate=="" || $jobDate==0)
	 return;
	 
	 $exp_hours = (int)get_option('number_of_hours');
	 
		//calculation of date based on zip code
		
		switch($cr_pcode){			
			case $cr_pcode>=5000 && $cr_pcode<=5799:
			case $cr_pcode>=5800 && $cr_pcode<=5999:
			case $cr_pcode>=800 && $cr_pcode<=899:
			case $cr_pcode>=900 && $cr_pcode<=999:
				$start_time = (9+$cutoff_time).":30:00";//ACST[UTC+9.5]
				$start_time_woct = "09:30:00";
				$end_time = "17:30:00";
				break;
			case $cr_pcode>=6000 && $cr_pcode<=6797:
			case $cr_pcode>=6800 && $cr_pcode<=6999:    
				$start_time =(11+$cutoff_time).":00:00";//AWST[UTC+8]
				$start_time_woct = "11:00:00";
				$end_time = "19:00:00";
				break;
			default:
				$start_time =(9+$cutoff_time).":00:00";//AEST[UTC+10]
				$start_time_woct = "09:00:00";
				$end_time = "17:00:00";
		}
		
	/* //==================Daylight saving start date and end date====================================//
		
	$dlsStartDate = strtotime(date('Y-m-d 00:00:00', strtotime('First Sunday Of October '.date('Y'))));
	$dlsEndDate = strtotime(date('Y-m-d 00:00:00', strtotime('First Sunday Of April '.(date('Y')+1))));	 
		
	// ==================== Daylight Saving period=================================//
	
	if(strtotime($jobDate) > $dlsStartDate && strtotime($jobDate) < $dlsEndDate){
		  
		switch($cr_pcode){
			// ==== South Australia ================//			
			case $cr_pcode>=5000 && $cr_pcode<=5799:
			case $cr_pcode>=5800 && $cr_pcode<=5999:
				$start_time = (8+$cutoff_time).":30:00";
				$start_time_woct = "08:30:00";
				$end_time = "16:30:00";
				$jobDate = date('Y-m-d H:i:s', strtotime($jobDate.'+1 hour 30 minutes'));
				break;
				// ==== NSW ================//
			case $cr_pcode>=1000 && $cr_pcode<=1999:
			case $cr_pcode>=2000 && $cr_pcode<=2599: 
			case $cr_pcode>=2620 && $cr_pcode<=2899:
			case $cr_pcode>=2921 && $cr_pcode<=2999: 
			    // ==== ACT ================//
			case $cr_pcode>=200 && $cr_pcode<=299:
			case $cr_pcode>=2600 && $cr_pcode<=2619:
			case $cr_pcode>=2900 && $cr_pcode<=2920:
			
			  // ==== VIC ================//
			
			case $cr_pcode>=3000 && $cr_pcode<=3999:  
			case $cr_pcode>=8000 && $cr_pcode<=8999: 
			  // ==== Tasmania ================//
			case $cr_pcode>=7000 && $cr_pcode<=7799: 
			case $cr_pcode>=7800 && $cr_pcode<=7999: 
			    if($cutoff_time > 1)
				$start_time =(8+$cutoff_time).":00:00";
				else
				$start_time ="0".(8+$cutoff_time).":00:00";	
					
				$jobDate = date('Y-m-d H:i:s', strtotime($jobDate.'+1 hour'));
				break;
		}
		  
	}*/
	 
	 $jobDateTime = $jobDate;
	
	 $jobDateTimeSec = strtotime($jobDateTime);
	 $jobDateTimeExp = explode(" ",$jobDateTime);
	
	 $jobDate = $jobDateTimeExp[0];
	 $jobTime = $jobDateTimeExp[1];
	
	 $jobStartDateTime = strtotime($jobDate." ".$start_time_woct);
	
	 $jobStartDateTimeOneHourlater = strtotime($jobDate." ".$start_time);
	
	 $jobEndDateTime = strtotime($jobDate." ".$end_time);
	 
	 $sql = "SELECT DATE_ADD('$jobDateTime',INTERVAL $cutoff_time HOUR) AS nextHour";
	 $resultNH = $wpdb->get_row($sql);	
	 $nextHour = strtotime($resultNH->nextHour); 
	
	 $sql = "SELECT DATE_SUB('$jobDateTime',INTERVAL $cutoff_time HOUR) AS preHour";	
	 $resultPH = $wpdb->get_row($sql);	
	 $preHour = strtotime($resultPH->preHour); 
	
	 $sql = "SELECT DATE_ADD('$jobDate',INTERVAL 1 DAY) AS nextDay";	
	 $resultND = $wpdb->get_row($sql);	
	 $nextDay = $resultND->nextDay." ".$start_time;
	
	 $finalDate = array();
	
	 if($jobEndDateTime > $nextHour && $nextHour > $jobStartDateTimeOneHourlater){	
	  $finalDate['date'] = $resultNH->nextHour;	
	  $format = "within ".$cutoff_time.($cutoff_time>1?" hours":" hour");
	 }
	 else{
		  if($jobStartDateTime > $jobDateTimeSec){
			$finalDate['date'] = $jobDate." ".$start_time;	
			$format = "by ".(9+$cutoff_time)."am";
		  }
		  else{
			$finalDate['date'] = $resultND->nextDay." ".$start_time;	
			$format = "by ".(9+$cutoff_time)."am Tomorrow";
		  }
	 } 
	 $calFinal_date = $finalDate['date'];
	 $finalDate['format'] = $format;
	 
	 $sql = "SELECT DATE_ADD('$calFinal_date',INTERVAL $exp_hours HOUR) AS offerExp";
	 $resultExpHour = $wpdb->get_row($sql);	
	 $offerExp = $resultExpHour->offerExp;
	 $finalDate['offerExp'] = $offerExp;
	 return $finalDate;
}

// save email templates
function save_email_template($template_name,$args){
	global $wpdb;
	$table_template  			  = 	$wpdb->prefix . 'email_templates';	
	$sql = "SELECT * from $table_template where template_name = '$template_name'";
	$result = $wpdb->get_row($sql);	
	if(count($result)>0){
		//template exists update the data
		$update_val = array(										
								'recipent'	  	=> $args['recipent'],
								'subject'		 => $args['subject'],
								'email_heading'   => $args['email_heading'],
								'email_template'  => addslashes($args['email_template']),								
								);
		$where = array('id' 	  => $result->id);		
		$updated = $wpdb->update($table_template,$update_val,$where);
	}
	else{
		//template does'nt exists insert the data
		$insert_val = array(
						'recipent'	  	=> $args['recipent'],
						'subject'		 => $args['subject'],
						'email_heading'   => $args['email_heading'],
						'email_template'  => addslashes($args['email_template']),
						'template_name'   => $template_name								
					);
		$insert = $wpdb->insert( $table_template, $insert_val);
	}	
}
function get_email_template($template_name){
	global $wpdb;
	$table_template  			  = 	$wpdb->prefix . 'email_templates';	
	$sql = "SELECT * from $table_template where template_name = '$template_name'";
	$result = $wpdb->get_row($sql);
	if(count($result)>0){		
		
		$args = array(										
					'recipent'	  	=> $result->recipent,
					'subject'		 => $result->subject,
					'email_heading'   => $result->email_heading,
					'email_template'  => stripslashes($result->email_template)
					);		
		return $args;
	}else{
		return 0;
	}
}

function product_condition($condition){
	switch($condition){
		case 'bn_in_box':
		$product_condition = "Brand New in Box";
		break;
		case 'bn_clearance_item':
		$product_condition = "Brand New in Box(Clearance Item)";
		break;
		case 'bn_carton_damaged':
		$product_condition = "Brand New(Carton Damaged)";
		break;
		case 'bn_floor_model':
		$product_condition = "Brand New out of Box(floor model or Demo)";
		break;
		case 'fc_carton_damaged':
		$product_condition = "Factory Second(Carton Damaged)";
		break;
		case 'fc_second':
		$product_condition = "Factory Second";
		break;
		case 'fc_refurbished':
		$product_condition = "Factory Second(Refurbished)";
		break;
		case 'fc_scratch_dent':
		$product_condition = "Factory Second(Scratch & Dent)";
		break;
		case 'pre_owned':
		$product_condition = "Pre-Owned Product";
		break;
		default:
		$product_condition = "";
		break;
	}
	return $product_condition;
}

function product_delivery_switch($condition){
	switch($condition){
		case 'asap':
		$work_start = "Today or Tomorrow ";
		break;
		case 'next_few_days':
		$work_start = "2-4 days";
		break;
		case 'next_week':
		$work_start = "5-8 days";
		break;
		case 'I_m_flexible':
		$work_start = "I am flexible";
		break;
		default:
		$work_start = "";
		break;
	}
	return $work_start;			
}

function get_retailer_jobs($user_ID,$job_status,$quote_status,$limit){
	global $wpdb;
	$table_quotation = $wpdb->prefix . 'jobquotation';
	$sql = "SELECT * FROM $table_quotation WHERE `retailerId`=\"".$user_ID."\" AND `status`=\"".$quote_status."\" ORDER BY quotationDate desc ";
	/*if($user_ID == "185"){
		echo $sql;
	}*/
	$quote_results = $wpdb->get_results($sql);
	$output = "<div class=\"quote-list\">";
	$output .="<div class=\"listing-header\">
					<span class=\"ji\">Job Id</span>
					<span class=\"jt\">Job Title</span>
					<span class=\"jpo\">Job Posted On</span>
					<span class=\"qa\">Job Total</span>
					<span class=\"act\">Action</span>
			   </div><!-- listing-header -->";
	$output .="<div class=\"listing-body\">";
	if(count($quote_results)>0){
		
	foreach($quote_results as $quote_result){
		
			$job = get_job("",$quote_result->jobId);
			if($job_status!=2){
			if($job->status != $job_status )
				continue;		
			}
			$product = wc_get_product( $job->productId );	
			if(!$product) continue;
			$user_info = get_userdata($job->userId);				
			$first_name = $user_info->first_name;
			$last_name = $user_info->last_name;
			$verify_code = rand(999999,1000000);
			$job_total =get_job_total($quote_result->id);
			$quotation_url = home_url('retailer-job-detail?&quote_id='.$quote_result->id);
			$output .="<div quote_status=\"".$quote_result->status."\"job_status=\"".$job->status."\" class=\"quote-list-rt\">
							<span class=\"ji\">".$quote_result->jobId."</span>
							<span class=\"jt\">".$product->get_title()."</span>
							<span class=\"jpo\">".date("l, F j, Y",strtotime($job->jobDate))."</span>
							<span class=\"qa\">".($job_total=='0'?'No quotation yet':'$'.$job_total)."</span>
							<span class=\"act\">
							<a href=\"".$quotation_url."\" >".(($quote_result->status=="0" && ($job->status == "2" ||$job->status == "3"))||($quote_result->status=="2" && ($job->status == "2" ||$job->status == "3"))||($quote_result->status=="1" && ($job->status == "2" ||$job->status == "3"))?"View Details":"Post Quotation")."</a>
							</span>
					   </div><!-- quote-list-rt -->"; 
						
			if($limit==1)
				break; 
			$limit--;              				
		}
		if($status == '2'){
			$request_url = home_url('selected-quotations');
		}else{
			$request_url = home_url('quotation-request');
		}
		$output .="<div class=\"more\"><a href=\"$request_url\">More</a></div>";
	}else{
		$output .= "Sorry no quotation available.";
	}
	$output .="</div><!-- listing-body -->
			</div><!-- quote-list -->";
	return $output;
}

function get_buyer_jobs($user_ID,$status,$limit){
	global $wpdb;
	$table_jobs = $wpdb->prefix . 'jobs';
	$table_jobquotation = $wpdb->prefix . 'jobquotation';	
	$sql = "SELECT * FROM $table_jobs WHERE `userId`=\"".$user_ID."\" AND `status`=\"".$status."\" ORDER BY quotationSendDate desc LIMIT 0 , $limit";
	$job_results = $wpdb->get_results($sql);
	//$jobs = get_jobs($user_ID,'',$status);
	print_r($jobs);
	//$job_results = $jobs['results'];
	$output = "<div class=\"job-list\">";
	if(count($job_results)>0){
		$output .="<div class=\"listing-header\"><span class=\"jt\">Job Title</span><span class=\"jpo\">Job Posted On</span><span class=\"qa\">Quotation Amount</span><span class=\"act\">Action</span></div>";
		$output .="<div class=\"listing-body\">";
		//print_r($job_results);
	foreach($job_results as $job_result){			
			$job = get_job("",$job_result->id);
			$quotation_response_sql = "SELECT * FROM $table_jobquotation WHERE `jobId`= ".$job_result->id;
			$quotation_response_results = $wpdb->get_row($quotation_response_sql);		
					
			$product = wc_get_product( $job_result->productId );	
			if(!$product) continue;
			$user_info = get_userdata($job_result->userId);				
			$first_name = $user_info->first_name;
			$last_name = $user_info->last_name;
			$verify_code = rand(999999,1000000);
			$quotation_url = home_url('job-detail?&id='.$job_result->id);
			$output .="<div class=\"job-list-rt\"><span class=\"jt\">".$product->get_title()."</span><span class=\"jpo\">".date("l, F j, Y",strtotime($job_result->jobDate))."</span><span class=\"qa\">".($quotation_response_results->quotationAmount=='0'?'No quotation yet':$quotation_response_results->quotationAmount)."</span><span class=\"act\"><a href=\"".$quotation_url."\" >".($job_result->status=="0"?"Post Quotation":"View Details")."</a></span></div>";                				
		}
		$output .="<div class=\"more\"><a href=\"".home_url('my-jobs')."\">More</a></div></div>";
	}else{
		$output .= "Sorry no jobs available.";
	}
	$output .="</div>";
	return $output;
}

function get_brand_image(){
	 global $product;	 
	$brand_terms = wp_get_post_terms( $product->id, 'product_brand', array("fields" => "all") );
	$output = "<div class=\"wb-single-img-cnt\">";
		//print_r($brand_terms);
        foreach ($brand_terms as $brand_item) {

            // Hunt out the child term that is a child of the parent
            if ( $brand_item->parent == $term_id ) {

                //Get the image of the brand
				$thumbnail_id = get_woocommerce_term_meta( $brand_item->term_id, 'thumbnail_id', true );
				$output .= "<a href=\"".home_url()."/?product_brand=".$brand_item->slug."\">";
				if ( $thumbnail_id )
					$output .= "<img src=".current(wp_get_attachment_image_src( $thumbnail_id))." alt=\"Brands\">"  ;                
				$output .= "</a>";
                break; //Assumes you've only assigned one Brand
            }
        }
		$output.="</div>";
		
		return $output;		
}

function delete_selected_user_jobs($user_id) {
   global $wpdb;
   // Job table & Job Meta & Job Quotations
   $jobTable = $wpdb->prefix . 'jobs';
   $jobMetaTable = $wpdb->prefix . 'jobmeta';
   $tableQuotation = $wpdb->prefix . 'jobquotation';
   
   $jobs  = $wpdb->get_results("select id from ".$jobTable." where userId = ".$user_id);
   $row_count = count($jobs);
   if($row_count>0){
	foreach($jobs as $job){ 
	   $wpdb->query('delete from '.$jobMetaTable.' where job_id = '.$job->id);
	   $wpdb->query('delete from '.$tableQuotation.' where jobId = '.$job->id);
       $wpdb->query('delete from '.$jobTable.' where id = '.$job->id);
	}
   }


   //Quotations Table
	$quotations  = $wpdb->get_results("select * from ".$tableQuotation." where retailerId = ".$user_id);
	$row_count = count($quotations);
	if($row_count>0){
		$wpdb->query('delete from '.$tableQuotation.' where retailerId='.$user_id);
	}
	
   //Brand Selection Tables
    $tableRetailerBrands = $wpdb->prefix . 'retailer_brands';
	$brands  = $wpdb->get_results("select * from ".$tableRetailerBrands." where user_id = ".$user_id);
	$row_count = count($brands);
	if($row_count>0){
      $wpdb->query('delete from '.$tableRetailerBrands.' where user_id='.$user_id);
	}
   // Cat selection tables
    $tableRetailerCats = $wpdb->prefix . 'retailer_cats';
	$cats  = $wpdb->get_results("select * from ".$tableRetailerCats." where user_id = ".$user_id);
	$row_count = count($cats);
	if($row_count>0){
      $wpdb->query('delete from '.$tableRetailerCats.' where user_id='.$user_id);
	}
}
add_action( 'delete_user', 'delete_selected_user_jobs');

function is_discontinued($product_id){
	$categories = get_the_terms( $product_id, 'product_cat' );
	return $categories;
}

// get dynamic attribute for a job
function get_dynamic_job_attributes($job_id){
	global $wpdb;
	$tablejobmeta = $wpdb->prefix . 'jobmeta';
	$tablepostmeta = $wpdb->prefix . 'postmeta';
	$meta_query = "SELECT `meta_key`,`meta_value` FROM $tablejobmeta WHERE job_id=$job_id";
	$meta_results = $wpdb->get_results($meta_query);
	$dynamic_attribute = array();
	foreach($meta_results as $meta_result){
		$fields_query = "SELECT `meta_key`,`meta_value` FROM $tablepostmeta WHERE `meta_key` = '".$meta_result->meta_key."' LIMIT 0,1" ;
		$fields_results = $wpdb->get_results($fields_query);
		foreach($fields_results as $fields_result){
			$dynamic_attribute['key'][] = $fields_result->meta_key;
		    $dynamic_attribute['customer_choice'][$fields_result->meta_key] = $meta_result->meta_value;			
			$da = json_decode($fields_result->meta_value);
			$dynamic_attribute['type'][$fields_result->meta_key] = $da->type;
			$dynamic_attribute['label'][$fields_result->meta_key] = $da->label;
			$dynamic_attribute['choices'][$fields_result->meta_key] = $da->choices;
		}
	}
	return $dynamic_attribute;
}

// get dynamic attribute for a quote
function get_dynamic_quote_attributes($quote_id){
	global $wpdb;
	$tablequotationmeta = $wpdb->prefix . 'quotationmeta';
		$quotation_meta_sql ="SELECT * from $tablequotationmeta WHERE quote_id=".$quote_id;
		$result1 = $wpdb->get_results($quotation_meta_sql);
		$metaDataArray = array();
		foreach($result1 as $result2){
			$metaDataArray[$result2->meta_key]=$result2->meta_value;
	}
	return $metaDataArray;
}

// get all dynamic attribute of field factory
function get_all_dynamic_attributes(){
	global $wpdb;
	$tablepostmeta = $wpdb->prefix . 'postmeta';
	
	$fields_query = "SELECT `meta_key`,`meta_value` FROM $tablepostmeta WHERE `meta_key` LIKE 'wccpf_%' GROUP BY `meta_key`" ;
	$fields_results = $wpdb->get_results($fields_query);
	$dynamic_attribute = array();
	foreach($fields_results as $fields_result){		
		$da = json_decode($fields_result->meta_value);
		if($fields_result->meta_key=="wccpf_condition_rules")
			continue;
		$dynamic_attribute['key'][] = $fields_result->meta_key;
		$dynamic_attribute['type'][$fields_result->meta_key] = $da->type;
		$dynamic_attribute['label'][$fields_result->meta_key] = $da->label;
		$dynamic_attribute['choices'][$fields_result->meta_key] = $da->choices;
	}	
	return $dynamic_attribute;
}

function force_close_job($job_id){
	global $wpdb;
	$tableJobs = $wpdb->prefix . 'jobs';
	$tableQuotation = $wpdb->prefix . 'jobquotation';
	
	$best3_quotations_email_template = "best_3_quotations_for_buyer";
	$best3_template_detail = get_email_template($best3_quotations_email_template);	
	
	$my_quotation_selected_template = "my_quotation_selected";
	$my_quotation_selected = get_email_template($my_quotation_selected_template);	
	
	
	//$sql = "SELECT *, NOW() as corTstmp FROM $tableJobs WHERE `status` = '1'";
	$sql = "SELECT *, NOW() as corTstmp FROM $tableJobs WHERE `id` = '$job_id'";
	$result = $wpdb->get_row($sql);
	
	//=============Job Config Data===============
	$job_no_of_quotes = (int)get_option('job_no_of_quotes');
	$leadFee = (float)get_option('lead_fee');
	
	/*
	============Status of Jobs================
	 0 ====> New job account not verified.
	 1 ====> New job
	 2 ====> Quotation send to cutomer 
	 10 ====> Quotation not available
	 
	 ============Status of Quotations================
	 0 ====> Request for Quotation
	 1 ====> Quotation submited
	 2 ====> Quotations selected to send to cutomer
	*/
	
		$media_url = get_stylesheet_directory_uri()."/img";
		// Product id and it's price rrp//
		$productId = $result->productId;
		$rrp = (float)get_post_meta($productId,'_price', true);
		
		//=========== Offer Exp Pickup from job table ==============//
		    
			 $calFinal_date = $result->corTstmp;				
			 $exp_hours = (int)get_option('number_of_hours');
			 $sql = "SELECT DATE_ADD('$calFinal_date',INTERVAL $exp_hours HOUR) AS offerExp";
			 $resultExpHour = $wpdb->get_row($sql);	
			 $offerExp = $resultExpHour->offerExp;
		
		//=================== End =============================//
	
			$quotationSendDate = strtotime($result->quotationSendDate);	
			$corTstmp = strtotime($result->corTstmp);
			$forcefully = "yes";
			if($corTstmp >= $quotationSendDate || $forcefully=="yes"){
			$forcefully = "no";
			
			$bestThree = array();
			$noBestThree = array();
			
			$sqlQuotations = "SELECT * FROM `$tableQuotation` WHERE `jobId` = '".$result->id."' AND `quotationAmount` > 0  AND status > 0";
			$quotationResults = $wpdb->get_results($sqlQuotations);

	  // echo "<pre>";print_r($quotationResults);echo "</pre>";

	   // ================= Email Token Variables ======================= 

	   $product = wc_get_product( $result->productId );
	   $usage_type = ($result->is_commercial==1?'Commercial':'Private');

	   $product_details =" <div style=\"width:100%; float:left; margin:0; padding:0\">

            <div style=\"width:96%; float:left; padding:10px 2%; margin:0; background-color: rgb(255, 142, 42); border-radius:4px 4px 0 0; text-align:left; color:#fff; font-weight:bold; font-size:16px\"> ".esc_html( $product->get_title() )." </div>

            <div style=\"width:96%; float:left; padding:10px 2% 20px; margin:0; background-color: rgb(255, 255, 255); border-radius:0 0 4px 4px; text-align:left; color:#000; font-weight:normal; font-size:14px\">

              <div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">Job id: <span style=\"color: rgb(255, 84, 0)\">".$result->id."</span></div>

              <div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">SKU: <span style=\"color: rgb(255, 84, 0);\">".$product->get_sku()."</span></div>
			  
			  <div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">Product Name: <span style=\"color: rgb(255, 84, 0);\">".$product->get_title()."</span></div>


              <div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">Quantity required: <span style=\"color: rgb(255, 84, 0);\">".$result->quantity."</span></div>

              <div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">Usage type: <span style=\"color: rgb(255, 84, 0);\">".$usage_type."</span></div>
			  <p></p>
				{dynamic_attributes}
            </div>
          </div>";

	  // Get Customer details

      $getJob = get_job("",$result->id);

      $customer_details = "<div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">Customer: <b>".$getJob->firstname."</b></div>";

      $customer_details .= "<div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">Email: <b>".$getJob->email."</b></div>";

	  $customer_details .= "<div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">Post Code: <b>".$getJob->Postcode."</b></div>";

	  if(get_option('show_phone_register_form')=="")

      	$customer_details .= "<div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">Phone: <b>".$getJob->telephone."</b></div>";

	  $customer_requirements = "<ul style=\"width:100%; float:left; margin:0 0; padding:0\">

                        <li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#000; border-radius:4px 4px 0 0; text-align:left; color:#fff; font-weight:normal; font-size:14px\">Customer's Requirement(s):</li>

                        <li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#fff; border-radius:0 0 4px 4px; text-align:left; color:#000; font-weight:normal; font-size:14px\">";

		///get dynamic attributes ////

		$das = get_dynamic_job_attributes($result->id);

		if(count($das)>0){

			$cr_da ="";
            $dynamic_attributes = "";
			$special_attribute = "<p></p>";
			foreach($das['key'] as $key => $value){

				if($value == "wccpf_when_do_you_need_the_product")
					$cr_da .= "<div style=\"width:100%;\">".$das['label'][$value]." : <span style=\"color: rgb(255, 84, 0);\">".product_delivery_switch($das['customer_choice'][$value])."</span></div>";
				else
					$cr_da .= "<div style=\"width:100%;\">".$das['label'][$value]." : <span style=\"color: rgb(255, 84, 0);\">".ucfirst($das['customer_choice'][$value])."</span></div>";

			//==================== For product details ========================//
			 if($value == "wccpf_deliverypickup"){
			  $special_attribute .= "<div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">".$das['label'][$value].": <span style=\"color: rgb(255, 84, 0);\">".ucfirst($das['customer_choice'][$value])."</span></div>";
			 }
			 else if($value == "wccpf_when_do_you_need_the_product"){
			  $special_attribute .= "<div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">Required by: <span style=\"color: rgb(255, 84, 0);\">".product_delivery_switch($das['customer_choice'][$value])."</span></div>";
			 }
			  else if($value == "wccpf_removal_of_old_appliance"){
			  $special_attribute .= "<div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">".$das['label'][$value].": <span style=\"color: rgb(255, 84, 0);\">".ucfirst($das['customer_choice'][$value])."</span></div>";
			 }
			 else if($value == "wccpf_installation"){
			  $special_attribute .= "<div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">".$das['label'][$value].": <span style=\"color: rgb(255, 84, 0);\">".ucfirst($das['customer_choice'][$value])."</span></div>";
			 }
			 else if($value == "wccpf_installation_required"){
			  $special_attribute .= "<div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">".$das['label'][$value].": <span style=\"color: rgb(255, 84, 0);\">".ucfirst($das['customer_choice'][$value])."</span></div>";
			 }
			 else{
			  $dynamic_attributes .= "<div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">".$das['label'][$value].": <span style=\"color: rgb(255, 84, 0);\">".ucfirst($das['customer_choice'][$value])."</span></div>";
			 }
			 //========================== End ==============================//
			}

			$customer_requirements .=$cr_da;
			
			$product_details = str_replace("{dynamic_attributes}",$dynamic_attributes.$special_attribute,$product_details);

		}

		$customer_requirements .="
						<!-- <div style=\"width:100%;\">Usage type: <span style=\"color: rgb(255, 84, 0);\">".($result->is_commercial=='1'?'Commercial':'Private')."</span></div> -->
					</li>
				</ul>"; 

	   $buyer_details = "<ul style=\"width:100%; float:left; margin:0 0; padding:0\">

                        <li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#000; border-radius:4px 4px 0 0; text-align:left; color:#fff; font-weight:normal; font-size:14px\">Buyer Details:</li>

                        <li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#fff; border-radius:0 0 4px 4px; text-align:left; color:#000; font-weight:normal; font-size:14px\">

                            <div style=\"width:100%;\">Customer: <span style=\"color: rgb(255, 84, 0);\">".$getJob->firstname." ".$getJob->lastname."</span></div>

							<div style=\"width:100%;\">Email: <span style=\"color: rgb(255, 84, 0);\">".$getJob->email."</span></div>

							<div style=\"width:100%;\">Postcode: <span style=\"color: rgb(255, 84, 0);\">".$getJob->Postcode."</span></div>";

		if(get_option('show_phone_register_form')=="")

			$buyer_details .= "<div style=\"width:100%;\">Phone: <span style=\"color: rgb(255, 84, 0);\">".$getJob->telephone."</span></div>";

		$buyer_details .="
                        </li>
                    </ul>";

		$buyer_name = $getJob->firstname." ".$getJob->lastname;


	// ================= Email Token Variables ======================= 						

	   if(count($quotationResults)>0){

		$i = 0;
		$ii=0;

		foreach($quotationResults as $quotationResult){

		 $Yes = 0;
		 $No  = 0;
		 $finalQuotationAmount  = 0; 
		 $da_quote = get_dynamic_quote_attributes($quotationResult->id);

		 if(count($da_quote) > 0){
			foreach($da_quote as $key => $value){
				$$key = $value;
			}
		}

		 

		 			$meet_timeframe 		= $quotationResult->meet_timeframe;			
					$quotation_amount 	  = $quotationResult->quotationAmount;
					$warranty_price 		= $quotationResult->warranty_price*$result->quantity;
							

					//get dynamic prices

					if(count($das['key']) > 0){
						$priceMetaDataArray = array();
						foreach($das['key'] as $key => $val){
							$variable = "price_".$val;
							if($$variable==0)
							continue;
							$priceMetaDataArray[$variable]=$$variable*$result->quantity;
						}
					}

					// best three criteria selection";
					if(count($das['key']) > 0){
						foreach($das['key'] as $key => $val){
							$meet_variable = "meet_".$val;
							if(isset($$meet_variable) && ($das['customer_choice'][$val] != $$meet_variable)){
								if($$meet_variable == "no"){
									$No++;
								}
								else{
									$Yes++;
								}
							}else{
								if(!$$meet_variable){
								 	continue;
								}
								if($das['customer_choice'][$val] == $$meet_variable){
									$Yes++;
								}
								else{
									$No++;
								}
							}
						}
					}
					

		 if($No > 0 ){

			$noBestThree[$ii]['quotationId'] = $quotationResult->id;
			$noBestThree[$ii]['retailerId'] = $quotationResult->retailerId;
			$noBestThree[$ii]['contact_person'] =  $quotationResult->contact_person;
			$noBestThree[$ii]['contact_details'] =  $quotationResult->contact_details;
			$noBestThree[$ii]['quotationAmount'] =  $quotationResult->quotationAmount;
            $noBestThree[$ii]['ticketAmount'] =  $quotationResult->ticketAmount;
			$noBestThree[$ii]['quotationDate'] =  $quotationResult->quotationDate;
			$noBestThree[$ii]['comments'] =  $quotationResult->comments;
			$noBestThree[$ii]['product_condition'] =  $quotationResult->product_condition;
			$noBestThree[$ii]['product_warranty'] =  $quotationResult->product_warranty;//comments on product condition
			$noBestThree[$ii]['product_comment'] =  $quotationResult->product_comment;
			$finalQuotationAmount = $quotationResult->quotationAmount*$result->quantity;

			if(count($das['key']) > 0){

				$priceMetaDataArray = array();

				foreach($das['key'] as $key => $val){

					$price_variable = "price_".$val;
					$meet_variable = "meet_".$val;
					$comment_variable = "comment_".$val;			
					$noBestThree[$ii][$price_variable] =  $$price_variable;
					$noBestThree[$ii][$meet_variable] =  $$meet_variable;
					$noBestThree[$ii][$comment_variable] =  $$comment_variable;
					$finalQuotationAmount += $$price_variable*$result->quantity;
					unset($$price_variable);unset($$meet_variable);unset($$comment_variable);
				}
			}

			

			$noBestThree[$ii]['warranty_price'] =  $quotationResult->warranty_price*$result->quantity;	
			if($quotationResult->warranty_price)
				$finalQuotationAmount += $quotationResult->warranty_price*$result->quantity;	
			$noBestThree[$ii]['finalQuotationAmount'] = $finalQuotationAmount;
			$noBestThree[$ii]['terms_and_conditions'] =  $quotationResult->terms_and_conditions;
			$ii++;

		 }else{

			$bestThree[$i]['quotationId'] = $quotationResult->id;
			$bestThree[$i]['retailerId'] = $quotationResult->retailerId; 
			$bestThree[$i]['contact_person'] =  $quotationResult->contact_person;
			$bestThree[$i]['contact_details'] =  $quotationResult->contact_details;
			$bestThree[$i]['quotationAmount'] =  $quotationResult->quotationAmount;
			$bestThree[$i]['ticketAmount'] =  $quotationResult->ticketAmount;
			$bestThree[$i]['quotationDate'] =  $quotationResult->quotationDate;
			$bestThree[$i]['comments'] 		=  $quotationResult->comments;
			$bestThree[$i]['product_condition'] 		=  $quotationResult->product_condition;
			$bestThree[$i]['product_warranty'] =  $quotationResult->product_warranty;//comments on product condition
			$bestThree[$i]['product_comment'] 		=  $quotationResult->product_comment;
			$finalQuotationAmount = $quotationResult->quotationAmount*$result->quantity;

			if(count($das['key']) > 0){
				$priceMetaDataArray = array();
				foreach($das['key'] as $key => $val){
					$price_variable = "price_".$val;
					$meet_variable = "meet_".$val;
					$comment_variable = "comment_".$val;					
					$bestThree[$i][$price_variable] =  $$price_variable;
					$bestThree[$i][$meet_variable] =  $$meet_variable;
					$bestThree[$i][$comment_variable] =  $$comment_variable;
					$finalQuotationAmount += $$price_variable*$result->quantity;
					unset($$price_variable);unset($$meet_variable);unset($$comment_variable);
				}
			}

			$bestThree[$i]['warranty_price'] =  $quotationResult->warranty_price*$result->quantity;
			if($quotationResult->warranty_price)
			$finalQuotationAmount += $quotationResult->warranty_price*$result->quantity;
			$bestThree[$i]['finalQuotationAmount'] = $finalQuotationAmount;
			$bestThree[$i]['terms_and_conditions'] =  $quotationResult->terms_and_conditions;
		 $i++;	
		 }						 
		}

		/*echo "<pre>";

		print_r($bestThree);

		echo "===============>";

		print_r($noBestThree);

		echo "</pre>";*/

		

		if(count($noBestThree)>0)
		     $noBestThree = record_sort($noBestThree,'finalQuotationAmount');	
		if(count($bestThree)>0)
			 $bestThree = record_sort($bestThree,'finalQuotationAmount');

		if(count($bestThree) >= $job_no_of_quotes)
		  $limit = $job_no_of_quotes;
		else
		  $limit = count($bestThree);	
		  $quotation_detail_list = "";

		 for($k = 0; $k < $limit; $k++){			 
			$company_name = get_user_meta($bestThree[$k]['retailerId'],'cr_company',true);
			$retaler_info = get_userdata($bestThree[$k]['retailerId']);
			$quotation_amount = $bestThree[$k]['quotationAmount'];
			//print_r($retaler_info);

			$retalerDisplayName = $retaler_info->display_name;
			if($company_name == ""){
				if($retalerDisplayName!="")
				$company_name = ucfirst($retalerDisplayName);
				else
				$company_name = ucfirst($retaler_info->first_name);
			 }

			$product_condition = product_condition($bestThree[$k]['product_condition']);
			$isgst = ($result->is_commercial==1?'(Inc GST)':'(Inc GST)');

			      //======================== For retailer =========================//

			$quotation_details ="<ul style=\"width:100%; float:left; margin:0 0; padding:0\">
            <li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#000; border-radius:4px 4px 0 0; text-align:left; color:#fff; font-weight:normal; font-size:14px\">Your Quotation Details:</li>
             <li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#fff; border-radius:0 0 4px 4px; text-align:left; color:#000; font-weight:normal; font-size:14px\">
              <div style=\"width:100%;\">Quotation Amount: <span style=\"color: rgb(255, 84, 0);\">$".$quotation_amount." per item</span></div>";

			if($bestThree[$k]['comments']!="")					  
			 $quotation_details .= "<div style=\"width:100%;\">Comments on Quoted Price:<span style=\"color: rgb(255, 84, 0);\">".stripslashes($bestThree[$k]['comments'])."</span> </div>";
           $quotation_details .="<div style=\"width:100%;\">Quotation Date: <span style=\"color: rgb(255, 84, 0);\">".date("l, F j, Y",strtotime($bestThree[$k]['quotationDate']))."</span></div>";

			     //======================== For customer =========================//

			$quotation_detail_list .="<div style=\"width:100%; margin:0 auto; padding:0 0 5px; background-color: rgb(255, 255, 255); border-radius:0 0 4px 4px\">
              <div style=\"width:100%; overflow:hidden; padding:9px 0; margin:0 auto; background-color: rgb(0, 0, 0); border-radius:4px 4px 0 0; text-align:left; color:#fff; font-weight:normal; font-size:14px; color:rgb(255, 84, 0); font-weight:bold; text-transform:uppercase\">
                <div style=\"width:95%; margin:0 auto;\">Quotation No :<span style=\"border-radius: 50%; background-color: rgb(255, 84, 0); color: rgb(255, 255, 255); float: right; height: 24px; width: 24px; text-align: center; font-weight: bold; line-height: 24px;\">".($k+1)."</span></div>
              </div>              

				<!-- retailer info starts -->             
				  <div style=\"width:100%; float:left; padding:0; margin:0 auto; text-align:center\">";
				  if(get_user_meta( $bestThree[$k]['retailerId'], 'cr_photo', true )){
						$quotation_detail_list .="<div style=\"float:left; width:40%; padding:10px 5px; margin:0; min-width:200px\">					
							<img src=\"".get_user_meta( $bestThree[$k]['retailerId'], 'cr_photo', true )."\" alt=\"\" style=\"float:left; margin:0 auto; display:inline-block; text-align:center; max-width:98%\">
						</div>";
				  }
					$quotationdAmount =  number_format($bestThree[$k]['ticketAmount']*$result->quantity,2);
					
					$quotation_detail_list .="<div style=\"width:".(get_user_meta( $bestThree[$k]['retailerId'], 'cr_photo', true )!=""?"55%":"100%")."; overflow:hidden; padding:10px 5px; margin:0 auto 15px; text-align:left; color:#000; font-weight:normal; font-size:14px; float:left\">
						<div style=\"width:100%;\">Retailer Name: <span style=\"color: rgb(255, 84, 0);\">".$company_name."</span></div>
						<div style=\"width:100%;\">Contact Name: <span style=\"color: rgb(255, 84, 0);\"> ".$bestThree[$k]['contact_person']."</span></div>
						<div style=\"width:100%;\">Contact Details: <span style=\"color: rgb(255, 84, 0);\">".$bestThree[$k]['contact_details']."</span></div>
					  </div>
				  </div>
				  <!-- retailer info ends -->           
			  <div style=\"width:95%; margin:0 auto 5px; padding:4px 5px; border:1px solid #dedede; overflow:hidden\">

              <!-- quotation amount starts -->
                <div style=\"width:100%; float:left; margin:0; padding:0\">
                  <div style=\"width:96%; float:left; margin:0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; \">
                    <div style=\"width:100%\">Quotation Amount $isgst:<span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">$".number_format($bestThree[$k]['ticketAmount'],2)." X ".$result->quantity." = $".$quotationdAmount."</span></div>
                  </div>
                ";

			if($bestThree[$k]['comments']!="")							  
				$quotation_detail_list .= "<div style=\"width:96%; float:left; margin:0; padding:6px 2%; float:left; font-size:13px; color:rgb(126, 126, 126); line-height:18px\">
                    <div style=\"width:100%;\"><span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments on Quoted Price:</span> <span style=\"width:100%; float:left\">".stripslashes($bestThree[$k]['comments'])."</span></div>
                  </div>";

				$quotation_detail_list .= "</div><!-- quotation amount ends -->";
				$quotation_detail_list .="<!-- product condition ends -->
				<div style=\"width:100%; float:left; margin:0\">
                  <div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238)\">
                    <div style=\"width:100%\">Product Condition:<span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".$product_condition."</span></div>
                  </div>                  
                ";				



			if($bestThree[$k]['product_warranty']!="")
				$quotation_detail_list .= "<div style=\"width:96%; float:left; margin:0; padding:6px 2%; float:left; font-size:13px; color:rgb(126, 126, 126); line-height:18px\">
                    <div style=\"width:100%;\"><span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments on Product Condition:</span> <span style=\"width:100%; float:left\">".stripslashes($bestThree[$k]['product_warranty'])."</span></div>
                  </div>";
				$quotation_detail_list .= "</div><!-- product condition ends -->";
				//// get dynamic data  starts ///////

		$da_quote = get_dynamic_quote_attributes($bestThree[$k]['quotationId']);
		 if(count($da_quote) > 0){
			foreach($da_quote as $key => $value){
				$$key = $value;
			}
		}				

				if(count($das['key']) > 0){
					$dynamic_quote ="";
					$quotation_detail_list.="{timeframe}";
					foreach($das['key'] as $key => $val){
						$price_variable = "price_".$val;
						$meet_variable = "meet_".$val;
						$comment_variable = "comment_".$val;

						if($val == "wccpf_when_do_you_need_the_product" || $comment_variable == "comment_wccpf_when_do_you_need_the_product"){
							if($val == "wccpf_when_do_you_need_the_product")
								/*$when_do_you_need_the_product = "
								<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; text-indec:1%\">
                    <div style=\"width:100%;\">Timeframe: <span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".product_delivery_switch($das['customer_choice'][$val])."</span> </div>
                  </div>
							  ";*/
							  $when_do_you_need_the_product = "";
							  if($bestThree[$k][$meet_variable]!=""){
									$when_do_you_need_the_product .="
									<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; \">
										<div style=\"width:100%\">Can you meet ".product_delivery_switch($das['customer_choice'][$val])." requirements? : <span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".ucfirst($bestThree[$k][$meet_variable])."</span> </div>
									</div>
									";
									$dynamic_quote .= "<div style=\"width:100%;\">Can you meet ".product_delivery_switch($das['customer_choice'][$val])." requirements? <span style=\"color: rgb(255, 84, 0);\">".ucfirst($bestThree[$k][$meet_variable])."</span></div>";
								}
							 if($bestThree[$k][$comment_variable]!=""){
								$when_do_you_need_the_product .="
								<div style=\"width:96%; float:left; margin:0; padding:6px 2%; float:left; font-size:13px; color:rgb(126, 126, 126); line-height:18px\">
									<div style=\"width:100%;\">
									<span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments:</span>
									<span style=\"width:100%; float:left\">".stripslashes($bestThree[$k][$comment_variable])."</span> </div>
								  </div>";								
							 }
							 if($$comment_variable != "")
							 $dynamic_quote .= "<div style=\"width:100%;\">Timeframe Comment  :<span style=\"color: rgb(255, 84, 0);\">".stripslashes($$comment_variable)."</span></div>";
							}
							
						elseif($val == "wccpf_deliverypickup" || $comment_variable == "comment_wccpf_deliverypickup"){
							$when_do_you_need_the_product2 = "";
						if($val == "wccpf_deliverypickup"){	    
								//$when_do_you_need_the_product2 ="<li style=\"width:98%; float:left; margin:5px 0 0 0; padding:6px 1%; float:left; list-style:none; background-color:#eee\">".ucfirst($das['customer_choice'][$val])." :<span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">Yes</span></li>";
								
								/*$when_do_you_need_the_product2 .="<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; \"><div style=\"width:100%\">".$das['label'][$val]." : <span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".ucfirst($das['customer_choice'][$val])."</span> </div></div>";

								$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." <span style=\"color: rgb(255, 84, 0);\">".ucfirst($das['customer_choice'][$val])."</span></div>";*/									
								
								
						}
						if($bestThree[$k][$meet_variable]!=""){
							$when_do_you_need_the_product2 .="
							<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; \">
								<div style=\"width:100%\">Can you meet ".$das['label'][$val]." requirements? : <span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".ucfirst($bestThree[$k][$meet_variable])."</span> </div>
							</div>
							";
							$dynamic_quote .= "<div style=\"width:100%;\">Can you meet ".$das['label'][$val]." requirements? <span style=\"color: rgb(255, 84, 0);\">".ucfirst($bestThree[$k][$meet_variable])."</span></div>";
						}
						
						
							if($bestThree[$k][$price_variable]!=""){
								$when_do_you_need_the_product2 .="
						<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff;\">";				
								$when_do_you_need_the_product2 .="
								<div style=\"width:100%; float:left; margin:3px 0\">".$das['label'][$val]." Price $isgst: <span style=\"color:#000; padding-left:3px; color:rgb(255, 84, 0)\">".($bestThree[$k][$price_variable]==0?"FREE":"$".$bestThree[$k][$price_variable])."</span> </div>
								";
								$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." Price <span style=\"color: rgb(255, 84, 0);\">$".$$price_variable." per item</span></div>";
							    $when_do_you_need_the_product2 .="</div>";
							}
							
							
							if($bestThree[$k][$comment_variable]!=""){
							    $when_do_you_need_the_product2 .="
								<div style=\"width:96%; font-size:13px; color:rgb(126, 126, 126); margin:0; padding:6px 2%; float:left; background-color: rgb(255, 255, 255)\">";		
								
								$when_do_you_need_the_product2 .="
								<div style=\"width:100%\"> <span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments:</span> <span style=\"width:100%; float:left\">".stripslashes($bestThree[$k][$comment_variable])."</span> </div>
								";
								$dynamic_quote .= "<div style=\"width:100%;\"> Commnets on ".$das['label'][$val]." <span style=\"color: rgb(255, 84, 0);\">".stripslashes($bestThree[$k][$comment_variable])."</span></div>";
								
								$when_do_you_need_the_product2 .="</div>";
							}
						
						
						
						
								
						}
						elseif($val == "wccpf_removal_of_old_appliance"){
							$when_do_you_need_the_product3 = "";
							if($das['customer_choice'][$val] == "no")//job requirements is no, no need to show it in form and quote template
								continue;
							else{
								if($bestThree[$k][$meet_variable]!=""){
									$when_do_you_need_the_product3 .="
									<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; \">
										<div style=\"width:100%\">".$das['label'][$val]." : <span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".ucfirst($bestThree[$k][$meet_variable])."</span> </div>
									</div>
									";
									$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." <span style=\"color: rgb(255, 84, 0);\">".ucfirst($bestThree[$k][$meet_variable])."</span></div>";
								}
								
										
										if($bestThree[$k][$price_variable]!=""){
											$when_do_you_need_the_product3 .="
									<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff;\">";				
											$when_do_you_need_the_product3 .="
											<div style=\"width:100%; float:left; margin:3px 0\">".$das['label'][$val]." Price $isgst: <span style=\"color:#000; padding-left:3px; color:rgb(255, 84, 0)\">".($bestThree[$k][$price_variable]==0?"FREE":"$".$bestThree[$k][$price_variable])."</span> </div>
											";
											$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." Price <span style=\"color: rgb(255, 84, 0);\">$".$$price_variable." per item</span></div>";
											$when_do_you_need_the_product3 .="</div>";
										}
										if($bestThree[$k][$comment_variable]!=""){
											$when_do_you_need_the_product3 .="
								<div style=\"width:96%; font-size:13px; color:rgb(126, 126, 126); margin:0; padding:6px 2%; float:left; background-color: rgb(255, 255, 255)\">";		
											$when_do_you_need_the_product3 .="
											<div style=\"width:100%\"> <span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments:</span> <span style=\"width:100%; float:left\">".stripslashes($bestThree[$k][$comment_variable])."</span> </div>
											";
											$dynamic_quote .= "<div style=\"width:100%;\">Comments on ".$das['label'][$val]." <span style=\"color: rgb(255, 84, 0);\">".stripslashes($bestThree[$k][$comment_variable])."</span></div>";
											$when_do_you_need_the_product3 .="</div>";
										}
									
							}
						}
						else{

							
							
							/*$when_do_you_need_the_product .= "
								<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; text-indec:1%\">
                    <div style=\"width:100%;\">".$das['label'][$val].": <span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".$das['customer_choice'][$val]."</span> </div>
                  </div>";*/

						if($bestThree[$k][$meet_variable]!=""){

								$quotation_detail_list .="<!-- dynamic meet requirement starts --><div style=\"width:100%; float:left; margin:0; padding:0\">";

								if($val =="wccpf_when_do_you_need_the_product"){

									$dynamic_quote .= "<div style=\"width:100%;\">Can you meet ".product_delivery_switch($das['customer_choice'][$val])." Timeframe? <span style=\"color: rgb(255, 84, 0);\">".ucfirst($$meet_variable)."</span></div>";

									if($$comment_variable != "")

									$dynamic_quote .= "<div style=\"width:100%;\">Timeframe Comment  :<span style=\"color: rgb(255, 84, 0);\">".stripslashes($$comment_variable)."</span></div>";

									continue;

								}else{

									if($val != "wccpf_deliverypickup"){

									  $dynamic_quote .= "<div style=\"width:100%;\">Can you meet ".$das['label'][$val]."? <span style=\"color: rgb(255, 84, 0);\">".ucfirst($$meet_variable)."</span></div>";

								    }

									/*$quotation_detail_list .="

									<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; background-color: rgb(238, 238, 238)\">

                    					<div style=\"width:100%\"> 

											<span style=\"float:left\">".$das['label'][$val]." :</span> 

											<span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".ucfirst($das['customer_choice'][$val])."</span> 

										</div>

									</div>

									";*/

								}

							$quotation_detail_list .= "<!-- requirements div starts --><div style=\"width:96%; float:left; margin:0; padding:6px 2%; font-size:13px; color:rgb(126, 126, 126); line-height:18px\">";

							$quotation_detail_list .=" 

							<div style=\"width:100%; float:left; margin:3px 0\"> <span style=\"float:left\">Can you meet the customers requirement?:</span> <span style=\"color:#000; padding-left:3px\">".ucfirst($bestThree[$k][$meet_variable])."</span> </div>								

								";

							if($bestThree[$k][$price_variable]!=""){				
								$quotation_detail_list .="
								<div style=\"width:100%; float:left; margin:3px 0\"> <span style=\"float:left\">".$das['label'][$val]." Price $isgst:</span> <span style=\"color:#000; padding-left:3px; color:rgb(255, 84, 0)\">".($bestThree[$k][$price_variable]==0?"FREE":"$".$bestThree[$k][$price_variable])."</span> </div>
								";							

								$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." Price <span style=\"color: rgb(255, 84, 0);\">$".$$price_variable." per item</span></div>";

							}

							if($bestThree[$k][$comment_variable]!=""){
								$quotation_detail_list .="
								<div style=\"width:100%\"> <span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments:</span> <span style=\"width:100%; float:left\">".stripslashes($bestThree[$k][$comment_variable])."</span> </div>
								";

								$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." Comment  <span style=\"color: rgb(255, 84, 0);\">".stripslashes($$comment_variable)."</span></div>";

							}

							$quotation_detail_list .="</div><!-- requirements div ends -->";
							$quotation_detail_list .="</div><!-- dynamic meet requirement ends -->";		
						}
						}
					}

					

					$when_do_you_need_the_product = "<div style=\"width:100%; float:left; margin:0\">".$when_do_you_need_the_product.$when_do_you_need_the_product2.$when_do_you_need_the_product3."</div>";
					$quotation_detail_list = str_replace("{timeframe}",$when_do_you_need_the_product,$quotation_detail_list);

				}

				

				$quotation_details .= $dynamic_quote;			
				$quotation_details .="<div style=\"width:100%;\">Contact Person: <span style=\"color: rgb(255, 84, 0);\">".$bestThree[$k]['contact_person']."</span></div>
				<div style=\"width:100%;\">Contact Details: <span style=\"color: rgb(255, 84, 0);\">".$bestThree[$k]['contact_details']."</span></div>						
				<div style=\"width:100%;\">Product Condition: <span style=\"color: rgb(255, 84, 0);\">".product_condition($bestThree[$k]['product_condition'])."</span></div>";

				if($bestThree[$k]['product_warranty']!="")

					$quotation_details .="<div style=\"width:100%;\">Comments on Product Condition: <span style=\"color: rgb(255, 84, 0);\">".stripslashes($bestThree[$k]['product_warranty'])."</span></div>";
				if($bestThree[$k]['terms_and_conditions']!="")	
					$quotation_details .="<div style=\"width:100%;\">Offer Comments: <span style=\"color: rgb(255, 84, 0);\">".stripslashes($bestThree[$k]['terms_and_conditions'])."</span></div>";
					
				$quotation_details .="<div style=\"width:100%;\">Grand Quotation Total: <span style=\"color: rgb(255, 84, 0);\">$".$bestThree[$k]['finalQuotationAmount']."</span></div>";
				$quotation_details .="</li></ul>";

			//// get dynamic data  ends ///////

			if($bestThree[$k]['warranty_price']!="")				
				$quotation_detail_list .="<div style=\"width:100%; float:left; margin:3px 0\">Warranty Price:<span style=\"color:#000; padding-left:3px; color:rgb(255, 84, 0)\">".($bestThree[$k]['warranty_price']==0?"FREE":"$".$bestThree[$k]['warranty_price'])."</span></div>";
				

			if($bestThree[$k]['terms_and_conditions']!="")
			$quotation_detail_list .= "<div style=\"width:100%; float:left; margin:0; padding:0; border-top:1px solid #dedede\">
                  <div style=\"width:96%; float:left; margin:0; padding:6px 2%; float:left; font-size:13px; color:rgb(126, 126, 126); line-height:18px\">
                    <div style=\"width:100%\"> <span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Offer Comments:</span> <span style=\"width:100%; float:left\">".$bestThree[$k]['terms_and_conditions']."</span> </div>
                  </div>

                </div><!-- offer comments -->";

			$quotation_detail_list .="
				<div style=\"width:100%; float:left; margin:0; padding:0; border-top:1px solid #dedede\">
                  <div style=\"width:96%; float:left; margin:9px 0 0; padding:6px 2%; float:left; font-size:21px; color:rgb(255, 84, 0); line-height:21px;\">"; 
				  
				  //=============bonus section=============//
				  
			   $grandTotal = $bestThree[$k]['finalQuotationAmount'];
			   $quotedAmount = ($grandTotal - ($bestThree[$k]['quotationAmount']*$result->quantity)) + ($bestThree[$k]['ticketAmount']*$result->quantity);
			   
			   $quotation_detail_list .="<div style=\"width:100%;\">		
				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
				<tr>
				<td align=\"left\" valign=\"top\"><table width=\"100%\" border=\"0\" cellspacing=\"6\" cellpadding=\"6\">
				  <tr>
					<td width=\"40%\" rowspan=\"3\" align=\"center\" valign=\"middle\">";
					  if(((float)$bestThree[$k]['ticketAmount']-(float)$bestThree[$k]['quotationAmount'])>0){					  
					  	$quotation_detail_list .="<img src=\"".home_url()."/images/bonus-cash-icon.jpg\" width=\"150\" height=\"150\" alt=\"\" />";
					  }else{
						$quotation_detail_list .="&nbsp;&nbsp;";
					  }
					$quotation_detail_list .="</td>
					<td width=\"\" align=\"right\" valign=\"middle\" style=\"color:rgb(255,120,0); font-weight:bold;  font-size:16px\">Quoted Price: $</td>
					<td width=\"18%\" align=\"center\" valign=\"middle\"><span style=\"background-color:rgb(0,0,0); color:rgb(255,255,255); font-weight:bold;  font-size:16px; height:25px; width:100%; padding:6px 9px; display:block; line-height:25px\">".number_format($quotedAmount,2)."</span></td>
				  </tr>";
				  
				  if(((float)$bestThree[$k]['ticketAmount']-(float)$bestThree[$k]['quotationAmount'])>0){
					  
					  
				  $quotation_detail_list .="<tr>
					<td align=\"right\" valign=\"middle\" style=\"color:rgb(255,0,0); font-weight:bold;  font-size:16px\">HAGGLEFREE Bonus: $<br /><span style=\"color:rgb(0,0,0); font-size:10px\">(Bonus Expires after ".date("g:i a d/m/Y",strtotime($offerExp)).")</span></td>
					<td align=\"center\" valign=\"middle\"><span style=\"background-color:rgb(0,0,0); color:rgb(255,255,255); font-weight:bold;  font-size:16px; height:25px; width:100%; padding:6px 9px; display:block; line-height:25px\">".number_format(((float)$quotedAmount-(float)$grandTotal),2)."</span></td>
				  </tr>
				  <tr>
					<td align=\"right\" valign=\"middle\" style=\"color:rgb(255,120,0); font-weight:bold;  font-size:16px\">HAGGLEFREE Price: $</td>
					<td align=\"center\" valign=\"middle\"><span style=\"background-color:rgb(0,0,0); color:rgb(255,255,255); font-weight:bold;  font-size:16px; height:25px; width:100%; padding:6px 9px; display:block; line-height:25px\">".number_format($grandTotal,2)."</span></td>					
				  </tr>";
				  }
				  
				$quotation_detail_list .="</table></td>
				</tr>
				</table>
				</div>";
				
				  //================end bomus section =============//
				  
				  
				$quotation_detail_list .="</div></div>
			 </div>
			</div>";				

			//===============Update quotation status================//

				$update_val = array('status' => 2);

				$where = array('id' => $bestThree[$k]['quotationId']);	

				$wpdb->update($tableQuotation,$update_val,$where);


		    //==========Calculate lead fee and insert in database===============//
			$userDiscount = (float)get_user_meta($retaler_info->ID, 'cr_lead_fee', true);

			if($userDiscount > 0){			
			   $calLeadFee = ($rrp*$leadFee)/100;
			   $calLeadFee = ($calLeadFee - (($calLeadFee*$userDiscount)/100));
			}
			else{
			   $calLeadFee = ($rrp*$leadFee)/100;
			}

			//===============Update lead fee================//

				$update_val = array('rrp' => $rrp,'leadFee' => $leadFee,'leadDiscount' => $userDiscount,'totalLeadFee' => $calLeadFee);

				$where = array('id' => $bestThree[$k]['quotationId']);	

				$wpdb->update($tableQuotation,$update_val,$where);			 

			    if($calLeadFee > 0){

					$leadFeeText = "<div style=\"width:100%;\"><b> $".round($calLeadFee,2)."</b></div>";

				}else{

					$leadFeeText = "<div style=\"width:100%;\"><b>No Lead fee applicable.</b></div>";

				}


		//echo "//================ send email to retailer selected quote==========2====================// 			 ";

			 	

			$headers[] = 'Content-Type: text/html; charset=UTF-8';

			$headers[] = 'From: '.get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'> ' . "\r\n";			

			$mail_content = stripslashes($my_quotation_selected['email_template']);

			

			//======== Token Replacement =================//

			$mail_content = str_replace("{site_name}",get_bloginfo( 'name' ),$mail_content);

			$mail_content = str_replace("{site_url}",home_url(),$mail_content);

			$mail_content = str_replace("{site_tag}",get_bloginfo( 'description' ),$mail_content);

			$mail_content = str_replace("{media_url}",$media_url,$mail_content);	

			$mail_content = str_replace("{email_heading}",stripslashes($my_quotation_selected['email_heading']),$mail_content);

			

			$mail_content = str_replace("{company_name}",$company_name,$mail_content);

			$mail_content = str_replace("{quotation_amount}",$bestThree[$k]['finalQuotationAmount'],$mail_content);

			$mail_content = str_replace("{product_details}",$product_details,$mail_content);

			$mail_content = str_replace("{buyer_details}",$buyer_details,$mail_content);

			$mail_content = str_replace("{customer_requirements}",$customer_requirements,$mail_content);

			$mail_content = str_replace("{quotation_details}",$quotation_details,$mail_content);

			$mail_content = str_replace("{lead_fee}",$leadFeeText,$mail_content);	



			$mail_content = str_replace("{admin_email}",get_option( 'admin_email' ),$mail_content);

			$mail_content = str_replace("{site_abn}",get_option( 'site_abn' ),$mail_content);

			$mail_content = str_replace("{site_phone}",get_option( 'site_phone' ),$mail_content);

			$mail_content = str_replace("{fb_url}",get_option( 'fb_url' ),$mail_content);

			

			$subject = $my_quotation_selected['subject'];

			$subject = str_replace("{job_id}",$result->id,$subject);

			$subject = str_replace("{sku}",$product->get_sku(),$subject);
			
			$subject = str_replace("{postcode}",$result->Postcode,$subject);
			
			$subject = str_replace("{qty}",$result->quantity,$subject);
			
			$subject = str_replace("{job_type}",($result->is_commercial=='1'?'Commercial':'Private'),$subject);

			//======== Token Replacement =================//

			

			wp_mail( $retaler_info->user_email, $subject, $mail_content,$headers);

			//echo $mail_content."=====".$subject."=======";			

		 }

		 

		 if(count($noBestThree) > 0){

		

		   $limit2 = ($job_no_of_quotes-$limit);

		   if($limit2 > count($noBestThree))

		   $limit2 = count($noBestThree);

		   

		   for($m = 0; $m < ($limit2); $m++){	 

			 

			$retaler_info = get_userdata($noBestThree[$m]['retailerId']);

			$company_name = get_user_meta($noBestThree[$m]['retailerId'],'cr_company',true);

			$quotation_amount = $noBestThree[$m]['quotationAmount'];

			//print_r($retaler_info);

			$retalerDisplayName = $retaler_info->display_name;

			 if($company_name == ""){

				if($retalerDisplayName!="")

				$company_name = ucfirst($retalerDisplayName);

				else

				$company_name = ucfirst($retaler_info->first_name);

			 }

			

			$product_condition = product_condition($noBestThree[$m]['product_condition']);

			$isgst = ($result->is_commercial==1?'(Inc GST)':'(Inc GST)');

			

			    //======================== For retailer =========================//

			$quotation_details ="<ul style=\"width:100%; float:left; margin:0 0; padding:0\">

            <li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#000; border-radius:4px 4px 0 0; text-align:left; color:#fff; font-weight:normal; font-size:14px\">Your Quotation Details:</li>

                    <li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#fff; border-radius:0 0 4px 4px; text-align:left; color:#000; font-weight:normal; font-size:14px\">

                        <div style=\"width:100%;\">Quotation Amount: <span style=\"color: rgb(255, 84, 0);\">$".$quotation_amount." per item</span></div>";

			if($noBestThree[$m]['comments']!="")							  

				$quotation_details .= "<div style=\"width:100%;\">Comments on Quoted Price:<span style=\"color: rgb(255, 84, 0);\">".stripslashes($noBestThree[$m]['comments'])."</span> </div>";

				

				$quotation_details .= "<div style=\"width:100%;\">Quotation Date: <span style=\"color: rgb(255, 84, 0);\">".date("l, F j, Y",strtotime($noBestThree[$m]['quotationDate']))."</span></div>";

			

			



			//======================== quotation detail list for customer =========================//

			$quotation_detail_list .="<div style=\"width:100%; margin:0 auto; padding:0 0 5px; background-color: rgb(255, 255, 255); border-radius:0 0 4px 4px\">

            

              <div style=\"width:100%; overflow:hidden; padding:9px 0; margin:0 auto; background-color: rgb(0, 0, 0); border-radius:4px 4px 0 0; text-align:left; color:#fff; font-weight:normal; font-size:14px; color:rgb(255, 84, 0); font-weight:bold; text-transform:uppercase\">

                <div style=\"width:95%; margin:0 auto;\">Quotation No :<span style=\"border-radius: 50%; background-color: rgb(255, 84, 0); color: rgb(255, 255, 255); float: right; height: 24px; width: 24px; text-align: center; font-weight: bold; line-height: 24px;\">".($k+$m+1)."</span></div>

              </div>              
				
				<!-- retailer info starts -->             
				  <div style=\"width:100%; float:left; padding:0; margin:0 auto; text-align:center\">";
				  if(get_user_meta( $noBestThree[$m]['retailerId'], 'cr_photo', true )){
						$quotation_detail_list .="<div style=\"float:left; width:40%; padding:10px 5px; margin:0; min-width:200px\">					
							<img src=\"".get_user_meta( $noBestThree[$m]['retailerId'], 'cr_photo', true )."\" alt=\"\" style=\"float:left; margin:0 auto; display:inline-block; text-align:center; max-width:98%\">
						</div>";
				  }
				  
				    $quotationdAmount =  number_format($noBestThree[$m]['ticketAmount']*$result->quantity,2);
				  
					$quotation_detail_list .="<div style=\"width:".(get_user_meta( $noBestThree[$m]['retailerId'], 'cr_photo', true )!=""?"55%":"100%")."; overflow:hidden; padding:10px 5px; margin:0 auto 15px; text-align:left; color:#000; font-weight:normal; font-size:14px; float:left\">
						<div style=\"width:100%;\">Retailer Name: <span style=\"color: rgb(255, 84, 0);\">".$company_name."</span></div>
						<div style=\"width:100%;\">Contact Name: <span style=\"color: rgb(255, 84, 0);\"> ".$noBestThree[$m]['contact_person']."</span></div>
						<div style=\"width:100%;\">Contact Details: <span style=\"color: rgb(255, 84, 0);\">".$noBestThree[$m]['contact_details']."</span></div>
					  </div>
				  </div>
				  <!-- retailer info ends --> 
			  

			  <div style=\"width:95%; margin:0 auto 5px; padding:4px 5px; border:1px solid #dedede; overflow:hidden\">

              <!-- quotation amount starts -->

                <div style=\"width:100%; float:left; margin:0; padding:0\">

                  <div style=\"width:96%; float:left; margin:0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; \">

                    <div style=\"width:100%\">Quotation Amount $isgst:<span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">$".number_format($noBestThree[$m]['ticketAmount'],2)." X ".$result->quantity." = $".$quotationdAmount."</span></div>

                  </div>

                ";

							  

			if($noBestThree[$m]['comments']!="")							  

				$quotation_detail_list .= "<div style=\"width:96%; float:left; margin:0; padding:6px 2%; float:left; font-size:13px; color:rgb(126, 126, 126); line-height:18px\">

                    <div style=\"width:100%;\"><span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments on Quoted Price:</span> <span style=\"width:100%; float:left\">".stripslashes($noBestThree[$m]['comments'])."</span></div>

                  </div>";

				

				$quotation_detail_list .= "</div><!-- quotation amount ends -->";

				

				$quotation_detail_list .="<!-- product condition ends -->

				<div style=\"width:100%; float:left; margin:0\">

                  <div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238)\">

                    <div style=\"width:100%\">Product Condition:<span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".$product_condition."</span></div>

                  </div>                  

                ";				



			if($noBestThree[$m]['product_warranty']!="")

				$quotation_detail_list .= "<div style=\"width:96%; float:left; margin:0; padding:6px 2%; float:left; font-size:13px; color:rgb(126, 126, 126); line-height:18px\">

                    <div style=\"width:100%;\"><span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments on Product Condition:</span> <span style=\"width:100%; float:left\">".stripslashes($noBestThree[$m]['product_warranty'])."</span></div>

                  </div>";

				$quotation_detail_list .= "</div><!-- product condition ends -->";			

				// dynamic quote detail 

		$da_quote = get_dynamic_quote_attributes($noBestThree[$m]['quotationId']);

		 if(count($da_quote) > 0){

			foreach($da_quote as $key => $value){

				$$key = $value;

			}

		}				


				if(count($das['key']) > 0){

					$dynamic_quote ="";

					$quotation_detail_list.="{timeframe}";

					foreach($das['key'] as $key => $val){

						$price_variable = "price_".$val;

						$meet_variable = "meet_".$val;

						$comment_variable = "comment_".$val;

						if($val == "wccpf_when_do_you_need_the_product" || $comment_variable == "comment_wccpf_when_do_you_need_the_product"){
							if($val == "wccpf_when_do_you_need_the_product")
							     $when_do_you_need_the_product = "";
								/*$when_do_you_need_the_product = "
								<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; text-indec:1%\">
                    <div style=\"width:100%;\">Timeframe: <span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".product_delivery_switch($das['customer_choice'][$val])."</span> </div>
                  </div>
							  ";*/
							 if($noBestThree[$m][$meet_variable]!=""){
									$when_do_you_need_the_product .="
									<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; \">
										<div style=\"width:100%\">Can you meet ".product_delivery_switch($das['customer_choice'][$val])." requirements? : <span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".ucfirst($noBestThree[$m][$meet_variable])."</span> </div>
									</div>
									";
									$dynamic_quote .= "<div style=\"width:100%;\">Can you meet ".product_delivery_switch($das['customer_choice'][$val])." requirements? <span style=\"color: rgb(255, 84, 0);\">".ucfirst($noBestThree[$m][$meet_variable])."</span></div>";
								} 
							 if($noBestThree[$m][$comment_variable]!=""){
								
								$when_do_you_need_the_product .="
								<div style=\"width:96%; float:left; margin:0; padding:6px 2%; float:left; font-size:13px; color:rgb(126, 126, 126); line-height:18px\">
									<div style=\"width:100%;\">
									<span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments:</span>
									<span style=\"width:100%; float:left\">".stripslashes($noBestThree[$m][$comment_variable])."</span> </div>
								  </div>";
							 }
							 if($$comment_variable != "")
							 $dynamic_quote .= "<div style=\"width:100%;\">Timeframe Comment :<span style=\"color: rgb(255, 84, 0);\">".stripslashes($$comment_variable)."</span></div>";
							}
							
						elseif($val == "wccpf_deliverypickup" || $comment_variable == "comment_wccpf_deliverypickup"){
							$when_do_you_need_the_product2 = "";
						if($val == "wccpf_deliverypickup"){	    
								//$when_do_you_need_the_product2 ="<li style=\"width:98%; float:left; margin:5px 0 0 0; padding:6px 1%; float:left; list-style:none; background-color:#eee\">".ucfirst($das['customer_choice'][$val])." :<span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">Yes</span></li>";
								/*$when_do_you_need_the_product2 .="
								<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; \">
                    <div style=\"width:100%\">".$das['label'][$val]." : <span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".ucfirst($das['customer_choice'][$val])."</span> </div>
                  </div>";

								$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." <span style=\"color: rgb(255, 84, 0);\">".ucfirst($das['customer_choice'][$val])."</span></div>";	*/								
								
								
						}
						if($noBestThree[$m][$meet_variable]!=""){
									$when_do_you_need_the_product2 .="
									<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; \">
										<div style=\"width:100%\">Can you meet ".$das['label'][$val]." requirements? : <span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".ucfirst($noBestThree[$m][$meet_variable])."</span> </div>
									</div>
									";
									$dynamic_quote .= "<div style=\"width:100%;\">Can you meet ".$das['label'][$val]." requirements? <span style=\"color: rgb(255, 84, 0);\">".ucfirst($noBestThree[$m][$meet_variable])."</span></div>";
						}
						$when_do_you_need_the_product2 .="
						<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff;\">";
						
							if($noBestThree[$m][$price_variable]!=""){				
								$when_do_you_need_the_product2 .="
								<div style=\"width:100%; float:left; margin:3px 0\">".$das['label'][$val]." Price $isgst: <span style=\"color:#000; padding-left:3px; color:rgb(255, 84, 0)\">".($noBestThree[$m][$price_variable]==0?"FREE":"$".$noBestThree[$m][$price_variable])."</span> </div>
								";
								$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." Price <span style=\"color: rgb(255, 84, 0);\">$".$$price_variable." per item</span></div>";
							}
							if($noBestThree[$m][$comment_variable]!=""){
								$when_do_you_need_the_product2 .="
								<div style=\"width:100%\"> <span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments:</span> <span style=\"width:100%; float:left\">".stripslashes($noBestThree[$m][$comment_variable])."</span> </div>
								";
								$dynamic_quote .= "<div style=\"width:100%;\">Commetns on ".$das['label'][$val]." <span style=\"color: rgb(255, 84, 0);\">".stripslashes($noBestThree[$m][$comment_variable])."</span></div>";
							}
							
						$when_do_you_need_the_product2 .="</div>";
								
						}
						elseif($val == "wccpf_removal_of_old_appliance"){
							$when_do_you_need_the_product3 = "";
							if($das['customer_choice'][$val] == "no")//job requirements is no, no need to show it in form and quote template
								continue;
							else{
								if($noBestThree[$m][$meet_variable]!=""){
									/*$when_do_you_need_the_product3 .="
									<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; \">
										<div style=\"width:100%\">".$das['label'][$val]." : <span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".ucfirst($noBestThree[$m][$meet_variable])."</span> </div>
									</div>
									";
									$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." <span style=\"color: rgb(255, 84, 0);\">".ucfirst($noBestThree[$m][$meet_variable])."</span></div>";*/
								}
								
										
										if($noBestThree[$m][$price_variable]!=""){
										
										   $when_do_you_need_the_product3 .="
									       <div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff;\">";	
															
											$when_do_you_need_the_product3 .="
											<div style=\"width:100%; float:left; margin:3px 0\">".$das['label'][$val]." Price $isgst: <span style=\"color:#000; padding-left:3px; color:rgb(255, 84, 0)\">".($noBestThree[$m][$price_variable]==0?"FREE":"$".$noBestThree[$m][$price_variable])."</span> </div>
											";
											$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." Price <span style=\"color: rgb(255, 84, 0);\">$".$$price_variable." per item</span></div>";
										    $when_do_you_need_the_product3 .="</div>";
										
										}
										
										
										
										if($noBestThree[$m][$comment_variable]!=""){
										
										    $when_do_you_need_the_product3 .="
									        <div style=\"width:96%; font-size:13px; color:rgb(126, 126, 126); margin:0; padding:6px 2%; float:left; background-color: rgb(255, 255, 255)\">";	
											
											$when_do_you_need_the_product3 .="
											<div style=\"width:100%\"> <span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments:</span> <span style=\"width:100%; float:left\">".stripslashes($noBestThree[$m][$comment_variable])."</span> </div>
											";
											$dynamic_quote .= "<div style=\"width:100%;\">Comments on ".$das['label'][$val]." <span style=\"color: rgb(255, 84, 0);\">".stripslashes($noBestThree[$m][$comment_variable])."</span></div>";
											$when_do_you_need_the_product3 .="</div>";
										}
										
									
							}
						}

						else{

							/*$when_do_you_need_the_product .= "

								<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; text-indec:1%\">

                    <div style=\"width:100%;\">".$das['label'][$val].": <span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".$das['customer_choice'][$val]."</span> </div>

                  </div>

							  ";*/	

						if($noBestThree[$m][$meet_variable]!=""){

								$quotation_detail_list .="<!-- dynamic meet requirement starts --><div style=\"width:100%; float:left; margin:0; padding:0\">";

								if($val =="wccpf_when_do_you_need_the_product"){

									$dynamic_quote .= "<div style=\"width:100%;\">Can you meet ".product_delivery_switch($das['customer_choice'][$val])." Timeframe? <span style=\"color: rgb(255, 84, 0);\">".ucfirst($$meet_variable)."</span></div>";

									if($$comment_variable != "")

									$dynamic_quote .= "<div style=\"width:100%;\">Timeframe Comment  :<span style=\"color: rgb(255, 84, 0);\">".stripslashes($$comment_variable)."</span></div>";

									continue;

								}else{

									if($val != "wccpf_deliverypickup"){

									  $dynamic_quote .= "<div style=\"width:100%;\">Can you meet ".$das['label'][$val]."? <span style=\"color: rgb(255, 84, 0);\">".ucfirst($$meet_variable)."</span></div>";

								    }

							        

									/*$quotation_detail_list .="

									<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; background-color: rgb(238, 238, 238)\">

                    					<div style=\"width:100%\"> 

											<span style=\"float:left\">".$das['label'][$val]." :</span> 

											<span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".ucfirst($das['customer_choice'][$val])."</span> 

										</div>

									</div>

									";
*/
								}

							$quotation_detail_list .= "<!-- requirements div starts --><div style=\"width:96%; float:left; margin:0; padding:6px 2%; font-size:13px; color:rgb(126, 126, 126); line-height:18px\">";

							$quotation_detail_list .=" 

							<div style=\"width:100%; float:left; margin:3px 0\"> <span style=\"float:left\">Can you meet the customers requirement?:</span> <span style=\"color:#000; padding-left:3px\">".ucfirst($noBestThree[$m][$meet_variable])."</span> </div>";

							if($noBestThree[$m][$price_variable]!=""){				

								$quotation_detail_list .="

								<div style=\"width:100%; float:left; margin:3px 0\"> <span style=\"float:left\">".$das['label'][$val]." Price $isgst:</span> <span style=\"color:#000; padding-left:3px; color:rgb(255, 84, 0)\">".($noBestThree[$m][$price_variable]==0?"FREE":"$".$noBestThree[$m][$price_variable])."</span> </div>

								";							

								$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." Price <span style=\"color: rgb(255, 84, 0);\">$".$$price_variable." per item</span></div>";

							}

							if($noBestThree[$m][$comment_variable]!=""){

								$quotation_detail_list .="

								<div style=\"width:100%\"> <span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments:</span> <span style=\"width:100%; float:left\">".stripslashes($noBestThree[$m][$comment_variable])."</span> </div>

								";

								$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." Comment  <span style=\"color: rgb(255, 84, 0);\">".stripslashes($$comment_variable)."</span></div>";

							}

							$quotation_detail_list .="</div><!-- requirements div ends -->";

							$quotation_detail_list .="</div><!-- dynamic meet requirement ends -->";		
						}
					  }
					}

					$when_do_you_need_the_product = "<div style=\"width:100%; float:left; margin:0\">".$when_do_you_need_the_product.$when_do_you_need_the_product2.$when_do_you_need_the_product3."</div>";

					$quotation_detail_list = str_replace("{timeframe}",$when_do_you_need_the_product,$quotation_detail_list);

				}

			        $quotation_details .= $dynamic_quote;			

					$quotation_details .="<div style=\"width:100%;\">Contact Person: <span style=\"color: rgb(255, 84, 0);\">".$noBestThree[$m]['contact_person']."</span></div>

					<div style=\"width:100%;\">Contact Details: <span style=\"color: rgb(255, 84, 0);\">".$noBestThree[$m]['contact_details']."</span></div>						

					<div style=\"width:100%;\">Product Condition: <span style=\"color: rgb(255, 84, 0);\">".product_condition($noBestThree[$m]['product_condition'])."</span></div>";

					if($noBestThree[$m]['product_warranty']!="")

						$quotation_details .="<div style=\"width:100%;\">Comments on Product Condition: <span style=\"color: rgb(255, 84, 0);\">".stripslashes($noBestThree[$m]['product_warranty'])."</span></div>";
						
					if($noBestThree[$m]['terms_and_conditions']!="")	
					$quotation_details .="<div style=\"width:100%;\">Offer Comments:<span style=\"color: rgb(255, 84, 0);\">".stripslashes($noBestThree[$m]['terms_and_conditions'])."</span></div>";
					
					$quotation_details .="<div style=\"width:100%;\">Grand Quotation Total: <span style=\"color: rgb(255, 84, 0);\">$".$noBestThree[$m]['finalQuotationAmount']."</span></div>";

                    $quotation_details .="</li></ul>";				


			if($noBestThree[$m]['warranty_price']!="")				

				$quotation_detail_list .="<div style=\"width:100%; float:left; margin:3px 0\">Warranty Price:<span style=\"color:#000; padding-left:3px; color:rgb(255, 84, 0)\">".($noBestThree[$m]['warranty_price']==0?"FREE":"$".$noBestThree[$m]['warranty_price'])."</span></div>";


			if($noBestThree[$m]['terms_and_conditions']!="")

			$quotation_detail_list .= "<div style=\"width:100%; float:left; margin:0; padding:0; border-top:1px solid #dedede\">

                  <div style=\"width:96%; float:left; margin:0; padding:6px 2%; float:left; font-size:13px; color:rgb(126, 126, 126); line-height:18px\">

                    <div style=\"width:100%\"> <span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Offer Comments:</span> <span style=\"width:100%; float:left\">".stripslashes($noBestThree[$m]['terms_and_conditions'])."</span> </div>

                  </div>

                </div><!-- offer comments -->";

			$quotation_detail_list .="
				<div style=\"width:100%; float:left; margin:0; padding:0; border-top:1px solid #dedede\">
                  <div style=\"width:96%; float:left; margin:9px 0 0; padding:6px 2%; float:left; font-size:21px; color:rgb(255, 84, 0); line-height:21px;\">"; 
				  
				   //=============bonus section=============//
			   $grandTotal = $noBestThree[$m]['finalQuotationAmount'];
			   $quotedAmount = ($grandTotal - ($noBestThree[$m]['quotationAmount']*$result->quantity)) + ($noBestThree[$m]['ticketAmount']*$result->quantity);
			  
			   $quotation_detail_list .="<div style=\"width:100%;\">	
				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
				<tr>
				<td align=\"right\" valign=\"top\" width=\"40%\">";
					  if(((float)$noBestThree[$m]['ticketAmount']-(float)$noBestThree[$m]['quotationAmount'])>0){
					  	$quotation_detail_list .="<img src=\"".home_url()."/images/bonus-cash-icon.jpg\" width=\"150\" height=\"150\" alt=\"\" />";
					  }else{
						$quotation_detail_list .="&nbsp;&nbsp;";
					  }
					$quotation_detail_list .="</td>
				<td valign=\"top\" width=\"60%\">
				<table width=\"100%\" border=\"0\" cellspacing=\"6\" cellpadding=\"6\">
				  <tr>					
					<td width=\"82%\" align=\"right\" valign=\"middle\" style=\"color:rgb(255,120,0); font-weight:bold;  font-size:16px\">Quoted Price: $</td>
					<td width=\"18%\" align=\"center\" valign=\"middle\"><span style=\"background-color:rgb(0,0,0); color:rgb(255,255,255); font-weight:bold;  font-size:16px; height:25px; width:100%; padding:6px 9px; display:block; line-height:25px\">".number_format($quotedAmount,2)."</span></td>
				  </tr>";
				  
				  if(((float)$noBestThree[$m]['ticketAmount']-(float)$noBestThree[$m]['quotationAmount'])>0){
				  $quotation_detail_list .="<tr>
					<td align=\"right\" valign=\"middle\" style=\"color:rgb(255,0,0); font-weight:bold;  font-size:16px\">HAGGLEFREE Bonus: $<br /><span style=\"color:rgb(0,0,0); font-size:10px\">(Bonus Expires after ".date("g:i a d/m/Y",strtotime($offerExp)).")</span></td>
					<td align=\"center\" valign=\"middle\"><span style=\"background-color:rgb(0,0,0); color:rgb(255,255,255); font-weight:bold;  font-size:16px; height:25px; width:100%; padding:6px 9px; display:block; line-height:25px\">".number_format(((float)$quotedAmount-(float)$grandTotal),2)."</span></td>
				  </tr>
				  <tr>
					<td align=\"right\" valign=\"middle\" style=\"color:rgb(255,120,0); font-weight:bold;  font-size:16px\">HAGGLEFREE Price: $</td>
					<td align=\"center\" valign=\"middle\"><span style=\"background-color:rgb(0,0,0); color:rgb(255,255,255); font-weight:bold;  font-size:16px; height:25px; width:100%; padding:6px 9px; display:block; line-height:25px\">".number_format($grandTotal,2)."</span></td>					
				  </tr>";
				  }
				  
				$quotation_detail_list .="</table>
				   </td>
				  </tr>
				 </table>
				 </td>
				</div>";
				
				  //================end bomus section =============//
				  
				  $quotation_detail_list .="</div></div>
			 
			 </div>
			</div>";			

			//================== quotation detail list ================================//

			//===============Update quotation status================//

				$update_val = array('status' => 2);

				$where = array('id' => $noBestThree[$m]['quotationId']);	

				$wpdb->update($tableQuotation,$update_val,$where);

				

		    //==========Calculate lead fee and insert in database===============//

			

			$userDiscount = (float)get_user_meta($retaler_info->ID, 'cr_lead_fee', true);

			if($userDiscount > 0){			

			   $calLeadFee = ($rrp*$leadFee)/100;

			   $calLeadFee = ($calLeadFee - (($calLeadFee*$userDiscount)/100));

			}

			else{

			   $calLeadFee = ($rrp*$leadFee)/100;

			}

			//===============Update lead fee================//

				$update_val = array('rrp' => $rrp,'leadFee' => $leadFee,'leadDiscount' => $userDiscount,'totalLeadFee' => $calLeadFee);

				$where = array('id' => $bestThree[$k]['quotationId']);	

				$wpdb->update($tableQuotation,$update_val,$where);			 

			

			if($calLeadFee > 0){

				$leadFeeText = "<div style=\"width:100%;\"><b> $".round($calLeadFee,2)."</b></div>";

			}else{

				$leadFeeText = "<div style=\"width:100%;\"><b>No Lead fee applicable.</b></div>";

			}

			

			//echo " //================ send email to retailer selected quote===========1===================//";

			 	

			$headers[] = 'Content-Type: text/html; charset=UTF-8';

			$headers[] = 'From: '.get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'> ' . "\r\n";			

			$mail_content = stripslashes($my_quotation_selected['email_template']);

			

			//======== Token Replacement =================//

			$mail_content = str_replace("{site_name}",get_bloginfo( 'name' ),$mail_content);

			$mail_content = str_replace("{site_url}",home_url(),$mail_content);

			$mail_content = str_replace("{site_tag}",get_bloginfo( 'description' ),$mail_content);

			$mail_content = str_replace("{media_url}",$media_url,$mail_content);	

			$mail_content = str_replace("{email_heading}",stripslashes($my_quotation_selected['email_heading']),$mail_content);

			

			$mail_content = str_replace("{company_name}",$company_name,$mail_content);

			$mail_content = str_replace("{quotation_amount}",$noBestThree[$m]['finalQuotationAmount'],$mail_content);

			$mail_content = str_replace("{product_details}",$product_details,$mail_content);

			$mail_content = str_replace("{buyer_details}",$buyer_details,$mail_content);

			$mail_content = str_replace("{customer_requirements}",$customer_requirements,$mail_content);

			$mail_content = str_replace("{quotation_details}",$quotation_details,$mail_content);

			$mail_content = str_replace("{lead_fee}",$leadFeeText,$mail_content);	

					

			$mail_content = str_replace("{admin_email}",get_option( 'admin_email' ),$mail_content);

			$mail_content = str_replace("{site_abn}",get_option( 'site_abn' ),$mail_content);

			$mail_content = str_replace("{site_phone}",get_option( 'site_phone' ),$mail_content);

			$mail_content = str_replace("{fb_url}",get_option( 'fb_url' ),$mail_content);

			

			$subject = $my_quotation_selected['subject'];

			$subject = str_replace("{job_id}",$result->id,$subject);

			$subject = str_replace("{sku}",$product->get_sku(),$subject);
			
			$subject = str_replace("{postcode}",$result->Postcode,$subject);
			
			$subject = str_replace("{qty}",$result->quantity,$subject);
			
			$subject = str_replace("{job_type}",($result->is_commercial=='1'?'Commercial':'Private'),$subject);

			//======== Token Replacement =================//

			wp_mail( $retaler_info->user_email, $subject, $mail_content,$headers);

			//echo $mail_content."=====".$subject."=======";


		   }

		 }


		//echo " //================ send email to customer===============best-3===============//";

		 

			$headers[] = 'Content-Type: text/html; charset=UTF-8';

			$headers[] = 'From: '.get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'> ' . "\r\n";

			

			//======== Token Replacement =================//

			$mail_content = str_replace("{site_name}",get_bloginfo( 'name' ),stripslashes($best3_template_detail['email_template']));
			
			$mail_content = str_replace("<img","<img style=\"max-width:100%\" ",$mail_content);

			$mail_content = str_replace("{site_url}",home_url(),$mail_content);

			$mail_content = str_replace("{site_tag}",get_bloginfo( 'description' ),$mail_content);

			$mail_content = str_replace("{media_url}",$media_url,$mail_content);	

			$mail_content = str_replace("{email_heading}",stripslashes($best3_template_detail['email_heading']),$mail_content);

			

			

			$mail_content = str_replace("{buyer_name}",$buyer_name,$mail_content);

			$mail_content = str_replace("{product_details}",$product_details,$mail_content);

			$mail_content = str_replace("{quotation_detail_list}",$quotation_detail_list,$mail_content);
			
			//========= review changes ============
			$mail_content = str_replace("{job_id}",$result->id,$mail_content);
			$mail_content = str_replace("{email}",$result->email,$mail_content);


			$mail_content = str_replace("{admin_email}",get_option( 'admin_email' ),$mail_content);

			$mail_content = str_replace("{site_abn}",get_option( 'site_abn' ),$mail_content);

			$mail_content = str_replace("{site_phone}",get_option( 'site_phone' ),$mail_content);

			$mail_content = str_replace("{fb_url}",get_option( 'fb_url' ),$mail_content);

			

			$subject = $best3_template_detail['subject'];

			$subject = str_replace("{job_id}",$result->id,$subject);

			$subject = str_replace("{sku}",$product->get_sku(),$subject);
			
			$subject = str_replace("{postcode}",$result->Postcode,$subject);
			
			$subject = str_replace("{qty}",$result->quantity,$subject);
			
			$subject = str_replace("{job_type}",($result->is_commercial=='1'?'Commercial':'Private'),$subject);

			//======== Token Replacement =================//

			$mail_content = '<!DOCTYPE html>

							<html lang="en">

								<head>

									<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

									<title>'.$subject.'</title>

								</head>					

								<body>'.$mail_content.'</body>

							</html>';
           
			

			wp_mail( $result->email,$subject, $mail_content,$headers);

		    wp_mail( get_option('admin_monitor_email'), "Notification for best quotation #".$result->id." / ".$product->get_sku(), $mail_content,$headers);

			//wp_mail( "chandan.k@nerdster.com.au", "Notification for best quotation #".$result->id." / ".$product->get_sku(), $mail_content,$headers);			

	      // echo $mail_content;

	      //================ Update job status not require for force closed==============================//   	 

				//$update_val = array('status' => 2);
				//$where = array('id' => $result->id);
				//$wpdb->update($tableJobs,$update_val,$where); 

	   }else{

		//echo "//================ Mail send to customer on no quotation found against a job==============================//   ";

			$headers[] = 'Content-Type: text/html; charset=UTF-8';

			$headers[] = 'From: '.get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'> ' . "\r\n";

			$template_name = "no_quotations_for_buyer";

			$media_url = get_stylesheet_directory_uri()."/img";

			//Codes: {site_url}, {site_tag}, {media_url}, {email_heading}, {site_name}, {buyer_name}, {post_date}, {product_details}, {buyer_details}, {customer_requirements}, {admin_email}, {site_abn}, {site_phone}, {fb_url}

			$template_detail = get_email_template($template_name);

	   		$post_date = "<div style=\"width:100%\"><span style=\"color: rgb(255, 84, 0);\">".date("Y-m-d",strtotime($getJob->jobDate))."</span> <span style=\"color: #787878;\">".date("H:i:s",strtotime($getJob->jobDate))."</span></div>";

			$mail_content = stripslashes($template_detail['email_template']);

			//======== Token Replacement =================//

			$mail_content = str_replace("{site_name}",get_bloginfo( 'name' ),$mail_content);

			$mail_content = str_replace("{site_url}",home_url(),$mail_content);

			$mail_content = str_replace("{site_tag}",get_bloginfo( 'description' ),$mail_content);

			$mail_content = str_replace("{media_url}",$media_url,$mail_content);	

			$mail_content = str_replace("{email_heading}",stripslashes($template_detail['email_heading']),$mail_content);


			$mail_content = str_replace("{buyer_name}",$buyer_name,$mail_content);

			$mail_content = str_replace("{post_date}",$post_date,$mail_content);

			$mail_content = str_replace("{product_details}",$product_details,$mail_content);

			$mail_content = str_replace("{buyer_details}",$buyer_details,$mail_content);

			$mail_content = str_replace("{customer_requirements}",$customer_requirements,$mail_content);

			$mail_content = str_replace("{admin_email}",get_option( 'admin_email' ),$mail_content);

			$mail_content = str_replace("{site_abn}",get_option( 'site_abn' ),$mail_content);

			$mail_content = str_replace("{site_phone}",get_option( 'site_phone' ),$mail_content);

			$mail_content = str_replace("{fb_url}",get_option( 'fb_url' ),$mail_content);

			$subject = $template_detail['subject'];

			$subject = str_replace("{job_id}",$result->id,$subject);

			$subject = str_replace("{sku}",$product->get_sku(),$subject);
			
			$subject = str_replace("{postcode}",$result->Postcode,$subject);
			
			$subject = str_replace("{qty}",$result->quantity,$subject);
			
			$subject = str_replace("{job_type}",($result->is_commercial=='1'?'Commercial':'Private'),$subject);

			//======== Token Replacement =================//

			wp_mail( $result->email, $subject, $mail_content,$headers);	

			wp_mail( get_option('admin_monitor_email'), "Notification for no quotation #".$result->id." / ".$product->get_sku(), $mail_content,$headers);

			//echo $mail_content;


			$update_val = array('status' => 10);

			$where = array('id' => $result->id);	

			$wpdb->update($tableJobs,$update_val,$where);	   

		}

		//===================================================End===================================================
		
	}
}
// Current cheapest quote
function get_current_cheapest($job_id){
	global $wpdb;
	$tablequotation = $wpdb->prefix . 'jobquotation';
	$job = get_job("",$job_id);	
	$cheap_quote =array();
	$fields_query = "SELECT id,quotationAmount FROM $tablequotation WHERE `jobId` = $job_id AND `status` >0" ;	
	$fields_results = $wpdb->get_results($fields_query);
	if(count($fields_results)>0){
	$i=0;
	$minAmount = 0;
	foreach($fields_results as $fields_result){
		$cheap_quote[$i]["id"] = $fields_result->id;
		$cheap_quote[$i]["quotationAmount"] = $fields_result->quotationAmount;	
		$priceMetaData = 0;
		$price_query = "SELECT `meta_value`  FROM `hagg_quotationmeta` WHERE `quote_id` = ".$fields_result->id." AND `meta_key` LIKE 'price_%'";
		$price_results = $wpdb->get_results($price_query);
		foreach($price_results as $price_result){
			$priceMetaData+=$price_result->meta_value*$job->quantity;
		}
		$cheap_quote[$i]["totQuoteAmount"] = round(($fields_result->quotationAmount*$job->quantity)+$priceMetaData);
		
		if($i == 0){
		   $minAmount = $cheap_quote[$i]["totQuoteAmount"];
		}
		else{
			if($minAmount > $cheap_quote[$i]["totQuoteAmount"]){
			  $minAmount = $cheap_quote[$i]["totQuoteAmount"];	
			}
		}
				
		$i++;	
	
	}
	return $minAmount;
	}
	else{	
	return "No quotes submitted";
	}
}

// Job total
function get_job_total($quote_id){
	global $wpdb;
	$tablequotation = $wpdb->prefix . 'jobquotation';
		
	$fields_query = "SELECT jobId,quotationAmount  FROM $tablequotation WHERE `id` = $quote_id" ;
	$job = get_job("",$fields_result->jobId);
	$fields_result = $wpdb->get_row($fields_query);
	$priceMetaData = "";
	$price_query = "SELECT `meta_value`  FROM `hagg_quotationmeta` WHERE `quote_id` = $quote_id AND `meta_key` LIKE 'price_%'";
	$price_results = $wpdb->get_results($price_query);
	foreach($price_results as $price_result){
		$priceMetaData+=$price_result->meta_value*$job->quantity;
	}	
	return round(($fields_result->quotationAmount*$job->quantity)+$priceMetaData,2);
}

//===================== get customer state======================

function getCustomerState($cr_pcode){
	
	switch($cr_pcode){
		// ==== South Australia ================//   
		case $cr_pcode>=5000 && $cr_pcode<=5799:
		case $cr_pcode>=5800 && $cr_pcode<=5999:
		return "SA";
		break;
		// ==== NSW ================//
		case $cr_pcode>=1000 && $cr_pcode<=1999:
		case $cr_pcode>=2000 && $cr_pcode<=2599: 
		case $cr_pcode>=2620 && $cr_pcode<=2899:
		case $cr_pcode>=2921 && $cr_pcode<=2999: 		
		return "NSW";
		break;
		
		// ==== ACT ================//
		case $cr_pcode>=200 && $cr_pcode<=299:
		case $cr_pcode>=2600 && $cr_pcode<=2619:
		case $cr_pcode>=2900 && $cr_pcode<=2920:
		return "ACT";
		break;
		
		// ==== VIC ================//
		
		case $cr_pcode>=3000 && $cr_pcode<=3999:  
		case $cr_pcode>=8000 && $cr_pcode<=8999:		
		return "VIC";
		break;
		
		// ==== Tasmania ================//
		case $cr_pcode>=7000 && $cr_pcode<=7799: 
		case $cr_pcode>=7800 && $cr_pcode<=7999: 
		return "TAS";
		break;
		
		// ===== Queensland  QLD  ====//
		
		case $cr_pcode>=4000 && $cr_pcode<=4999: 
		case $cr_pcode>=9000 && $cr_pcode<=9999:		
		return "QLD";
		break;
		// ===== Western Australia  ====//
		
		case $cr_pcode>=6000 && $cr_pcode<=6797: 
		case $cr_pcode>=6800 && $cr_pcode<=6999: 		
		return "WA";
		break;
		// ===== Northern Territory  ====//
		
		case $cr_pcode>=0800 && $cr_pcode<=0899: 
		case $cr_pcode>=0900 && $cr_pcode<=0999: 
		return "NT";
		break;
		
		default:
		return "";
		break;
	}
}