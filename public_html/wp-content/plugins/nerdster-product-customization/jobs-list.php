<?php 
/*-Needs to search on date, name, email and request number

-show date, time, job number, name, postcode, 

# of merchants invited, # of quotes, lowest price (product only), 

Status (this can be time left, closed, purchased, deleted etc) option to close the job here

-once you click on a job then the request details along will all invites, all quotes received and flag which quotes were successful, 

show lead fee, quote numbers, request numbers etc etc this is the only place we can use for support

*/
function jobs_list_on_dashboard(){

	global $wpdb, $pagenum, $limit, $start;

		if(trim(get_option('job_limit'))){

			$limit = get_option('job_limit');

		}else{

	 		$limit = 10; // number of jobs in page

		}

	$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;

	$start =  ($pagenum - 1) * $limit;

?>

<div class="wrap">

<div id="icon-users" class="icon32"><br/></div>

<h2>Job List</h2>

<?php

$startN    = new DateTime($wpdb->get_var('SELECT MIN( jobDate ) as minJobDate  FROM  `'. $wpdb->prefix .'jobs`'));

$endN = new DateTime( $wpdb->get_var('SELECT MAX( jobDate ) as maxJobDate  FROM  `'. $wpdb->prefix .'jobs`'));

$periodN = new DatePeriod($startN->modify('first day of this month'), DateInterval::createFromDateString('1 month'),$endN->modify('last day of this month'));

?>

<form action="<?php echo admin_url('admin.php?page=jobs_on_dashboard'); ?>" id="searchjobsfrm" name="searchjobsfrm" method="get">

<input type="hidden" name="page" value="jobs_on_dashboard" />

<p class="search-box">

	

	<input type="text" step="width:250px" placeholder="Name, Email or Request Number" value="<?php echo $_REQUEST['searchjobs']; ?>" name="searchjobs" id="searchjobs-input">

	<select name="jobdaterange" id="jobdaterange">

    	<option value="">Select date</option>

        <?php foreach ($periodN as $dtN) { ?>

        <option value="<?php echo $dtN->format("Y-m");?>" <?php if($_REQUEST['jobdaterange'] == $dtN->format("Y-m")){?> selected="selected" <?php } ?>><?php echo $dtN->format("M-Y");?></option>

        <?php } ?>

    </select>

    <input type="submit" value="Search Jobs" class="button" id="search-submit">

    

</p>

</form>

<table class="widefat" width="100%" border="0" cellspacing="5" cellpadding="5">

	<thead>

      <tr>

      	<td><strong>Job Id</strong></th>

        <td><strong>Product</strong></th>

        <th><strong>Customer</strong></th>

        <th><strong>Applied On</strong></th>

        <th><strong>Postcode</strong></th>

        <th><strong># of merchants invited</strong></th>

        <th><strong># of quotes</strong></th>

        <th><strong>Lowest Price</strong></th>

        <th><strong>Status</strong></th>

        <th><strong>Action</strong></th>

      </tr>

  </thead>

  <tfoot>

      <tr>

        <td><strong>Job Id</strong></th>

        <td><strong>Product</strong></th>

        <th><strong>Customer</strong></th>

        <th><strong>Applied On</strong></th>

        <th><strong>Postcode</strong></th>

        <th><strong># of merchants invited</strong></th>

        <th><strong># of quotes</strong></th>

        <th><strong>Lowest Price</strong></th>

        <th><strong>Status</strong></th>

        <th><strong>Action</strong></th>

      </tr>

  </tfoot>

  <tbody>

	 <?php

	 	$filterString = $_REQUEST['searchjobs'];

		$datefilterString = $_REQUEST['jobdaterange'];

		$offset = ( $pagenum - 1 ) * $limit;

		$jobs = get_jobs('','','','',$filterString,$datefilterString);

		$total = $jobs['totalData'];

		$num_of_pages = ceil( $total / $limit );

		

		$jobsData = $jobs['results'];

		

        // Array of WP_User objects.

        foreach ( $jobsData as $job ) {

            //////////////////////

        $product = wc_get_product( $job->productId );

            

        $user_info = get_userdata($job->userId);

        $first_name = $user_info->first_name;

        $last_name = $user_info->last_name; 

		$noOfInvites = $wpdb->get_var("SELECT  COUNT(*)  FROM  `". $wpdb->prefix ."jobquotation` where jobid='".$job->id."' ");

		$noOfQuotes = $wpdb->get_var("SELECT  COUNT(*)  FROM  `". $wpdb->prefix ."jobquotation` where jobid='".$job->id."' and quotationAmount >0 ");

		//$minQuotesAmount = $wpdb->get_var("SELECT  MIN(quotationAmount)  FROM  `". $wpdb->prefix ."jobquotation` where jobid='".$job->id."' ");

		$minQuotesAmount = get_current_cheapest($job->id);
		$min_quote_mssg = ($minQuotesAmount=="No quotes submitted"?$minQuotesAmount:"$$minQuotesAmount</span>");
		
        $status = "";

        switch($job->status){

            case 1:

            $status = "<span style=\"color:#ff7800\">New Job</span>";

            break;

            case 2:

            $status = "<span style=\"color:green\">Retailer Assigned & Closed</span>";

            break;

            case 3:

            $status = "<span style=\"color:red\">Closed Forcefully</span>";

			break;

			case 10:

            $status = "Null";

            break;

        }
            ?>

			<tr>

            <td><?= $job->id; ?></td>

			<td><span><a href="<?php echo admin_url('admin.php?page=jobs_details_on_dashboard&id='.$job->id); ?>"><?php echo esc_html( $product->get_title() ) ; ?></a></span></td>

			<td nowrap><span><?php echo esc_html( $first_name." ".$last_name ) ; ?></span></td>

			<td nowrap><span><?php echo date("F j, Y, g:i a",strtotime($job->jobDate)) ; ?></span></td>

			<td><?php echo $job->Postcode; ?></td>

			<td><?php echo $noOfInvites; ?></td>

            <td><?php echo $noOfQuotes; ?></td>

            <td><?php echo $min_quote_mssg; ?></td>

			<td><?php echo $status; ?></td>

            <td><?= ($job->status == "1"?"<a href=\"".admin_url('admin.php?page=jobs_on_dashboard&id='.$job->id)."\">Close</a>":"")?></td>
			</tr>
        <?php
        }

		$page_links = paginate_links( array(

			'base' => add_query_arg( 'pagenum', '%#%' ),

			'format' => '',

			'prev_text' => __( '&laquo;', 'text-domain' ),

			'next_text' => __( '&raquo;', 'text-domain' ),

			'total' => $num_of_pages,

			'current' => $pagenum

		) );

		if ( $page_links ) {
			echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $page_links . '</div></div>';
		}
    ?>
   </tbody>
</table>

 </div>

<?php

	if (isset($_GET['id'])) {

		$cj = close_job_forcefully($_GET['id']);

		$url = home_url()."/wp-admin/admin.php?page=jobs_on_dashboard";

		if($cj){						

			echo "<div class=\"admin-mssg\" style=\"color:green\">Job : ".$_GET['id']." closed forcefully</div>";

			echo "<script>window.location.href='".$url."'</script>";

		}

	}	

}



function close_job_forcefully($job_id){

	global $wpdb;

	

	$close_job = $wpdb->update( 

		$wpdb->prefix .'jobs', 

		array( 

			'status' => '3'	// string			 

		), 

		array( 'id' => $job_id ), 

		array( 			

			'%d'	// value2

		), 

		array( '%d' ) 

	);

	force_close_job($job_id);

	return $close_job;

}



function jobs_details_on_dashboard(){

	global $wpdb;

?>



<h2>Job Details</h2>

<hr/>

<?php

$id= $_GET['id'];

$job = get_job("",$id);

$sqlQuotation = "SELECT * FROM  `". $wpdb->prefix ."jobquotation` where jobId='".$id."' ";

$resultsQuotation = $wpdb->get_results($sqlQuotation);

//print_r($job);

$product = wc_get_product($job->productId);

//echo $p->get_title();

//echo $job->productId;

//$jobs = $jobs['results'];

//print_r($jobs);

//foreach ( $jobs as $job ) {

            //////////////////////

        //$product = wc_get_product( $job->productId );

            

        $user_info = get_userdata($job->userId);

		if($job->retailerId !='0'){

			$retailer_info = get_userdata($job->retailerId);

		}

        $first_name = $user_info->first_name;

        $last_name = $user_info->last_name; 

		$address = $job->Postcode;

        $status = "";

        switch($job->status){

            case 1:

            $status = "New Job";

            break;

            case 2:

            $status = "Retailer Assigned";

            break;

            case 3:

            $status = "Accepted Quote";

            break;

        }

		?>

        <div class="admin-job-detail">

        	<fieldset class="job-fieldset">

    			<legend>Job detail</legend>	

                <span><strong>Job Title</strong> : </span> <?=esc_html( $product->get_title() )?><br />

                <span><strong>Posted Date</strong> : </span> <?=$job->jobDate?><br />

<!--              <span><strong>Retailer</strong> : </span> <?=($job->retailerId=='0'?'No retailer assigned yet':$retailer_info->first_name)?><br />

               <span><strong>Quotation Accept Date</strong> : </span> <?=($job->qacceptDate=='0000-00-00 00:00:00'?'No quotation yet':date("l, F j, Y",strtotime($job->qacceptDate)))?><br />

                <span><strong>Quotation Amount</strong> : </span> <?=($job->quotationAmount=='0'?'No quotation yet':$job->quotationAmount)?><br />

-->            </fieldset>

            <fieldset class="job-fieldset">

            	<?php

                	///get dynamic attributes ////

					$das = get_dynamic_job_attributes($id);

					/*echo "<pre>";

					print_r($das);

					echo "</pre>";*/

					$additional_information ="<legend>Job additional information</legend>" ;

									

					if(count($das)>0){									

						foreach($das['key'] as $key => $value){

							if($value == "wccpf_when_do_you_need_the_product")

								$additional_information .="<span><strong>".$das['label'][$value]." : </strong> </span>".product_delivery_switch($das['customer_choice'][$value])."<br />";

							else

							$additional_information .="<span><strong>".$das['label'][$value]." : </strong> </span>".ucfirst($das['customer_choice'][$value])."<br />";

						}

					}

					

					$additional_information .="<span><strong>Usage Type : </strong></span>  ".strtoupper(($job->is_commercial=='1'?'Commercial':'Private'))."<br />";

					if($is_extended_warranty!=''){

						 $additional_information.="<span><strong>Extended Warranty :</strong> </span>  $is_extended_warranty<br />";

					 }

					

					

					echo $additional_information;	

					

					

					//////////dynamic attributes ends////////////////

				?>

    			

                <!--<span><strong>Is Commercial ? </strong> : </span> <?php //($job->is_commercial=='1'?'Yes':'No')?><br />-->

                <?php if($job->is_commercial==1){?>

				<span><strong>Buisness Name </strong> : </span> <?=$job->businessName?><br />

                <span><strong>ABN no </strong> : </span> <?=$job->abn_no?><br />

				<?php }?>

                <span><strong>Quantity </strong> : </span> <?=$job->quantity?><br />  

           </fieldset>

           <fieldset class="job-fieldset">

    			<legend>Address</legend>

                <span><strong>Buyer Name</strong> : </span> <?=$first_name.' '.$last_name?><br />

                <span><strong>Email</strong> : </span> <?=$job->email?><br />

                <span><strong>Postcode</strong> : </span> <?=$address?><br />

                <span><strong>Telephone</strong> : </span> <?=$job->telephone?><br />

           </fieldset>

           <?php
				//////////// quotation details starts here /////////
				
				//========== Bonus exp date pickup=====
              
				  $offerExp = $job->offerExp;	   
			   
			   //=============== End ====================
		   		

				$i = 0;

				$quotation_detail_list = "";

				$das = get_dynamic_job_attributes($id);

				//echo "<pre>";print_r($das);echo "</pre>";				

           		foreach ( $resultsQuotation as $quotation ) {

					//echo "<pre>";print_r($quotation);echo "</pre>";

					if($quotation->status=="0")

						continue;

					$da_quote = get_dynamic_quote_attributes($quotation->id);

					 if(count($da_quote) > 0){

						foreach($da_quote as $key => $value){

							$$key = $value;

						}

					}

					$bestThree[$i]['quotationId'] = $quotation->id;
					$bestThree[$i]['retailerId'] = $quotation->retailerId; 
					$bestThree[$i]['contact_person'] =  $quotation->contact_person;
					$bestThree[$i]['contact_details'] =  $quotation->contact_details;
					$bestThree[$i]['quotationAmount'] =  $quotation->quotationAmount;					
					$bestThree[$i]['ticketAmount'] =  $quotation->ticketAmount;
					$bestThree[$i]['quotationDate'] =  $quotation->quotationDate;
					$bestThree[$i]['comments'] 		=  $quotation->comments;
					$bestThree[$i]['product_condition'] 		=  $quotation->product_condition;
					$bestThree[$i]['product_warranty'] =  $quotation->product_warranty;//comments on product condition
					$bestThree[$i]['product_comment'] 		=  $quotation->product_comment;					
					$bestThree[$i]['status'] 		=  $quotation->status;					
					$bestThree[$i]['autoBidStatus'] 		=  $quotation->autoBidStatus;
					$finalQuotationAmount = $quotation->quotationAmount*$job->quantity;

					if(count($das['key']) > 0){

						$priceMetaDataArray = array();

						foreach($das['key'] as $key => $val){

							$price_variable = "price_".$val;

							$meet_variable = "meet_".$val;

							$comment_variable = "comment_".$val;					

							$bestThree[$i][$price_variable] =  $$price_variable;

							$bestThree[$i][$meet_variable] =  $$meet_variable;

							$bestThree[$i][$comment_variable] =  $$comment_variable;

							$finalQuotationAmount += $$price_variable*$job->quantity;

						}

					}

						$bestThree[$i]['warranty_price'] =  $quotation->warranty_price*$job->quantity;

						if($quotationResult->warranty_price)
						$finalQuotationAmount += $quotation->warranty_price*$job->quantity;
						
						$bestThree[$i]['finalQuotationAmount'] = $finalQuotationAmount;
						$bestThree[$i]['terms_and_conditions'] =  $quotation->terms_and_conditions;

				

					$i++;	

				}
					//=======
				//echo "<pre>";print_r($bestThree);echo "</pre>";

					for($k=0;$k < count($bestThree);$k++){
					$company_name = get_user_meta($bestThree[$k]['retailerId'],'cr_company',true);
					$retaler_info = get_userdata($bestThree[$k]['retailerId']);
					$retalerDisplayName = $retaler_info->display_name;

					if($company_name == ""){

						if($retalerDisplayName!="")

						$company_name = ucfirst($retalerDisplayName);

						else

						$company_name = ucfirst($retaler_info->first_name);

					 }

					 	$product_condition = product_condition($bestThree[$k]['product_condition']);

					 	$isgst = (($job->is_commercial=='1'&&(strtotime($job->jobDate)<strtotime('2016-09-15 23:59:59')))?'(Ex GST)':'(Inc GST)');

						$quotation_detail_list .= "

						<div style=\"width:100%; margin:0 auto; padding:0 0 5px; background-color: rgb(255, 255, 255); border-radius:0 0 4px 4px\">

            

						  <div style=\"width:100%; overflow:hidden; padding:9px 0; margin:0 auto; background-color: rgb(0, 0, 0); border-radius:4px 4px 0 0; text-align:left; color:#fff; font-weight:normal; font-size:14px; color:#ff5400; font-weight:bold; text-transform:uppercase\">

							<div style=\"width:95%; margin:0 auto;\">Quotation No :<span style=\"border-radius: 50%; background-color: rgb(255, 84, 0); color: rgb(255, 255, 255); float: right; height: 24px; width: 24px; text-align: center; font-weight: bold; line-height: 24px;\">".($k+1)."</span></div>

						  </div>  

						  <!-- retailer info starts -->             

							  <div style=\"width:100%; float:left; padding:0; margin:0 auto; text-align:center\">";

							  if(get_user_meta( $bestThree[$k]['retailerId'], 'cr_photo', true )){

									$quotation_detail_list .="<div style=\"float:left; width:40%; padding:10px 5px; margin:0; min-width:200px\">					

										<img src=\"".get_user_meta( $bestThree[$k]['retailerId'], 'cr_photo', true )."\" alt=\"\" style=\"float:none; margin:0 auto; display:inline-block; text-align:center\">

									</div>";

							  }
							  
								
								$quotation_detail_list .="<div style=\"width:".(get_user_meta( $bestThree[$k]['retailerId'], 'cr_photo', true )!=""?"55%":"100%")."; overflow:hidden; padding:10px 5px; margin:0 auto 15px; text-align:left; color:#000; font-weight:normal; font-size:14px; float:left\">

									<div style=\"width:100%;\">Retailer Name: <span style=\"color: #ff5400;\">".$company_name."</span></div>

									<div style=\"width:100%;\">Contact Name: <span style=\"color: #ff5400;\"> ".$bestThree[$k]['contact_person']."</span></div>

									<div style=\"width:100%;\">Contact Details: <span style=\"color: #ff5400;\">".$bestThree[$k]['contact_details']."</span></div>

									".($bestThree[$k]['status']==-1?"<div style=\"padding-top:0; padding-bottom:0; line-height:18px; margin-top:0; margin-bottom:0; width:100%\"><span style=\"color: #ff5400;\">Rejected</span></div>":"")."

								  </div>

							  </div>

							  <!-- retailer info ends --> 

						  

						  <div style=\"width:95%; margin:0 auto 5px; padding:4px 5px; border:1px solid #dedede; overflow:hidden\">

						  <!-- quotation amount starts -->

							<div style=\"width:100%; float:left; margin:0; padding:0\">

							  <div style=\"width:96%; float:left; margin:0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; \">

								<div style=\"width:100%\">Quotation Amount $isgst:<span style=\"color: #ff5400; padding:0 0 0 3px\">$".$bestThree[$k]['ticketAmount']." X ".$job->quantity." = $".((float)$bestThree[$k]['ticketAmount']*$job->quantity)."</span></div>

							  </div>";

							  

			if($bestThree[$k]['comments']!="")							  

				$quotation_detail_list .= "<div style=\"width:96%; float:left; margin:0; padding:6px 2%; float:left; font-size:13px; color:#7e7e7e; line-height:18px\">

                    <div style=\"width:100%;\"><span style=\"color: #ff5400; text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments on Quoted Price:</span> <span style=\"width:100%; float:left\">".$bestThree[$k]['comments']."</span></div>

                  </div>";

				

				$quotation_detail_list .= "</div><!-- quotation amount ends -->";

				

				$quotation_detail_list .="<!-- product condition ends -->

				<div style=\"width:100%; float:left; margin:0\">

                  <div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238)\">

                    <div style=\"width:100%\">Product Condition:<span style=\"color: #ff5400; padding:0 0 0 3px\">".$product_condition."</span></div>

                  </div>                  

                ";				



			if($bestThree[$k]['product_warranty']!="")

				$quotation_detail_list .= "<div style=\"width:96%; float:left; margin:0; padding:6px 2%; float:left; font-size:13px; color:#7e7e7e; line-height:18px\">

                    <div style=\"width:100%;\"><span style=\"color: #ff5400; text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments on Product Condition:</span> <span style=\"width:100%; float:left\">".$bestThree[$k]['product_warranty']."</span></div>

                  </div>";

				

				$quotation_detail_list .= "</div><!-- product condition ends -->";

				

						//// get dynamic data  starts ///////

		$da_quote = get_dynamic_quote_attributes($bestThree[$k]['quotationId']);

		 if(count($da_quote) > 0){

			foreach($da_quote as $key => $value){

				$$key = $value;

			}

		}				

				

				if(count($das['key']) > 0){

					$dynamic_quote ="";

					$quotation_detail_list.="{timeframe}";

					foreach($das['key'] as $key => $val){

						$price_variable = "price_".$val;

						$meet_variable = "meet_".$val;

						$comment_variable = "comment_".$val;

						if($val == "wccpf_when_do_you_need_the_product" || $comment_variable == "comment_wccpf_when_do_you_need_the_product"){

							if($val == "wccpf_when_do_you_need_the_product")

								$when_do_you_need_the_product = "

								<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; text-indec:1%\">

                    <div style=\"width:100%;\">Timeframe: <span style=\"color: #ff5400; padding:0 0 0 3px\">".product_delivery_switch($das['customer_choice'][$val])."</span> </div>

                  </div>

							  ";

							  if($bestThree[$k][$meet_variable]!=""){

									$when_do_you_need_the_product .="

									<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; \">

										<div style=\"width:100%\">Can you meet ".product_delivery_switch($das['customer_choice'][$val])." requirements? : <span style=\"color: #ff5400; padding:0 0 0 3px\">".ucfirst($bestThree[$k][$meet_variable])."</span> </div>

									</div>

									";

									$dynamic_quote .= "<div style=\"width:100%;\">Can you meet ".product_delivery_switch($das['customer_choice'][$val])." requirements? <span style=\"color: #ff5400;\">".ucfirst($bestThree[$k][$meet_variable])."</span></div>";

								}

							 if($bestThree[$k][$comment_variable]!=""){

								$when_do_you_need_the_product .="

								<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; text-indec:1%\">

                    <div style=\"width:100%;\">Comments: <span style=\"color: #ff5400; padding:0 0 0 3px\">".$bestThree[$k][$comment_variable]."</span> </div>

                  </div>

								";								

							 }

							 $dynamic_quote .= "<div style=\"width:100%;\">Timeframe Comment  <span style=\"color: #ff5400;\">".$$comment_variable."</span></div>";

							}

							

						elseif($val == "wccpf_deliverypickup" || $comment_variable == "comment_wccpf_deliverypickup"){

							$when_do_you_need_the_product2 = "";

						if($val == "wccpf_deliverypickup"){	    

								//$when_do_you_need_the_product2 ="<li style=\"width:98%; float:left; margin:5px 0 0 0; padding:6px 1%; float:left; list-style:none; background-color:#eee\">".ucfirst($das['customer_choice'][$val])." :<span style=\"color: #ff5400; padding:0 0 0 3px\">Yes</span></li>";

								$when_do_you_need_the_product2 .="

								<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; \">

                    <div style=\"width:100%\">".$das['label'][$val]." : <span style=\"color: #ff5400; padding:0 0 0 3px\">".ucfirst($das['customer_choice'][$val])."</span> </div>

                  </div>";



								$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." <span style=\"color: #ff5400;\">".ucfirst($das['customer_choice'][$val])."</span></div>";									

								

								

						}

						if($bestThree[$k][$meet_variable]!=""){

							$when_do_you_need_the_product2 .="

							<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; \">

								<div style=\"width:100%\">Can you meet ".$das['label'][$val]." requirements? : <span style=\"color: #ff5400; padding:0 0 0 3px\">".ucfirst($bestThree[$k][$meet_variable])."</span> </div>

							</div>

							";

							$dynamic_quote .= "<div style=\"width:100%;\">Can you meet ".$das['label'][$val]." requirements? <span style=\"color: #ff5400;\">".ucfirst($bestThree[$k][$meet_variable])."</span></div>";

						}

						$when_do_you_need_the_product2 .="

						<div style=\"width:96%; font-size:13px; color:#7e7e7e; margin:0; padding:6px 2%; float:left; background-color: rgb(255, 255, 255)\">";

						

							if($bestThree[$k][$price_variable]!=""){				

								$when_do_you_need_the_product2 .="

								<div style=\"width:100%; float:left; margin:3px 0\">".$das['label'][$val]." Price $isgst: <span style=\"color:#000; padding-left:3px; color:#ff5400\">$".$bestThree[$k][$price_variable]."</span> </div>

								";

								$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." Price <span style=\"color: #ff5400;\">$".$$price_variable." per item</span></div>";

							}

							if($bestThree[$k][$comment_variable]!=""){

								$when_do_you_need_the_product2 .="

								<div style=\"width:100%\"> <span style=\"color: #ff5400; text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments:</span> <span style=\"width:100%; float:left\">".$bestThree[$k][$comment_variable]."</span> </div>

								";

								$dynamic_quote .= "<div style=\"width:100%;\"> Commnets on ".$das['label'][$val]." <span style=\"color: #ff5400;\">".$bestThree[$k][$comment_variable]."</span></div>";

							}

							

						$when_do_you_need_the_product2 .="</div>";

								

						}

						elseif($val == "wccpf_removal_of_old_appliance"){

							$when_do_you_need_the_product3 = "";

							if($das['customer_choice'][$val] == "no")//job requirements is no, no need to show it in form and quote template

								continue;

							else{

								if($bestThree[$k][$meet_variable]!=""){

									$when_do_you_need_the_product3 .="

									<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; \">

										<div style=\"width:100%\">".$das['label'][$val]." : <span style=\"color: #ff5400; padding:0 0 0 3px\">".ucfirst($bestThree[$k][$meet_variable])."</span> </div>

									</div>

									";

									$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." <span style=\"color: #ff5400;\">".ucfirst($bestThree[$k][$meet_variable])."</span></div>";

								}

								$when_do_you_need_the_product3 .="

									<div style=\"width:96%; font-size:13px; color:#7e7e7e; margin:0; padding:6px 2%; float:left; background-color: rgb(255, 255, 255)\">";

										

										if($bestThree[$k][$price_variable]!=""){				

											$when_do_you_need_the_product3 .="

											<div style=\"width:100%; float:left; margin:3px 0\">".$das['label'][$val]." Price $isgst: <span style=\"color:#000; padding-left:3px; color:#ff5400\">$".$bestThree[$k][$price_variable]."</span> </div>

											";

											$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." Price <span style=\"color: #ff5400;\">$".$$price_variable." per item</span></div>";

										}

										if($bestThree[$k][$comment_variable]!=""){

											$when_do_you_need_the_product3 .="

											<div style=\"width:100%\"> <span style=\"color: #ff5400; text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments:</span> <span style=\"width:100%; float:left\">".$bestThree[$k][$comment_variable]."</span> </div>

											";

											$dynamic_quote .= "<div style=\"width:100%;\">Comments on ".$das['label'][$val]." <span style=\"color: #ff5400;\">".$bestThree[$k][$comment_variable]."</span></div>";

										}

										

									$when_do_you_need_the_product3 .="</div>";

							}

						}

						else{

							$when_do_you_need_the_product .= "

								<div style=\"width:100%; float:left; margin:4px 0 0 0; padding:6px 2%; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; text-indec:1%\">

                    <div style=\"width:100%;\">".$das['label'][$val].": <span style=\"color: #ff5400; padding:0 0 0 3px\">".$das['customer_choice'][$val]."</span> </div>

                  </div>

							  ";	

						if($bestThree[$k][$meet_variable]!=""){

								$quotation_detail_list .="<!-- dynamic meet requirement starts --><div style=\"width:100%; float:left; margin:0; padding:0\">";

								if($val =="wccpf_when_do_you_need_the_product"){

									$dynamic_quote .= "<div style=\"width:100%;\">Can you meet ".product_delivery_switch($das['customer_choice'][$val])." Timeframe? <span style=\"color: #ff5400;\">".ucfirst($$meet_variable)."</span></div>";

									if($$comment_variable != "")

									$dynamic_quote .= "<div style=\"width:100%;\">Timeframe Comment  <span style=\"color: #ff5400;\">".$$comment_variable."</span></div>";

									continue;

								}else{

									if($val != "wccpf_deliverypickup"){

									  $dynamic_quote .= "<div style=\"width:100%;\">Can you meet ".$das['label'][$val]."? <span style=\"color: #ff5400;\">".ucfirst($$meet_variable)."</span></div>";

								    }

							        

									$quotation_detail_list .="

									<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; background-color: rgb(238, 238, 238)\">

                    					<div style=\"width:100%\"> 

											<span style=\"float:left\">".$das['label'][$val]." :</span> 

											<span style=\"color: #ff5400; padding:0 0 0 3px\">".ucfirst($das['customer_choice'][$val])."</span> 

										</div>

									</div>

									";

								}

							$quotation_detail_list .= "<!-- requirements div starts --><div style=\"width:96%; float:left; margin:0; padding:6px 2%; font-size:13px; color:#7e7e7e; line-height:18px\">";

							$quotation_detail_list .=" 

							<div style=\"width:100%; float:left; margin:3px 0\"> <span style=\"float:left\">Can you meet the customers requirement?:</span> <span style=\"color:#000; padding-left:3px\">".ucfirst($bestThree[$k][$meet_variable])."</span> </div>								

								";

								

								

							if($bestThree[$k][$price_variable]!=""){				

								$quotation_detail_list .="

								<div style=\"width:100%; float:left; margin:3px 0\"> <span style=\"float:left\">".$das['label'][$val]." Price $isgst:</span> <span style=\"color:#000; padding-left:3px; color:#ff5400\">$".$bestThree[$k][$price_variable]."</span> </div>

								";							

								$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." Price <span style=\"color: #ff5400;\">$".$$price_variable." per item</span></div>";

							}

							if($bestThree[$k][$comment_variable]!=""){

								$quotation_detail_list .="

								<div style=\"width:100%\"> <span style=\"color: #ff5400; text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments:</span> <span style=\"width:100%; float:left\">".$bestThree[$k][$comment_variable]."</span> </div>

								";

								$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." Comment  <span style=\"color: #ff5400;\">".$$comment_variable."</span></div>";

							}

							

							$quotation_detail_list .="</div><!-- requirements div ends -->";

								

							$quotation_detail_list .="</div><!-- dynamic meet requirement ends -->";		

						}

						}

						

					}

					

					$when_do_you_need_the_product = "<div style=\"width:100%; float:left; margin:0\">".$when_do_you_need_the_product.$when_do_you_need_the_product2.$when_do_you_need_the_product3."</div>";

					$quotation_detail_list = str_replace("{timeframe}",$when_do_you_need_the_product,$quotation_detail_list);

				}

				

				$quotation_details .= $dynamic_quote;			

				$quotation_details .="<div style=\"width:100%;\">Contact Person: <span style=\"color: #ff5400;\">".$bestThree[$k]['contact_person']."</span></div>

				<div style=\"width:100%;\">Contact Details: <span style=\"color: #ff5400;\">".$bestThree[$k]['contact_details']."</span></div>						

				<div style=\"width:100%;\">Product Condition: <span style=\"color: #ff5400;\">".product_condition($bestThree[$k]['product_condition'])."</span></div>";

				if($bestThree[$k]['product_warranty']!="")

					$quotation_details .="<div style=\"width:100%;\">Comments on Product Condition: <span style=\"color: #ff5400;\">".$bestThree[$k]['product_warranty']."</span></div>";

				$quotation_details .="<div style=\"width:100%;\">Grand Quotation Total: <span style=\"color: #ff5400;\">$".$bestThree[$k]['finalQuotationAmount']."</span></div>";

				$quotation_details .="</li></ul>";

				

				

			//// get dynamic data  ends ///////

						if($bestThree[$k]['warranty_price']!="")				

				$quotation_detail_list .="<div style=\"width:100%; float:left; margin:3px 0\">Warranty Price:<span style=\"color:#000; padding-left:3px; color:#ff5400\">$".$bestThree[$k]['warranty_price']."</span></div>";

				

					

			if($bestThree[$k]['terms_and_conditions']!="")

			$quotation_detail_list .= "<div style=\"width:100%; float:left; margin:0; padding:0; border-top:1px solid #dedede\">

                  <div style=\"width:96%; float:left; margin:0; padding:6px 2%; float:left; font-size:13px; color:#7e7e7e; line-height:18px\">

                    <div style=\"width:100%\"> <span style=\"color: #000; text-transform:uppercase; font-size:11px; width:100%; float:left\">Offer Comments:</span> <span style=\"width:100%; float:left\">".$bestThree[$k]['terms_and_conditions']."</span> </div>

                  </div>

                </div><!-- offer comments -->";

							

							

			$quotation_detail_list .="

				<div style=\"width:100%; float:left; margin:0; padding:0; border-top:1px solid #dedede\">

                  <div style=\"width:96%; float:left; margin:9px 0 0; padding:6px 2%; float:left; font-size:21px; color:#ff5400; line-height:21px; font-weight:bold\"> ";
				  
				  	 //=============bonus section=============//					 
					 
					$grandTotal = (float)$bestThree[$k]['finalQuotationAmount'];
					$ticketTotal = (((float)$grandTotal - ((float)$bestThree[$k]['quotationAmount']*$job->quantity)) + ((float)$bestThree[$k]['ticketAmount']*$job->quantity)) ;
					//echo "<br/>---".(float)$bestThree[$k]['ticketAmount']-(float)$bestThree[$k]['quotationAmount'];		   
				   $quotation_detail_list .="<div style=\"width:100%;\">		
					<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
					<tr>
					<td align=\"left\" valign=\"top\"><table width=\"100%\" border=\"0\" cellspacing=\"6\" cellpadding=\"6\">
					  <tr>
						<td width=\"\" rowspan=\"3\" align=\"right\" valign=\"middle\" >";
						  if(((float)$bestThree[$k]['ticketAmount']-(float)$bestThree[$k]['quotationAmount'])>0){
							$quotation_detail_list .="<img src=\"".home_url()."/images/bonus-cash-icon.jpg\" width=\"150\" height=\"150\" alt=\"\" />";
						  }else{
							$quotation_detail_list .="&nbsp;&nbsp;";
						  }
						$quotation_detail_list .="</td>
						<td width=\"40%\" align=\"right\" valign=\"middle\" style=\"color:rgb(255,120,0); font-weight:bold; font-family:Verdana, Geneva, sans-serif; font-size:16px; white-space:nowrap\">Quoted Price: $</td>
						<td width=\"18%\" align=\"center\" valign=\"middle\"><span style=\"background-color:rgb(0,0,0); color:rgb(255,255,255); font-weight:bold; font-family:Verdana, Geneva, sans-serif; font-size:16px; height:25px; width:100%; padding:6px 9px; display:block; line-height:25px\">".number_format($ticketTotal,2)."</span></td>
					  </tr>";
					  
					  if(((float)$bestThree[$k]['ticketAmount']-(float)$bestThree[$k]['quotationAmount'])>0){
					  $quotation_detail_list .="<tr>
						<td align=\"right\" valign=\"middle\" style=\"color:rgb(255,0,0); font-weight:bold; font-family:Verdana, Geneva, sans-serif; font-size:16px\">HAGGLEFREE Bonus: $<br /><span style=\"color:rgb(0,0,0); font-size:10px\">(Bonus Expires after ".date("g:i a d/m/Y",strtotime($offerExp)).")</span></td>
						<td align=\"center\" valign=\"middle\"><span style=\"background-color:rgb(0,0,0); color:rgb(255,255,255); font-weight:bold; font-family:Verdana, Geneva, sans-serif; font-size:16px; height:25px; width:100%; padding:6px 9px; display:block; line-height:25px\">".number_format((float)$ticketTotal-(float)$grandTotal,2)."</span></td>
					  </tr>
					  <tr>
						<td align=\"right\" valign=\"middle\" style=\"color:rgb(255,120,0); font-weight:bold; font-family:Verdana, Geneva, sans-serif; font-size:16px\">HAGGLEFREE Price: $</td>
						<td align=\"center\" valign=\"middle\"><span style=\"background-color:rgb(0,0,0); color:rgb(255,255,255); font-weight:bold; font-family:Verdana, Geneva, sans-serif; font-size:16px; height:25px; width:100%; padding:6px 9px; display:block; line-height:25px\">".number_format($grandTotal,2)."</span></td>					
					  </tr>";
					  }
					  
					$quotation_detail_list .="</table></td>
					</tr>
					</table>
					</div>";
					
					  //================end bomus section =============//
				  
				  
				  $quotation_detail_list .="
				  
				  </div>
                </div><!-- grand total end -->

			 

			 </div>
			</div>";		

							

					}

		   ?>

           <fieldset class="job-fieldset">

            	<legend>Quotation Details </legend>                

             	<?=$quotation_detail_list?>                        

           </fieldset>

           

        </div>

        

        <div class="wrap">

		<h2>Quotation List</h2>

        <table class="widefat" width="100%" border="0" cellspacing="5" cellpadding="5">

            <thead>

              <tr>

                <td><strong>Job Id</strong></th>

                <th><strong>Retailer</strong></th>

                <th><strong>Quotation Amount</strong></th>

                <th><strong>Comments</strong></th>

                <th><strong>Product warranty</strong></th>

                <th><strong>Quotation Date</strong></th>

                <th><strong>Assigned Date</strong></th>

                <th><strong>Status</strong></th>

                <th><strong>Action</strong></th>

              </tr>

          </thead>

          

          <tbody>



	 <?php

	 	

        foreach ( $resultsQuotation as $quotation ) {

			

			$sqlRetailer = "SELECT * FROM  `". $wpdb->prefix ."users` where ID='".$quotation->retailerId."' ";

			$resultsRetailer = $wpdb->get_results($sqlRetailer);

			foreach ( $resultsRetailer as $retailer ) {

				$retailerName = $retailer->display_name;

				$retailerEmail = $retailer->user_email;

			}		
			
			?>

            <tr>

                <td><?=$quotation->jobId?></td>

                <td><?=$retailerName?>[<?=$retailerEmail?>]</td>

                <td>$<?=$quotation->quotationAmount?></td>

                <td><?=$quotation->comments?></td>

                <td><?=$quotation->product_warranty?></td>

                <td><?=$quotation->quotationDate?></td>

                <td><?=$quotation->assignedDate?></td>

                <td><?=($quotation->status==-1?"Rejected":($quotation->autoBidStatus==1?$quotation->status." / Auto Bid".($quotation->autoBidUpdateStatus=="1"?"<br>/Manual Updated":""):$quotation->status))?></td>

                <td><?= ($quotation->status == "1"?"<a class=\"reject\" data-confirm=\"Are you sure to reject this quotation?\" href=\"".admin_url('admin.php?page=jobs_details_on_dashboard&id='.$job->id.'&quote_id='.$quotation->id)."\">Reject</a>":"")?></td>

            </tr>

      		<?php

        }

		

    ?>

   </tbody>

</table>
<script language="javascript">
var deleteLinks = document.querySelectorAll('.reject');

for (var i = 0; i < deleteLinks.length; i++) {
  deleteLinks[i].addEventListener('click', function(event) {
      event.preventDefault();

      var choice = confirm(this.getAttribute('data-confirm'));

      if (choice) {
        window.location.href = this.getAttribute('href');
      }
  });
}
</script>
 </div>

        

        

        

        <div class="haggle-back-to"><a href="<?php menu_page_url('jobs_on_dashboard')?>" class="haggle-link">Back to list</a></div>

        <?php            

  //      }

  if (isset($_GET['quote_id'])) {

		$cj = reject_quote($_GET['quote_id']);

		$url = home_url()."/wp-admin/admin.php?page=jobs_details_on_dashboard&id=".$_GET['id'];

		if($cj){						

			echo "<div class=\"admin-mssg\" style=\"color:green\">Quote : ".$_GET['quote_id']." rejected</div>";

			echo "<script>window.location.href='".$url."'</script>";

		}

	}

}

//============== Quotation Rejected Function =================//

function reject_quote($quote_id){

	global $wpdb;
	$reject_quote = $wpdb->update( 
		$wpdb->prefix .'jobquotation', 
		array( 
			'status' => '-1'	// string			 
		), 
		array( 'id' => $quote_id ), 
		array( 			
			'%d'	// value2
		), 
		array( '%d' ) 
	);
	return $reject_quote;
}
?>