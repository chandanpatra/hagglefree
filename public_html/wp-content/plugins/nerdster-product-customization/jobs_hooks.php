<?php
//hooks for adding product image and description on find a deal form
add_action( 'woocommerce_before_add_to_cart_button', 'nerdster_add_product_detail_to_cart_form' );
function nerdster_add_product_detail_to_cart_form(){
	global $product;
	$id = $product->id;
	$product = wc_get_product( $id );
	$price = $product->get_price();
	$save = "";
	if($price<100){
		$save = "?????";
	}elseif($price<400){
		$save = "$$'s of dollars";
	}elseif($price<10000){
		$save = "$$$'s of dollars";
	}elseif($price>10000){
		$save = "$$$$'s of dollars";
	}else{
		$save = "?????";
	}	
    ?>
    	<div class="product-description-form">
        	<div class="left"> 
                <div class="product-image">
                    <?=$product->get_image()?>
                </div>
            </div>
            <div class="right">
                <div class="product-title"><?=the_title()?></div>
                <div class="product-sku"><span>SKU:</span><?=$product->get_sku()?></div>
                <?=nerdster_show_brand_single()?>
                <div class="product-rrp"><span>RRP:</span><?=$product->get_price_html()?></div>
                <div class="product-rrp save"><span>SAVE:</span><?=$save?></div>
                <div class="haggle-stamp1"><img src="<?=home_url()?>/wp-content/uploads/2016/guarantee_stamp.png"/></div>
            </div>
        </div>
        <?php /*?> <?php if( get_option('show_phone_job_form')!="1"){?>
            <div class="field phone-hooked">
              <label class="required" for="telephone"><em>*</em>Best Number to contact you on
              	<?php //if(get_option('comment_jobpost_phone')!=""){?>
                	<span class="smallipop haggle-hint" style="display:inline-block;">
                        &nbsp;
                        <span class="smallipop-hint">
                            <?= get_option('comment_jobpost_phone')?>
                        </span>
                    </span>
                    <?php // }?>
              </label>
              <div class="input-box">
                <input type="text" class="input-text required-entry required" title="Telephone" value="<?=get_user_meta( $user_ID, 'cr_phone', true )?>" id="cr_phone" name="cr_phone"/>
              </div>
            </div>          
          <?php } ?><?php */?>
                      
       
    <?php
}
// show brand in product desscription page.
add_action( 'woocommerce_single_product_summary', 'nerdster_show_brand_single',12 );
function nerdster_show_brand_single(){	
	$show_brand = new pw_woocommerc_brans_Wc_Brands();
	echo $show_brand->pw_woocommerc_show_brand();
}

// change position of related product 
add_action( 'init' , 'haggle_related_products' , 15 );
function haggle_related_products() {
        remove_action('woocommerce_after_single_product_summary','woocommerce_output_related_products',20);
		remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );		
		add_action('woocommerce_after_main_content','woocommerce_output_related_products',40);
		remove_action( 'woocommerce_after_shop_loop_item', 'gridlist_buttonwrap_open', 9);
		remove_action( 'woocommerce_after_shop_loop_item', 'gridlist_buttonwrap_open', 9);
		add_action( 'woocommerce_after_shop_loop_item', 'nerdster_gridlist_buttonwrap_open', 9);
		add_action( 'woocommerce_after_shop_loop_item', 'nerdster_gridlist_buttonwrap_close', 11);
		add_action('woocommerce_single_product_summary','woocommerce_template_single_price',6);
		remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10);
		add_action( 'woocommerce_after_shop_loop_item', 'nerdster_woocommerce_template_loop_product_title', 7);
		
}
function nerdster_woocommerce_template_loop_product_title(){
	echo "<div class=\"product-brand\">".get_brand_image()."</div><div class=\"product-title\"><h3>" . get_the_title() . "</div></h3>";
}
// Button wrap
function nerdster_gridlist_buttonwrap_open() {
	?>
		<div class="related-buttonwrap">
	<?php
}
function nerdster_gridlist_buttonwrap_close() {
	?>
		</div>
	<?php
}
add_filter( 'woocommerce_output_related_products_args', 'nerdster_related_products_args' );
  function nerdster_related_products_args( $args ) {
	$args['posts_per_page'] = 10; // 4 related products
	$args['columns'] = 4; // arranged in 2 columns
	return $args;
}
//Change free wording for zero priced product
add_filter('woocommerce_free_price_html', 'zero_price_message');
  function zero_price_message() {
    return '<span class="price_request">Price on Request</span>';
  }
  
//modify pagination @ top
add_action( 'woocommerce_before_shop_loop', 'woocommerce_pagination', 25 );

// modify result count and catolog odering @below
add_action( 'woocommerce_after_shop_loop', 'woocommerce_result_count', 8 );
add_action( 'woocommerce_after_shop_loop', 'woocommerce_catalog_ordering', 30 );