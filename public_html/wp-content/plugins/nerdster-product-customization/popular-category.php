<?php 
function popular_category(){
	global $wpdb;

?>
<center><h2>Popular Category</h2></center>
<hr/>
<?php	
	
	$table_popular_cat  = 	$wpdb->prefix . 'options';
	$option_name		=	'popular_categories';
	  if ($_POST['save_cat']) {
			$product_categories = implode(",",$_POST['product_category']);			
			$sql = "select * from `".$table_popular_cat."` where `option_name` LIKE '".$option_name."'";			
			$result = $wpdb->get_row($sql);
			if(count($result)>0){
				//echo '.....';
				update_option( $option_name, $product_categories );
			}else{
				add_option( $option_name, $product_categories, '', 'yes' );		
			}
			
			
        }
	  $pop_category = explode(',',get_option( $option_name ));
	  //print_r($pop_category);
	  $output  	   = "<form class='category-form' action='".esc_url($_SERVER['REQUEST_URI'])."' method='post'>";	  
	  
 	  $args = array(
			 'taxonomy'     => 'product_cat',
			 'orderby'      => 'term_order',
			 'show_count'   => $cat_attr['show_count'],
			 'pad_counts'   => $cat_attr['pad_counts'],
			 'hierarchical' => $cat_attr['hierarchical'],
			 'title_li'     => $cat_attr['title'],
			 'hide_empty'   => $cat_attr['empty']
	  );
	 $all_categories = get_categories( $args );
	 $output .= "<ul class=\"first-level\">";
	 $sql = "select `cat_id` from ".$table_catbrand." where user_id = ".$user_ID;	 
	 $result = $wpdb->get_results($sql);
	 $selected_cat = array();
	 if(count($result)>0){
	 	foreach($result as $rs){
			$selected_cat[] = $rs->cat_id;
		}
	 }
	 //print_r($selected_cat);
	 foreach ($all_categories as $cat) {
		if($cat->category_parent == 0) {
			$category_id = $cat->term_id;	
			$children = get_term_children($category_id, 'product_cat');
			if (!empty($children)) {
			 	$sub_cat_class = "genericon-rightarrow";//has childeren	
				$collapser = "<span class='plus catg'>&nbsp;</span>";			
			}     
			else{
				$sub_cat_class = "genericon-noarrow";
				$collapser = "";
			}	
			// first level
			$output	.=	"<li><input type='checkbox' name='product_category[]' value='".$category_id."'".(in_array($category_id,$pop_category)?" checked":"").">". $cat->name .$collapser;
			
			//second level
			$args2 = array(
					'taxonomy'     => 'product_cat',
					'child_of'     => 0,
					'parent'       => $category_id,
					'orderby'      => 'term_order',
					'show_count'   => $cat_attr['show_count'],
					'pad_counts'   => $cat_attr['pad_counts'],
					'hierarchical' => $cat_attr['hierarchical'],
					'title_li'     => $cat_attr['title'],
					'hide_empty'   => $cat_attr['empty']
			);
			$sub_cats = get_categories( $args2 );
			//print_r($sub_cats);
			if($sub_cats) {
				$output .= "<ul class=\"second-level\">";
				foreach($sub_cats as $sub_category) {
					$sub_category_id = $sub_category->term_id;
					$sub_children = get_term_children($sub_category_id, 'product_cat');
					if (!empty($sub_children)) {
						$sub_cat_class = "genericon-rightarrow has-children";//has childeren	
						$collapser = "<span class='plus subcatg'>&nbsp;</span>";			
					}     
					else{
						$sub_cat_class = "genericon-noarrow";
						$collapser = "";
					}
					$output	.=	"<li><input type='checkbox' name='product_category[]' value='".$sub_category_id."'".(in_array($sub_category_id,$pop_category)?" checked":"").">". $sub_category->name .$collapser;
					
					//third level
					$args3 = array(
							'taxonomy'     => 'product_cat',
							'child_of'     => 0,
							'parent'       => $sub_category_id,
							'orderby'      => 'term_order',
							'show_count'   => $cat_attr['show_count'],
							'pad_counts'   => $cat_attr['pad_counts'],
							'hierarchical' => $cat_attr['hierarchical'],
							'title_li'     => $cat_attr['title'],
							'hide_empty'   => $cat_attr['empty']
					);
					$sub_sub_cats = get_categories( $args3 );
					
					if($sub_sub_cats) {
						$output .= "<ul class=\"third-level\">";
						foreach($sub_sub_cats as $sub_sub_category) {
							$sub_sub_category_id = $sub_sub_category->term_id;
							$output	.=	"<li><input type='checkbox' name='product_category[]' value='".$sub_sub_category_id."'".(in_array($sub_sub_category_id,$pop_category)?" checked":"").">". $sub_sub_category->name ."<br></li>";
						}
						$output .="</ul><!-- third-level -->";
					}
					$output .="</li>";
				}
				$output .="</ul><!-- second-level -->";
			}//end subact
			$output .="</li>";
		}  //end cat     
	}
	$output	  .= "</ul><!-- first-level -->";
	$output	  .= "<input class='button-primary' type='submit' name='save_cat' value='Save Popular Category'/>
					</form>";
	/*$output      .="<script type='text/javascript'>
					jQuery(document).ready(function(){
						jQuery('ul.category-chekbox span.plus').click(function(){
						  jQuery(this).parent().find('ul.sub-category').toggle();
						  jQuery(this).parent().find('span.catg').toggleClass('minus');
    					  return false;						  
						});
						jQuery('ul.sub-category span.plus').click(function(){
						  jQuery(this).parent().find('ul.sub-sub-category').toggle();
						  jQuery(this).parent().find('span.subcatg').toggleClass('minus');
    					  return false;						  
						});
					});
					</script>";*/
	
	
	echo  $output;
}
?>