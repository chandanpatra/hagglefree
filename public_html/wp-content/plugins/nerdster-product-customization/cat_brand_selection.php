<?php
function cat_brand_selection(){
global $wpdb;

$prod_bands = get_terms( 'product_brand', array(
        'orderby'    => 'name',
        'order'      => 'ASC',
        'hide_empty' => false
    )); 
	
	
$table_catbrand_selection = $wpdb->prefix . 'catbrand_selection';
echo "<center><h2>Category Brand Selection</h2></center><hr/>";	
	
	
$output  	   = "<form class='category-form' action='".esc_url($_SERVER['REQUEST_URI'])."' method='post'>";	  
	  
 	  $args = array(
			 'taxonomy'     => 'product_cat',
			 'orderby'      => $cat_attr['orderby'],
			 'show_count'   => $cat_attr['show_count'],
			 'pad_counts'   => $cat_attr['pad_counts'],
			 'hierarchical' => $cat_attr['hierarchical'],
			 'title_li'     => $cat_attr['title'],
			 'hide_empty'   => $cat_attr['empty'],
			 'parent'       => '0',
	  );
	 $all_categories = get_categories( $args );
	 
	 $output .= "<ul class=\"first-level cat-brand-selection\">";

	 foreach ($all_categories as $cat) {
			$category_id = $cat->term_id;	
		 // Save record
		 if(isset($_POST['save_cat_brand'])){
			 $wpdb->query("DELETE FROM $table_catbrand_selection WHERE catId=\"$category_id\"");
			 if(count($_POST['cat_brand_'.$category_id])>0){
				 foreach( $_POST['cat_brand_'.$category_id] as $seletedBrand){
					 $table_catbrand_selection_value = array(
					   'catId'   => $category_id,
					   'brandId' => $seletedBrand
					 );
					 $wpdb->insert( $table_catbrand_selection, $table_catbrand_selection_value);
				 }
			 }
		 }
		 
		 // end save //
		 
			$output	.=	"<li><!--<input type='checkbox' name='product_category[]' value='".$category_id."'"./*(in_array($category_id,$pop_category)?" checked":"").*/">--><span>".$cat->name."</span>";
			
			$output	.=	"<ul class=\"brand-chekbox\">";
			
			$getBrands = getBrandsOfThisCat($category_id);
			
			foreach($prod_bands as $pbrand){
						
				 $is_selected = (in_array($pbrand->term_id,$getBrands)?" checked=\"checked\"":"");
				$output .="<li><span class=\"checkbox-wrap\"><input type=\"checkbox\" value=\"".$pbrand->term_id."\" name=\"cat_brand_".$category_id."[]\" $is_selected id=\"cat_brand_".$category_id."_".$pbrand->term_id."\" 
				class=\"checkbox-custom\"><label for=\"cat_brand_".$category_id."_".$pbrand->term_id."\" class=\"checkbox-custom-label\">".$pbrand->name."</label></span></li>";		
			}			
			$output .="</ul>";
			$output .="</li>";
	}
	$output	  .= "</ul>";
	$output	  .= "<input class='button-primary' type='submit' name='save_cat_brand' value='Save Category Brand selction'/>
					</form>";	
	
	echo $output;
}

function getBrandsOfThisCat($category_id = 0){
	global $wpdb;
	$table_catbrand_selection = $wpdb->prefix . 'catbrand_selection';
	if($category_id==0)
	return;
	else{
		$brands = array();
		$sql = "select * from ".$table_catbrand_selection." where catId = '".$category_id."'";
		$results = $wpdb->get_results($sql);
		if(count($results)>0){
			foreach($results as $result){
				$brands[] = $result->brandId;
			}
		}
	 return $brands;
	}
}
?>