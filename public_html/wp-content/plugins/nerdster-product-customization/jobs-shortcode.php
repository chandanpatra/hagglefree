<?php

/*

============Status of Jobs================

 0 ====> New job account not verified.

 1 ====> New job

 2 ====> Quotation send to cutomer 

 10 ====> Quotation not available
 

 ============Status of Quotations================

 0 ====> Request for Quotation

 1 ====> Quotation submited

 2 ====> Quotations selected to send to cutomer


*/

//shortcode for filter by brand 

function brands_filter_widget($atts) {

		 $brand_attr = shortcode_atts( array(			

			'title' => 'Shop by Brand',// title of widget

			'display_type' =>'list' //list,dropdown

		), $atts );

		global $_chosen_attributes, $woocommerce, $_attributes_array;

		$perm ="";

		$perm = 'filter_product_'.get_option( 'pw_woocommerce_brands_base','brand' );

		

		if ( ! is_post_type_archive( 'product' ) && ! is_tax( array_merge( is_array( $_attributes_array ) ? $_attributes_array : array(), array( 'product_cat', 'product_tag' ) ) ) )

			return;

       

		$current_term 	= $_attributes_array && is_tax( $_attributes_array ) ? get_queried_object()->term_id : '';

		$current_tax 	= $_attributes_array && is_tax( $_attributes_array ) ? get_queried_object()->taxonomy : '';



		$title 			= $brand_attr['title'];

		$taxonomy 		= 'product_brand';

		$display_type 	= $brand_attr['display_type'];



		if ( ! taxonomy_exists( $taxonomy ) )

			return;

         $display_type;

		$terms = get_terms( $taxonomy, array( 'hide_empty' => '1' ) );


		if ( count( $terms ) > 0 ) {

			ob_start();

			$found = false;

			if ( ! $_attributes_array || ! is_tax( $_attributes_array ) )

				if ( is_array( $_chosen_attributes ) && array_key_exists( $taxonomy, $_chosen_attributes ) )

					$found = true;

			$output = "<div class=\"brand-layered-navigation\">";

			$output .= "<h2 class=\"haggle-sidebar-title\">$title</h2>";

			if ( $display_type == 'dropdown' ) {

				if ( $current_tax && $taxonomy == $current_tax ) {

					$found = false;

				} else {

					$taxonomy_filter = $taxonomy;

					$found = true;
					$output .= "<select id=\"dropdown_layered_nav_$taxonomy_filter\" name=\"tech\" class=\"tech\">";
					$output .= "<option value=\"\">" . __( 'Any brand', 'woocommerce-brands' ) ."</option>";

				foreach ( $terms as $term ) {

					$transient_name = 'wc_ln_count_' . md5( sanitize_key( $taxonomy ) . sanitize_key( $term->term_id ) );

					if ( false === ( $_products_in_term = get_transient( $transient_name ) ) ) {

						$_products_in_term = get_objects_in_term( $term->term_id, $taxonomy );
						set_transient( $transient_name, $_products_in_term );

					}

					$option_is_set = ( isset( $_chosen_attributes[ $taxonomy ] ) && in_array( $term->term_id, $_chosen_attributes[ $taxonomy ]['terms'] ) );

					$count = sizeof( array_intersect( $_products_in_term, $woocommerce->query->filtered_product_ids ) );

					if ( $current_term == $term->term_id )

						continue;

					if ( $count > 0 && $current_term !== $term->term_id )

						$found = true;



					if ( $count == 0 && ! $option_is_set )

						continue;

					$current_filter = ( isset( $_GET[ $perm ] ) ) ? explode( ',', $_GET[ $perm ] ) : array();



					if ( ! is_array( $current_filter ) )

						$current_filter = array();



					if ( ! in_array( $term->term_id, $current_filter ) )

						$current_filter[] = $term->term_id;



					if ( defined( 'SHOP_IS_ON_FRONT' ) ) {

						$link = home_url();

					} elseif ( is_post_type_archive( 'product' ) || is_page( woocommerce_get_page_id('shop') ) ) {

						$link = get_post_type_archive_link( 'product' );

					} else {

						$link = get_term_link( get_query_var('term'), get_query_var('taxonomy') );

					}

					if ( $_chosen_attributes ) {

						foreach ( $_chosen_attributes as $name => $data ) {

							if ( $name !== 'product_brand' ) {

								while ( in_array( $current_term, $data['terms'] ) ) {

									$key = array_search( $current_term, $data );

									unset( $data['terms'][$key] );

								}

								if ( ! empty( $data['terms'] ) )

									$link = add_query_arg( sanitize_title( str_replace( 'pa_', 'filter_', $name ) ), implode(',', $data['terms']), $link );

							}

						}

					}

					if ( isset( $_chosen_attributes['product_brand'] ) && is_array( $_chosen_attributes['product_brand']['terms'] ) && in_array( $term->term_id, $_chosen_attributes['product_brand']['terms'] ) ) {

						$class = 'class="chosen"';

						if ( sizeof( $current_filter ) > 1 ) {

							$current_filter_without_this = array_diff( $current_filter, array( $term->term_id ) );

							$link = add_query_arg( $perm, implode( ',', $current_filter_without_this ), $link );

						}



					} else {

						$class = '';

						$link = add_query_arg( $perm, implode( ',', $current_filter ), $link );

					}

					if ( get_search_query() )

						$link = add_query_arg( 's', get_search_query(), $link );

					if ( isset( $_GET['post_type'] ) )

						$link = add_query_arg( 'post_type', $_GET['post_type'], $link );



					$output .= "<option value=\"". $term->term_id . "\" ".selected( isset( $_GET[ $perm ] ) ? $_GET[ $perm ] : '' , $term->term_id, false ) . ">" . $term->name . "</option>";												



				}					


					$output .= "</select>";
					$js = "
						jQuery(document).ready(function() {

							jQuery('.tech').msDropdown();

						});					

						jQuery('#dropdown_layered_nav_$taxonomy_filter').change(function(){

							value=jQuery('#dropdown_layered_nav_$taxonomy_filter').val();

							var val=Array();

							val=value.split('@');

							if(val[0]=='url')

								window.location= val[1];							

							else

								location.href = '" . add_query_arg('filtering', '1', remove_query_arg( $perm ) ) . "&".$perm."=' + jQuery('#dropdown_layered_nav_$taxonomy_filter').val();								

						});

					";



					if ( function_exists( 'wc_enqueue_js' ) ) {

						wc_enqueue_js( $js );

					} else {

						$woocommerce->add_inline_js( $js );

					}



				}



			}

			else {



				$output .= "<ul>";

 		
				foreach ( $terms as $term ) {



					$transient_name = 'wc_ln_count_' . md5( sanitize_key( $taxonomy ) . sanitize_key( $term->term_id ) );

					//print_r(get_transient( $transient_name ));

					if ( false === ( $_products_in_term = get_transient( $transient_name ) ) ) {



						$_products_in_term = get_objects_in_term( $term->term_id, $taxonomy );



						set_transient( $transient_name, $_products_in_term );

					}



					$option_is_set = ( isset( $_chosen_attributes[ $taxonomy ] ) && in_array( $term->term_id, $_chosen_attributes[ $taxonomy ]['terms'] ) );



					$count = sizeof( array_intersect( $_products_in_term, $woocommerce->query->filtered_product_ids ) );



					if ( $current_term == $term->term_id )

						continue;

					//skip test brand

					/*if($term->term_id == "5944")

						continue;*/

					if ( $count > 0 && $current_term !== $term->term_id )

						$found = true;



					if ( $count == 0 && ! $option_is_set )

						continue;



					$current_filter = ( isset( $_GET[ $perm ] ) ) ? explode( ',', $_GET[ $perm ] ) : array();



					if ( ! is_array( $current_filter ) )

						$current_filter = array();



					if ( ! in_array( $term->term_id, $current_filter ) )

						$current_filter[] = $term->term_id;



					if ( defined( 'SHOP_IS_ON_FRONT' ) ) {

						$link = home_url();

					} elseif ( is_post_type_archive( 'product' ) || is_page( woocommerce_get_page_id('shop') ) ) {

						$link = get_post_type_archive_link( 'product' );

					} else {

						$link = get_term_link( get_query_var('term'), get_query_var('taxonomy') );

					}



					if ( $_chosen_attributes ) {

						foreach ( $_chosen_attributes as $name => $data ) {

							if ( $name !== 'product_brand' ) {



								while ( in_array( $current_term, $data['terms'] ) ) {

									$key = array_search( $current_term, $data );

									unset( $data['terms'][$key] );

								}



								if ( ! empty( $data['terms'] ) )

									$link = add_query_arg( sanitize_title( str_replace( 'pa_', 'filter_', $name ) ), implode(',', $data['terms']), $link );

							}

						}

					}



					if ( isset( $_GET['min_price'] ) )

						$link = add_query_arg( 'min_price', $_GET['min_price'], $link );



					if ( isset( $_GET['max_price'] ) )

						$link = add_query_arg( 'max_price', $_GET['max_price'], $link );

						

					if ( isset( $_chosen_attributes['product_brand'] ) && is_array( $_chosen_attributes['product_brand']['terms'] ) && in_array( $term->term_id, $_chosen_attributes['product_brand']['terms'] ) ) {



						$class = 'class="chosen"';



						// Remove this term is $current_filter has more than 1 term filtered

						if ( sizeof( $current_filter ) > 1 ) {

							$current_filter_without_this = array_diff( $current_filter, array( $term->term_id ) );

							$link = add_query_arg( $perm, implode( ',', $current_filter_without_this ), $link );

						}



					} else {

						$class = '';

						$link = add_query_arg( $perm, implode( ',', $current_filter ), $link );

					}



					// Search Arg

					if ( get_search_query() )

						$link = add_query_arg( 's', get_search_query(), $link );



					// Post Type Arg

					if ( isset( $_GET['post_type'] ) )

						$link = add_query_arg( 'post_type', $_GET['post_type'], $link );



					$output .= "<li $class >";





					$url	= esc_html(get_woocommerce_term_meta( $term->term_id, 'url', true ));							

					

					$output .= ( $count > 0 || $option_is_set ) ? "<a href=\"" .($url=="" ? $link : $url). "\">" : "<span>";



					$output .= $term->name;



					$output .= ( $count > 0 || $option_is_set ) ? "</a>" : "</span>";



					$output .= " <small class=\"count\">" . $count . "</small></li>";

				}



				$output .= "</ul>";



			} // End display type conditional



						

			$output .="</div>";

			if ( ! $found )

				ob_end_clean();

			else

				echo ob_get_clean();

		} 

			

		

		return $output;

	

}

add_shortcode('brands_filter', 'brands_filter_widget');



//shortcode for browse by category on sidebar section

function browse_category_widget($atts){

	$cat_attr = shortcode_atts( array(			

			'title' => ''// title of widget			

			

		), $atts );

	$title 			= $cat_attr['title'];

	$output = "<div class=\"browse-category-widget\">";

	$output .= "<h2 class=\"haggle-sidebar-title\">$title</h2>";

	$output .= get_product_categorylist("side-menu","no");

	$output .="</div>";

	return $output;

}

add_shortcode('browse-by-category','browse_category_widget');

//shortcode for product search

function product_shortcode_widget(){

	$output = "<div class=\"widget_product_search product_shortcode\">";

	

	$output .= "<form action=\"".home_url()."\" class=\"woocommerce-product-search\" method=\"get\" role=\"search\">

				<label for=\"woocommerce-product-search-field\" class=\"screen-reader-text\">Search.....</label>

				<input type=\"search\" title=\"Search.....\" name=\"s\" value=\"\" placeholder=\"Search Products…\" class=\"search-field\" id=\"woocommerce-product-search-field\">

				<input type=\"submit\" value=\"Search\">

				<input type=\"hidden\" value=\"product\" name=\"post_type\">

			</form>";

	$output .="</div>";

	return $output;

}

add_shortcode('products_search', 'product_shortcode_widget');







//shortcode for popular category slider on homepage

function popular_categories_widget(){

	//include css for carousel

	wp_enqueue_style( 'carousel-theme-style',plugin_dir_url(__FILE__).'css/owl.theme.css');

	

	$pop_category = 	get_option( 'popular_categories' );

	$no_image 	 =	home_url()."wp-content/uploads/2016/05/category_placeholder.jpg";

	

	$prod_categories = get_terms( 'product_cat', array(

        'orderby'    => 'term_order',

        'order'      => 'ASC',

        'hide_empty' => false,

		'include'	=> $pop_category

    ));

		

	$output = "<div id=\"popular-category\" class=\"owl-carousel\">";

	/*echo '<pre>';

	print_r($prod_categories);

	echo '</pre>';*/

	foreach($prod_categories as $catid){

		 // get the thumbnail id using the queried category term_id

		$thumbnail_id = get_woocommerce_term_meta( $catid->term_id,'thumbnail_id', true ); 

		$shop_catalog_img = wp_get_attachment_image_src( $thumbnail_id, 'shop_catalog' );

		$term_link = get_term_link( $catid, 'product_cat' );

		

		if($thumbnail_id){

			$output .= "<div class=\"item\"><a href=".$term_link." ><img class=\"lazyOwl\" src=".$shop_catalog_img[0]." alt='".$catid->name."' id='".$catid->term_id."'></a></div>";	

		}else{

			$output .= "<div class=\"item\"><a href=".$term_link." ><img class=\"lazyOwl\" src=".$no_image." alt='".$catid->name."' ><span>".$catid->name."</span></a></div>";	

		}		

	}

                    

    $output  .= "</div><!-- #popular-category -->";

		

	$output .= "<script>

    jQuery(document).ready(function() {

      jQuery(\"#popular-category\").owlCarousel({

        items : 5,        

        nav : true,

		pagination : false,

		responsiveClass:true,

		responsive:{

			320:{ items:1 }, // Mobile portrait

			600:{ items:2 }, // Small tablet portrait

			768:{ items:3 }, // Tablet portrait

			979:{ items:5 }  // Desktop

		}

      });

    });

    </script>";	

	

	return $output;

}

add_shortcode('popular_category_slider','popular_categories_widget');





function get_brands($user_id,$table_brand){

	global $wpdb;

	$prod_bands = get_terms( 'product_brand', array(

        'orderby'    => 'name',

        'order'      => 'ASC',

        'hide_empty' => false

    )); 

	

	//$output ="<select multiple name='cat_brand_".$id."[]'>";

	$output = "<div class=\"brands-check\"><ul class=\"brand-chekbox\">";

	//$table_brand = $wpdb->prefix . 'retailer_brands'; 

	$sql = "SELECT `brand_id` FROM $table_brand WHERE `user_id` = $user_id";

	$result = $wpdb->get_results($sql);

	$brand_ids = array();

	if(count($result)>0){

		foreach($result as $rs){

			$brand_ids[] = $rs->brand_id;

		}

	}

	//$catBrands = adminAssignCatBrand($id);

	foreach($prod_bands as $pbrand){

		        // if(!in_array($pbrand->term_id,$catBrands))

				// continue;

						

				$is_selected = (in_array($pbrand->term_id,$brand_ids)?" checked=\"checked\"":"");

				//$output .="<option value=\"".$pbrand->term_id."\" $is_selected>".$pbrand->name."</option>";

				$output .="<li><span class=\"checkbox-wrap\"><input type=\"checkbox\" value=\"".$pbrand->term_id."\" name=\"brands[]\" $is_selected id=\"brand_".$pbrand->term_id."\" 

				class=\"checkbox-custom\"><label for=\"brand_".$pbrand->term_id."\" class=\"checkbox-custom-label\">".$pbrand->name."</label></span></li>";		

			}

	//$output .="</select>";

	$output .="</ul></div>";

	return $output;

}



function adminAssignCatBrand($category_id=0){

	global $wpdb;

	$table_catbrand_selection = $wpdb->prefix . 'catbrand_selection';

	if($category_id==0)

	return;

	else{

		$brands = array();

		$sql = "select * from ".$table_catbrand_selection." where catId = '".$category_id."'";

		$results = $wpdb->get_results($sql);

		if(count($results)>0){

			foreach($results as $result){

				$brands[] = $result->brandId;

			}

		}

	 return $brands;

	}

}



////////////////////// Shortcode for BUYERS //////////////////////////



//Shortcode for job listing for buyer

function job_list_widget() {

		//check is user logged in else redirect to homepage

		 if(is_user_logged_in ())

		{ 

			$user_ID = get_current_user_id();

			global $wpdb;

			$user = new WP_User( $user_ID );

			$role = $user->roles[0];

			// only show this section to customer

			if($role == 'buyer-account'){

				/////// pagination start //////

				global $pagenum, $limit, $start;

				if(trim(get_option('job_limit'))){

						$limit = get_option('job_limit');

					}else{

						$limit = 10; // number of jobs in page

					}

				$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;

				$start =  ($pagenum - 1) * $limit;

				

				//////pagination ends/////

				$jobs = get_jobs($user_ID);

				$total = $jobs['totalData'];

				$num_of_pages = ceil( $total / $limit );

				//print_r($jobs);

				$jobs_data = $jobs['results'];

				//print_r($a);

				if($jobs == NULL){

					$output = "<div class=\"job-list\"><p class=\"woocommerce-info\">No jobs yet.</p></div>";

				}else{

					$output = "<div class=\"job-list\">

									<div class=\"listing-header\">
									
										<span class=\"jt\">Job Title</span>

										<span class=\"jt\">Job Title</span>

										<span class=\"ao\">Applied On</span>

										<span class=\"Status\">Status</span>

										<span class=\"act\">Action</span>

									</div>

									<div class=\"listing-body\">";	

				

					 foreach ( $jobs_data as $job ) {

						//////////////////////

						$product = wc_get_product( $job->productId );

						$user_info = get_userdata($job->userId);

						$first_name = $user_info->first_name;

						$last_name = $user_info->last_name; 

						$status = "";

						switch($job->status){

							case 1:

							$status = "New Job";

							break;

							case 2:

							$status = "Retailer Assigned";

							break;

							case 3:

							$status = "Accepted Quote";

							break;

						}

						

							$output.="<div class=\"job-list-rt\"> 
							
										

										<span class=\"jt\">" .esc_html( $product->get_title() ) . " </span>

										<span class=\"ao\">" .date("l, F j, Y",strtotime($job->jobDate)) . "</span>

										<span class=\"Status\">".($job->status==1?"Waiting for quotation":($job->status==2?"Quotation Sent":($job->status==3?"Job Closed":($job->status==10?"No Quotation Posted":""))))."</span>		

										<span class=\"act\"><a href=\"".home_url('job-detail?id='.$job->id)."\">View Detail</a></span>

									  </div>";	

							

						}

						$output.="</div></div>";

						$page_links = paginate_links( array(

							'base' => add_query_arg( 'pagenum', '%#%' ),

							'format' => '',

							'prev_text' => __( '&laquo;', 'text-domain' ),

							'next_text' => __( '&raquo;', 'text-domain' ),

							'total' => $num_of_pages,

							'current' => $pagenum

						) );

						$paginate_end = $limit*$pagenum;

						if($paginate_end>$total){

							$paginate_end = $total;

						}

						if ( $page_links ) {

							$output.="<div class=\"haggle-tablenav\"><div class=\"haggle-now-showing\">Showing ".($start+1)."&ndash;".($paginate_end)." of $total results</div><div class=\"tablenav-pages\" style=\"margin: 1em 0\">$page_links</div></div>";

						}

				}

			}else{

				$output = "<script type=\"text/javascript\">

							window.location.href=\"".home_url('my-account')."\";

					   </script>";

			}

		}else{

			$output = "<script type=\"text/javascript\">

							window.location.href=\"".home_url('my-account')."\";

					   </script>";

		}

		return $output;

	

}

add_shortcode('job_list', 'job_list_widget');



//Shortcode for job listing for buyer

function job_detail_widget() {

		//check is user logged in else redirect to homepage

		if(is_user_logged_in ())

		{ 

			$user_ID = get_current_user_id();

			$user = new WP_User( $user_ID );

			$role = $user->roles[0];

			// only show this section to customer

			if($role == 'buyer-account'){

			$job_id  = $_GET['id'];			

			global $wpdb;

			$job = get_job($user_ID,$job_id);

			//print_r($job);

			if($job == NULL){

				$output = "<div class=\"job-list\"><p class=\"woocommerce-info\">No jobs yet.</p></div>";

			}else{

			//print_r($jobs);

			//foreach ( $jobs_data as $job ) {

						//////////////////////

					$product = wc_get_product( $job->productId );

						

					$user_info = get_userdata($job->userId);

					if($job->retailerId !='0'){

						$retailer_info = get_userdata($job->retailerId);

					}

					$first_name = $user_info->first_name;

					$last_name = $user_info->last_name; 

					$address = $job->Postcode;

					$status = "";

					switch($job->status){

						case 1:

						$status = "New Job";

						break;

						case 2:

						$status = "Retailer Assigned";

						break;

						case 3:

						$status = "Accepted Quote";

						break;

					}

					$output = "

					<div class=\"admin-job-detail\">

						<div class=\"buyer-job-fieldset\">

							<div class=\"fieldset-title\">Job Detail</div>	

							<div class=\"fieldset-content\">

								<div><span>Job Title : ".esc_html( $product->get_title() )."</div>

								<div><span>Posted Date : </span> ".$job->jobDate."</div>

								<div><span>Retailer : </span> ".($job->retailerId=='0'?'No retailer assigned yet':$retailer_info->first_name)."</div>

								<div><span>Quotation Accept Date : </span> ".($job->qacceptDate=='0000-00-00 00:00:00'?'No quotation yet':$job->qacceptDate)."</div>

								<div><span>Quotation Amount : </span> ".($job->quotationAmount=='0'?'No quotation yet':$job->quotationAmount)."</div>

							</div>

						</div>

						<div class=\"buyer-job-fieldset\">

							<div class=\"fieldset-title\">Job additional information</div>

							<div class=\"fieldset-content\">

							<div><span>Delivery/Pickup : </span> ".get_job_meta($job->id,'wccpf_deliverypickup')."</div>";

							$work_start = product_delivery_switch(get_job_meta($job->id,'wccpf_when_do_you_need_the_product'));

									

							$output .="<div><span>When do you need the product : </span> $work_start </div>";

							$removal_of_old_appliance = get_job_meta($job->id,'wccpf_removal_of_old_appliance');

							if($removal_of_old_appliance!=''){

								$output .= "<div><span>Removal of old appliance ? : </span> $removal_of_old_appliance</div>";

							}

							$oven_type = get_job_meta($job->id,'wccpf_oven_type');

							if($oven_type!=''){

								$output .= "<div><span>Oven Type ? : </span> $oven_type</div>";

							}

							$output .= '<div><span>Is Commercial ?  : </span> '.($job->is_commercial=='1'?'Yes':'No').'</div>';

					if($job->is_commercial==1){

						$output .='<div><span>Buisness Name : </span> '.$job->businessName.'</div>

							<div><span>ABN no : </span> '.$job->abn_no.'</div>';

					}

					$output .='						

							<div><span>Quantity : </span> '.$job->quantity.'</div>

							</div>  

					   </div>

					   <div class="buyer-job-fieldset">

							<div class="fieldset-title">Address</div>

								<div class="fieldset-content">

									<div><span>Buyer Name : </span> '.$first_name.' '.$last_name.'</div>

									<div><span>Email : </span> '.$job->email.'</div>

									<div><span>Post Code : </span> '.$address.'</div>

									<div><span>Telephone : </span> '.$job->telephone.'</div>

								</div>

					   		</div>

					</div>';

					$output .= "<div class=\"haggle-back-to\"><a class=\"haggle-link\" href=\"".home_url('my-jobs')."\">Back to list</a></div></div>";

								

				//	}

			}

			}// endif buyer account

			else{

				$output = "<script type=\"text/javascript\">

							window.location.href=\"".home_url('my-account')."\";

					   </script>";

			}

		}else{

			$output = "<script type=\"text/javascript\">

							window.location.href=\"".home_url('my-account')."\";

					   </script>";

		}

		return $output;

	

}

add_shortcode('job_detail', 'job_detail_widget');

//Shortcode for best quotation listing for buyer

function best_quotation_widget(){

	if(is_user_logged_in ())

		{ 

			$user_ID = get_current_user_id();

			$user = new WP_User( $user_ID );

			$role = $user->roles[0];

			global $wpdb;

			// only show this section to customer

			if($role == 'buyer-account'){

				

				if(isset($_POST['submitAcceptQuotation'])){

					$tableQuotation = $wpdb->prefix . 'jobquotation';

					$tableJobs = $wpdb->prefix . 'jobs';

					

					$jobId = $_POST['jobId'];

					$quotationId = $_POST['selectedQuotation'];

					// update job status

						$update_val = array('status' => 3,'selectedQuotationId' =>$quotationId );

						$where = array('id' => $jobId);	

						$wpdb->update($tableJobs,$update_val,$where);

					// update quotation status status

						$update_val = array('status' => 3);

						$where = array('id' => $quotationId);	

						$wpdb->update($tableQuotation,$update_val,$where);					

					

					// send mail to retailer

					

				  $headers[] = 'Content-Type: text/html; charset=UTF-8';

				  $headers[] = 'From: '.get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'> ' . "\r\n";

				  $template_name = "my_quotation_accepted";

				  $template_detail = get_email_template($template_name);	

				  // get retailer info

				  

				  $sql = "SELECT * FROM $tableQuotation WHERE id = '$quotationId'";

				  $retailer = $wpdb->get_row($sql);

				  $retailerId = $retailer->retailerId;				  

				  $retailerData = new WP_User( $retailerId );

				  

				  // Get Job details

				  

				  $jobDetails = email_job_details($jobId,"yes");

				  

				  

				  // Get Customer details

				  

				  $getJob = get_job($user_ID,$jobId);

				  $product = wc_get_product( $getJob->productId );

				  $productName = $product->get_title();

				  

				  $customerCallDetails = "<br>Customer: <b>".$getJob->firstname."</b>";

				  $customerCallDetails .= "<br>Email: <b>".$getJob->email."</b>";

				  $customerCallDetails .= "<br>Phone: <b>".$getJob->telephone."</b>";

				  

				  

				  //Get Quotation details

				  $quantity = $_POST['quantity'];

				  $quotationDetails = email_quotation_details($quotationId,$quantity);

				  

				  //token replacement	

				  /*

				  	{site_name}

				  */	

				  $mail_content = str_replace("{site_name}",get_bloginfo( 'name' ),stripslashes($template_detail['email_template']));				  

				  

				  $mail_content = str_replace("{name}",$retailerData->display_name,$mail_content);

				  

				  $mail_content = str_replace("{quotation_url}",get_site_url()."/selected-quotations/?quote_id=".$quotationId,$mail_content);

				  

				  $mail_content = str_replace("{job_details}",$jobDetails,$mail_content);

				  

				  $mail_content = str_replace("{user_details}",$customerCallDetails,$mail_content);

				  

				  $mail_content = str_replace("{quotation_details}",$quotationDetails,$mail_content);

				  

				  $mail_content = str_replace("{postcode}",$getJob->Postcode,$mail_content);

				  

				  wp_mail( $retailerData->user_email, $template_detail['subject']." on ".$productName, $mail_content,$headers);

				  			

					

				}

				

			/////// pagination start //////

				global $pagenum, $limit, $start;

				if(trim(get_option('job_limit'))){

						$limit = get_option('job_limit');

					}else{

						$limit = 10; // number of jobs in page

					}

				$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;

				$start =  ($pagenum - 1) * $limit;

				//////pagination ends/////				

				

				$jobs = get_jobs($user_ID, "", 2);	

				$total = $jobs['totalData'];

				$num_of_pages = ceil( $total / $limit );

							

				if($jobs == NULL){

					$output = "<div class=\"best-quote-list\"><p class=\"woocommerce-info\">No jobs yet.</p></div>";

				}else{

					$output = "<div class=\"best-quote-list\">

									<div class=\"listing-header\">

										<span class=\"jt\">Job Title</span>

										<span class=\"qa\">List Quotation Amount</span>

										<span class=\"qd\">Quotation Date</span>	

										<span class=\"qd\">Status</span>									

										<span class=\"act\">Action</span>

									</div>

									<div class=\"listing-body\">";

									

					$jobs_data = $jobs['results'];

										

					foreach($jobs_data as $job){

						$job_id = $job->id;

						$product = wc_get_product( $job->productId );

						if($product){

						$job_title = esc_html( $product->get_title());

						$table_jobquotation = $wpdb->prefix . 'jobquotation';

						$quotation_response_sql = "SELECT * FROM $table_jobquotation WHERE `jobId` = \"$job_id\" AND status = \"2\" ORDER BY quotationAmount DESC LIMIT 0,1";

						$quotation_response_result = $wpdb->get_results($quotation_response_sql);

						$quotation_amount = "$".$quotation_response_result[0]->quotationAmount;

						$quotation_date = date("l, F j, Y",strtotime($quotation_response_result[0]->quotationDate)) ;

						$status = $quotation_response_result[0]->status;

						$quote_id = $quotation_response_result[0]->id;

						/*echo "<pre>";

						print_r($quotation_response_result);

						echo "</pre>";*/

						if(count($quotation_response_result)>0){

							if($status=='2'){

							$output .="<div class=\"best-quote-list-byr\">";

							$output .= "<span class=\"jt\">$job_title</span>

										<span class=\"qa\">$quotation_amount</span>

										<span class=\"qd\">$quotation_date</span>	

										<span class=\"qd\">".($job->status==1?"Waiting for quotation":($job->status==2?"Quotation Sent":($job->status==3?"Job Closed":($job->status==10?"No Quotation Posted":""))))."</span>									

										<span class=\"act\"><a href=\"".home_url('quotation-detail?job_id='.$job_id)."\">View Detail</a></span>"; 

							$output .="</div>";

							}

						}						

						}

					}

					

					$output .="</div><!-- listing-body --></div><!-- best-quote-list-->";

					$page_links = paginate_links( array(

							'base' => add_query_arg( 'pagenum', '%#%' ),

							'format' => '',

							'prev_text' => __( '&laquo;', 'text-domain' ),

							'next_text' => __( '&raquo;', 'text-domain' ),

							'total' => $num_of_pages,

							'current' => $pagenum

						) );

						$paginate_end = $limit*$pagenum;

						if($paginate_end>$total){

							$paginate_end = $total;

						}

						if ( $page_links ) {

							$output.="<div class=\"haggle-tablenav\"><div class=\"haggle-now-showing\">Showing ".($start+1)."&ndash;".($paginate_end)." of $total results</div><div class=\"tablenav-pages\" style=\"margin: 1em 0\">$page_links</div></div>";

						}

				}

				

			}//endif buyer

			else{

				$output = "<script type=\"text/javascript\">

							window.location.href=\"".home_url('my-account')."\";

					   </script>";

			}

		}else{

			$output = "<script type=\"text/javascript\">

							window.location.href=\"".home_url('my-account')."\";

					   </script>";

		}

		return $output;

}

add_shortcode('best-quotation', 'best_quotation_widget');

function quotation_detail_widget(){

	if(is_user_logged_in ())

		{ 

			$loggedin_userid = get_current_user_id();

			$user = new WP_User( $loggedin_userid );

			$role = $user->roles[0];

			global $wpdb;

			// only show this section to customer

			if($role == 'buyer-account'){

				$job_id = $_GET['job_id'];

				// get data from job table and checking with customer //

				$jobData = get_job($loggedin_userid,$job_id);								

				if($jobData){

					

			   // get job details //

			   

			   $jobDetails = job_details_for_report($job_id);

			   $quantity = (int)$jobData->quantity;

			   $output = "<div class=\"quotaion-details\">$jobDetails</div>";

			   $output .= "<div class=\"list-of-quotations\">";			   

			  // $output .= "<form name=\"acceptQuotation\" action=\"".home_url('best-quotation')."\" method=\"post\">";

			   // get quotation details		

				$table_jobquotation = $wpdb->prefix . 'jobquotation';

				$quotation_response_sql = "SELECT * FROM $table_jobquotation WHERE `jobId`= \"$job_id\" AND `status`='2' ORDER BY `quotationAmount` ASC";

				

				$quotation_response_results = $wpdb->get_results($quotation_response_sql);

				foreach($quotation_response_results as $quotation_response_result){				

				    $quotationTotal = 0;

					$user_info = get_userdata($quotation_response_result->retailerId);

					

					$output .= "<div class=\"quotaion-details\"><div class=\"fieldset-title radio-wrap\">					

					Quotaion detail of ".$user_info->display_name."

					</div><div class=\"fieldset-content\">";

					if($quotation_response_result->quotationAmount!=""){

						$output .="<div class=\"qd-row\"><label>Quotation Amount ".(($jobData->is_commercial=='1'&&(strtotime($jobData->jobDate)<strtotime('2016-09-15 23:59:59')))?'(Ex GST)':'(Inc GST)').": </label>$".$quotation_response_result->quotationAmount."<span> per item</span></div>";

						$quotationTotal += $quotation_response_result->quotationAmount*$quantity;

					}

					

					if($quotation_response_result->comments!="")

						$output .="<div class=\"qd-row\"><label>Quotation Comment : </label>".$quotation_response_result->comments."</div>";

					

					if($quotation_response_result->quotationDate!="")

						$output .="<div class=\"qd-row\"><label>Quotation Date : </label>".date("l, F j, Y",strtotime($quotation_response_result->quotationDate))."</div>";

					

					if($quotation_response_result->warranty_price!=""){

						$output .="<div class=\"qd-row\"><label>Extended Warranty Price ".(($jobData->is_commercial=='1'&&(strtotime($jobData->jobDate)<strtotime('2016-09-15 23:59:59')))?'(Ex GST)':'(Inc GST)').": </label>$".$quotation_response_result->warranty_price."<span> per item</span></div>";

						$quotationTotal += $quotation_response_result->warranty_price*$quantity;

					}

					

					if($quotation_response_result->delivery_price!=""){

						$output .="<div class=\"qd-row\"><label>Delivery Price ".(($jobData->is_commercial=='1'&&(strtotime($jobData->jobDate)<strtotime('2016-09-15 23:59:59')))?'(Ex GST)':'(Inc GST)').": </label>$".$quotation_response_result->delivery_price."<span> per item</span></div>";

						$quotationTotal += $quotation_response_result->delivery_price*$quantity;

					}

					

					if($quotation_response_result->delivery_comment!="")

						$output .="<div class=\"qd-row\"><label>Delivery Comment : </label>".$quotation_response_result->delivery_comment."</div>";

					

					if($quotation_response_result->installation_price!=""){

						$output .="<div class=\"qd-row\"><label>Installation Price ".(($jobData->is_commercial=='1'&&(strtotime($jobData->jobDate)<strtotime('2016-09-15 23:59:59')))?'(Ex GST)':'(Inc GST)').": </label>$".$quotation_response_result->installation_price."<span> per item</span></div>";

						$quotationTotal += $quotation_response_result->installation_price*$quantity;

					}

					

					if($quotation_response_result->installation_comment!="")

						$output .="<div class=\"qd-row\"><label>Installation Comment : </label>".$quotation_response_result->installation_comment."</div>";

						

					if($quotation_response_result->removal_price!=""){

						$output .="<div class=\"qd-row\"><label>Removal Price ".(($jobData->is_commercial=='1'&&(strtotime($jobData->jobDate)<strtotime('2016-09-15 23:59:59')))?'(Ex GST)':'(Inc GST)').": </label>$".$quotation_response_result->removal_price."<span> per item</span></div>";

						$quotationTotal += $quotation_response_result->removal_price*$quantity;

					}

					

					if($quotation_response_result->roa_comment!="")

						$output .="<div class=\"qd-row\"><label>Removal Comment : </label>".$quotation_response_result->roa_comment."</div>";

						

					if($quotation_response_result->timeframe_comments!="")

						$output .="<div class=\"qd-row\"><label>Timeframe Comment : </label>".$quotation_response_result->timeframe_comments."</div>";

					

					if($quotation_response_result->terms_and_conditions!="")

						$output .="<div class=\"qd-row\"><label>Terms & Condition : </label>".$quotation_response_result->terms_and_conditions."</div>";

					

					if($quotation_response_result->contact_person!="")

						$output .="<div class=\"qd-row\"><label>Contact Person : </label>".$quotation_response_result->contact_person."</div>";

					

					if($quotation_response_result->contact_details!="")

						$output .="<div class=\"qd-row\"><label>Contact Details : </label>".$quotation_response_result->contact_details."</div>";

					

					if($quotation_response_result->meet_timeframe!="")

						$output .="<div class=\"qd-row\"><label>Can you meet the customer's timeframe? : </label>".$quotation_response_result->meet_timeframe."</div>";

					

					if($quotation_response_result->product_condition!=""){	

						$product_condition = product_condition($quotation_response_result->product_condition);					

						$output .="<div class=\"qd-row\"><label>Product Condition : </label>".$product_condition."</div>";

					}

					

					if($quotation_response_result->product_comment!="")

						$output .="<div class=\"qd-row\"><label>Product Comment : </label>".$quotation_response_result->product_comment."</div>";

						

					$output .="<div class=\"qd-row\"><label>Grand Quotation Total : </label>$".$quotationTotal."</div>";	

						

					$output .="</div><!-- fieldset-content --></div>";

					

						//$output .= "<div class=\"haggle-back-to\"><a class=\"haggle-link\" href=\"".home_url('best-quotation')."\">Back to list</a></div></div>";

					/*echo '<pre>';

					print_r($quotation_response_result);

					echo '</pre>';*/

					}

					$output .="

					</div>";

				}else{

						$output ="Opps something went wrong please click correct Best Quotation.";

					}

			}else{

				$output = "<script type=\"text/javascript\">

							window.location.href=\"".home_url('my-account')."\";

					   </script>";

			}

		}else{

			$output = "<script type=\"text/javascript\">

						window.location.href=\"".home_url('my-account')."\";

				   	   </script>";

		}

	return $output;

}

add_shortcode('quotation-detail', 'quotation_detail_widget');

////////////////////// Shortcode for BUYERS //////////////////////////



////////////////////// Shortcode for RETAILERS //////////////////////////



// shortcode to show category list checkbox in category management section for retailer

function product_category_form($atts){

	

	global $wpdb;

	//check is user logged in else redirect to homepage

		if(is_user_logged_in ())

		{ 

			$user_ID = get_current_user_id();

			$user = new WP_User( $user_ID );

			$role = $user->roles[0];

			$table_cat = $wpdb->prefix . 'retailer_cats';

			//$table_brand = $wpdb->prefix . 'retailer_brands';

			

			$cat_attr = shortcode_atts( array(

				'orderby' 		  => 'term_order',		

				'title' 			=> '',		

				'show_count' 	   =>'0',	 // 1 for yes, 0 for no

				'pad_counts'	   => '0',	// 1 for yes, 0 for no

				'hierarchical'	 => '1',	  // 1 for yes, 0 for no

				'empty'			=> '0'		

			), $atts );	  

			  if($role == "retailer-account"){

			  if ($_POST['cat_submit']) {

				  if(count($_POST['product_category'])>0){

						$product_categories = implode(",",$_POST['product_category']);

						

						///// delete the entry if there is a product category not assigned for this user on next save

						$wpdb->query("delete from $table_cat where user_id=\"$user_ID\" AND cat_id NOT IN($product_categories)");

						

						/// delete data from brand table where cat_id not in selected categories for this retailer

						//$wpdb->query("delete from $table_brand where user_id=\"$user_ID\" AND cat_id NOT IN($product_categories)");

						

						foreach($_POST['product_category'] as $cid){

							$sql = "select * from ".$table_cat." where user_id = ".$user_ID. " and cat_id = ".$cid;

							$result = $wpdb->get_results($sql);

							if(count($result)>0){

								//do nothing

								continue;

							}

							else{

								//insert

								$insert_val = array(

												'user_id' 	  => $user_ID,

												'cat_id'   => $cid									

											);

								$insert = $wpdb->insert( $table_cat, $insert_val);

							}

							

						}

				  }//end if

				  else{

					///// delete the entry if there is a product category not assigned for this user on next save

						$wpdb->query('delete from '.$table_cat.' where user_id='.$user_ID);

						//$wpdb->query('delete from '.$table_brand.' where user_id='.$user_ID);

				  }

				}

			  

			  $output  	   = "<form class='category-form' action='".esc_url($_SERVER['REQUEST_URI'])."' method='post'>
			  					<span class=\"checkbox-wrap\"><input type=\"checkbox\" class=\"checkbox-custom\" id=\"select-all\" name=\"select-all\" ><label class=\"checkbox-custom-label\" for=\"select-all\">Select all</label></span>";	  

			  

			  $args = array(

					 'taxonomy'     => 'product_cat',

					 'orderby'      => 'term_order',

					 'show_count'   => $cat_attr['show_count'],

					 'pad_counts'   => $cat_attr['pad_counts'],

					 'hierarchical' => $cat_attr['hierarchical'],

					 'title_li'     => $cat_attr['title'],

					 'hide_empty'   => $cat_attr['empty']

			  );

			 $all_categories = get_categories( $args );

			 $output .= "<ul class=\"category-chekbox\">";

			 $sql = "select `cat_id` from ".$table_cat." where user_id = ".$user_ID;	 

			 $result = $wpdb->get_results($sql);

			 $selected_cat = array();

			 if(count($result)>0){

				foreach($result as $rs){

					$selected_cat[] = $rs->cat_id;

				}

			 }

			//print_r($selected_cat);

			 foreach ($all_categories as $cat) {

				if($cat->category_parent == 0) {

					$category_id = $cat->term_id;	

					$children = get_term_children($category_id, 'product_cat');

					if (!empty($children)) {

						$sub_cat_class = "genericon-rightarrow";//has childeren	

						//$collapser = "<span class='plus catg minus'>&nbsp;</span>";			

					}     

					else{

						$sub_cat_class = "genericon-noarrow";

						$collapser = "";

					}	

					// first level

					$output	.=	"<li><span class=\"checkbox-wrap\"><input type=\"checkbox\" class=\"checkbox-custom\" id=\"product-checkbox-".str_replace(' ','_',$cat->name)."\" name='product_category[]' value='".$category_id."'".(in_array($category_id,$selected_cat)?" checked":"")."><label class=\"checkbox-custom-label\" for=\"product-checkbox-".str_replace(' ','_',$cat->name)."\">". $cat->name ."</label></span>".$collapser;

					

					//second level

					$args2 = array(

							'taxonomy'     => 'product_cat',

							'child_of'     => 0,

							'parent'       => $category_id,

							'orderby'      => 'term_order',

							'show_count'   => $cat_attr['show_count'],

							'pad_counts'   => $cat_attr['pad_counts'],

							'hierarchical' => $cat_attr['hierarchical'],

							'title_li'     => $cat_attr['title'],

							'hide_empty'   => $cat_attr['empty']

					);

					$sub_cats = get_categories( $args2 );

					//print_r($sub_cats);

					if($sub_cats) {

						$output .= "<ul class=\"sub-category\">";

						foreach($sub_cats as $sub_category) {

							$sub_category_id = $sub_category->term_id;

							$sub_children = get_term_children($sub_category_id, 'product_cat');

							if (!empty($sub_children)) {

								$sub_cat_class = "genericon-rightarrow has-children";//has childeren	

								$collapser = "<span class='plus subcatg minus'>&nbsp;</span>";			

							}     

							else{

								$sub_cat_class = "genericon-noarrow";

								$collapser = "";

							}

							$output	.=	"<li><span class=\"checkbox-wrap\"><input type=\"checkbox\" class=\"checkbox-custom\" id=\"product-checkbox-".str_replace(' ','_',$sub_category->name)."\" name=\"product_category[]\" value='".$sub_category_id."'".(in_array($sub_category_id,$selected_cat)?" checked":"")."><label class=\"checkbox-custom-label\" for=\"product-checkbox-".str_replace(' ','_',$sub_category->name)."\">". $sub_category->name ."</label></span>".$collapser;

							

							//third level

							$args3 = array(

									'taxonomy'     => 'product_cat',

									'child_of'     => 0,

									'parent'       => $sub_category_id,

									'orderby'      => 'term_order',

									'show_count'   => $cat_attr['show_count'],

									'pad_counts'   => $cat_attr['pad_counts'],

									'hierarchical' => $cat_attr['hierarchical'],

									'title_li'     => $cat_attr['title'],

									'hide_empty'   => $cat_attr['empty']

							);

							$sub_sub_cats = get_categories( $args3 );

							

							if($sub_sub_cats) {

								$output .= "<ul class=\"sub-sub-category\">";

								foreach($sub_sub_cats as $sub_sub_category) {

									$sub_sub_category_id = $sub_sub_category->term_id;

									$output	.=	"<li><span class=\"checkbox-wrap\"><input class=\"checkbox-custom\" type='checkbox' id=\"product-checkbox-".str_replace(' ','_',$sub_sub_category->name)."\" name='product_category[]' value='".$sub_sub_category_id."'".(in_array($sub_sub_category_id,$selected_cat)?" checked":"")."><label class=\"checkbox-custom-label\" for=\"product-checkbox-".str_replace(' ','_',$sub_sub_category->name)."\">". $sub_sub_category->name ."</label></span><br></li>";

								}

								$output .="</ul><!-- sub-sub-category -->";

							}

							$output .="</li>";

						}

						$output .="</ul><!-- sub-category -->";

					}//end subact

					$output .="</li>";

				}  //end cat     

			}

			$output	  .= "</ul>";

			$output	  .= "<input class='haggle_button fullwidth_button' type='submit' name='cat_submit' value='Update Category'/>

							</form>";

			$output      .="<script type='text/javascript'>

							jQuery(document).ready(function(){

								jQuery('ul.category-chekbox span.plus').click(function(){

								  jQuery(this).parent().find('ul.sub-category').toggle();

								  jQuery(this).parent().find('span.catg').toggleClass('minus');

								  return false;						  

								});

								jQuery('ul.sub-category span.plus').click(function(){

								  jQuery(this).parent().find('ul.sub-sub-category').toggle();

								  jQuery(this).parent().find('span.subcatg').toggleClass('minus');

								  return false;						  

								});
								jQuery('#select-all').click(function(event) {   
									if(this.checked) {
										// Iterate each checkbox
										jQuery(':checkbox').each(function() {
											this.checked = true;                        
										});
									}else{
										jQuery(':checkbox').each(function() {
											this.checked = false;                        
										});
									}
								});

							});

							</script>";

			

			  }else{

				$output = "This option is for retailer only";

			  }

		}else{

			$output = "<script type=\"text/javascript\">

							window.location.href=\"".home_url('my-account')."\";

					   </script>";

		}

	  

	return $output;

}

add_shortcode('product_category_form','product_category_form');

//shortcode for auto bid category with % discount
function product_category_autobid_form($atts){
	global $wpdb;
	//check is user logged in else redirect to homepage
		if(is_user_logged_in ())
		{ 
			$user_ID = get_current_user_id();
			$user = new WP_User( $user_ID );
			$role = $user->roles[0];
			$table_autobid_cat = $wpdb->prefix . 'retailer_autobid_cats';
			$cat_attr = shortcode_atts( array(
				'orderby' 		  => 'term_order',		
				'title' 			=> '',		
				'show_count' 	   =>'0',	 // 1 for yes, 0 for no
				'pad_counts'	   => '0',	// 1 for yes, 0 for no
				'hierarchical'	 => '1',	  // 1 for yes, 0 for no
				'empty'			=> '0'		
			), $atts );	  
			  if($role == "retailer-account"){
			  if ($_POST['cat_submit']) {
				  
				 
				  if(count($_POST['product_category'])>0){
						$product_categories = implode(",",$_POST['product_category']);
						///// delete the entry if there is a product category not assigned for this user on next save
						$wpdb->query("delete from $table_autobid_cat where user_id=\"$user_ID\" AND cat_id NOT IN($product_categories)");

						
						foreach($_POST['product_category'] as $cid){
							$discount = $_POST['autobid_discount_'.$cid];
							$sql = "select * from ".$table_autobid_cat." where user_id = ".$user_ID. " and cat_id = ".$cid;
							$result = $wpdb->get_results($sql);
							
							
							if(count($result)>0){
								//check if the cat discount is updated or not
								if($discount == "" ){
									//update discount									
									$update_val = array(												
											'discount'   => "" 	
										);
									 $where = array('user_id' => $user_ID,'cat_id'	 => $cid);	
									 $update = $wpdb->update( $table_autobid_cat, $update_val,$where);
								}elseif($result->discount!=$discount){
									//update discount									
										$update_val = array(												
												'discount'   => $discount 	
											);									 
									  $where = array('user_id' => $user_ID,'cat_id'	 => $cid);	
										$update = $wpdb->update( $table_autobid_cat, $update_val,$where);
								}else{
									continue;
								}
							}
							else{
								//insert if new category is selected
								$insert_val = array(
												'user_id'	=> $user_ID,
												'cat_id'	 => $cid,
												'discount'   => $discount 	
											);								
								
								$insert = $wpdb->insert( $table_autobid_cat, $insert_val);
							}
						}
				  }//end if
				  else{
					///// delete the entry if there is a product category not assigned for this user on next save
						$wpdb->query('delete from '.$table_autobid_cat.' where user_id='.$user_ID);						
				  }
				}			  

			  $output  	   = "<form class='category-form' action='".esc_url($_SERVER['REQUEST_URI'])."' method='post'>
			  					<span class=\"checkbox-wrap\"><input type=\"checkbox\" class=\"checkbox-custom\" id=\"select-all\" name=\"select-all\" ><label class=\"checkbox-custom-label\" for=\"select-all\">Select all</label></span>";	  

			  

			  $args = array(
					 'taxonomy'     => 'product_cat',
					 'orderby'      => 'term_order',
					 'show_count'   => $cat_attr['show_count'],
					 'pad_counts'   => $cat_attr['pad_counts'],
					 'hierarchical' => $cat_attr['hierarchical'],
					 'title_li'     => $cat_attr['title'],
					 'hide_empty'   => $cat_attr['empty']
			  );

			 $all_categories = get_categories( $args );
			 $output .= "<ul class=\"category-chekbox\">";
			 $sql = "select * from ".$table_autobid_cat." where user_id = ".$user_ID;
			 $result = $wpdb->get_results($sql);
			 $selected_cat = array();
			 $discount_applied = array();
			 if(count($result)>0){
				 $i=0;
				foreach($result as $rs){
					$selected_cat[$i] = $rs->cat_id;
					$discount_applied[$rs->cat_id] = $rs->discount;
					$i++;
				}
			 }
			
			 foreach ($all_categories as $cat) {
				if($cat->category_parent == 0) {
					$category_id = $cat->term_id;
					$children = get_term_children($category_id, 'product_cat');
					if (!empty($children)) {
						$sub_cat_class = "genericon-rightarrow";//has childeren	
					}   

					else{

						$sub_cat_class = "genericon-noarrow";

						$collapser = "";

					}	

					// first level

					$output	.="<li>
									<span class=\"checkbox-wrap\">
										<input type=\"checkbox\" class=\"checkbox-custom\" id=\"product-checkbox-".str_replace(' ','_',$cat->name)."\" name='product_category[]' value='".$category_id."'".(in_array($category_id,$selected_cat)?" checked":"").">
											<label class=\"checkbox-custom-label\" for=\"product-checkbox-".str_replace(' ','_',$cat->name)."\">". $cat->name ."</label>
									</span>".$collapser;
					if($discount_applied[$category_id]){
						$discount_amount = $discount_applied[$category_id];
					}else{
						$discount_amount = "";
					}

					// discount field
					$output .="<span class=\"input-wrap\">
									<label for=\"autobid_discount_$category_id\">Discount(%)</label>
									<input type=\"text\" class=\"autobid-discount\" value=\"".$discount_amount."\"/ name=\"autobid_discount_$category_id\" id=\"autobid_discount_$category_id\">									
								</span>";
					$output .="</li>";

				}  //end cat    
				
			}

			$output	  .= "</ul>";

			$output	  .= "<input class='haggle_button fullwidth_button' type='submit' name='cat_submit' value='Update Category'/>

							</form>";

			$output      .="<script type='text/javascript'>

							jQuery(document).ready(function(){
								
								jQuery('#select-all').click(function(event) {   
									if(this.checked) {
										// Iterate each checkbox
										jQuery(':checkbox').each(function() {
											this.checked = true;                        
										});
									}else{
										jQuery(':checkbox').each(function() {
											this.checked = false;                        
										});
									}
								});

							});

							</script>";

			

			  }else{

				$output = "This option is for retailer only";

			  }

		}else{

			$output = "<script type=\"text/javascript\">

							window.location.href=\"".home_url('my-account')."\";

					   </script>";

		}

	  

	return $output;

}

add_shortcode('product_category_autobid_form','product_category_autobid_form');

//shortcode for autobid brand
function autobid_brand_management_widget(){

	$selected_category = array();
	global $wpdb;

	//check is user logged in else redirect to homepage

	if(is_user_logged_in ())

	{ 

		$user_ID = get_current_user_id();

		$user = new WP_User($user_ID);

		$role = $user->roles[0];

		$table_brand = $wpdb->prefix . 'retailer_autobid_brands';

		if($role == "retailer-account"){


			if($_POST['brand_submit']){

					if(count($_POST['brands'])>0){

					    $selected_brand = implode(",",$_POST['brands']);				

					    $wpdb->query("delete from $table_brand where user_id=\"$user_ID\" AND `brand_id` NOT IN($selected_brand)");

					}else{

					    $wpdb->query("delete from $table_brand where user_id=\"$user_ID\"");

					}

					if(count($_POST['brands'])>0){						

						foreach($_POST['brands'] as $brand_id){

						//check wether there is entry of brand w.r.t selected categories for this retailer

						$sql = "select * from ".$table_brand." where user_id = ".$user_ID. " AND brand_id = ".$brand_id;

						$result = $wpdb->get_results($sql);					

						if(count($result)>0){
							//do nothing
							continue;
						}
						else{
							//insert
							$insert_val = array(
											'user_id' 	  => $user_ID,
											'brand_id'	=> $brand_id										
										);						

							$insert = $wpdb->insert( $table_brand, $insert_val);
						}
						}//end of brand id
					}
			}

			$output = "<div class=\"brand-management\"><h3>Selected Brands</h3><p><span style=\"color:#ff0000\">***</span> Please tick brands below which will receive the category discount when auto quoting. Any brands not selected will be auto quoted without discount.</p>";

			$output .= "<form name=\"save_brand\" class=\"save-brand\" method=\"post\"><ul class='selected-category'>
						<span class=\"checkbox-wrap\"><input type=\"checkbox\" class=\"checkbox-custom\" id=\"select-all\" name=\"select-all\" ><label class=\"checkbox-custom-label\" for=\"select-all\">Select all</label></span>";

				$output	.=	"<li>".get_brands($user_ID,$table_brand)."</li>";

			$output .= "</ul>";

			$output	  .= "<input class='haggle_button fullwidth_button' type='submit' name='brand_submit' value='Update Brands'/>

							</form>";

			$output .="</div>";

			$output .="<script type='text/javascript'>
						jQuery(document).ready(function(){
							jQuery('#select-all').click(function(event) {   
								if(this.checked) {
									// Iterate each checkbox
									jQuery(':checkbox').each(function() {
										this.checked = true;                        
									});
								}else{
									jQuery(':checkbox').each(function() {
										this.checked = false;                        
									});
								}
							});
						});
						</script>";
		}else{
			$output = "This option is for retailer only";
		}
	}else{
			$output = "<script type=\"text/javascript\">
							window.location.href=\"".home_url('my-account')."\";
					   </script>";
		}
	return $output;
}

add_shortcode('manage-autobid-brand','autobid_brand_management_widget');

//Brand management for retailer

function brand_management_widget(){
	$selected_category = array();
	global $wpdb;
	//check is user logged in else redirect to homepage
	if(is_user_logged_in ())
	{ 
		$user_ID = get_current_user_id();

		$user = new WP_User($user_ID);

		$role = $user->roles[0];

		//$table_cat = $wpdb->prefix . 'retailer_cats';

		$table_brand = $wpdb->prefix . 'retailer_brands';

		if($role == "retailer-account"){
			if($_POST['brand_submit']){

					if(count($_POST['brands'])>0){

					    $selected_brand = implode(",",$_POST['brands']);				

					    $wpdb->query("delete from $table_brand where user_id=\"$user_ID\" AND `brand_id` NOT IN($selected_brand)");

					}else{

					    $wpdb->query("delete from $table_brand where user_id=\"$user_ID\"");

					}

					if(count($_POST['brands'])>0){						

						foreach($_POST['brands'] as $brand_id){

						//check wether there is entry of brand w.r.t selected categories for this retailer

						$sql = "select * from ".$table_brand." where user_id = ".$user_ID. " AND brand_id = ".$brand_id;

						$result = $wpdb->get_results($sql);					

						if(count($result)>0){
							//do nothing
							continue;
						}
						else{
							//insert
							$insert_val = array(
											'user_id' 	  => $user_ID,
											'brand_id'	=> $brand_id										
										);						
							$insert = $wpdb->insert( $table_brand, $insert_val);
						}

						}//end of brand id

					}
			}
			$output = "<div class=\"brand-management\"><h3>Selected Brands</h3>";
			$output .= "<form name=\"save_brand\" class=\"save-brand\" method=\"post\"><ul class='selected-category'>
						<span class=\"checkbox-wrap\"><input type=\"checkbox\" class=\"checkbox-custom\" id=\"select-all\" name=\"select-all\" ><label class=\"checkbox-custom-label\" for=\"select-all\">Select all</label></span>";

				$output	.=	"<li>".get_brands($user_ID,$table_brand)."</li>";

			$output .= "</ul>";

			$output	  .= "<input class='haggle_button fullwidth_button' type='submit' name='brand_submit' value='Update Brands'/>

							</form>";

			$output .="</div>";

			$output .="<script type='text/javascript'>
						jQuery(document).ready(function(){
							jQuery('#select-all').click(function(event) {   
								if(this.checked) {
									// Iterate each checkbox
									jQuery(':checkbox').each(function() {
										this.checked = true;                        
									});
								}else{
									jQuery(':checkbox').each(function() {
										this.checked = false;                        
									});
								}
							});
						});
						</script>";

		}else{

			$output = "This option is for retailer only";

		}

	}else{

			$output = "<script type=\"text/javascript\">

							window.location.href=\"".home_url('my-account')."\";

					   </script>";
		}
	return $output;
}

add_shortcode('brand-management','brand_management_widget');



//Shortcode for quotation request for retailers on frontend

function quotation_request_widget() {
	global $wpdb, $pagenum, $limit, $start;
	//check is user logged in else redirect to homepage
	
        if(trim(get_option('job_limit'))){
			$limit = get_option('job_limit');
		}else{
	 		$limit = 10; // number of jobs in page
		}
	   $pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;
	   $start =  ($pagenum - 1) * $limit;	
	   
	   $offset = ( $pagenum - 1 ) * $limit;
	   
	
	if(is_user_logged_in ())
	{ 
		$user_ID = get_current_user_id();
		$user = new WP_User($user_ID);
		$role = $user->roles[0];
		//display the shortccde only for retailer
		if($role == "retailer-account"){
			$table_quotation = $wpdb->prefix . 'jobquotation';
			$table_jobs = $wpdb->prefix . 'jobs';
			
			$sqlTotal = "SELECT count(*) as total FROM $table_quotation as qt, $table_jobs as jt WHERE jt.id = qt.jobId AND jt.status !=10 AND  qt.retailerId=\"".$user_ID."\" AND qt.status != 2 ORDER BY qt.assignedDate DESC";
			$sqlTotal_results = $wpdb->get_row($sqlTotal);
			$total = $sqlTotal_results->total;
			$num_of_pages = ceil( $total / $limit );
			
			$paginate_end = $limit*$pagenum;
			  if($paginate_end>$total){
			   $paginate_end = $total;
			  }
			
			$sql = "SELECT qt.id,qt.jobId,qt.quotationAmount,qt.status FROM $table_quotation as qt, $table_jobs as jt WHERE jt.id = qt.jobId AND jt.status !=10 AND  qt.retailerId=\"".$user_ID."\" AND qt.status != 2 ORDER BY qt.assignedDate DESC  LIMIT $start, $limit";
			$quote_results = $wpdb->get_results($sql);
			$output = "<div class=\"quote-request\">";
			$output .="<div class=\"listing-header\">
			<span class=\"ji\">Job Id</span>
			<span class=\"jt\">Job Title</span>
			<span class=\"jpo\">Job Posted On</span>
			<span class=\"qa\">Job Total</span>
			<span class=\"jst\">Job Status</span>
			<span class=\"qst\">Quote Status</span>
			<span class=\"act\">Action</span>
			</div><!-- listing-header -->";
			$output .="<div class=\"listing-body\">";
			if(count($quote_results)>0){
			$cnt = 0;	
			foreach($quote_results as $quote_result){
				$job = get_job("",$quote_result->jobId);
				
				$product = wc_get_product( $job->productId );	
				//================if product is deleted======================//
				if(!$product) continue;
				//======================================//
				$user_info = get_userdata($job->userId);				
				$first_name = $user_info->first_name;
				$last_name = $user_info->last_name;
				$verify_code = rand(999999,1000000);	
				$job_total =get_job_total($quote_result->id);											 
				$quotation_url = home_url('retailer-job-detail?&quote_id='.$quote_result->id);
				
				$output .="<div quote_status=\"".$quote_result->status."\"job_status=\"".$job->status."\"class=\"quote-list-rt\">
				<span class=\"ji\">".$quote_result->jobId."</span>
				<span class=\"jt\">".$product->get_title()."</span>
				<span class=\"jpo\">".date("l, F j, Y",strtotime($job->jobDate))."</span>
				<span class=\"qa\">".($job_total=='0'?'No quotation yet':'$'.$job_total)."</span>
				<span class=\"jst\">".($job->status=="1"?'<p class="haggle_pending">Pending</p>':'<p class="haggle_pending">Closed</p>')."</span>
				<span class=\"qst\">".($quote_result->status=="0"?'<p class="haggle_pending">Pending</p>':'<p class="haggle_submit">Submitted'.($quote_result->status=="-1"?"<br>/Rejected":"").($quote_result->autoBidStatus=="1"?"<br>/Auto Bid":"").($quote_result->autoBidUpdateStatus=="1"?"<br>/Manual Updated":"").'</p>')."</span>
				<span class=\"act\"><a href=\"".$quotation_url."\" >".(($quote_result->status=="1" || $job->status == "2")?"View Details":"Post Quotation")."</a></span>
				</div><!-- quote-list-rt -->";
				   
				$cnt++;            				
			}
			
			$page_links = paginate_links( array(
			'base' => add_query_arg( 'pagenum', '%#%' ),
			'format' => '',
			'prev_text' => __( '&laquo;', 'text-domain' ),
			'next_text' => __( '&raquo;', 'text-domain' ),
			'total' => $num_of_pages,
			'current' => $pagenum
		) );

		if ( $page_links ) {
			 //$output .= '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $page_links . '</div></div>';
			 $output.="<div class=\"haggle-tablenav\"><div class=\"haggle-now-showing\">Showing ".($start+1)."&ndash;".($paginate_end)." of $total results</div><div class=\"tablenav-pages\" style=\"margin: 1em 0\">$page_links</div></div>";
		}
		
			if($cnt == 0)
				$output .= "Sorry no jobs available for you.";
			}else{
				$output .= "Sorry no jobs available for you.";
			}
				$output .="</div><!-- listing-body -->
				</div><!-- quote-request -->";
		}else{
			$output = "This option is for retailer only";
		}
	}else{
		$output = "<script type=\"text/javascript\">
		window.location.href=\"".home_url('my-account')."\";
		</script>";
	}
return $output;
}

add_shortcode('quotation-request', 'quotation_request_widget');

//shortcode for job detail page for retailer

function retailer_job_detail_widget(){

	global $wpdb;

	//check is user logged in else redirect to homepage

	if(is_user_logged_in ())

	{

		$user_ID = get_current_user_id();

		$user = new WP_User($user_ID);

		$role = $user->roles[0];

		//display the shortccde only for retailer

		if($role == "retailer-account"){

			$table_jobquotation = $wpdb->prefix . 'jobquotation';

			$table_job = $wpdb->prefix . 'jobs';

			$quote_id = $_GET['quote_id'];

			$quote_sql = "SELECT * FROM $table_jobquotation WHERE `id`=$quote_id";

			$quote_result = $wpdb->get_row($quote_sql);

			if(count($quote_result)>0)

			{
				$retiler_id = $quote_result->retailerId;

				if($user_ID==$retiler_id){
		 			wp_enqueue_style( 'smaillipop_css', plugins_url( '/css/jquery.smallipop.css', __FILE__ ) );

					wp_enqueue_script('modernizer_js',plugins_url( '/js/modernizr.js' , __FILE__ ),array( 'jquery' ));

					//wp_enqueue_script('smallipop_js',plugins_url( '/js/jquery.smallipop.js' , __FILE__ ),array( 'jquery' ));

					$job = get_job("",$quote_result->jobId);

					///get extended warranty info section

					$job_sql = "SELECT `is_extended_warranty` FROM $table_job WHERE id=".$quote_result->jobId;

					$job_result = $wpdb->get_row($job_sql);

					$job_quantity = $job->quantity;

					$is_extended_warranty = $job_result->is_extended_warranty;					

					////////

					///get dynamic attributes ////

					$das = get_dynamic_job_attributes($quote_result->jobId);

					$additional_information ="<div class=\"retailer-job-fieldset\">

									<div class=\"fieldset-title\">Job additional information</div>

									<div class=\"fieldset-content\">" ;

									

					if(count($das)>0){									

						foreach($das['key'] as $key => $value){

							if($value == "wccpf_when_do_you_need_the_product")

								$additional_information .="<span><strong>".$das['label'][$value]." : </strong> ".product_delivery_switch($das['customer_choice'][$value])."</span>";

							else

							$additional_information .="<span><strong>".$das['label'][$value]." : </strong> ".ucfirst($das['customer_choice'][$value])."</span>";

						}

					}

					$additional_information .="<span><strong>Usage Type :   ".strtoupper((($job->is_commercial=='1'&&(strtotime($job->jobDate)<strtotime('2016-09-15 23:59:59')))?'Commercial':'Private'))."</strong></span>";

					if($is_extended_warranty!=''){

						 $additional_information.="<span><strong>Extended Warranty :</strong>   $is_extended_warranty</span>";

					 }

					$additional_information .="</div></div><!-- retailer-job-fieldset -->";

					//////////dynamic attributes ends////////////////

					///get delivery info section

					$delivery_option = get_job_meta($quote_result->jobId,'wccpf_deliverypickup');					

					////get installation info section

					$installation_option = get_job_meta($quote_result->jobId,'wccpf_installation_required');	

					////get removal of old appliance section	

					$removal_of_old_appliance = get_job_meta($quote_result->jobId,'wccpf_removal_of_old_appliance');	

					////get oven type section	

					$oven_type = get_job_meta($quote_result->jobId,'wccpf_oven_type');			

					$product = wc_get_product( $job->productId );

					$job_title = $product->get_title();

					$job_title_sku = $product->get_sku();
					
					$product_price = $product->get_price();

					$user_info = get_userdata($job->userId);				

					$first_name = $user_info->first_name;

					$last_name = $user_info->last_name;

					$job_status = $job->status; // 2 closed job

					$output = " <script type=\"text/javascript\" src=\"http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js\"></script>\n

					<script type=\"text/javascript\" src=\"".plugins_url( '/js/jquery.smallipop.js' , __FILE__ )."\"></script>

					<div class=\"quote-detail\">";

					$output .= "<div class=\"retailer-job-fieldset\">

									<div class=\"fieldset-title\">Job detail</div>	

									<div class=\"fieldset-content\" id=\"".$quote_result->jobId."\">
									
									<span><strong>Job Id : </strong> ".$quote_result->jobId." </span>

									<span><strong>Job Title : </strong> $job_title </span>

									<span><strong>Posted Date : </strong> ".date("l, F j, Y",strtotime($job->jobDate))."<br /></span>

									<span><strong>Customer's postcode: </strong> ".$job->Postcode."</span>";

					if((get_option('show_phone_job_form')!="1") && ($quote_result->status == 2))

						$output .= "	 <span><strong>Best Number to contact Customer: </strong> ".$job->telephone."</span>";

					$output .= "

									</div>							

								</div>";

					$output .= $additional_information;

			// get contact details sql				   

					$table_retailer_contacts = $wpdb->prefix.'retailer_contacts';					

					$autosql = "SELECT DISTINCT `contactName`,`contactDetails` FROM $table_retailer_contacts WHERE `retailerId`=\"".$user_ID."\" ORDER BY id desc";

					$auto_results = $wpdb->get_results($autosql);								   

					 if ($_POST['submit_quote']){

						 $quotation_amount  = $_POST['quotation_amount'];
						 
						 $ticketAmount 	  = $_POST['ticketAmount'];
						 
						 $comments		  = $_POST['comments'];

						 $product_warranty  = $_POST['product_warranty'];

						 $warranty_price    = $_POST['warranty_price'];

						 $warranty_years    = $_POST['warranty_years'];

						 $product_condition = $_POST['product_condition'];

						 $product_comment   = $_POST['product_comment'];


						 /// quotation question ///

					$postArray = array();

					$table_jobquotation_meta = $wpdb->prefix.'quotationmeta';

					$quotation_meta_sql ="SELECT * from $table_jobquotation_meta WHERE quote_id=".$quote_id;

					$result1 = $wpdb->get_results($quotation_meta_sql);

					$metaDataArray = array();

					foreach($result1 as $result2){

						$metaDataArray[$result2->meta_key]=$result2->meta_value;

					}

					if(count($das)>0){

					foreach($das['key'] as $key => $value){

								

						if($das['type'][$value]=="radio" || $das['type'][$value]=="select"){

							$price_requirement = "price_".$value;

							if($das['customer_choice'][$value] == "delivery"){

							  $postArray[$price_requirement]=$_POST[$price_requirement];

							}else{

							      $meet_requirement = "meet_".$value;	

								  $postArray[$meet_requirement]=$_POST[$meet_requirement];

								  $postArray[$price_requirement]=$_POST[$price_requirement];							

							}

						  $comment_requirement = "comment_".$value;

						  $postArray[$comment_requirement]=$_POST[$comment_requirement];

						}

				      }

					}

					// end for dynamic question

						 $wpdb->query( "DELETE FROM $table_jobquotation_meta WHERE `quote_id` = '$quote_id'" );

						 foreach($postArray as $key=> $postData){

							 if(trim($postData) !=""){							 								

								$sql ="INSERT INTO $table_jobquotation_meta VALUES('','$quote_id','$key','$postData')";

								$wpdb->query( $sql ); 

							 }

						 }

						//echo "</pre>".$sql; 

						 $terms_and_conditions = $_POST['terms_and_conditions'];

						 $contact_person = $_POST['contact_person'];

						 $contact_details = $_POST['contact_details'];

						 $quote_date = "SELECT NOW() AS times";

						 $result1 = $wpdb->get_row($quote_date);

						 $quote_time = $result1->times;

						 $update_val = array(

						 				'quotationAmount'   => $quotation_amount,	

										'ticketAmount'	  => $ticketAmount,										

										'warranty_price'	=> $warranty_price,

										'warranty_years'	=> $warranty_years,																			

										'product_condition' => $product_condition,

										'product_comment'   => $product_comment,										

										'comments'		  => $comments,

										'product_warranty'  => $product_warranty,

										'terms_and_conditions'=> $terms_and_conditions,

										'contact_person'=> $contact_person,

										'contact_details'=> $contact_details,

										'status'	=>	1,
										
										'autoBidUpdateStatus' => $_POST['autoBidUpdateStatus'],

										'quotationDate'	=>$quote_time);

						 $where = array('id' => $quote_id);

						 $updated = $wpdb->update($table_jobquotation,$update_val,$where);

						 // update terms & contindion in user meta

						 update_user_meta( $user_ID, 'retailer_terms_and_conditions', $terms_and_conditions );

						 $checkAutosql = "SELECT *  FROM $table_retailer_contacts WHERE `retailerId`=\"".$user_ID."\" AND `contactName` =  \"".$contact_person."\" AND `contactDetails` = \"".addslashes($contact_details)."\"";

					     $checkAuto_results = $wpdb->get_row($checkAutosql);

						 if(count($checkAuto_results)==0){

							 $table_retailer_contacts_val = array(

								'retailerId' 	  => $user_ID,

								'contactName'     => $contact_person,

								'contactDetails'  => $contact_details

							);	

		                    $wpdb->insert( $table_retailer_contacts, $table_retailer_contacts_val);	

						 }

						 if ( false === $updated ) {

							// There was an error.

							$mssg ="<div class=\"haggle_alert danger\">There was an error while posting quotation</div>";

						}else{

							$mssg ="<div class=\"haggle_alert success\">Quotation posted successfully </div>";

						}

					 }

					 else{

							$quotation_sql ="SELECT * from $table_jobquotation WHERE id=".$_GET['quote_id'];

							$result = $wpdb->get_row($quotation_sql);

							$quotation_amount = $result->quotationAmount;
							
							$ticketAmount = $result->ticketAmount;

							$comments = $result->comments;

							$product_warranty = $result->product_warranty;							

							$warranty_price = $result->warranty_price;

							$warranty_years = $result->warranty_years;

							$product_condition = $result->product_condition;

							$product_comment = $result->product_comment;
							
							$autoBidStatus = $result->autoBidStatus;


							$terms_and_conditions =	$result->terms_and_conditions;

							$contact_person =	$result->contact_person;

							$contact_details =	$result->contact_details;
							// get terms & conditions from user meta

							if(trim($terms_and_conditions)==""){

								$terms_and_conditions = get_user_meta( $user_ID, 'retailer_terms_and_conditions', true );

							}

							// get contact person and it's details

							if(trim($contact_person)==""){

								if(count($auto_results) > 0){

										$contact_person =	$auto_results[0]->contactName;

										$contact_details =   $auto_results[0]->contactDetails;

								}

							}
					}
					

					/// quotation question ///

					$quotation_dynamic_questions = "";

					$jquery_attribute = "";

					$table_jobquotation_meta = $wpdb->prefix.'quotationmeta';

					$quotation_meta_sql ="SELECT * from $table_jobquotation_meta WHERE quote_id=".$quote_id;

					$result1 = $wpdb->get_results($quotation_meta_sql);

					$metaDataArray = array();

					foreach($result1 as $result2){

						$metaDataArray[$result2->meta_key]=$result2->meta_value;

					}

					if(count($das)>0){

					foreach($das['key'] as $key => $value){

						if(($das['customer_choice'][$value] == "yes") || ($value == "wccpf_when_do_you_need_the_product") || ($value == "wccpf_deliverypickup"))

							$quotation_dynamic_questions .="<ul class=\"fieldset-row fieldset-special\">";

									

						if($value == "wccpf_when_do_you_need_the_product")

							$quotation_dynamic_questions .="<li><h3>The product is for <span>".product_delivery_switch($das['customer_choice'][$value])." ".ucfirst($das['customer_choice']['wccpf_deliverypickup'])."</span></h3>";

						elseif($value == "wccpf_deliverypickup")

							$quotation_dynamic_questions .="<li><h3>Customer requires <span>".ucfirst($das['customer_choice'][$value])."</span></h3>";

						else{

							if($das['customer_choice'][$value] == "yes")

								$quotation_dynamic_questions .="<li><h3>Customer requires <span>".ucfirst($das['label'][$value])."</span></h3>";

								

						}

						

							

						if($das['type'][$value]=="radio" || $das['type'][$value]=="select"){

							$price_requirement = "price_".$value;

							$meet_requirement = "meet_".$value;

							if($das['customer_choice'][$value] == "delivery"){								

								$quotation_dynamic_questions .= ($job_quantity>0?'<div class="special-inner">':'')."

												<label>Delivery Price $".(($job->is_commercial=='1'&&(strtotime($job->jobDate)<strtotime('2016-09-15 23:59:59')))?'(Ex GST)':'(Inc GST)')."</label>

												<input type=\"text\" value=\"".$metaDataArray[$price_requirement]."\" name=\"$price_requirement\" class=\"required\" />".($job_quantity>0?'<span>per item</span>':'')."

												".($job_quantity>0?'</div>':'');

												

							}elseif($value == "wccpf_when_do_you_need_the_product" || $das['customer_choice'][$value] == "pickup"){

								

								$quotation_dynamic_questions .= "<label>Can you meet the customer's requirement?</label>

																<span class=\"radio-wrap\">

																<input type=\"radio\" value=\"yes\" id=\"product-radio-$value-yes\" name=\"$meet_requirement\" class=\"product-radio required\" ".(($metaDataArray[$meet_requirement]=='yes') ? 'checked' : null).">

																<label class=\"radio-custom-label\" for=\"product-radio-$value-yes\">Yes</label>									

															</span>

															<span class=\"radio-wrap\">

																<input type=\"radio\" value=\"no\" id=\"product-radio-$value-no\" name=\"$meet_requirement\" class=\"product-radio required\" ".(($metaDataArray[$meet_requirement]=='no') ? 'checked' : null).">

																<label class=\"radio-custom-label\" for=\"product-radio-$value-no\">No</label>									

															</span>";	

							}

							else{

								

							if($das['customer_choice'][$value] == "yes"){						

							$quotation_dynamic_questions .= "<label>Can you meet the customer's requirement?</label>

															 <span class=\"radio-wrap\">

																<input type=\"radio\" value=\"yes\" id=\"product-radio-$value-yes\" name=\"$meet_requirement\" class=\"product-radio required\" ".(($metaDataArray[$meet_requirement]=='yes') ? 'checked' : null).">

																<label class=\"radio-custom-label\" for=\"product-radio-$value-yes\">Yes</label>									

															</span>

															<span class=\"radio-wrap\">

																<input type=\"radio\" value=\"no\" id=\"product-radio-$value-no\" name=\"$meet_requirement\" class=\"product-radio required\" ".(($metaDataArray[$meet_requirement]=='no') ? 'checked' : null).">

																<label class=\"radio-custom-label\" for=\"product-radio-$value-no\">No</label>									

															</span>"

															.($job_quantity>0?"<div class=\"special-inner $meet_requirement\" ".((($metaDataArray[$meet_requirement]=='no')||($metaDataArray[$meet_requirement]==''))?'style="display:none"':'').">":"")."

															<label>Price $</label>

															<input type=\"text\" value=\"".$metaDataArray[$price_requirement]."\" name=\"$price_requirement\" class=\"$price_requirement\" />".($job_quantity>0?'<span>per item</span>':'')."

															".($job_quantity>0?'</div>':'')."</li>";

															

							$jquery_attribute .= "jQuery(\"#product-radio-$value-yes\").click(function() {

										   if(jQuery(\"#product-radio-$value-yes\").is(\":checked\")) { 

											 jQuery(\".$meet_requirement\").show();

											 jQuery(\".$price_requirement\").addClass(\"required\");

										   }

										});										

										jQuery(\"#product-radio-$value-no\").click(function() {

										   if(jQuery(\"#product-radio-$value-no\").is(':checked')) { 

											 jQuery(\".$meet_requirement\").hide();

											 jQuery(\".$price_requirement\").val('');

											 jQuery(\".$price_requirement\").removeClass(\"required\");

										   }

										});	";

							

							}

							}

						}

						if($is_extended_warranty=='yes'){

							$quotation_dynamic_questions .= "<div class=\"fieldset-content ".($job_quantity>0?'fieldset-special':'')."\">

											".($job_quantity>0?'<div class="special-inner">':'')."

											<label>Warranty Price $".(($job->is_commercial=='1'&&(strtotime($job->jobDate)<strtotime('2016-09-15 23:59:59')))?'(Ex GST)':'(Inc GST)')."</label>

											<input type=\"text\" value=\"".$warranty_price."\" name=\"warranty_price\" class=\"required\" />".($job_quantity>0?'<span>per item</span>':'')."

											".($job_quantity>0?'</div>':'')."

										</div>";

							$quotation_dynamic_questions .= "<div class=\"fieldset-content\">

											<div class=\"special-inner\">

											<label>Warranty Years</label>

											<input type=\"text\" value=\"".$warranty_years."\" name=\"warranty_years\"/>

											</div>

										</div>";

						}

						if(($das['customer_choice'][$value] == "yes") || ($value == "wccpf_when_do_you_need_the_product") || ($value == "wccpf_deliverypickup")){

							$quotation_dynamic_questions .= "<script type=\"text/javascript\">

										jQuery(document).ready(function(){";

							$quotation_dynamic_questions .= $jquery_attribute;

							$quotation_dynamic_questions .="														

										});

									</script>";

						}

						if(($das['customer_choice'][$value] == "yes") || ($value == "wccpf_when_do_you_need_the_product") || ($value == "wccpf_deliverypickup")){

						$quotation_dynamic_questions .= "<li><label>Comments ".($value == "wccpf_when_do_you_need_the_product"?"on Timeframe":($value == "wccpf_deliverypickup"?"on ".ucfirst($das['customer_choice'][$value]):"if required"));

						

						$comment_requirement = "comment_".$value;

						$smallipop_comment = "smallipop_".$value;

						if(get_option($smallipop_comment)!=""){

							$quotation_dynamic_questions .= "<span class=\"smallipop haggle-hint\" style=\"display:inline-block;\">

													&nbsp;

													<span class=\"smallipop-hint\">

														".get_option($smallipop_comment)."

													</span>

												</span>";

						}

						$quotation_dynamic_questions .="</label>

							<textarea name=\"$comment_requirement\">".stripslashes($metaDataArray[$comment_requirement])."</textarea>

						</li>";

						}

						if(($das['customer_choice'][$value] == "yes") || ($value == "wccpf_when_do_you_need_the_product") || ($value == "wccpf_deliverypickup"))

							$quotation_dynamic_questions .="</ul>";

						

					}

					}// end for dynamic question

					

					

					

					$cheapest_quote = get_current_cheapest($quote_result->jobId);	
									 
					$cheapest_quote_mssg = ($cheapest_quote=="No quotes submitted"?$cheapest_quote:"Current Cheapest Quote =  <span>$$cheapest_quote</span>");
					
					$output .= "<div class=\"retailer-job-fieldset\">

									<div class=\"fieldset-title\">Quotation$mssg</div>

									<h3>Product Required : <span>$job_title</span></h3>

									<h3>SKU : <span>$job_title_sku</span></h3>

									<h3>QTY Required : <span>$job_quantity</span></h3>";
					$output .= "<h3>$cheapest_quote_mssg</h3>";

					$output .= "<div class=\"fieldset-content\">";

					if($job_status !=2)

					$output.="				<form method=\"post\" name=\"retailer-job-quotation-form\" id=\"retailer-job-quotation-form\" action=\"\">";

					

					$output.="					<ul class=\"fieldset-row ".($job_quantity>0?'fieldset-special':'')."\">

											<li>

											".($job_quantity>0?'<div class="special-inner ticket-price">':'')."

											<label class=\"comment-label\">Ticket Price $".(($job->is_commercial=='1'&&(strtotime($job->jobDate)<strtotime('2016-09-15 23:59:59')))?'(Ex GST)':'(Inc GST)')."</label>

											<input type=\"text\" value=\"".$ticketAmount."\" id=\"ticketAmount\" name=\"ticketAmount\" class=\"required\" />".($job_quantity>0?"<span>per item</span>":"")."

											".($job_quantity>0?'</div>':'').($job_quantity>0?'<br /><div class="special-inner">':'')."

											<label class=\"comment-label\">HAGGLEFREE Price<br />$&nbsp;".(($job->is_commercial=='1'&&(strtotime($job->jobDate)<strtotime('2016-09-15 23:59:59')))?'(Ex GST)':'(Inc GST)')."</label>

											<input type=\"text\" value=\"".$quotation_amount."\" id=\"quotation_amount\" name=\"quotation_amount\" class=\"required\" />".($job_quantity>0?"<span>per item</span>":"")."

											".($job_quantity>0?'</div>':'')."

											</li>

												<li><label>Comment on the quoted price";

												if(get_option('comment_quoted_price')!=""){

										$output .= "<span class=\"smallipop haggle-hint\" style=\"display:inline-block;\">

													&nbsp;

													<span class=\"smallipop-hint\">

														".get_option('comment_quoted_price')."

													</span>

												</span>";

												}

											

										$output.="</label> 

											<textarea name=\"comments\">".stripslashes($comments)."</textarea></li>

										</ul>

										<!--<ul class=\"fieldset-row\">

											<li><label>Comments on Delivery Timeframe</label> </li>

											<li><textarea name=\"timeframe_comments\">".stripslashes($timeframe_comments)."</textarea></li>

										</ul>-->";

					$output .= "<ul class=\"fieldset-row\">

									<li>

										<label>Product Condition</label>

										<select name=\"product_condition\" class=\"condition-select required\"> 

											<option classname=\"invalid\" value=\"bn_in_box\" ".(($product_condition=='bn_in_box') ? 'selected' : null).">Brand New in Box</option>

											<option classname=\"invalid\" value=\"bn_clearance_item\" ".(($product_condition=='bn_clearance_item') ? 'selected' : null).">Brand New in Box(Clearance Item)</option>

											<option classname=\"invalid\" value=\"bn_carton_damaged\" ".(($product_condition=='bn_carton_damaged') ? 'selected' : null).">Brand New(Carton Damaged)</option>

											<option classname=\"invalid\" value=\"bn_floor_model\" ".(($product_condition=='bn_floor_model') ? 'selected' : null).">Brand New out of Box(floor model or Demo)</option>
											
											<option classname=\"invalid\" value=\"fc_second\" ".(($product_condition=='fc_second') ? 'selected' : null).">Factory Second</option>

											<option classname=\"invalid\" value=\"fc_carton_damaged\" ".(($product_condition=='fc_carton_damaged') ? 'selected' : null).">Factory Second(Carton Damaged)</option>

											<option classname=\"invalid\" value=\"fc_refurbished\" ".(($product_condition=='fc_refurbished') ? 'selected' : null).">Factory Second(Refurbished)</option>

											<option classname=\"invalid\" value=\"fc_scratch_dent\" ".(($product_condition=='fc_scratch_dent') ? 'selected' : null).">Factory Second(Scratch & Dent)</option>

											<option classname=\"invalid\" value=\"pre_owned\" ".(($product_condition=='pre_owned') ? 'selected' : null).">Pre-Owned Product</option>

										</select>

									</li>

									<li>

										<label>Condition and Warranty if not Brand New";

												if(get_option('comment_warranty')!=""){

										$output .= "<span class=\"smallipop haggle-hint\" style=\"display:inline-block;\">

													&nbsp;

													<span class=\"smallipop-hint\">

														".get_option('comment_warranty')."

													</span>

												</span>";

												}

											

										$output.="</label>

										<textarea name=\"product_warranty\">".stripslashes($product_warranty)."</textarea>

									</li>

								</ul>";


					$output  .= $quotation_dynamic_questions;

					$output  .= "<div class=\"fieldset-content\">

						    	 	<label>Offer Comments</label> 

									<textarea name=\"terms_and_conditions\">".stripslashes($terms_and_conditions)."</textarea>

								</div>

								<div class=\"fieldset-content\">

									<label>Contact Person</label> 

									<input type=\"text\" value=\"".$contact_person."\" name=\"contact_person\" id=\"contact_person\" class=\"required\" />

								</div>

								<div class=\"fieldset-content\">

									<label>Contact Details</label> 

									<input type=\"text\" value=\"".$contact_details."\" name=\"contact_details\" id=\"contact_details\" class=\"required\" />								

								</div>";

					if($job_status !=2)

						$output  .="<div class=\"fieldset-content\">

									<input type=\"submit\" value=\"".($result->status=='0'?'Post Quotation':'Update Quotation')."\" name=\"submit_quote\">

								</div>
                                    <input type=\"hidden\" name=\"autoBidUpdateStatus\" value=".($autoBidStatus==1?1:0)." />
									<input type=\"hidden\" name=\"jobId\" value=".$result->jobId." /></form>";
									
                                     
					

					$output .="</div>

								</div>";

					$output .= "<div class=\"haggle-back-to\"><a class=\"haggle-link\" href=\"".home_url('quotation-request')."\">Back to list</a></div></div>";

					$output .= "<script type=\"text/javascript\">

									jQuery(document).ready(function(){

										jQuery(\".smallipop\").smallipop();

										jQuery('#quotation_amount').keyup(function(){																						
											jQuery('#quotation_amount').val(jQuery('#quotation_amount').val().replace(/[^\d.]/g, ''));
										});
										
										jQuery('#ticketAmount').keyup(function(){																						
											jQuery('#ticketAmount').val(jQuery('#ticketAmount').val().replace(/[^\d.]/g, ''));
										});										
										
										
										jQuery('#retailer-job-quotation-form').submit(function() {
											var original_price = parseFloat(".$product_price.");
											var ticket_amount = parseFloat(jQuery.trim(jQuery(\"#ticketAmount\").val()));
											var haggle_amount = parseFloat(jQuery.trim(jQuery(\"#quotation_amount\").val()));											
											if (haggle_amount == \"0\" && ticket_amount == \"0\") {
												alert('Enter any one of HAGGLEFREE price or Ticket price');
												return false;
											}
											else if(ticket_amount == \"0\" &&  haggle_amount > 0) {
												jQuery(\"#ticketAmount\").val( jQuery.trim(jQuery(\"#quotation_amount\").val()));
											}
											else if(ticket_amount > 0 &&  haggle_amount  == \"0\") {
												jQuery(\"#quotation_amount\").val( jQuery.trim(jQuery(\"#ticketAmount\").val()));
											}
											else if(ticket_amount < haggle_amount) {
												alert('Ticket price should be more than or equal HAGGLEFREE price');
												return false;
											}
											else if(original_price > 0 && original_price < ticket_amount){
												alert('Warning: Ticket price is higher than RRP $".number_format($product_price,2)."');
											}
											else{
												if (haggle_amount == \"0\"){												
												      jQuery(\"#quotation_amount\").val(jQuery.trim(jQuery(\"#ticketAmount\").val()));
												}
												if (ticket_amount == \"0\"){ 
												    jQuery(\"#ticketAmount\").val( jQuery.trim(jQuery(\"#quotation_amount\").val()));
												}
												return true;
											}
										});
										
										jQuery(\"#removal-old-appliance-yes\").click(function() {

										   if(jQuery(\"#removal-old-appliance-yes\").is(\":checked\")) { 

											 jQuery(\".roa\").show();

											 jQuery(\".removal_price\").addClass(\"required\");

										   }

										});										

										jQuery(\"#removal-old-appliance-no\").click(function() {

										   if(jQuery(\"#removal-old-appliance-no\").is(':checked')) { 

											 jQuery(\".roa\").hide();

											 jQuery(\".removal_price\").removeClass(\"required\");

										   }

										});

										jQuery(\"#meet-requirement-install-yes\").click(function() {

										   if(jQuery(\"#meet-requirement-install-yes\").is(\":checked\")) { 

											 jQuery(\".requirement-install\").show();

											 jQuery(\".installation_price\").addClass(\"required\");

										   }

										});										

										jQuery(\"#meet-requirement-install-no\").click(function() {

										   if(jQuery(\"#meet-requirement-install-no\").is(':checked')) { 

											 jQuery(\".requirement-install\").hide();

											 jQuery(\".installation_price\").removeClass(\"required\");

										   }

										});

										jQuery(\"#retailer-job-quotation-form\").validate();

									});

								</script>";

		// Autocomplit contact person and details

		if(count($auto_results)>0){

			$name = "";

			$contactDetails = "";

			  $index = 0;

			  foreach($auto_results as $auto_result){

				 if($name ==""){

					 $name = "{label:\"".$auto_result->contactName."\",idx:$index}";

				  }else{

					 $name .= ",{label:\"".$auto_result->contactName."\",idx:$index}"; 

				  }

				  

				  if($contactDetails == ""){

					 $contactDetails = "'".strip_tags(str_replace(","," " ,$auto_result->contactDetails))."'";

				  }else{

					 $contactDetails .= ",'".strip_tags(str_replace(","," " ,$auto_result->contactDetails))."'"; 

				  }			  

				  $index++;

			   }
			   

				$output .= "<script src=\"https://code.jquery.com/ui/1.12.0/jquery-ui.js\"></script>\n
				<script type=\"text/javascript\">\n
							jQuery( function() {\n
								var nameTags = [".$name."];\n
								var conatctTags = [".$contactDetails."];\n
								jQuery( \"#contact_person\" ).autocomplete({\n
							  		source: nameTags,\n
							  		select: function(event, ui) {jQuery( \"#contact_details\" ).val(conatctTags[ui.item.idx])}\n
								});	\n
						  	});\n
						  </script>\n";			   

			   

				}//same user logged in and retailer

				}else{

					$output.="Opps something went wrong.Please click <a class=\"haggle-link\" href=\"".home_url('quotation-request')."\">here</a> to go back to quotation list.";

				}

			}

		}else{

			$output = "This option is for retailer only";

		}

	}else{

		$output = "<script type=\"text/javascript\">

						window.location.href=\"".home_url('my-account')."\";

				   </script>";

	}

	

	return $output;

}

add_shortcode('retailer-job-detail','retailer_job_detail_widget');



//Shortcode for selected quotation for buyer on frontend

function buyer_accepted_quotations($atts) {

		 $quotation_attr = shortcode_atts( array(			

			'limit' => '3'// limitation of quotation		

		), $atts );

		global $wpdb;

		//check is user logged in else redirect to homepage

		if(is_user_logged_in ())

		{ 

			$user_ID = get_current_user_id();

			$user = new WP_User($user_ID);

			$role = $user->roles[0];

			//display the shortccde only for retailer

			if($role == "buyer-account"){

				$table_jobs = $wpdb->prefix . 'jobs';

				$sql = "SELECT * FROM $table_jobs WHERE `userId`=\"".$user_ID."\" AND `status`=\"3\" ORDER BY quotationSendDate desc LIMIT 0 , ".$quotation_attr['limit']."";

				$job_results = $wpdb->get_results($sql);

				$output = "<div class=\"job-list\">";

				if(count($job_results)>0){

					$output .="<div class=\"listing-header\"><span class=\"jt\">Job Title</span><span class=\"jpo\">Job Posted On</span><span class=\"qa\">Quotation Amount</span><span class=\"act\">Action</span></div>";

					$output .="<div class=\"listing-body\">";

				foreach($job_results as $job_result){

						$job = get_job("",$job_result->jobId);		

						$product = wc_get_product( $job->productId );	

						if(!$product) continue;

						$user_info = get_userdata($job->userId);				

						$first_name = $user_info->first_name;

						$last_name = $user_info->last_name;

						$verify_code = rand(999999,1000000);

						$quotation_url = home_url('retailer-job-detail?&quote_id='.$job_result->id);

						$output .="<div class=\"job-list-rt\"><span class=\"jt\">".$product->get_title()."</span><span class=\"jpo\">".date("l, F j, Y",strtotime($job->jobDate))."</span><span class=\"qa\">".($job_result->quotationAmount=='0'?'No quotation yet':$job_result->quotationAmount)."</span><span class=\"act\"><a href=\"".$quotation_url."\" >".($job_result->status=="0"?"Post Quotation":"View Details")."</a></span></div>";                				

					}

					$output .="<div class=\"more\"><a href=\"".home_url('my-jobs')."\">More</a></div></div>";

				

				}else{

					$output .= "Sorry no jobs available for you.";

				}

				$output .="</div>";

			}else{

				$output = "This option is for buyer only";

			}

		}else{

			$output = "<script type=\"text/javascript\">

							window.location.href=\"".home_url('my-account')."\";

					   </script>";

		}

		return $output;

	

}

add_shortcode('buyer-accepted-quotations', 'buyer_accepted_quotations');



//Shortcode for selected quotation for retailer on frontend

function get_retailer_jobs_st($atts) {
	$retailer_jobs_attr = shortcode_atts( array(
	'status' => ''// status of quotation				
	), $atts );
	global $wpdb, $pagenum, $limit, $start;
	
	if(trim(get_option('job_limit'))){
			$limit = get_option('job_limit');
	}else{
	 	$limit = 10; // number of jobs in page
	}
   $pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;
   $start =  ($pagenum - 1) * $limit;	
   
   $offset = ( $pagenum - 1 ) * $limit;
	   
	//check is user logged in else redirect to homepage
	if(is_user_logged_in ())
	{ 
		$user_ID = get_current_user_id();
		$user = new WP_User($user_ID);
		$role = $user->roles[0];
		//display the shortccde only for retailer
		if($role == "retailer-account"){
			$table_quotation = $wpdb->prefix . 'jobquotation';
			
			$sqlTotal = "SELECT count(*) as total FROM $table_quotation WHERE `retailerId`=\"".$user_ID."\" AND `status`=\"".$retailer_jobs_attr['status']."\" ORDER BY quotationDate DESC ";
			$sqlTotal_results = $wpdb->get_row($sqlTotal);
			$total = $sqlTotal_results->total;
			$num_of_pages = ceil( $total / $limit );
			
			$paginate_end = $limit*$pagenum;
			  if($paginate_end>$total){
			   $paginate_end = $total;
			  }
			  
			$sql = "SELECT * FROM $table_quotation WHERE `retailerId`=\"".$user_ID."\" AND `status`=\"".$retailer_jobs_attr['status']."\" ORDER BY quotationDate DESC LIMIT $start, $limit ";
			/*if($user_ID == "185"){
			echo $sql;
			}*/
			$job_results = $wpdb->get_results($sql);
			$output = "<div class=\"quote-list\">";
			if(count($job_results)>0){
				$output .="<div class=\"listing-header\"><span class=\"ji\">Job Id</span><span class=\"jt\">Job Title</span><span class=\"jpo\">Job Posted On</span><span class=\"qa\">Job Total</span><span class=\"act\">Action</span></div>";
				$output .="<div class=\"listing-body\">";
				foreach($job_results as $job_result){
					$job = get_job("",$job_result->jobId);		
					$product = wc_get_product( $job->productId );	
					if(!$product) continue;
					$user_info = get_userdata($job->userId);				
					$first_name = $user_info->first_name;
					$last_name = $user_info->last_name;
					$verify_code = rand(999999,1000000);
					$job_total =get_job_total($job_result->id);
					$quotation_url = home_url('selected-quotation-details?&quote_id='.$job_result->id);
					$output .="<div class=\"quote-list-rt\"><span class=\"ji\">".$job_result->jobId."</span><span class=\"jt\">".$product->get_title()."</span><span class=\"jpo\">".date("l, F j, Y",strtotime($job->jobDate))."</span><span class=\"qa\">".($job_total=='0'?'No quotation yet':'$'.$job_total)."</span><span class=\"act\"><a href=\"".$quotation_url."\" >".($job_result->status=="0"?"Post Quotation":"View Details")."</a></span></div>";                				
				}
				$page_links = paginate_links( array(
					'base' => add_query_arg( 'pagenum', '%#%' ),
					'format' => '',
					'prev_text' => __( '&laquo;', 'text-domain' ),
					'next_text' => __( '&raquo;', 'text-domain' ),
					'total' => $num_of_pages,
					'current' => $pagenum
				) );
		
				if ( $page_links ) {
					 $output.="<div class=\"haggle-tablenav\"><div class=\"haggle-now-showing\">Showing ".($start+1)."&ndash;".($paginate_end)." of $total results</div><div class=\"tablenav-pages\" style=\"margin: 1em 0\">$page_links</div></div>";
				}
				$output .="<!-- <div class=\"more\"><a href=\"".home_url('quotation-request')."\">More</a></div> --></div>";
			}else{
				$output .= "Sorry no quotation available.";
			}
			$output .="</div>";
			return $output;
		}else{
			$output = "This option is for buyer only";
		}
	}else{
		$output = "<script type=\"text/javascript\">
		window.location.href=\"".home_url('my-account')."\";
		</script>";
	}
	return $output;
}

add_shortcode('get-retailer-jobs', 'get_retailer_jobs_st');



function my_preferences_function(){

	global $wpdb;

	if(is_user_logged_in ())

		{

			$user_ID = get_current_user_id();

			$user = new WP_User( $user_ID );			

			$email = $user->user_email;

			

if(isset($_POST['submit_preferance'])){

 	

	update_user_meta( $user_ID, 'first_name', trim($_POST['first_name']));

	update_user_meta( $user_ID, 'last_name', trim($_POST['last_name']));

	update_user_meta( $user_ID, 'cr_pcode', trim($_POST['cr_pcode']));

	update_user_meta( $user_ID, 'cr_phone', trim($_POST['cr_phone']));





	$table_subs = $wpdb->prefix."subscribe";

	

	if(count($_POST['subscribs'])>0){

	$subscribs = implode(",",$_POST['subscribs']);

	

	$sql = "SELECT * FROM $table_subs WHERE `email` = '$email'";

	$result = $wpdb->get_row($sql);

	

	if($result){

		$update_val = array(

			   'hotDeals'   => $subscribs

		);

		$where = array('email' => $email);

		$wpdb->update($table_subs,$update_val,$where);		

	}else{

		$insert_val = array(

					'email'      => $email,

					'hotDeals'   => $subscribs         

		);

		$insert = $wpdb->insert( $table_subs, $insert_val);

	}

	}else{

		$wpdb->delete( $table_subs, array( 'email' => $email ) );

	}

}



$output = "<div class=\"my-preference\">

						<div class=\"buyer-job-fieldset\">

							<div class=\"preferences-title\">Your Details</div>	

							<div class=\"fieldset-content\">";

ob_start();							

?>

<form action="" method="post" name="preference" id="preference">

<div class="user-infos" id="user-infos">

  <ul class="form-list">

    <li id="user-info-form">

        <ul>

          <li class="fields">

            <div class="customer-name-middlename">

              <div class="field name-firstname">

                <label class="required" for="name"><em>*</em>First Name</label>

                <div class="input-box">

                  <input type="text" class="input-text required-entry required" maxlength="255" title="First Name" value="<?=get_user_meta( $user_ID, 'first_name', true )?>" name="first_name" id="first_name">

                </div>

              </div>              

            </div>

          </li>

          <li class="fields">

            <div class="customer-name-middlename">

              <div class="field name-firstname">

                <label class="required" for="name"><em>*</em>Last Name</label>

                <div class="input-box">

                  <input type="text" class="input-text required-entry required" maxlength="255" title="Last Name" value="<?=get_user_meta( $user_ID, 'last_name', true )?>" name="last_name" id="last_name">

                </div>

              </div>              

            </div>

          </li>

          <li class="wide">

            <label class="required" for="email"><em>*</em>Email Address</label>

            <div class="input-box">

              <input class="input-text validate-email required-entry required" title="Email Address" readonly="readonly" value="<?=$user->user_email?>" id="email" name="email" type="email">

            </div>

          </li>

          <li class="fields">

            <div class="field">

              <label class="required" for="postcode"><em>*</em>Postcode</label>

              <div class="input-box">

                <input type="text" maxlength="4" class="input-text validate-zip-international required-entry required" title="Postcode" value="<?=get_user_meta( $user_ID, 'cr_pcode', true )?>" id="cr_pcode" name="cr_pcode">

              </div>

            </div>            

          </li>          

          <li class="fields">

            <div class="field">

              <label class="required" for="telephone"><em>*</em>Best Number to contact you on</label>

              <div class="input-box">

                <input type="text" class="input-text required-entry required" title="Telephone" value="<?=get_user_meta( $user_ID, 'cr_phone', true )?>" id="cr_phone" name="cr_phone">

              </div>

            </div>

          </li>          

        </ul>

    </li>

  </ul> 

</div>

<?php							

$ob_content2 = ob_get_contents();

ob_end_clean();

        $output .= $ob_content2;						

		$output .= "</div>

						</div>

						<div class=\"buyer-job-fieldset\">

							<div class=\"preferences-title\">Marketing Selection</div>

							<p class=\"notify\">Notify me via email when there are HOT deals on:</p>";









ob_start();

$table_subs = $wpdb->prefix."subscribe";

$sql = "SELECT * FROM $table_subs WHERE `email` = '$email'";

$result = $wpdb->get_row($sql);



$subs = array();

if($result){

 $subs = explode(",",$result->hotDeals);

}

?>

<ul>

    <li><input type="checkbox" class="subscribs" name="subscribs[]" value="White Goods"<?=(in_array("White Goods",$subs)?" checked=\"checked\"":"")?> />White Goods</li>

    <li><input type="checkbox" class="subscribs" name="subscribs[]" value="Television & Audio equipment"<?=(in_array("Television & Audio equipment",$subs)?" checked=\"checked\"":"")?> />Television & Audio equipment</li>

    <li><input type="checkbox" class="subscribs" name="subscribs[]" value="Cooling & Heating"<?=(in_array("Cooling & Heating",$subs)?" checked=\"checked\"":"")?> />Cooling & Heating</li>

    <li><input type="checkbox" class="subscribs" name="subscribs[]" value="Bathroom Fixtures"<?=(in_array("Bathroom Fixtures",$subs)?" checked=\"checked\"":"")?> />Bathroom Fixtures</li>

    <li><input type="checkbox" class="subscribs" name="subscribs[]" value="Hot Water Systems"<?=(in_array("Hot Water Systems",$subs)?" checked=\"checked\"":"")?> />Hot Water Systems</li>

    <li><input type="checkbox" class="subscribs" name="subscribs[]" value="BBQ"<?=(in_array("BBQ",$subs)?" checked=\"checked\"":"")?> />BBQ</li>

</ul>

<div class="fieldset-content buttonsection">

 <input type="submit" name="submit_preferance" value="Update">

</div>

</form>

<?php

$ob_content = ob_get_contents();

ob_end_clean();

$output .= $ob_content;



							$output .= "</div>";

			

		}else{

		

		$output = "<script type=\"text/javascript\">

							window.location.href=\"".home_url('my-account')."\";

					   </script>";

		}

		return $output;

}

add_shortcode('my-preferences', 'my_preferences_function');

//manage account
function manage_account_function(){
	global $wpdb;
	$table_usermeta = $wpdb->prefix."usermeta";
	if(is_user_logged_in ())
		{
			$user_ID = get_current_user_id();
			$user = new WP_User( $user_ID );			
			$email = $user->user_email;
			
			
			if(isset($_POST['update_account'])){
				
				// upload photo
				$profile_image_url = $wpdb->get_results("SELECT `meta_value` FROM `$table_usermeta` WHERE `user_id` = '$user_ID' && `meta_key` = 'cr_photo'");
				// file upload
				$upload_dir = wp_upload_dir();
				$target_dir = $upload_dir['basedir']."/retailer-avatar/";
				$orignal_dir = $upload_dir['basedir']."/retailer-avatar/orig/";
				
				$uniquename = substr(microtime(),2,8).rand(1000, 9999);
				$oldname = strtolower($_FILES['cr_photo']['name']);
				$newname = preg_replace('/(.*)\.(.*)$/i', $uniquename.'.$2', $oldname);
				$medium_name = (str_replace('.','_medium.',$newname));	
				$original_name  = (str_replace('.','_orig.',$newname));
					
				$target_file = $target_dir . $medium_name;
				$orig_target_file = $orignal_dir . $original_name;
				
				$uploadOk = 1;
				$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
				$width =250;
				$height = 100;
				
				if(!is_uploaded_file($_FILES['cr_photo']['tmp_name'])) {
					$cr_photo   = $profile_image_url[0]->meta_value;
				}
				else{
					$check = getimagesize($_FILES["cr_photo"]["tmp_name"]);
					if($check !== false) {				
						$uploadOk = 1;
					} else {
						echo 'File is not an image.';				
						$uploadOk = 0;
					}
				}
				if(is_uploaded_file($_FILES['cr_photo']['tmp_name'])) {
					if ($_FILES["cr_photo"]["size"] > 2000000) {
					echo 'Sorry, your file is too large.';			
					$uploadOk = 0;
				} 
				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
					echo 'Sorry, only JPG, JPEG, PNG & GIF files are allowed.';			
					$uploadOk = 0;
				} 
				if ($uploadOk == 0) {
					echo 'Sorry, your file was not uploaded.';
					
				} else {
					/* Get original image x y*/
					list($w, $h) = getimagesize($_FILES['cr_photo']['tmp_name']);
					/* calculate new image size with ratio */
					$ratio = $w / $h;
					if (($width / $height) > $ratio) {
						   $width = $height * $ratio;
					} else {
						   $height = $width / $ratio;
					}	
					
					/* read binary data from image file */
					$imgString = file_get_contents($_FILES['cr_photo']['tmp_name']);
					/* create image from string */
					$image = imagecreatefromstring($imgString);
					$tmp = imagecreatetruecolor($width, $height);
					imagecopyresampled($tmp, $image, 0, 0, $x, 0, $width, $height, $w, $h);
					
					/* Save image */
					switch ($_FILES['cr_photo']['type']) {
						case 'image/jpeg':
							imagejpeg($tmp, $target_file, 100);
							imagejpeg($image, $orig_target_file, 100);
							break;
						case 'image/png':
							imagepng($tmp, $target_file, 0);
							imagepng($image, $orig_target_file, 0);
							break;
						case 'image/gif':
							imagegif($tmp, $target_file);
							imagegif($image, $orig_target_file);
							break;
						default:
							exit;
							break;
					}
					
					$_mgm_cf_photo   =  $upload_dir['baseurl']."/retailer-avatar/".$medium_name;
					update_user_meta($user_ID, 'cr_photo', $_mgm_cf_photo);
				}
			}
				
				update_user_meta( $user_ID, 'first_name', trim($_POST['first_name']));
				update_user_meta( $user_ID, 'last_name', trim($_POST['last_name']));	
				update_user_meta( $user_ID, 'cr_phone', trim($_POST['cr_phone']));
				update_user_meta( $user_ID, 'cr_company', trim($_POST['cr_company']));
				update_user_meta( $user_ID, 'cr_street', trim($_POST['cr_street']));
				update_user_meta( $user_ID, 'cr_suburb', trim($_POST['cr_suburb']));
				update_user_meta( $user_ID, 'cr_pcode', trim($_POST['cr_pcode']));
				
				// Update if exist the auto bid fields
				update_user_meta( $user_ID, 'auto_quote', trim($_POST['auto_quote']));
				update_user_meta( $user_ID, 'radius_range', trim($_POST['radius_range']));
				update_user_meta( $user_ID, 'delivery_price_auto_quote', trim($_POST['delivery_price_auto_quote']));
				update_user_meta( $user_ID, 'delivery_comment_auto_quote', trim($_POST['delivery_comment_auto_quote']));				
				update_user_meta( $user_ID, 'instock_timeframe_comment_auto_quote', (trim($_POST['instock_timeframe_comment_auto_quote'])!=""?trim($_POST['instock_timeframe_comment_auto_quote']):"The product is in stock, contact us for confirmation of delivery schedules"));
				
				update_user_meta( $user_ID, 'outstock_timeframe_comment_auto_quote', (trim($_POST['outstock_timeframe_comment_auto_quote'])!=""?trim($_POST['outstock_timeframe_comment_auto_quote']):"Currently not in stock, will be ordered in and timeframe confirmed ASAP"));
				
				update_user_meta( $user_ID, 'rooa', trim($_POST['rooa']));
				update_user_meta( $user_ID, 'rooa_price', trim($_POST['rooa_price']));
				update_user_meta( $user_ID, 'rooa_comment', trim($_POST['rooa_comment']));
				//update_user_meta( $user_ID, 'installation_price_auto_quote', trim($_POST['installation_price_auto_quote']));
				update_user_meta( $user_ID, 'installation_comment_auto_quote', trim($_POST['installation_comment_auto_quote']));
				update_user_meta( $user_ID, 'contact_person_auto_quote', trim($_POST['contact_person_auto_quote']));
				update_user_meta( $user_ID, 'contact_details_auto_quote', trim($_POST['contact_details_auto_quote']));
				update_user_meta( $user_ID, 'pickup_radius_auto_quote', trim($_POST['pickup_radius_auto_quote']));
				update_user_meta( $user_ID, 'offer_pickup', trim($_POST['offer_pickup']));
				update_user_meta( $user_ID, 'pickup_comment', trim($_POST['pickup_comment']));
				    
	          	//========================== Check Feed url and insert feed data ==============================
				
				$oldFeedUrl = get_user_meta($user->ID,'feed_url',true);
			  	$newFeedUrl = trim($_POST['feed_url']);
			  
			  	if($newFeedUrl!="" && $oldFeedUrl != $newFeedUrl ){					
				    update_user_meta( $user_ID, 'feed_url', trim($_POST['feed_url']));
					$tableFeed = $wpdb->prefix . 'feedRetalProducts';
					$homepage = trim(file_get_contents($newFeedUrl));
					$data = explode("\n",$homepage);
					$retailerId = $user_ID;
					$count=0;
					if(count($data) > 0){
					foreach($data as $rec){
						if(trim($rec)!="")
						{   
							$count++;
							$fsku = "";
							$price = "";
							$stockQ = "";
							$condition = "";
							$url = "";
							$mprice = "";
											
							$pdata = explode("|",$rec);
							
							if(trim($pdata[0])!=""){
								$fsku = trim($pdata[0]);
							}
							$price = (float)trim($pdata[3]);
							$stockQ = trim($pdata[4]);
							$condition = trim($pdata[5]);
							$url = trim($pdata[6]);
							if($pdata[7]){				
								$mprice = (float)trim($pdata[7]);
							}else{
								$mprice = 0;
							}
							
							
							 $sql = "SELECT * FROM $tableFeed WHERE `retailerId` = '$retailerId' AND `sku` = '$fsku'";
							 $result = $wpdb->get_row($sql);
							 if($result){
								$sqlupdate = "UPDATE $tableFeed SET `price` = '$price',`mprice` = '$mprice',`qty`='$stockQ', `productCondition` = '$condition',`productUrl` = '$url',`status` = '2' WHERE `retailerId` = '$retailerId' AND `sku` = '$fsku'"; 
								$wpdb->query($sqlupdate);
							 }else{
								$sqlInsert = "INSERT INTO $tableFeed SET `price` = '$price',`mprice` = '$mprice',`qty`='$stockQ',`retailerId` = '$retailerId',`sku` = '$fsku', `productCondition` = '$condition',`productUrl` = '$url',`status` = '2'"; 
								$wpdb->query($sqlInsert);
							 }
							unset($pdata);
						}
						   
					}
					if(count($data) == $count){
						$sqlDelete = "DELETE FROM $tableFeed WHERE `retailerId` = '$retailerId' AND `status` = '1'";
						$wpdb->query($sqlDelete);
					}
					$sqlUpdate = "UPDATE $tableFeed SET `status` = '1' WHERE `retailerId` = '$retailerId' AND `status` = '2'";
					$wpdb->query($sqlUpdate);
				  }
			}
				//================================== End Feed data checking =====================
				
				//update_user_meta( $user_ID, 'cr_avtar', trim($_POST['cr_avtar']));
				
			}
$profile_image_url = $wpdb->get_results("SELECT `meta_value` FROM `$table_usermeta` WHERE `user_id` = '$user_ID' && `meta_key` = 'cr_photo'");
$output = "<div class=\"my-preference manage-account\">
						<div class=\"buyer-job-fieldset\">
							<div class=\"preferences-title\">Your Details</div>	
							<div class=\"fieldset-content\">";
ob_start();							
?>
<form action="" method="post" name="manage_account" id="manage_account" enctype="multipart/form-data">
<div class="user-infos" id="user-infos">
  <ul class="form-list">
    <li id="user-info-form">
        <ul>
          <li class="fields">
            <div class="customer-name-middlename">
              <div class="field name-firstname">
                <label class="required" for="name"><em>*</em>First Name</label>
                <div class="input-box">
                  <input type="text" class="input-text" maxlength="255" title="First Name" value="<?=get_user_meta( $user_ID, 'first_name', true )?>" name="first_name" id="first_name">
                </div>
              </div>              
            </div>
          </li>
          <li class="fields">
            <div class="customer-name-middlename">
              <div class="field name-firstname">
                <label class="required" for="name"><em>*</em>Last Name</label>
                <div class="input-box">
                  <input type="text" class="input-text" maxlength="255" title="Last Name" value="<?=get_user_meta( $user_ID, 'last_name', true )?>" name="last_name" id="last_name">
                </div>
              </div>              
            </div>
          </li>
          <li class="wide">
            <label class="required" for="email"><em>*</em>Email Address</label>
            <div class="input-box">
              <input class="input-text" title="Email Address" readonly="readonly" value="<?=$user->user_email?>" id="email" name="email" type="email">
            </div>
          </li>
          
          <li class="fields">
            <div class="field">
              <label class="required" for="telephone"><em>*</em>Best Number to contact you on</label>
              <div class="input-box">
                <input type="text" class="input-text required-entry required" title="Telephone" value="<?=get_user_meta( $user_ID, 'cr_phone', true )?>" id="cr_phone" name="cr_phone">
              </div>
            </div>
          </li>
          <li class="fields">
            <div class="customer-name-middlename">
              <div class="field name-firstname">
                <label class="required" for="cr_company"><em>*</em>Company Name</label>
                <div class="input-box">
                  <input type="text" class="input-text" maxlength="255" title="First Name" value="<?=get_user_meta( $user_ID, 'cr_company', true )?>" name="cr_company" id="cr_company">
                </div>
              </div>              
            </div>
          </li>
          <li class="fields">
            <div class="customer-name-middlename">
              <div class="field name-firstname">
                <label class="required" for="cr_street"><em>*</em>Street Address</label>
                <div class="input-box">
                  <input type="text" class="input-text" maxlength="255" title="First Name" value="<?=get_user_meta( $user_ID, 'cr_street', true )?>" name="cr_street" id="cr_street">
                </div>
              </div>              
            </div>
          </li>
          <li class="fields">
            <div class="customer-name-middlename">
              <div class="field name-firstname">
                <label class="required" for="cr_suburb"><em>*</em>Suburb</label>
                <div class="input-box">
                  <input type="text" class="input-text" maxlength="255" title="First Name" value="<?=get_user_meta( $user_ID, 'cr_suburb', true )?>" name="cr_suburb" id="cr_suburb">
                </div>
              </div>              
            </div>
          </li>
          
          <li class="fields">
            <div class="field">
              <label class="required" for="postcode"><em>*</em>Postcode</label>
              <div class="input-box">
                <input type="text" maxlength="4" class="input-text validate-zip-international required-entry required" title="Postcode" value="<?=get_user_meta( $user_ID, 'cr_pcode', true )?>" id="cr_pcode" name="cr_pcode">
              </div>
            </div>            
          </li> 
          <li class="fields">
            <div class="field">
              <label for="feed_url">Feed URL</label>
              <div class="input-box">
                <input type="text" class="input-text" title="Feed URL" value="<?=get_user_meta( $user_ID, 'feed_url', true )?>" id="feed_url" name="feed_url">
              </div>
            </div>
          </li> 
          <li class="fields">
          	<div class="photo_box field">
                <ul>
                    <li class="photo_img">
                    <?php if($profile_image_url[0]->meta_value !=""){?>
                    <img src="<?=$profile_image_url[0]->meta_value?>" />
                    <?php }?>
                    </li>
                    <li class="upload"><span><input type="file" name="cr_photo" id="cr_photo"/><a id="fileSelect" href="javascript:void(0);">&nbsp;</a></span>[size 250X100]</li>
                </ul>      
              </div>
          </li>  
          <!-- Auto bid fields starts-->
          <li class="fields">
            <div class="field field-auto-quote">
              <label for="auto_quote">Auto Quote</label>
              <div class="input-box">              
              	<span class="radio-wrap">
                    <input type="radio" <?=(get_user_meta( $user_ID, 'auto_quote', true )=="yes"?"checked":null)?> class="product-radio" name="auto_quote" id="product-radio-auto_quote-yes" value="yes">
                    <label for="product-radio-auto_quote-yes" class="radio-custom-label">Yes</label>									
                </span>
                <span class="radio-wrap">
                    <input type="radio" <?=(get_user_meta( $user_ID, 'auto_quote', true )=="no"?"checked":null)?> class="product-radio" name="auto_quote" id="product-radio-auto_quote-no" value="no">
                    <label for="product-radio-auto_quote-no" class="radio-custom-label">No</label>									
                </span>             
                
              </div>
            </div>
            <div class="auto-bid-wrap" style="display:<?=(get_user_meta( $user_ID, 'auto_quote', true )=="yes"?"block":"none")?>">
            	<ul>
                	<li class="fields">
                    	 <div class="field field-radius-range">
                            <label for="radius_range">Radius range for auto quote(KM)</label>
                            <div class="input-box">
                                <input type="text" class="input-text" title="Radius range for auto quote(KM)" value="<?=get_user_meta( $user_ID, 'radius_range', true )?>" id="radius_range" name="radius_range">
                            </div>
                         </div>
                    </li>
                	<li class="fields">
                    	 <div class="field field-delivery-price">
                            <label for="delivery_price_auto_quote">Delivery price applied for auto quote($)</label>
                            <div class="input-box">
                                <input type="text" class="input-text" title="Delivery price applied for auto quote" value="<?=get_user_meta( $user_ID, 'delivery_price_auto_quote', true )?>" id="delivery_price_auto_quote" name="delivery_price_auto_quote">
                            </div>
                         </div>
                    </li>
                    <li class="fields">
                    	 <div class="field field-delivery-comment">
                            <label for="delivery_comment_auto_quote">Comment on delivery for auto quote</label>
                            <div class="input-box">
                                <input type="text" class="input-text" title="Delivery comment for auto quote" value="<?=get_user_meta( $user_ID, 'delivery_comment_auto_quote', true )?>" id="delivery_comment_auto_quote" name="delivery_comment_auto_quote">
                            </div>
                         </div>
                    </li>
                    <li>
                    	<div class="field field-pickup-radius">
                        	<label for="pickup_radius_auto_quote">Pickup radius for auto quote</label>
                            <div class="input-box">
                                <input type="text" class="input-text" title="Pickup radius for auto quote" value="<?=get_user_meta( $user_ID, 'pickup_radius_auto_quote', true )?>" id="pickup_radius_auto_quote" name="pickup_radius_auto_quote">
                            </div>                        
                        </div>
                    </li>
                    <li>
                    	<div class="field field-pickup-offer">
                        	<label for="offer_pickup">Offer pickup</label>
                            <div class="input-box">              
                            <span class="radio-wrap">
                                <input type="radio" <?=(get_user_meta( $user_ID, 'offer_pickup', true )=="yes"?"checked":null)?> class="product-radio" name="offer_pickup" id="product-radio-offer-pickup-yes" value="yes">
                                <label for="product-radio-offer-pickup-yes" class="radio-custom-label">Yes</label>									
                            </span>                            
                            <span class="radio-wrap">
                                <input type="radio" <?=(get_user_meta( $user_ID, 'offer_pickup', true )=="no"?"checked":null)?> class="product-radio" name="offer_pickup" id="product-radio-offer-pickup-no" value="no">
                                <label for="product-radio-offer-pickup-no" class="radio-custom-label">No</label>									
                            </span>
                          </div>                                                  
                        </div>
                    </li>
                    <li>
                    	<div class="field field-pickup-comment">
                        	<label for="pickup_comment">Comment on Pickup for auto quote</label>
                            <div class="input-box">
                                <input type="text" class="input-text" title="Pickup comment" value="<?=get_user_meta( $user_ID, 'pickup_comment', true )?>" id="pickup_comment" name="pickup_comment">
                            </div>                        
                        </div>
                    </li>
                    <li class="fields">
                    	 <div class="field field-timeframe-comment">
                            <label for="instock_timeframe_comment_auto_quote">Comment on timeframe for in-stock product</label>
                            <div class="input-box">
                            <?php
							$instock_timeframe_comment_auto_quote = get_user_meta( $user_ID, 'instock_timeframe_comment_auto_quote', true );
							?>
                             <input type="text" class="input-text" title="Comment on timeframe for in-stock product" value="<?php echo ($instock_timeframe_comment_auto_quote!=""?$instock_timeframe_comment_auto_quote:"The product is in stock, contact us for confirmation of delivery schedules");?>" id="instock_timeframe_comment_auto_quote" name="instock_timeframe_comment_auto_quote">
                            </div>
                         </div>
                    </li>
                    <li class="fields">
                    	 <div class="field field-timeframe-comment">
                            <label for="outstock_timeframe_comment_auto_quote">Comment on timeframe for out-of-stock product</label>
                            <div class="input-box">
                            <?php
							$outstock_timeframe_comment_auto_quote = get_user_meta( $user_ID, 'outstock_timeframe_comment_auto_quote', true );
							?>
                            <input type="text" class="input-text" title="Comment on timeframe for out-of-stock product" value="<?php echo ($outstock_timeframe_comment_auto_quote!=""?$outstock_timeframe_comment_auto_quote:"Currently not in stock, will be ordered in and timeframe confirmed ASAP");?>" id="outstock_timeframe_comment_auto_quote" name="outstock_timeframe_comment_auto_quote">
                            </div>
                         </div>
                    </li>
                    
                    <li>
                        <div class="field field-rooa">
                          <label for="rooa">Removal of old appliance</label>
                          <div class="input-box">              
                            <span class="radio-wrap">
                                <input type="radio" <?=(get_user_meta( $user_ID, 'rooa', true )=="yes"?"checked":null)?> class="product-radio" name="rooa" id="product-radio-rooa-yes" value="yes">
                                <label for="product-radio-rooa-yes" class="radio-custom-label">Yes</label>									
                            </span>                            
                            <span class="radio-wrap">
                                <input type="radio" <?=(get_user_meta( $user_ID, 'rooa', true )=="no"?"checked":null)?> class="product-radio" name="rooa" id="product-radio-rooa-no" value="no">
                                <label for="product-radio-rooa-no" class="radio-custom-label">No</label>									
                            </span>
                          </div>
                          	<div class="field field-rooa-price" style="display:<?=(get_user_meta( $user_ID, 'rooa', true )=="yes"?"block":"none")?>">
                            	<label for="rooa_price">Price($)</label>
                                <div class="input-box">
                                    <input type="text" class="input-text" title="Removal of old appliances price" value="<?=get_user_meta( $user_ID, 'rooa_price', true )?>" id="rooa_price" name="rooa_price">
                                </div>
                            </div>
                         	<div class="field field-rooa-comment">
                            	<label for="rooa_comment">Comment on Removal of old appliances for auto quote</label>
                                <div class="input-box">
                                    <input type="text" class="input-text" title="Comment on Removal of old appliances for auto quote" value="<?=get_user_meta( $user_ID, 'rooa_comment', true )?>" id="rooa_comment" name="rooa_comment">
                                </div>
                         	</div>
                        </div>
                    </li>
                    <!--<li>
                    	<div class="field field-installation-comment">
                        	<label for="installation_price_auto_quote">Installation price($)</label>
                            <div class="input-box">
                                <input type="text" class="input-text" title="Installation price for auto quote" value="<?=get_user_meta( $user_ID, 'installation_price_auto_quote', true )?>" id="installation_price_auto_quote" name="installation_price_auto_quote">
                            </div>                        
                        </div>
                    </li>-->
                    <li>
                    	<div class="field field-installation-comment">
                        	<label for="installation_comment_auto_quote">Comment on installation for auto quote</label>
                            <div class="input-box">
                                <input type="text" class="input-text" title="Installation comment for auto quote" value="<?=get_user_meta( $user_ID, 'installation_comment_auto_quote', true )?>" id="installation_comment_auto_quote" name="installation_comment_auto_quote">
                            </div>                        
                        </div>
                    </li>
                    <li>
                    	<div class="field field-contact-person">
                        	<label for="contact_person_auto_quote">Contact person for auto quote</label>
                            <div class="input-box">
                                <input type="text" class="input-text" title="Contact person for auto quote" value="<?=get_user_meta( $user_ID, 'contact_person_auto_quote', true )?>" id="contact_person_auto_quote" name="contact_person_auto_quote">
                            </div>                        
                        </div>
                    </li>
                    <li>
                    	<div class="field field-contact-detail">
                        	<label for="contact_details_auto_quote">Contact details for auto quote</label>
                            <div class="input-box">
                                <input type="text" class="input-text" title="Contact details for auto quote" value="<?=get_user_meta( $user_ID, 'contact_details_auto_quote', true )?>" id="contact_details_auto_quote" name="contact_details_auto_quote">
                            </div>                        
                        </div>
                    </li>                                        
                </ul>
            
            </div>
          </li> 
          <!-- Auto bid fields ends--> 
        </ul>
    </li>
  </ul> 
</div>
<?php							
$ob_content2 = ob_get_contents();
ob_end_clean();
        $output .= $ob_content2;						
		$output .= "</div>
						</div>
						<div class=\"buyer-job-fieldset\">";
ob_start();
?>

<div class="fieldset-content buttonsection">
 <input type="submit" name="update_account" value="Update">
</div>
</form>
<?php
$ob_content = ob_get_contents();
ob_end_clean();
$output .= $ob_content;

							$output .= "</div>";
							$output .="
									<script type=\"text/javascript\">
										jQuery(document).ready(function(){
											jQuery(\"#product-radio-auto_quote-yes\").click(function() {
											   if(jQuery(\"#product-radio-auto_quote-yes\").is(\":checked\")) { 
												 jQuery(\".auto-bid-wrap\").show();											 
											   }	
											});	
											jQuery(\"#product-radio-auto_quote-no\").click(function() {
											   if(jQuery(\"#product-radio-auto_quote-no\").is(':checked')) { 
												 jQuery(\".auto-bid-wrap\").hide();
												 jQuery(\"#delivery_price_auto_quote\").val('');
											   }
											});
											jQuery(\"#product-radio-rooa-yes\").click(function() {
											   if(jQuery(\"#product-radio-rooa-yes\").is(\":checked\")) { 
												 jQuery(\".field-rooa-price\").show();											 
											   }	
											});	
											jQuery(\"#product-radio-rooa-no\").click(function() {
											   if(jQuery(\"#product-radio-rooa-no\").is(':checked')) { 
												 jQuery(\".field-rooa-price\").hide();
												 jQuery(\"#rooa_price\").val('');
											   }
											});	
										});
										</script>";
			
		}else{
		
		$output = "<script type=\"text/javascript\">
							window.location.href=\"".home_url('my-account')."\";
					   </script>";
		}
		return $output;
}
add_shortcode('manage-account', 'manage_account_function');
?>