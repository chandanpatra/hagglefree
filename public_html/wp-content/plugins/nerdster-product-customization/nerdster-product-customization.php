<?php
/**
 * Plugin Name: Nerdster Product Customization 
 * Description: Additional Tab Product Inquiry.
 * Author: Nerdster
 * Author URI: http://nerdster.com.au
 * Version: 0.1
 *
 *
 * @package     
 * @author      Nerdster
 * @category    Plugin
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */
 
 
/**
 * Exit if accessed directly
 **/
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Check if WooCommerce is active
 **/
if ( ! in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
    return;
}

// Adding admin control for jobs\
//menu items
add_action('admin_menu','jobs_on_dashboard');
function jobs_on_dashboard() {
	//this is the main item for the menu
	add_menu_page('Jobs', //page title
	'Jobs', //menu title
	'manage_options', //capabilities
	'jobs_on_dashboard', //menu slug
	'jobs_list_on_dashboard', //function
	 plugins_url( 'nerdster-product-customization/css/images/jobs-list.png' )
	);
	
	add_submenu_page('jobs_on_dashboard',
	'Listing', //page title
	'Listing', //menu title
	'manage_options', //capabilities
	'jobs_on_dashboard', //menu slug
	'jobs_list_on_dashboard' //function	 
	);
	
	add_submenu_page('jobs_on_dashboard',
	'Settings', //page title
	'Settings', //menu title
	'manage_options', //capabilities
	'jobs_configuration', //menu slug
	'jobs_configuration'
	);
	
	add_submenu_page('jobs_on_dashboard',
	'Email Templates', //page title
	'Email Templates', //menu title
	'manage_options', //capabilities
	'jobs_email_templates', //menu slug
	'jobs_email_templates'
	);
	
	add_submenu_page('jobs_on_dashboard',
	'Lead Fee Stat', //page title
	'Lead Fee Stat', //menu title
	'manage_options', //capabilities
	'lead_fee_stats', //menu slug
	'lead_fee_stats'
	);
	
	//this submenu is HIDDEN, however, we need to add it anyways
	 add_submenu_page('', //parent slug
	 'Job Details', //page title
	 'Job Details', //menu title
	 'manage_options', //capability
	 'jobs_details_on_dashboard', //menu slug
	 'jobs_details_on_dashboard'); //function
	 
	 //this submenu is HIDDEN, however, we need to add it anyways
	 add_submenu_page('', //parent slug
	 'Lead Stat Details', //page title
	 'Lead Stat Details', //menu title
	 'manage_options', //capability
	 'lead_details_on_dashboard', //menu slug
	 'lead_details_on_dashboard'); //function
	 
	 //Menu for managing popular category
	 add_submenu_page('edit.php?post_type=product', //parent slug
	 'Popular Category', //page title
	 'Popular Category', //menu title
	 'manage_options', //capability
	 'popular_category', //menu slug
	 'popular_category'); //function
	 
	/* //Menu for managing brand
	 add_submenu_page('edit.php?post_type=product', //parent slug
	 'Cat Brand Selection', //page title
	 'Cat Brand Selection', //menu title
	 'manage_options', //capability
	 'cat_brand_selection', //menu slug
	 'cat_brand_selection'); //function*/
 
}
define('ROOTDIR', plugin_dir_path(__FILE__));
require_once(ROOTDIR . 'jobs-list.php');

//Jobs Widgets
require_once(ROOTDIR . 'jobs-widgets.php');

//Jobs Shortcode
require_once(ROOTDIR . 'jobs-shortcode.php');

// Popular Category
require_once(ROOTDIR . 'popular-category.php');

// Jobs Configuration
require_once(ROOTDIR . 'jobs_configuration.php');

// Jobs Essential Functions
require_once(ROOTDIR . 'jobs_functions.php');

// Jobs Essential Hooks
require_once(ROOTDIR . 'jobs_hooks.php');

// Jobs Email Templates
require_once(ROOTDIR . 'jobs_email_templates.php');

// Lead fee stats
require_once(ROOTDIR . 'lead_fee_stats.php');

//Brand Management 
require_once(ROOTDIR . 'cat_brand_selection.php');

wp_enqueue_style( 'custom-job-style',plugin_dir_url(__FILE__).'css/custom_job.css');

$headers[] = 'Content-Type: text/html; charset=UTF-8';
$headers[] = 'From: '.get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'> ' . "\r\n";

/////////////////////////job filters/////////////////////////////

//custom cart success message
add_filter( 'wc_add_to_cart_message', 'job_post_message' );
function job_post_message() {
	global $woocommerce;
	// Output success messages
	if (get_option('woocommerce_cart_redirect_after_add')=='yes') :
		$return_to 	= get_permalink(woocommerce_get_page_id('shop'));
		$message 	= sprintf('%s', __('', 'woocommerce') );
	else :
		$message 	= sprintf('%s', __('', 'woocommerce') );
	endif;
		return false;
}
// cart button text
add_filter( 'woocommerce_product_single_add_to_cart_text', 'job_post_button_text' );    // < 2.1

function job_post_button_text() { 
        return __( 'Request a Quote', 'woocommerce' ); 
}

function remove_loop_button(){
	remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
}
add_action('init','remove_loop_button');

add_action('woocommerce_after_shop_loop_item','job_post_cart_button_text',10);
function job_post_cart_button_text() {
	global $product;
	$link = $product->get_permalink();
	$link1 = explode("/",$link);
	if ( is_user_logged_in()){
		$user_ID = get_current_user_id();
		$user = new WP_User( $user_ID );
		$role = $user->roles[0];
		if($role == "retailer-account"){
			echo do_shortcode('<a href="'.$link.'" class="button viewdetailbutton">View Details</a>');
		}else{
		//echo do_shortcode('<a href="http://'.$link1[2].'/get-a-quote/?pname='.$link1[4].' & pid='.$product->id.' & get_quote=1" class="button getquotesbutton">Find Me A Deal</a>&nbsp;<a href="'.$link.'" class="button viewdetailbutton">View Details</a>');
		echo do_shortcode('<a href="'.$link.'?get_quote=1" class="button getquotesbutton">Find Me A Deal</a>&nbsp;<a href="'.$link.'" class="button viewdetailbutton">View Details</a>');
	}
	}else{
		//echo do_shortcode('<a href="http://'.$link1[2].'/get-a-quote/?pname='.$link1[4].' & pid='.$product->id.' & get_quote=1" class="button getquotesbutton">Find Me A Deal</a>&nbsp;<a href="'.$link.'" class="button viewdetailbutton">View Details</a>');
		echo do_shortcode('<a href="'.$link.'?get_quote=1" class="button getquotesbutton">Find Me A Deal</a>&nbsp;<a href="'.$link.'" class="button viewdetailbutton">View Details</a>');
	}	
}

///product form
add_action('woocommerce_single_product_summary','product_enquiry',20);

function product_enquiry(){
	global $product;
	$price = $product->get_price();
	$save = "";
	if($price<100){
		$save = "?????";
	}elseif($price<400){
		$save = "$$'s of dollars";
	}elseif($price<10000){
		$save = "$$$'s of dollars";
	}elseif($price>10000){
		$save = "$$$$'s of dollars";
	}else{
		$save = "?????";
	}
	$visibility = get_post_meta($product->get_id(),"_visibility",true);

	if ( is_user_logged_in()){
		$user_ID = get_current_user_id();
		$user = new WP_User( $user_ID );
		$role = $user->roles[0];
		if($role == "retailer-account"){	
		}else{
		echo '<span class="price"><span class="unit">RRP:</span>'.$product->get_price_html().'</span>
				<span class="price save"><span class="unit">SAVE:</span>'.$save.'</span>
		<div class="findDeal">
			<div class="deal-content">
				We will contact a number of local retailers in your area, get you a few offers on this product and you get to choose one that meets all your requirements.
				<ul class="listings">
					<li>We will find you a deal in 1 Hour</li>
					<li>Obligation Free</li>
					<li>100% Free Quotes</li>
				</ul>
			</div>
			<div class="deal-additional">
				<span>It\'s that easy!</span>
				<div class="haggle-stamp">
				<img src="'.home_url().'/wp-content/uploads/2016/'.($visibility == "hidden"?'no_longer_stamp.png':'guarantee_stamp.png').'"/></div>';

			if($visibility != "hidden"){
			if($_GET['get_quote'])
				echo 	'<a id="product_form_link" href="#jobDetails">Find <span>me</span> a deal</a>';
			else
				echo 	'<a id="product_form_link" href="'.$product->get_permalink().'?get_quote=1">Find <span>me</span> a deal</a>';
			}
			echo '</div>			
		</div>';
      
	}
	}else{
		echo '<span class="price"><span class="unit">RRP:</span>'.$product->get_price_html().'</span>
			<span class="price save"><span class="unit">SAVE:</span>'.$save.'</span>
		<div class="findDeal">
			<div class="deal-content">
				We will contact a number of local retailers in your area, get you a few offers on this product and you get to choose one that meets all your requirements.
				<ul class="listings">
					<li>We will find you a deal in 1 Hour</li>
					<li>Obligation Free</li>
					<li>100% Free Quotes</li>
				</ul>
			</div>
<div class="deal-additional"><span>It\'s that easy!</span>
<div class="haggle-stamp"><img src="'.home_url().'/wp-content/uploads/2016/'.($visibility == "hidden"?'no_longer_stamp.png':'guarantee_stamp.png').'"/></div>';
	if($visibility != "hidden"){
			if($_GET['get_quote'])
				echo 	'<a id="product_form_link" href="#jobDetails">Find <span>me</span> a deal</a>';
			else
				echo 	'<a id="product_form_link" href="'.$product->get_permalink().'?get_quote=1">Find <span>me</span> a deal</a>';
	}
			echo '</div>
			
			</div>';
      
	}
	
}
/////////////////////////job filters/////////////////////////////
?>