<?php
function jobs_email_templates(){
?>
<h3>Email Templates</h3> 
<hr/>
<?php

    $selectedTab = "0";
	
	if($_POST['save_quotation_template']){
			
									
				$subject 				   = 	$_POST['job_quotation_subject'];
				$email_heading 			 = 	$_POST['job_quotation_subject_email_heading'];
				$email_template 			= 	$_POST['job_quotation_template'];
				$template_name1 			=   $_POST['quotation_request_for_new_job'];
							
			$args = array(					
					'subject' 		 => $subject,
					'email_heading'   => $email_heading,
					'email_template'  => $email_template
			);
			
			save_email_template($template_name1,$args);	
			$selectedTab = "0";		
	}

	
	
	
		if($_POST['save_best_3_quotations_for_buyer_template']){
			$subject 				   = 	$_POST['best_3_quotations_subject'];
			$email_heading 			 = 	$_POST['best_3_quotations_heading'];
			$email_template 			= 	$_POST['best_3_quotations_for_buyer_template'];
			$template_name3 			 =   $_POST['best_3_quotations_for_buyer'];
			$args = array(					
						'subject' 		 => $subject,
						'email_heading'   => $email_heading,
						'email_template'  => $email_template
				);
			
			save_email_template($template_name3,$args);
			$selectedTab = "1";
		}
		
			if($_POST['save_no_quotations_for_buyer_template']){
			$subject 				   = 	$_POST['no_quotations_subject'];
			$email_heading 			 = 	$_POST['no_quotations_heading'];
			$email_template 			= 	$_POST['no_quotations_for_buyer_template'];
			$template_name4 			 =   $_POST['no_quotations_for_buyer'];
			$args = array(					
						'subject' 		 => $subject,
						'email_heading'   => $email_heading,
						'email_template'  => $email_template
				);
			
			save_email_template($template_name4,$args);
			$selectedTab = "2";
		    }	
			
			
			if($_POST['save_my_quotation_selected_template']){
			$subject 				   = 	$_POST['my_quotation_selected_subject'];
			$email_heading 			 = 	$_POST['my_quotation_selected_heading'];
			$email_template 			= 	$_POST['my_quotation_selected_template'];
			$template_name5 			 =   $_POST['my_quotation_selected'];
			$args = array(					
						'subject' 		 => $subject,
						'email_heading'   => $email_heading,
						'email_template'  => $email_template
				);
			
			save_email_template($template_name5,$args);
			$selectedTab = "3";
		    }
			
			/*if($_POST['save_my_quotation_accepted_template']){
			$subject 				   = 	$_POST['quotation_accepted_subject'];
			$email_heading 			 = 	$_POST['quotation_accepted_heading'];
			$email_template 			= 	$_POST['my_quotation_accepted_template'];
			$template_name6 			 =   $_POST['my_quotation_accepted'];
			$args = array(					
						'subject' 		 => $subject,
						'email_heading'   => $email_heading,
						'email_template'  => $email_template
				);
			
			save_email_template($template_name6,$args);
			$selectedTab = "4";
		    }*/
			if($_POST['save_account_creation_template']){
				$subject 				   = 	$_POST['account_creation_subject'];
				$email_heading 			 = 	$_POST['account_creation_heading'];
				$email_template 			= 	$_POST['account_creation_template'];
				$template_name2 			 =   $_POST['account_creation_for_buyer'];
				$args = array(					
							'subject' 		 => $subject,
							'email_heading'   => $email_heading,
							'email_template'  => $email_template
					);
					
					save_email_template($template_name2,$args);
					$selectedTab = "4";
			}
			if($_POST['save_verification_template']){
				$subject 				   = 	$_POST['email_verification_subject'];
				$email_heading 			 = 	$_POST['email_verification_heading'];
				$email_template 			= 	$_POST['email_verification_template'];
				$template_name2 			 =   $_POST['email_verification_for_buyer'];
				$args = array(					
							'subject' 		 => $subject,
							'email_heading'   => $email_heading,
							'email_template'  => $email_template
					);
					
					save_email_template($template_name2,$args);
					$selectedTab = "5";
			}
			if($_POST['save_admin_notification_retailer_template']){
				$subject 				   = 	$_POST['admin_notification_retailer_subject'];
				$email_heading 			 = 	$_POST['admin_notification_retailer_heading'];
				$email_template 			= 	$_POST['admin_notification_retailer_template'];
				$template_name2 			 =   $_POST['admin_notification_for_retailer'];
				$args = array(					
							'subject' 		 => $subject,
							'email_heading'   => $email_heading,
							'email_template'  => $email_template
					);
					
					save_email_template($template_name2,$args);
					$selectedTab = "6";
			}	
			if($_POST['save_retailer_notification_activation_template']){
				$subject 				   = 	$_POST['retailer_notification_activation_subject'];
				$email_heading 			 = 	$_POST['retailer_notification_activation_heading'];
				$email_template 			= 	$_POST['retailer_notification_activation_template'];
				$template_name2 			 =   $_POST['retailer_notification_for_account_activation'];
				$args = array(					
							'subject' 		 => $subject,
							'email_heading'   => $email_heading,
							'email_template'  => $email_template
					);
					
					save_email_template($template_name2,$args);
					$selectedTab = "7";
			}
?>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script>
  jQuery(function() {
	jQuery( "#email_template_tabs" ).tabs({active: <?=$selectedTab?>});
  });
  </script>


<div id="email_template_tabs">
  <ul>
    <li><a href="#tabs-1">Quotation request to Retailer for New Job</a></li>    
    <li><a href="#tabs-2">Email on Best 3 + 1 Quotations for Buyer</a></li> 
    <li><a href="#tabs-3">No Quotations available for Buyer</a></li> 
    <li><a href="#tabs-4">Quotations selected Email</a></li>
<!--    <li><a href="#tabs-5">Quotation accepted</a></li>
-->    <li><a href="#tabs-5">Account Creation Email for buyer</a></li>
    <li><a href="#tabs-6">Email Verification for Buyer</a></li> 
    <li><a href="#tabs-7">Admin notification for retailer signup</a></li>
    <li><a href="#tabs-8">Retailer notification for account activation</a></li>
  </ul>
 <?php
 	$settings = array(
    'editor_height' => 425, // In pixels, takes precedence and has no default value
    'textarea_rows' => 20,  // Has no visible effect if editor_height is set, default is 20
	);
?>
  <div id="tabs-1">
    <p>
    	<form method="post" action="">
    	<?php 
			$quotation_result = get_email_template("quotation_request_for_new_job");
		?>
    	<p>Emails for quotation are sent to retailer(s) when a new job is posted.</p>
		<p>Codes: {site_url}, {site_tag}, {media_url}, {email_heading}, {site_name}, {company_name}, {post_date}, {quotation_url}, {product_details}, {customer_requirements}, {admin_email}, {site_abn}, {site_phone}, {fb_url}</p>
           <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="job_quotation_subject">Subject</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>Subject</span></legend>              
                    <input class="input-text regular-input" size="50" type="text" name="job_quotation_subject" id="job_quotation_subject" value="<?=$quotation_result['subject']?>">                
                </fieldset>
            </td>
          </tr>
          <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="job_quotation_subject_email_heading">Email Heading</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>Email Heading</span></legend>              
                    <input class="input-text regular-input" size="50" type="text" name="job_quotation_subject_email_heading" id="job_quotation_subject_email_heading" value="<?=$quotation_result['email_heading']?>">                
                </fieldset>
            </td>
          </tr>
          <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="job_quotation_template">Email Template</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>>Email Template</span></legend>              
                       <?php wp_editor( stripslashes($quotation_result['email_template']), "job_quotation_template",$settings ); ?>
                </fieldset>
            </td>
          </tr>      
    </table>
    <input type="hidden" name="quotation_request_for_new_job" value="quotation_request_for_new_job" />
        <input type="submit" value="Save Templates" name="save_quotation_template" class="button button-primary"/>
    </form>

    </p>
  </div>
    
  <div id="tabs-2">
    <p>
    	<form method="post" action="">
    	<?php 
			$best_3_result = get_email_template("best_3_quotations_for_buyer");
		?>
    	<p>Best 3 + 1 Quotations for Buyer.</p>
		<p>Codes: {site_url}, {site_tag}, {media_url}, {email_heading}, {site_name}, {buyer_name}, {product_details}, {quotation_detail_list}, {admin_email}, {site_abn}, {site_phone}, {fb_url}</p>
        <table class="form-table" width="100%" border="0" cellspacing="5" cellpadding="5">
           <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="best_3_quotations_subject">Subject</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>Subject</span></legend>              
                    <input class="input-text regular-input" size="50" type="text" name="best_3_quotations_subject" id="best_3_quotations_subject" value="<?=$best_3_result['subject']?>">                
                </fieldset>
            </td>
          </tr>
          <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="best_3_quotations_heading">Email Heading</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>Email Heading</span></legend>              
                    <input class="input-text regular-input" size="50" type="text" name="best_3_quotations_heading" id="best_3_quotations_heading" value="<?=$best_3_result['email_heading']?>">                
                </fieldset>
            </td>
          </tr>
          <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="email_verification_template">Email Template</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>>Email Template</span></legend>              
                    <?php wp_editor( stripslashes($best_3_result['email_template']), "best_3_quotations_for_buyer_template",$settings ); ?>
                </fieldset>
            </td>
          </tr>      
    </table>
    <input type="hidden" name="best_3_quotations_for_buyer" value="best_3_quotations_for_buyer" />
    <input type="submit" value="Save Templates" name="save_best_3_quotations_for_buyer_template" class="button button-primary"/>
    </form>
    </p>
  </div>
  <div id="tabs-3">
    <p>
    	<form method="post" action="">
    	<?php 
			$no_result = get_email_template("no_quotations_for_buyer");
		?>
    	<p>No Quotations available for Buyer.</p>
        <p>Codes: {site_url}, {site_tag}, {media_url}, {email_heading}, {site_name}, {retailer_name}, {post_date}, {product_details}, {buyer_details}, {customer_requirements}, {admin_email}, {site_abn}, {site_phone}, {fb_url}</p>
        <table class="form-table" width="100%" border="0" cellspacing="5" cellpadding="5">
           <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="best_3_quotations_subject">Subject</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>Subject</span></legend>              
                    <input class="input-text regular-input" size="50" type="text" name="no_quotations_subject" id="no_quotations_subject" value="<?=$no_result['subject']?>">                
                </fieldset>
            </td>
          </tr>
          <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="best_3_quotations_heading">Email Heading</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>Email Heading</span></legend>              
                    <input class="input-text regular-input" size="50" type="text" name="no_quotations_heading" id="no_quotations_heading" value="<?=$no_result['email_heading']?>">                
                </fieldset>
            </td>
          </tr>
          <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="email_verification_template">Email Template</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>>Email Template</span></legend>              
                    <?php wp_editor( stripslashes($no_result['email_template']), "no_quotations_for_buyer_template",$settings ); ?>
                </fieldset>
            </td>
          </tr>      
    </table>
    <input type="hidden" name="no_quotations_for_buyer" value="no_quotations_for_buyer" />
    <input type="submit" value="Save Templates" name="save_no_quotations_for_buyer_template" class="button button-primary"/>
    </form>
    </p>
  </div>
  <div id="tabs-4">
    <p>
    	<form method="post" action="">
    	<?php 
			$my_quotation_selected = get_email_template("my_quotation_selected");
		?>
    	<p>Quotations selected.</p>
        <p>Codes: {site_url}, {site_tag}, {media_url}, {email_heading}, {site_name}, {company_name}, {quotation_amount}, {product_details}, {buyer_details}, {customer_requirements}, {quotation_details}, {admin_email}, {site_abn}, {site_phone}, {fb_url}</p>
        <table class="form-table" width="100%" border="0" cellspacing="5" cellpadding="5">
           <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="best_3_quotations_subject">Subject</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>Subject</span></legend>              
                    <input class="input-text regular-input" size="50" type="text" name="my_quotation_selected_subject" id="my_quotation_selected_subject" value="<?=$my_quotation_selected['subject']?>">                
                </fieldset>
            </td>
          </tr>
          <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="best_3_quotations_heading">Email Heading</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>Email Heading</span></legend>              
                    <input class="input-text regular-input" size="50" type="text" name="my_quotation_selected_heading" id="my_quotation_selected_heading" value="<?=$my_quotation_selected['email_heading']?>">                
                </fieldset>
            </td>
          </tr>
          <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="email_verification_template">Email Template</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>>Email Template</span></legend>              
                    <?php wp_editor( stripslashes($my_quotation_selected['email_template']), "my_quotation_selected_template",$settings ); ?>
                </fieldset>
            </td>
          </tr>      
    </table>
    <input type="hidden" name="my_quotation_selected" value="my_quotation_selected" />
    <input type="submit" value="Save Templates" name="save_my_quotation_selected_template" class="button button-primary"/>
    </form>
    </p>
  </div>
  <!--<div id="tabs-5">
    <p>
    	<form method="post" action="">
    	<?php 
			//$my_quotation_accepted = get_email_template("my_quotation_accepted");
		?>
    	<p>Quotation accepted.</p>
        <p>Codes: {site_name}, {name}, {job_details}, {quotation_details}, {user_details}, {quotation_url}</p>
        <table class="form-table" width="100%" border="0" cellspacing="5" cellpadding="5">
           <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="quotation_accepted">Subject</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>Subject</span></legend>              
                    <input class="input-text regular-input" size="50" type="text" name="quotation_accepted_subject" id="quotation_accepted_subject" value="<?=$my_quotation_accepted['subject']?>">                
                </fieldset>
            </td>
          </tr>
          <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="quotation_accepted_heading">Email Heading</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>Email Heading</span></legend>              
                    <input class="input-text regular-input" size="50" type="text" name="quotation_accepted_heading" id="quotation_accepted_heading" value="<?=$my_quotation_accepted['email_heading']?>">                
                </fieldset>
            </td>
          </tr>
          <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="quotation_accepted_template">Email Template</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>>Email Template</span></legend>              
                    <?php //wp_editor( stripslashes($my_quotation_accepted['email_template']), "my_quotation_accepted_template",$settings ); ?>
                </fieldset>
            </td>
          </tr>      
    </table>
    <input type="hidden" name="my_quotation_accepted" value="my_quotation_accepted" />
    <input type="submit" value="Save Templates" name="save_my_quotation_accepted_template" class="button button-primary"/>
    </form>
    </p>
  </div>-->
  <div id="tabs-5">
    <p>
    	<form method="post" action="">
    	<?php 
			$buyer_account_creation_result = get_email_template("account_creation_for_buyer");
		?>
    	<p>Account creation notification for new buyer while posting job.</p>
        <p>Codes: {site_url}, {site_tag}, {media_url}, {email_heading}, {site_name}, {verify_url}, {admin_email}, {site_abn}, {site_phone}, {fb_url}</p>
        <table class="form-table" width="100%" border="0" cellspacing="5" cellpadding="5">
           <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="account_creation_subject">Subject</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>Subject</span></legend>              
                    <input class="input-text regular-input" size="50" type="text" name="account_creation_subject" id="account_creation_subject" value="<?=$buyer_account_creation_result['subject']?>">                
                </fieldset>
            </td>
          </tr>
          <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="email_verification_heading">Email Heading</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>Email Heading</span></legend>              
                    <input class="input-text regular-input" size="50" type="text" name="account_creation_heading" id="account_creation_heading" value="<?=$buyer_account_creation_result['email_heading']?>">                
                </fieldset>
            </td>
          </tr>
          <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="account_creation_template">Email Template</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>Email Template</span></legend>                     
                    <?php wp_editor( stripslashes($buyer_account_creation_result['email_template']), "account_creation_template",$settings ); ?>
                </fieldset>
            </td>
          </tr>      
    </table>
    <input type="hidden" name="account_creation_for_buyer" value="account_creation_for_buyer" />
    <input type="submit" value="Save Templates" name="save_account_creation_template" class="button button-primary"/>
    </form>
    </p>
  </div>
  <div id="tabs-6">
    <p>
    	<form method="post" action="">
    	<?php 
			$verification_result = get_email_template("email_verification_for_buyer");
		?>
    	<p>Emails verification for new buyer while posting job.</p>
        <p>Codes: {site_url}, {site_tag}, {media_url}, {email_heading}, {site_name}, {login_url}, {admin_email}, {site_abn}, {site_phone}, {fb_url}</p>
        <table class="form-table" width="100%" border="0" cellspacing="5" cellpadding="5">
           <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="email_verification_subject">Subject</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>Subject</span></legend>              
                    <input class="input-text regular-input" size="50" type="text" name="email_verification_subject" id="email_verification_subject" value="<?=$verification_result['subject']?>">                
                </fieldset>
            </td>
          </tr>
          <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="email_verification_heading">Email Heading</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>Email Heading</span></legend>              
                    <input class="input-text regular-input" size="50" type="text" name="email_verification_heading" id="email_verification_heading" value="<?=$verification_result['email_heading']?>">                
                </fieldset>
            </td>
          </tr>
          <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="email_verification_template">Email Template</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>>Email Template</span></legend>
                     <!--<textarea cols="50" rows="20" name="email_verification_template" id="email_verification_template"></textarea>                         -->
                    <?php wp_editor( stripslashes($verification_result['email_template']), "email_verification_template",$settings ); ?>
                </fieldset>
            </td>
          </tr>      
    </table>
    <input type="hidden" name="email_verification_for_buyer" value="email_verification_for_buyer" />
    <input type="submit" value="Save Templates" name="save_verification_template" class="button button-primary"/>
    </form>
    </p>
  </div>
  <div id="tabs-7">
    <p>
    	<form method="post" action="">
    	<?php 
			$admin_notification_retailer_result = get_email_template("admin_notification_for_retailer");
		?>
    	<p>Admin notification for new retailer signup</p>
        <p>Codes: {site_url}, {site_tag}, {media_url}, {email_heading}, {site_name}, {login_url}, {admin_email}, {site_abn}, {site_phone}, {fb_url}</p>
        <table class="form-table" width="100%" border="0" cellspacing="5" cellpadding="5">
           <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="admin_notification_retailer_subject">Subject</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>Subject</span></legend>              
                    <input class="input-text regular-input" size="50" type="text" name="admin_notification_retailer_subject" id="admin_notification_retailer_subject" value="<?=$admin_notification_retailer_result['subject']?>">                
                </fieldset>
            </td>
          </tr>
          <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="admin_notification_retailer_heading">Email Heading</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>Email Heading</span></legend>              
                    <input class="input-text regular-input" size="50" type="text" name="admin_notification_retailer_heading" id="admin_notification_retailer_heading" value="<?=$admin_notification_retailer_result['email_heading']?>">                
                </fieldset>
            </td>
          </tr>
          <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="admin_notification_retailer_template">Email Template</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>Email Template</span></legend>              
                    <?php wp_editor( stripslashes($admin_notification_retailer_result['email_template']), "admin_notification_retailer_template",$settings ); ?>
                </fieldset>
            </td>
          </tr>      
    </table>
    <input type="hidden" name="admin_notification_for_retailer" value="admin_notification_for_retailer" />
    <input type="submit" value="Save Templates" name="save_admin_notification_retailer_template" class="button button-primary"/>
    </form>
    </p>
  </div>
  <div id="tabs-8">
    <p>
    	<form method="post" action="">
    	<?php 
			$retailer_notification_for_account_activation_result = get_email_template("retailer_notification_for_account_activation");
		?>
    	<p>Retailer notification for account activation</p>
        <p>Codes: {site_url}, {site_tag}, {media_url}, {email_heading}, {site_name}, {login_url}, {retailer_email}, {retailer_password}, {admin_email}, {site_abn}, {site_phone}, {fb_url}</p>
        <table class="form-table" width="100%" border="0" cellspacing="5" cellpadding="5">
           <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="retailer_notification_activation_subject">Subject</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>Subject</span></legend>              
                    <input class="input-text regular-input" size="50" type="text" name="retailer_notification_activation_subject" id="retailer_notification_activation_subject" value="<?=$retailer_notification_for_account_activation_result['subject']?>">                
                </fieldset>
            </td>
          </tr>
          <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="retailer_notification_activation_heading">Email Heading</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>Email Heading</span></legend>              
                    <input class="input-text regular-input" size="50" type="text" name="retailer_notification_activation_heading" id="retailer_notification_activation_heading" value="<?=$retailer_notification_for_account_activation_result['email_heading']?>">                
                </fieldset>
            </td>
          </tr>
          <tr valign="top">
            <th scope="row" class="titledesc"> 
                <label for="retailer_notification_activation_template">Email Template</label>
            </th>
            <td class="forminp">
                <fieldset>
                    <legend class="screen-reader-text"><span>Email Template</span></legend>              
                    <?php wp_editor( stripslashes($retailer_notification_for_account_activation_result['email_template']), "retailer_notification_activation_template",$settings ); ?>
                </fieldset>
            </td>
          </tr>      
    </table>
    <input type="hidden" name="retailer_notification_for_account_activation" value="retailer_notification_for_account_activation" />
    <input type="submit" value="Save Templates" name="save_retailer_notification_activation_template" class="button button-primary"/>
    </form>
    </p>
  </div>
</div>
<?php } ?>