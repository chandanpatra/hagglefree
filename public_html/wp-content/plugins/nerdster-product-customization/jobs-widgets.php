<?php

// Creating the widget for Retailer
class retailer_info_widget extends WP_Widget {
 
	function __construct() {
		parent::__construct(
		// Base ID of your widget
		'retailer_info_widget',
		 
		// Widget name will appear in UI
		__('Retailer Info Widget', 'wpb_widget_domain'),
		 
		// Widget description
		array( 'description' => __( 'This widget displays the retailer information on the sidebar.', 'retailer_info_widget_domain' ), )
		);
	}
 
	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		
		$title = apply_filters( 'widget_title', $instance['title'] );	// block title
					
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
				 
		// This is where you run the code and display the output
		?>
		<div class="widget-retailer-info">        		 
            	<?php if($title!="") { ?>
                    <h2 class="retailer-sidebar-title">                
                        <?=$title?>
                    </h2>                
                <?php } ?> 
                <div class="textwidget sidebar-content">  
                	<ul class="member-links"> 
                    	<?php 
							$current_url = explode('/',$_SERVER['REQUEST_URI']);
							$page_slug = $current_url[1];
						?>             	
                		<li><a class="<?=($page_slug=="retailer-product-category"?"active":"")?>" href="<?=home_url('retailer-product-category')?>"><?php _e( 'Manage Category Selection', 'twentysixteen' ); ?></a></li>
                        <li><a class="<?=($page_slug=="brand-management"?"active":"")?>" href="<?=home_url('brand-management')?>"><?php _e( 'Manage Brand Selection', 'twentysixteen' ); ?></a></li>
                        <li><a class="<?=($page_slug=="manage-autobid-category"?"active":"")?>" href="<?=home_url('manage-autobid-category')?>"><?php _e( 'Manage Autobid Category', 'twentysixteen' ); ?></a></li>
                        <li><a class="<?=($page_slug=="manage-autobid-brand"?"active":"")?>" href="<?=home_url('manage-autobid-brand')?>"><?php _e( 'Manage Autobid Brand', 'twentysixteen' ); ?></a></li>
                        <li><a class="<?=($page_slug=="quotation-request"||$page_slug=="retailer-job-detail"?"active":"")?>" href="<?=home_url('quotation-request')?>"><?php _e( 'Invitations To Quote', 'twentysixteen' ); ?></a></li>
                        <li><a class="<?=($page_slug=="selected-quotations"||$page_slug=="selected-quotation-details"?"active":"")?>" href="<?=home_url('selected-quotations')?>"><?php _e( 'Selected Quotations', 'twentysixteen' ); ?></a></li>
                        <li><a class="<?=($page_slug=="manage-account"?"active":"")?>" href="<?=home_url('manage-account')?>"><?php _e( 'Manage Account', 'twentysixteen' ); ?></a></li>
                        <?php //} ?>
                    </ul>
                </div>
		</div>
		<?php
		echo $args['after_widget'];
	}
		 
	// Widget Backend
	public function form($instance) {
	
		if (isset( $instance['title'])) {
			$title = $instance['title'];		
		}			
	
		// Widget admin form
		?>
		<p>
		  <label for="<?php echo $this->get_field_id( 'title' ); ?>">
			<?php _e( 'Title:' ); ?>
		  </label>
		  <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>		
		<?php
	}
	 
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';		
		return $instance;
	}
} 

// Register and load the widget
function retailer_info_load_widget() {
	register_widget( 'retailer_info_widget' );
}
add_action( 'widgets_init', 'retailer_info_load_widget' );

// Creating the widget for Buyer
class buyer_info_widget extends WP_Widget {
 
	function __construct() {
		parent::__construct(
		// Base ID of your widget
		'buyer_info_widget',
		 
		// Widget name will appear in UI
		__('Buyer Info Widget', 'wpb_widget_domain'),
		 
		// Widget description
		array( 'description' => __( 'This widget displays the buyer information on the sidebar.', 'buyer_info_widget_domain' ), )
		);
	}
 
	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		
		$title = apply_filters( 'widget_title', $instance['title'] );	// block title
					
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
				 
		// This is where you run the code and display the output
		?>
		<div class="widget-buyer-info">        		 
            	<?php if($title!="") { ?>
                    <h2 class="buyer-sidebar-title">                
                        <?=$title?>
                    </h2>                
                <?php } ?> 
                <div class="textwidget sidebar-content">  
                	<ul class="member-links">  
                    	<?php 
							$current_url = explode('/',$_SERVER['REQUEST_URI']);
							$page_slug = $current_url[1];
						?> 
                         
                    <li><a class="<?=($page_slug=="my-preferences"||$page_slug=="my-preferences"?"active":"")?>" href="<?=home_url('my-preferences')?>"><?php _e( 'My Preferences', 'twentysixteen' ); ?></a></li>        	
                    <li><a class="<?=($page_slug=="my-jobs"||$page_slug=="job-detail"?"active":"")?>" href="<?=home_url('my-jobs')?>"><?php _e( 'My Jobs', 'twentysixteen' ); ?></a></li>   
                    <li><a class="<?=($page_slug=="best-quotation"||$page_slug=="quotation-detail"?"active":"")?>" href="<?=home_url('best-quotation')?>"><?php _e( 'Best Quotations', 'twentysixteen' ); ?></a></li>  
                        <!--<li><a class="<?=($page_slug=="buyer-accepted-quotations"?"active":"")?>" href="<?=home_url('buyer-accepted-quotations')?>"><?php _e( 'Accepted Quotations', 'twentysixteen' ); ?></a></li>     -->                
                    </ul>
                </div>
		</div>
		<?php
		echo $args['after_widget'];
	}
		 
	// Widget Backend
	public function form($instance) {
	
		if (isset( $instance['title'])) {
			$title = $instance['title'];		
		}			
	
		// Widget admin form
		?>
		<p>
		  <label for="<?php echo $this->get_field_id( 'title' ); ?>">
			<?php _e( 'Title:' ); ?>
		  </label>
		  <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>		
		<?php
	}
	 
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';		
		return $instance;
	}
} 

// Register and load the widget
function buyer_info_load_widget() {
	register_widget( 'buyer_info_widget' );
}
add_action( 'widgets_init', 'buyer_info_load_widget' );


?>