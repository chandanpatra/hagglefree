<?php
/*
Plugin Name: Custom Testimonial
Description: Create custom testimonial for your site
Version: 1.0
Author: Nerdster
*/

/**********************************************
CUSTOM POST TYPE: Testimonials
***********************************************/	

if ( ! function_exists( 'testimonials_post_type' ) ) :
	function testimonials_post_type() { 		 
		 wp_enqueue_script('sis_carousel_main_script',plugins_url( '/assets/js/owl.carousel.js' , __FILE__ ),array( 'jquery' ));
		 wp_enqueue_style( 'testimonials_css', plugins_url( '/assets/css/owl.carousel.css', __FILE__ ) );
		//registering the custom post
		register_post_type( 'testimonials',
			array(
				'labels' => array(
					'name' 				 => __( 'Testimonials', 'codeex_theme_name' ),
					'singular_name' 		=> __( 'Testimonial', 'codeex_theme_name' ),
					'menu_name'			=> __( 'Testimonials', 'codeex_theme_name'),//name of menu for the custom post
					'add_new' 			  => __( 'Add New', 'codeex_theme_name' ),
					'add_new_item'		 => __( 'Add New Testimonial', 'codeex_theme_name' ),
					'edit_item'			=> __( 'Edit Testimonial', 'codeex_theme_name' ),
					'search_items' 		 => __( 'Search Testimonial', 'codeex_theme_name' ),
					'not_found' 			=> __( 'No testimonial found.', 'codeex_theme_name' ),
					'not_found_in_trash'   => __( 'No testimonial found in Trash.', 'codeex_theme_name' ),
				),
				'description' => 'Adding new testimonial',
				'public' => true,
				'has_archive' => true,	
				'publicly_queryable' => true,
				'show_ui' => true, 
				'show_in_menu' => true, 
				'query_var' => true,	
			   'rewrite' => array( 'slug' => __( 'testimonial-item', 'codeex_theme_name' ), 'with_front' => FALSE ),
			   'supports' => array('title', 'editor', 'author', 'comments', 'thumbnail', 'page-attributes', 'custom-fields', 'excerpt'),
				'menu_icon' =>plugins_url( 'custom-testimonial/assets/testimonials.png' )
			)
		);
	}
endif;
add_action( 'init', 'testimonials_post_type' );

/*----------------------------
		MetaBoxes
----------------------------*/

if ( ! function_exists( 'testimonials_meta' ) ) :
	function testimonials_meta() {
		global $post;
		$testimonials_client_name = sanitize_text_field( get_post_meta($post->ID, 'testimonials_client_name', TRUE ) );
		$testimonials_client_website = get_post_meta($post->ID, 'testimonials_client_website', TRUE );		
		?>
        <div class="mabuc-form-wrap">	
            <table>					                      
                <tr>
                    <td><label for="testimonials_client_website"><?php _e( 'Client Website', 'codeex_theme_name' ); ?></label> </td>
                    <td>
                        <textarea name="testimonials_client_website" id="testimonials_client_website"><?php echo esc_attr( $testimonials_client_website ); ?></textarea>																
                        &nbsp;<span class="desc"><?php _e( 'Provide client website here', 'codeex_theme_name' ); ?></span>
                    </td>
                </tr>                                                
                <tr>
                    <td><label for="testimonials_client_name"><?php _e( 'Client Name', 'codeex_theme_name' ); ?></label> </td>
                    <td>
                        <textarea name="testimonials_client_name" id="testimonials_client_name"><?php echo esc_attr( $testimonials_client_name ); ?></textarea>																
                        &nbsp;<span class="desc"><?php _e( 'Provide client name here', 'codeex_theme_name' ); ?></span>
                    </td>
                </tr>                				
            </table>					
        </div>		
		<?php	
	}
endif;

/*----------------------------
	Save and Update
	----------------------------*/
	
	if ( ! function_exists( 'testimonials_properties' ) ) :
		function testimonials_properties(){
			add_meta_box(
				"testimonials_meta", 
				__( 'Testimonials Options', 'codeex_theme_name' ), 
				"testimonials_meta", 
				"testimonials", 
				"normal", 
				"low"
			);
		}	
	endif;
	add_action( 'add_meta_boxes', 'testimonials_properties' );
	

	if ( ! function_exists( 'testimonials_save_properties' ) ) :
		function testimonials_save_properties( $post_id ){
			if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || (defined('DOING_AJAX') && DOING_AJAX)) return;
			if ( 'page' == isset($_POST['post_type']) ) { if ( !current_user_can( 'edit_page', $post_id ) ) return;
			} else { if ( !current_user_can( 'edit_post', $post_id ) ) return; }

			$rm_fields = array('testimonials_client_name', 'testimonials_client_website');

			foreach ($rm_fields as $rm_value) {
	         if( isset($rm_value) ) :

	            $rm_new = false;
	            $rm_old = get_post_meta( $post_id, $rm_value, true );

	            if ( isset( $_POST[$rm_value] ) ) :
	               $rm_new = $_POST[$rm_value];
	           	endif;

	            if ( isset( $rm_new ) && '' == $rm_new && $rm_old ) :
	               delete_post_meta( $post_id, $rm_value, $rm_old );
	            elseif ( false === $rm_new || !isset( $rm_new ) ) :
	            	delete_post_meta( $post_id, $rm_value, $rm_old );
	            elseif ( isset( $rm_new ) && $rm_new != $rm_old ) :
	            	update_post_meta( $post_id, $rm_value, $rm_new );
	           	elseif ( ! isset( $rm_old ) && isset( $rm_new ) ) :
	               add_post_meta( $post_id, $rm_value, $rm_new );
	            endif;

	         endif;
	      }
		}	
	endif;
	add_action('save_post', 'testimonials_save_properties');		   
	
include_once 'shortcode.php';	
?>
