<?php

if(!class_exists('Client_Testimonials_Shortcode')):

class Client_Testimonials_Shortcode {

	public static function get_testimonial_new($posts_per_page = -1, $orderby = 'none'){
	
		ob_start();

		$args = array(
			'posts_per_page' => (int) $posts_per_page,
			'post_type' => 'testimonials',
			'orderby' => $orderby,
			'no_found_rows' => true
		);

		$query = new WP_Query( $args  );

		if ( $query->have_posts() ):
			?>
           
            <?php
			while ( $query->have_posts() ) : $query->the_post();
			?>
					<div class="single-feedback">			
	                <?php if ( has_post_thumbnail() ): 
							$feat_image = wp_get_attachment_url( get_post_thumbnail_id() );
					?>
						<!--<div class="testimonial-avatar">
	                		<?php //the_post_thumbnail( array(64,64)); ?>
                            <img src="<?=$feat_image?>" />
						</div>-->
	                <?php endif; ?>					
						<div class="testimonial-content genericon-quote">
							<?php echo get_the_content(); ?>
						</div>									
				</div>
			<?php
			endwhile;
			?>
           
            <?php
		endif;wp_reset_query();

		$feedback = ob_get_clean();
		return $feedback;
	}

	public static function testimonials_slide( $items_desktop = 4, $items_tablet = 3, $items_tablet_small = 2, $items_mobile = 1, $posts_per_page = -1, $orderby = 'none'){
		ob_start();

		$id = rand(0, 99);
		?>
        <div class="container inner-wrap">
            <div class="row">
                <div id="testimonials-<?php echo $id; ?>" class="owl-carousel testimonial-wrap col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?php echo self::get_testimonial_new($posts_per_page, $orderby); ?>
                </div>
            </div>
        </div>
	    <script type="text/javascript">
			jQuery(document).ready(function($) {
	  			$('#testimonials-<?php echo $id; ?>').owlCarousel({
					items : <?php echo $items_desktop; ?>,
					nav : true,					
					loop : true,
					autoplay: true,
					autoplayHoverPause: true,
					responsiveClass:true,
				    responsive:{
				        320:{ items:<?php echo $items_mobile; ?> }, // Mobile portrait
				        600:{ items:<?php echo $items_tablet_small; ?> }, // Small tablet portrait
				        768:{ items:<?php echo $items_tablet; ?> }, // Tablet portrait
				        979:{ items:<?php echo $items_desktop; ?> }  // Desktop
				    }
				});
			});
	    </script>
		<?php
		$feedback = ob_get_clean();
		return $feedback;
	}

	public static function testimonials_slide_shortcode( $atts, $content = null ){
		extract(shortcode_atts(array(
	                        'items_desktop' => 1,
	                        'items_tablet' => 1,
	                        'items_tablet_small' => 1,
	                        'items_mobile' => 1,
	                        'posts_per_page' => -1,
	                        'orderby' => 'none'
	                ), $atts));

		return self::testimonials_slide($items_desktop, $items_tablet, $items_tablet_small, $items_mobile, $posts_per_page, $orderby );
	}

	/**
	 * Display a testimonial
	 *
	 * @param	int $post_per_page  The number of testimonials you want to display
	 * @param	string $orderby  The order by setting  https://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters
	 * @param	array $testimonial_id  The ID or IDs of the testimonial(s), comma separated
	 *
	 * @return	string  Formatted HTML
	 */
	public static function get_testimonial( $posts_per_page = 1, $orderby = 'none', $testimonial_id = null ) {
		$args = array(
			'posts_per_page' => (int) $posts_per_page,
			'post_type' => 'testimonials',
			'orderby' => $orderby,
			'no_found_rows' => true,
		);
		if ( $testimonial_id )
			$args['post__in'] = array( $testimonial_id );

		$query = new WP_Query( $args  );

		$testimonials = "";
		if ( $query->have_posts() ) {
			$testimonials .="<div class=\"testimonial-page-wrap\">";
			while ( $query->have_posts() ) : $query->the_post();
				$post_id = get_the_ID();
				$client_name = get_post_meta( $post_id, 'client_name', true );				
				$post_date = get_the_date( 'F j, Y', $post_id ); 
				$testimonials .= "<div class=\"testimonial-desc\">";
				if ( has_post_thumbnail() ): 
						$feat_image = wp_get_attachment_url( get_post_thumbnail_id() );
						//$testimonials .= "<div class=\"testimonial-avatar\"><img src=\"$feat_image\" /></div>";
				 endif; 
				if($client_name!=''){
					$testimonials .= "<h5>$client_name</h5>";				
				}
				$testimonials .= "<span class=\"quote\">" . get_the_content() . "</span>";
				$testimonials .= "<span class=\"testimonial-date\">$post_date</span>";
				$testimonials .= "</div>";

			endwhile;
			$testimonials .="</div>";
			wp_reset_postdata();
		}

		return $testimonials;
	}

	/**
	 * Shortcode to display testimonials
	 *
	 * This functions is attached to the 'testimonial' action hook.
	 *
	 * [testimonial posts_per_page="1" orderby="none" testimonial_id=""]
	 */
	public static function testimonial_shortcode( $atts ) {
		extract( shortcode_atts( array(
			'posts_per_page' => '1',
			'orderby' => 'none',
			'testimonial_id' => '',
		), $atts ) );

		return self::get_testimonial( $posts_per_page, $orderby, $testimonial_id );
	}

}

add_shortcode( 'testimonial', array( 'Client_Testimonials_Shortcode', 'testimonial_shortcode' ) );
add_shortcode( 'testimonials-slider', array( 'Client_Testimonials_Shortcode', 'testimonials_slide_shortcode' ) );
add_shortcode( 'client-testimonials', array( 'Client_Testimonials_Shortcode', 'testimonials_slide_shortcode' ) );
endif;