<?
/*
Plugin Name: Nerdster Custom Registration
Description: Create custom registration for your site
Version: 1.0
Author: Nerdster
*/
if( !defined( 'ABSPATH' ) ) exit;
class Custom_registration_form
{

    private $username;
    private $email;
	private $cemail;
	private $firstname;
    private $lastname;
    private $password1;
	private $cpassword1;
	/// new attributes	
	private $cr_company;
	private $cr_abn;
	private $cr_verify;
	private $cr_phone;
	private $cr_street;
	private $cr_suburb;
	private $cr_pcode;
	private $cr_region;
	private $cr_state;
	


    function __construct()
    {

        add_shortcode('register_form', array($this, 'register_shortcode'));
		add_shortcode('verify_email', array($this, 'email_verify_shortcode'));
		add_shortcode('register_retailer', array($this, 'retailer_shortcode'));
        add_action('wp_enqueue_scripts', array($this, 'flat_ui_kit'));
    }    
	
	/// regiteration process starts /////
    
	function register_shortcode()
    {
        ob_start();
        if ($_POST['reg_submit']) {
			$name = trim($_POST['reg_name']);
			$nams = explode(' ',$name);
			$this->lastname  = $nams[count($nams)-1];
			$fname = '';
			for($i=0;$i<count($nams)-1;$i++){
				if($fname == '')
				$fname=$nams[$i];
				else
				$fname.= " ".$nams[$i];
			}			
			$this->firstname = $fname;
            $this->username = $_POST['reg_email'];
            $this->email = $_POST['reg_email'];
			$this->cemail = $_POST['reg_cemail'];            
            $this->cr_phone = $_POST['cr_phone'];
           // $this->cr_company = $_POST['cr_company'];
           // $this->cr_abn = $_POST['cr_abn'];

            $this->register_validation();
            $this->registration();
        }
		if(!is_user_logged_in()) {
			$this->registration_form();
		}
        return ob_get_clean();
    }
	
	public function registration_form()
    {
        ?>
		<div class="form-close">X</div>
        <form method="post" action="<?php echo esc_url($_SERVER['REQUEST_URI']); ?>">
            <div class="login-form">
                <div class="form-group">
                    <input name="reg_name" type="text" class="form-control login-field"
                           value="<?php echo(isset($_POST['reg_name']) ? $_POST['reg_name'] : null); ?>"
                           placeholder="Your name" id="reg-name" required/>
                    <label class="login-field-icon fui-user" for="reg-name"></label>
                </div>

                <div class="form-group">
                    <input name="reg_email" type="email" class="form-control login-field"
                           value="<?php echo(isset($_POST['reg_email']) ? $_POST['reg_email'] : null); ?>"
                           placeholder="Your email address" id="reg-email" required/>
                    <label class="login-field-icon fui-mail" for="reg-email"></label>
                </div>
                <div class="form-group">
                    <input name="reg_cemail" type="email" class="form-control login-field"
                           value="<?php echo(isset($_POST['reg_cemail']) ? $_POST['reg_cemail'] : null); ?>"
                           placeholder="Confirm email address" id="reg-cemail" required/>
                    <label class="login-field-icon fui-mail" for="reg-cemail"></label>
                </div>
                <?php if(get_option('show_phone_register_form')!="1"){?>
				<div class="form-group">
                    <input name="cr_phone" type="text" class="form-control login-field"
                           value="<?php echo(isset($_POST['cr_phone']) ? $_POST['cr_phone'] : null); ?>"
                           placeholder="Best Number to contact you on" id="cr-phone"/>
                    <label class="login-field-icon fui-time" for="cr-phone"></label>
                </div>
                <?php }?>
                <!--<div class="form-group">
                	<p class="notes">Is this a business purchase?(invoice will be issued to the business)</p>
                    <input name="cr_company" type="text" class="form-control login-field"
                           value="<?php //echo(isset($_POST['cr_company']) ? $_POST['cr_company'] : null); ?>"
                           placeholder="Business Name" id="cr-company"/>
                    <label class="login-field-icon fui-user" for="cr-company"></label>
                </div>
                <div class="form-group">
                    <input name="cr_abn" type="text" class="form-control login-field"
                           value="<?php //echo(isset($_POST['cr_abn']) ? $_POST['cr_abn'] : null); ?>"
                           placeholder="ABN" id="reg-bio"/>
                    <label class="login-field-icon fui-new" for="reg-bio"></label>
                </div>-->
                
                </div>
                <div class="haggle-seperator"><span>Or</span></div>				
                <div class="social-login">			
                    <?php echo do_shortcode("[wordpress_social_login]");?>
                </div>
                <div class="register-button-wrap">
                	<div class="field terms">   
                    <label>By clicking Register, you agree to the Hagglefree 
                        <a href="<?= home_url('customer-tc');?>" target="_blank">Terms and conditions</a>.
                    </label>
                    </div>
                    <input class="haggle_button fullwidth_button" type="submit" name="reg_submit" value="Register"/>                
                </div>
        </form>        
    <?php
    }
	
	function register_validation()
    {

        if (empty($this->username) || empty($this->email)) {
            return new WP_Error('field', 'Required form field is missing');
        }
		if ($this->email != $this->cemail){
			return new WP_Error('email_mismatch', 'Email id mismatch');
		}
        if (strlen($this->username) < 4) {
            return new WP_Error('username_length', 'Username too short. At least 4 characters is required');
        }
        if (!is_email($this->email)) {
            return new WP_Error('email_invalid', 'Email is not valid');
        }
        if (email_exists($this->email)) {
            return new WP_Error('email', 'Email Already in use');
        }      

        $details = array('Username' => $this->username            
        );

        foreach ($details as $field => $detail) {
            if (!validate_username($detail)) {
                return new WP_Error('name_invalid', 'Sorry, the "' . $field . '" you entered is not valid');
            }
        }

    }
	
	function registration()
    {
		$mail_verify = rand(100000,999999);		
		$headers[] = 'Content-Type: text/html; charset=UTF-8';
		$headers[] = 'From: '.get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'> ' . "\r\n";
		
        $userdata = array(		
            'user_login' => esc_attr($this->username),
            'user_email' => esc_attr($this->email), 
			'first_name' => esc_attr($this->firstname), 
			'last_name' => esc_attr($this->lastname), 
        );

        if (is_wp_error($this->register_validation())) {
            echo '<div style="margin-bottom: 6px" class="btn btn-block btn-lg btn-danger">';
            echo '<strong>' . $this->register_validation()->get_error_message() . '</strong>';
            echo '</div>';
        } else {
            $register_user = wp_insert_user($userdata);
            if (!is_wp_error($register_user)) {
				wp_update_user( array ('ID' => $register_user, 'role' => 'buyer-account' ) ) ;
				update_user_meta($register_user, 'cr_verify', $mail_verify);		
				update_user_meta($register_user, 'cr_phone', $this->cr_phone);
				//update_user_meta($register_user, 'cr_company', $this->cr_company);
				//update_user_meta($register_user, 'cr_abn', $this->cr_abn);
				update_user_meta($register_user, 'cr_status', 'inactive');
				////mail content
				$verify_url = home_url()."/verify-email/?verify-email=$mail_verify";
				$media_url = get_stylesheet_directory_uri()."/img";
				$template_name = "account_creation_for_buyer";
				//Codes: {site_name}, {site_url}, {site_tag}, {media_url}, {email_heading}, {verify_url}, {admin_email}, {site_abn}, {site_phone}, {fb_url}
				$template_detail = get_email_template($template_name);
				
				//======== Token Replacement =================//
				$mail_content = str_replace("{site_name}",get_bloginfo( 'name' ),stripslashes($template_detail['email_template']));				
				$mail_content = str_replace("{site_url}",home_url(),$mail_content);
				$mail_content = str_replace("{site_tag}",get_bloginfo( 'description' ),$mail_content);
				$mail_content = str_replace("{media_url}",$media_url,$mail_content);	
				$mail_content = str_replace("{email_heading}",stripslashes($template_detail['email_heading']),$mail_content);
				$mail_content = str_replace("{verify_url}",$verify_url,$mail_content);
				$mail_content = str_replace("{admin_email}",get_option( 'admin_email' ),$mail_content);
				$mail_content = str_replace("{site_abn}",get_option( 'site_abn' ),$mail_content);
				$mail_content = str_replace("{site_phone}",get_option( 'site_phone' ),$mail_content);
				$mail_content = str_replace("{fb_url}",get_option( 'fb_url' ),$mail_content);
				//======== Token Replacement =================//
									
				wp_mail( $this->email, $template_detail['subject'], $mail_content,$headers);
			    echo "<script type='text/javascript'>
							window.location.href=\"".home_url('thank-you-for-signup')."\";
					  </script>";					
				//wp_redirect( home_url('thank-you-for-signup'), 301 ); exit; ;
                echo '<div style="margin-bottom: 6px" class="btn btn-block btn-lg btn-danger">';
                echo '<strong>Registration complete. Goto <a href="' .site_url('my-account'). '">login page</a></strong>';
                echo '</div>';
            } else {
                echo '<div style="margin-bottom: 6px" class="btn btn-block btn-lg btn-danger">';
                echo '<strong>' . $register_user->get_error_message() . '</strong>';
                echo '</div>';
            }
        }

    }
	
	/// regiteration process ends /////
	
	/// Email verification process starts /////
	function email_verify_shortcode(  $atts)
	{
		
		ob_start();
		$atts = shortcode_atts( array(
			'email' => '',
			'id'  => ''			
		), $atts, 'verify_email' );		
		if ($_POST['reg_psubmit']) {	
		
			$this->password1 = $_POST['reg_password'];
			$this->cpassword1 = $_POST['reg_cpassword'];
			$this->email_verify_validation();
			$this->verify_form($atts['id'],$atts['email']);
		}		
		
		
		if(!is_user_logged_in()) {
			$this->verify_email_form($atts['id'],$atts['email']);
			
		}		
		return ob_get_clean();
	}
	
	function email_verify_validation()
    {			
        if (empty($this->password1) || empty($this->cpassword1)) {
            return new WP_Error('fields', 'Required form field is missing');
        }
        if (strlen($this->password1) < 5) {
            return new WP_Error('password1', 'Password length must be greater than 5');
        }
		if (trim($this->password1) != trim($this->cpassword1)){
			return new WP_Error('password_missmatch', 'Password missmatch');
		}
		//return new WP_Error('debugging', 'Debugging mode on');
    }
	
	function verify_form($id,$email)
	{	
		global $wpdb;		
		$table_user_manager = $wpdb->prefix . 'user_status_manager';
					
		$headers[] = 'Content-Type: text/html; charset=UTF-8';
		$headers[] = 'From: '.get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'> ' . "\r\n";
		if (is_wp_error($this->email_verify_validation())) {
            echo '<div style="margin-bottom: 6px" class="btn btn-block btn-lg btn-danger">';
            echo '<strong>' . $this->email_verify_validation()->get_error_message() . '</strong>';
            echo '</div>';
        } else {			
			//$register_user1 = wp_update_user(array('ID' => $id, 'user_pass' => $this->password1));            
           // if (!is_wp_error($register_user1)) {
			   
				wp_set_password( $this->password1, $id );				
				update_user_meta($id, 'cr_verify', '');		
				update_user_meta($id, 'cr_status', 'active');
				$update_val = array('status'	  => 0);
			    $where = array('user_id' 	  => $id);	
				$wpdb->update($table_user_manager,$update_val,$where);	
				$template_name = "email_verification_for_buyer";
				$media_url = get_stylesheet_directory_uri()."/img";
				//Codes: {site_name}, {site_url}, {site_tag}, {media_url}, {email_heading}, {login_url}, {admin_email}, {site_abn}, {site_phone}, {fb_url}
				$template_detail = get_email_template($template_name);
				
				//======== Token Replacement =================//
				$mail_content = str_replace("{site_name}",get_bloginfo( 'name' ),stripslashes($template_detail['email_template']));				
				$mail_content = str_replace("{site_url}",home_url(),$mail_content);
				$mail_content = str_replace("{site_tag}",get_bloginfo( 'description' ),$mail_content);
				$mail_content = str_replace("{media_url}",$media_url,$mail_content);	
				$mail_content = str_replace("{email_heading}",stripslashes($template_detail['email_heading']),$mail_content);
				$mail_content = str_replace("{login_url}",home_url("my-account"),$mail_content);
				$mail_content = str_replace("{admin_email}",get_option( 'admin_email' ),$mail_content);
				$mail_content = str_replace("{site_abn}",get_option( 'site_abn' ),$mail_content);
				$mail_content = str_replace("{site_phone}",get_option( 'site_phone' ),$mail_content);
				$mail_content = str_replace("{fb_url}",get_option( 'fb_url' ),$mail_content);
				//======== Token Replacement =================//
				
				wp_mail( $email, $template_detail['subject'], $mail_content,$headers);
				
                echo '<div style="margin-bottom: 6px" class="btn btn-block btn-lg btn-danger">';
                echo '<strong>Email verification complete. Go to <a href="' . home_url('my-account') . '" class="login_link">login page</a></strong>';
                echo '</div>';
/*            } else {
                echo '<div style="margin-bottom: 6px" class="btn btn-block btn-lg btn-danger">';
                echo '<strong>' . $register_user1->get_error_message() . '</strong>';
                echo '</div>';
            }
*/        }
	}
	
	
	public function verify_email_form($id,$email)
    {		
        ?>		
        <form method="post" action="<?php echo esc_url($_SERVER['REQUEST_URI']); ?>">
            <div class="password-update-form">
                <div class="form-group">
                    <input name="reg_password" type="password" class="form-control login-field"
                           value="<?php echo(isset($_POST['reg_password']) ? $_POST['reg_password'] : null); ?>"
                           placeholder="Password" id="reg-pass" required/>
                    <label class="login-field-icon fui-lock" for="reg-pass"></label>
                </div>
                
                <div class="form-group">
                    <input name="reg_cpassword" type="password" class="form-control login-field"
                           value="<?php echo(isset($_POST['reg_cpassword']) ? $_POST['reg_cpassword'] : null); ?>"
                           placeholder="Confirm Password" id="reg-cpass" required/>
                    <label class="login-field-icon fui-lock" for="reg-pass"></label>
                </div>
                <div class="form-group">
                	<input type="hidden" value="<?=$email?>" name="reg_email" />
                </div>
                
                <input class="haggle_button fullwidth_button" type="submit" name="reg_psubmit" value="Update Password"/>
                </div>
        </form>        
    <?php
    }
	
	/// Email verification process ends /////
	
	/// Retailer registration process starts /////
	
	function retailer_shortcode(){
		ob_start();
        if ($_POST['ret_submit']) {
			
			$this->username = $_POST['reg_email'];
			$this->firstname = $_POST['reg_fname'];
			$this->lastname = $_POST['reg_lname'];            
            $this->email = $_POST['reg_email'];   			         
            $this->cr_phone = $_POST['reg_phone'];
            $this->cr_company = $_POST['reg_company'];
            $this->cr_street = $_POST['reg_address'];
			$this->cr_suburb = $_POST['reg_suburb'];
			$this->cr_pcode = $_POST['reg_pcode'];
			$this->cr_region = $_POST['reg_region'];
			$this->cr_state = $_POST['reg_state'];
			
			$this->retailer_validation();
            $this->retailer_registration();
		}
		if(!is_user_logged_in()) {
			$this->retailer_form();
		}
        return ob_get_clean();
	}
	
	function retailer_validation(){
		if (empty($this->cr_street) || empty($this->email) || empty($this->firstname) || empty($this->lastname) || empty($this->cr_phone) || empty($this->cr_company) || empty($this->cr_region) || empty($this->cr_pcode)) {
            return new WP_Error('field', 'Required form field is missing');
        }       
        if (!is_email($this->email)) {
            return new WP_Error('email_invalid', 'Email is not valid');
        }
        if (email_exists($this->email)) {
            return new WP_Error('email', 'Email Already in use');
        }        
		//return new WP_Error('debugging', 'Debugging mode on');
		
	}
	
	function retailer_registration(){		
		$headers[] = 'Content-Type: text/html; charset=UTF-8';
		$headers[] = 'From: '.get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'> ' . "\r\n";
		
		 $userdata = array(		
            'user_login' => esc_attr($this->username),
            'user_email' => esc_attr($this->email), 
			'first_name' => esc_attr($this->firstname), 
			'last_name' => esc_attr($this->lastname), 
        );
		
		 if (is_wp_error($this->retailer_validation())) {
            echo '<div style="margin-bottom: 6px" class="btn btn-block btn-lg btn-danger">';
            echo '<strong>' . $this->retailer_validation()->get_error_message() . '</strong>';
            echo '</div>';
        } else {
			
            $register_user = wp_insert_user($userdata);
            if (!is_wp_error($register_user)) {
				wp_update_user( array ('ID' => $register_user, 'role' => 'retailer-account' ) ) ;						
				update_user_meta($register_user, 'cr_phone', $this->cr_phone);
				update_user_meta($register_user, 'cr_company', $this->cr_company);
				update_user_meta($register_user, 'cr_street', $this->cr_street);
				update_user_meta($register_user, 'cr_suburb', $this->cr_suburb);
				update_user_meta($register_user, 'cr_pcode', $this->cr_pcode);
				update_user_meta($register_user, 'cr_region', $this->cr_region);
				update_user_meta($register_user, 'cr_state', $this->cr_state);
				update_user_meta($register_user, 'cr_status', 'inactive');
																
				//geo code lat/long calculation				
				 $addr = $this->cr_street.', '.$this->cr_suburb.', '.$this->cr_state;
				 $addr = str_replace(" ", "+", $addr);
				 $geocode = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$addr&components=country:AU&sensor=false");
				 $output = json_decode($geocode);
				 $result = (array) json_decode($geocode);
				 if( !empty($output->results) && $output->status == 'OK' ) {
					$lat 			= $output->results[0]->geometry->location->lat;
					$long 			= $output->results[0]->geometry->location->lng;
					update_user_meta($register_user, 'cr_lat', $lat);
					update_user_meta($register_user, 'cr_long', $long);
				}
				// geo calculation ends 
				$template_name = "admin_notification_for_retailer";
				$media_url = get_stylesheet_directory_uri()."/img";
				//Codes: {site_name}, {site_url}, {site_tag}, {media_url}, {email_heading}, {login_url}, {admin_email}, {site_abn}, {site_phone}, {fb_url}
				$template_detail = get_email_template($template_name);
				//======== Token Replacement =================//
				$mail_content = str_replace("{site_name}",get_bloginfo( 'name' ),stripslashes($template_detail['email_template']));				
				$mail_content = str_replace("{site_url}",home_url(),$mail_content);
				$mail_content = str_replace("{site_tag}",get_bloginfo( 'description' ),$mail_content);
				$mail_content = str_replace("{media_url}",$media_url,$mail_content);	
				$mail_content = str_replace("{email_heading}",stripslashes($template_detail['email_heading']),$mail_content);
				$mail_content = str_replace("{login_url}",home_url("my-account"),$mail_content);
				$mail_content = str_replace("{admin_email}",get_option( 'admin_email' ),$mail_content);
				$mail_content = str_replace("{site_abn}",get_option( 'site_abn' ),$mail_content);
				$mail_content = str_replace("{site_phone}",get_option( 'site_phone' ),$mail_content);
				$mail_content = str_replace("{fb_url}",get_option( 'fb_url' ),$mail_content);
				//======== Token Replacement =================//
				
				wp_mail( get_option( 'admin_email' ), $template_detail['subject'], $mail_content,$headers);
                echo '<div style="margin-bottom: 6px" class="btn btn-block btn-lg btn-danger">';
                echo '<strong>Registration Received. Referred for approval by Haggle Free Management.</strong>';
                echo '</div>';
            } else {
                echo '<div style="margin-bottom: 6px" class="btn btn-block btn-lg btn-danger">';
                echo '<strong>' . $register_user->get_error_message() . '</strong>';
                echo '</div>';
            }
        
		}
	}
	
	public function retailer_form(){
		?>        
		<form method="post" action="<?php echo esc_url($_SERVER['REQUEST_URI']); ?>">
            <div class="retailer-form">
                <div class="form-group">
                    <input name="reg_fname" type="text" class="form-control login-field"
                           value="<?php echo(isset($_POST['reg_fname']) ? $_POST['reg_fname'] : null); ?>"
                           placeholder="First Name" id="reg-fname" required/>
                    <label class="login-field-icon fui-user" for="reg-fname"></label>
                </div>
                
                <div class="form-group">
                    <input name="reg_lname" type="text" class="form-control login-field"
                           value="<?php echo(isset($_POST['reg_lname']) ? $_POST['reg_lname'] : null); ?>"
                           placeholder="Last Name" id="reg-lname" required/>
                    <label class="login-field-icon fui-user" for="reg-lname"></label>
                </div>
                
                 <div class="form-group">
                    <input name="reg_email" type="email" class="form-control login-field"
                           value="<?php echo(isset($_POST['reg_email']) ? $_POST['reg_email'] : null); ?>"
                           placeholder="Email Address" id="reg-email" required/>
                    <label class="login-field-icon fui-mail" for="reg-email"></label>
                </div>
                
                <div class="form-group">
                    <input name="reg_phone" type="text" class="form-control login-field"
                           value="<?php echo(isset($_POST['reg_phone']) ? $_POST['reg_phone'] : null); ?>"
                           placeholder="Phone Number" id="reg-phone" required/>
                    <label class="login-field-icon genericon genericon-phone" for="reg-phone"></label>
                </div>
                
                <div class="form-group">
                    <input name="reg_company" type="text" class="form-control login-field"
                           value="<?php echo(isset($_POST['reg_company']) ? $_POST['reg_company'] : null); ?>"
                           placeholder="Company Name" id="reg-company" required/>
                    <label class="login-field-icon genericon genericon-home" for="reg-company"></label>
                </div>
                
                <div class="form-group">
                    <input name="reg_address" type="text" class="form-control login-field"
                           value="<?php echo(isset($_POST['reg_address']) ? $_POST['reg_address'] : null); ?>"
                           placeholder="Street Address" id="reg-address" required/>
                    <label class="login-field-icon genericon genericon-location" for="reg-address"></label>
                </div>
                
                <div class="form-group">
                    <input name="reg_suburb" type="text" class="form-control login-field"
                           value="<?php echo(isset($_POST['reg_suburb']) ? $_POST['reg_suburb'] : null); ?>"
                           placeholder="Suburb" id="reg-suburb" required/>
                    <label class="login-field-icon genericon genericon-location" for="reg-suburb"></label>
                </div>
                
                <div class="form-group">
                    <input name="reg_pcode" type="text" class="form-control login-field"
                           value="<?php echo(isset($_POST['reg_pcode']) ? $_POST['reg_pcode'] : null); ?>"
                           placeholder="Postal Code" id="reg-pcode" required/>
                    <label class="login-field-icon genericon genericon-location" for="reg-pcode"></label>
                </div>
                
                 <div class="form-group haggle-select">                    
                    <select name="reg_region" class="form-control login-field" id="reg_region">                    	
                      <option classname="invalid" value="">Applied for</option>
                      <?php
                      		$levels = pmpro_level_list();
							foreach($levels as $level){
								?>
                                <option value="<?php echo $level->id;?>" <?php echo(($_POST['reg_region']==$level->id) ? 'selected' : null); ?>><?php echo $level->name;?></option>
                             <?php 
							}
					  ?>                                           
                    </select>       
                    <label class="login-field-icon" for="reg-region"></label>
                </div>
                
                <div class="form-group haggle-select" id="reg_state"  style="display:none">                    
                    <select name="reg_state" class="form-control login-field">                    	
                      <option classname="invalid" value="">State</option>
                      <option value="ACT" <?php echo(($_POST['reg_state']=='ACT') ? 'selected' : null); ?>>ACT</option>
                      <option value="NSW"  <?php echo(($_POST['reg_state']=='NSW') ? 'selected' : null); ?>>NSW</option>
                      <option value="NT"  <?php echo(($_POST['reg_state']=='NT') ? 'selected' : null); ?>>NT</option>
                      <option value="QLD"  <?php echo(($_POST['reg_state']=='QLD') ? 'selected' : null); ?>>QLD</option>
                      <option value="SA"  <?php echo(($_POST['reg_state']=='SA') ? 'selected' : null); ?>>SA</option>
                      <option value="TAS"  <?php echo(($_POST['reg_state']=='TAS') ? 'selected' : null); ?>>TAS</option>
                      <option value="VIC"  <?php echo(($_POST['reg_state']=='VIC') ? 'selected' : null); ?>>VIC</option>
                      <option value="WA"  <?php echo(($_POST['reg_state']=='WA') ? 'selected' : null); ?>>WA</option>
                    </select>       
                    <label class="login-field-icon genericon genericon-location" for="reg-state"></label>
                </div>                
                </div>
                <!--<div class="haggle-seperator"><span>Or</span></div>				
                <div class="social-login">			
                    <?php //echo do_shortcode("[wordpress_social_login]");?>
                </div> -->               
                <?php if (!$_POST['ret_submit']) {?>
                <div class="register-button-wrap">
                	
                	<div class="field terms">   
                        <label>By clicking Enroll Now, you agree to the Hagglefree 
                            <a href="<?= home_url('retailer-tc');?>" target="_blank">Terms and conditions</a>.
                        </label>
                    </div>
                    <input class="haggle_button fullwidth_button" type="submit" name="ret_submit" value="Enroll Now"/>
                </div>
               <?php }?> 
        </form> 
        <script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery('#reg_region').change(function(){
					 if(jQuery(this).val() == '2')
					 {
						  jQuery('#reg_state').show();
					 }else{
						 jQuery('#reg_state').hide();
					 }
				});
			});
		</script>
     <?php   
	}
	
	/// Retailer registration process ends /////
	
	function flat_ui_kit()
    {
        wp_enqueue_style('bootstrap-css', plugins_url('bootstrap/css/bootstrap.css', __FILE__));
        wp_enqueue_style('flat-ui-kit', plugins_url('css/flat-ui.css', __FILE__));

    }
}

new Custom_registration_form;

// Hooks near the bottom of profile page (if current user) 
add_action('show_user_profile', 'cr_user_profile_fields');

// Hooks near the bottom of the profile page (if not current user) 
add_action('edit_user_profile', 'cr_user_profile_fields');

// @param WP_User $user
function cr_user_profile_fields( $user ) {
	global $wpdb;
	$user_id = $user->ID;
	$user = new WP_User( $user_id );
	$role = $user->roles[0];
	$table_usermeta = $wpdb->prefix."usermeta";
	$profile_image_url = $wpdb->get_results("SELECT `meta_value` FROM `$table_usermeta` WHERE `user_id` = '$user_id' && `meta_key` = 'cr_photo'");
?>	
	<h2>Additional Information</h2>
    <table class="form-table">
        <tr>
            <th>
                <label for="cr_photo"><?php _e( 'Avatar' ); ?></label>
            </th>
            <td>             		
                <input type="file" name="cr_photo" id="cr_photo" class="upload" /><a id="fileSelect" href="javascript:void(0);">&nbsp;</a>
            </td>
        </tr>        
    </table>
    <table class="form-table">
        <tr>
            <th>
                <label for="cr_phone"><?php _e( 'Phone' ); ?></label>
            </th>
            <td>
                <input type="text" name="cr_phone" id="cr_phone" value="<?php echo esc_attr( get_the_author_meta( 'cr_phone', $user->ID ) ); ?>" class="regular-text" />
            </td>
        </tr>
    </table>
    <table class="form-table">
        <tr>
            <th>
                <label for="cr_company"><?php _e( 'Company' ); ?></label>
            </th>
            <td>
                <input type="text" name="cr_company" id="cr_company" value="<?php echo esc_attr( get_the_author_meta( 'cr_company', $user->ID ) ); ?>" class="regular-text" />
            </td>
        </tr>
    </table>
    <table class="form-table">
        <tr>
            <th>
                <label for="cr_street"><?php _e( 'Street Address' ); ?></label>
            </th>
            <td>
                <input type="text" name="cr_street" id="cr_street" value="<?php echo esc_attr( get_the_author_meta( 'cr_street', $user->ID ) ); ?>" class="regular-text" />
            </td>
        </tr>
    </table>
    <table class="form-table">
        <tr>
            <th>
                <label for="cr_suburb"><?php _e( 'Suburb' ); ?></label>
            </th>
            <td>
                <input type="text" name="cr_suburb" id="cr_suburb" value="<?php echo esc_attr( get_the_author_meta( 'cr_suburb', $user->ID ) ); ?>" class="regular-text" />
            </td>
        </tr>
    </table>
    <table class="form-table">
        <tr>
            <th>
                <label for="cr_pcode"><?php _e( 'Post Code' ); ?></label>
            </th>
            <td>
                <input type="text" name="cr_pcode" id="cr_pcode" value="<?php echo esc_attr( get_the_author_meta( 'cr_pcode', $user->ID ) ); ?>" class="regular-text" />
            </td>
        </tr>
    </table>
    <table class="form-table">
        <tr>
            <th>
                <label for="cr_region"><?php _e( 'Applied for' ); ?></label>
            </th>
            <td>
            	<select name="cr_region" class="form-control login-field" id="cr_region">                    	
                      <option classname="invalid" value="">Applied for</option>
                      <?php
                      		$levels = pmpro_level_list();
							foreach($levels as $level){
								?>
                                <option value="<?php echo $level->id;?>" <?php echo((get_the_author_meta( 'cr_region', $user->ID )==$level->id)? 'selected' : null); ?>><?php echo $level->name;?></option>
                             <?php 
							}
					  ?>                                           
                    </select>             	
            </td>
        </tr>
    </table>
    <table class="form-table">
        <tr>
            <th>
                <label for="cr_state"><?php _e( 'State' ); ?></label>
            </th>
            <td>
                <input type="text" name="cr_state" id="cr_state" value="<?php echo esc_attr( get_the_author_meta( 'cr_state', $user->ID ) ); ?>" class="regular-text" />
            </td>
        </tr>
    </table>
     <table class="form-table">
        <tr>
            <th>
                <label for="cr_lat"><?php _e( 'Latitude' ); ?></label>
            </th>
            <td>
                <input type="text" name="cr_lat" id="cr_lat" value="<?php echo esc_attr( get_the_author_meta( 'cr_lat', $user->ID ) ); ?>" class="regular-text" />
            </td>
        </tr>
    </table>
     <table class="form-table">
        <tr>
            <th>
                <label for="cr_long"><?php _e( 'Longitude' ); ?></label>
            </th>
            <td>
                <input type="text" name="cr_long" id="cr_long" value="<?php echo esc_attr( get_the_author_meta( 'cr_long', $user->ID ) ); ?>" class="regular-text" />
            </td>
        </tr>
    </table>
     <table class="form-table">
        <tr>
            <th>
                <label for="cr_abn"><?php _e( 'ABN' ); ?></label>
            </th>
            <td>
                <input type="text" name="cr_abn" id="cr_abn" value="<?php echo esc_attr( get_the_author_meta( 'cr_abn', $user->ID ) ); ?>" class="regular-text" />
            </td>
        </tr>
    </table>
    <?php if($role == "retailer-account"){?>
    <table class="form-table">
        <tr>
            <th>
                <label for="cr_lead_fee"><?php _e( 'Lead Fee Discount' ); ?></label>
            </th>
            <td>
                <input type="text" name="cr_lead_fee" id="cr_lead_fee" value="<?php echo esc_attr( get_the_author_meta( 'cr_lead_fee', $user->ID ) ); ?>" class="regular-text" />%
            </td>
        </tr>
    </table>
    <table class="form-table">
        <tr>
            <th>
                <label for="cr_retailer_type"><?php _e( 'Retailer Type' ); ?></label>
            </th>
            <td>
                <select name="cr_retailer_type" class="form-control login-field" id="cr_retailer_type">                    	
                  <option classname="invalid" value="">Please choose</option>                  
                   <option value="commercial" <?php echo((get_the_author_meta( 'cr_retailer_type', $user->ID )=='commercial')? 'selected' : null); ?>>Commercial</option>                                                             
                   <option value="retail" <?php echo((get_the_author_meta( 'cr_retailer_type', $user->ID )=='retail')? 'selected' : null); ?>>Retail</option>                                                             
                   <option value="both" <?php echo((get_the_author_meta( 'cr_retailer_type', $user->ID )=='both')? 'selected' : null); ?>>Both</option>                                                             
                </select>         
            </td>
        </tr>
    </table>
    <?php }?>
<?php
}


// Hook is used to save custom fields that have been added to the WordPress profile page (if current user) 
add_action( 'personal_options_update', 'cr_update_extra_profile_fields' );

// Hook is used to save custom fields that have been added to the WordPress profile page (if not current user) 
add_action( 'edit_user_profile_update', 'cr_update_extra_profile_fields' );

function cr_update_extra_profile_fields( $user_id ) {
	global $wpdb;
	$table_usermeta = $wpdb->prefix."usermeta";
    if ( current_user_can( 'edit_user', $user_id ) ){
        update_user_meta( $user_id, 'cr_phone', $_POST['cr_phone'] );
		update_user_meta( $user_id, 'cr_company', $_POST['cr_company'] );
		update_user_meta( $user_id, 'cr_street', $_POST['cr_street'] );
		update_user_meta( $user_id, 'cr_suburb', $_POST['cr_suburb'] );
		update_user_meta( $user_id, 'cr_pcode', $_POST['cr_pcode'] );						
		update_user_meta( $user_id, 'cr_region', $_POST['cr_region']);       
		update_user_meta( $user_id, 'cr_state', $_POST['cr_state'] );
		update_user_meta( $user_id, 'cr_abn', $_POST['cr_abn'] );
		update_user_meta( $user_id, 'cr_lat', $_POST['cr_lat'] );
		update_user_meta( $user_id, 'cr_long', $_POST['cr_long'] );
		update_user_meta( $user_id, 'cr_lead_fee', $_POST['cr_lead_fee'] );
		update_user_meta( $user_id, 'cr_retailer_type', $_POST['cr_retailer_type'] );
				
		// upload photo
				$profile_image_url = $wpdb->get_results("SELECT `meta_value` FROM `$table_usermeta` WHERE `user_id` = '$user_id' && `meta_key` = 'cr_photo'");
				// file upload
				$upload_dir = wp_upload_dir();
				$target_dir = $upload_dir['basedir']."/retailer-avatar/";
				$orignal_dir = $upload_dir['basedir']."/retailer-avatar/orig/";
				
				$uniquename = substr(microtime(),2,8).rand(1000, 9999);
				$oldname = strtolower($_FILES['cr_photo']['name']);
				$newname = preg_replace('/(.*)\.(.*)$/i', $uniquename.'.$2', $oldname);
				$medium_name = (str_replace('.','_medium.',$newname));
				$original_name  = (str_replace('.','_orig.',$newname));
						
				$target_file = $target_dir . $medium_name;
				$orig_target_file = $orignal_dir . $original_name;
				
				$uploadOk = 1;
				$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
				$width =200;
				$height = 150;
				
				if(!is_uploaded_file($_FILES['cr_photo']['tmp_name'])) {
					$cr_photo   = $profile_image_url[0]->meta_value;
				}
				else{
					$check = getimagesize($_FILES["cr_photo"]["tmp_name"]);
					if($check !== false) {				
						$uploadOk = 1;
					} else {
						echo 'File is not an image.';				
						$uploadOk = 0;
					}
				}
				if(is_uploaded_file($_FILES['cr_photo']['tmp_name'])) {
					if ($_FILES["cr_photo"]["size"] > 2000000) {
					echo 'Sorry, your file is too large.';			
					$uploadOk = 0;
				} 
				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
					echo 'Sorry, only JPG, JPEG, PNG & GIF files are allowed.';			
					$uploadOk = 0;
				}
				if ($uploadOk == 0) {
					echo 'Sorry, your file was not uploaded.';
					
				} else {
					/* Get original image x y*/
					list($w, $h) = getimagesize($_FILES['cr_photo']['tmp_name']);
					/* calculate new image size with ratio */
					$ratio = $w / $h;
					if (($width / $height) > $ratio) {
						   $width = $height * $ratio;
					} else {
						   $height = $width / $ratio;
					}					
										
					/* read binary data from image file */
					$imgString = file_get_contents($_FILES['cr_photo']['tmp_name']);
					/* create image from string */
					$image = imagecreatefromstring($imgString);
					$tmp = imagecreatetruecolor($width, $height);
					
					
					imagecopyresampled($tmp, $image, 0, 0, $x, 0, $width, $height, $w, $h);
					
					
					/* Save image */
					switch ($_FILES['cr_photo']['type']) {
						case 'image/jpeg':
							imagejpeg($tmp, $target_file, 100);
							imagejpeg($image, $orig_target_file, 100);
							break;
						case 'image/png':
							imagepng($tmp, $target_file, 0);
							imagepng($image, $orig_target_file, 0);
							break;
						case 'image/gif':
							imagegif($tmp, $target_file);
							imagegif($image, $orig_target_file);
							break;
						default:
							exit;
							break;
					}
					
					$_mgm_cf_photo   =  $upload_dir['baseurl']."/retailer-avatar/".$medium_name;
					update_user_meta($user_id, 'cr_photo', $_mgm_cf_photo);
					set_custom_avatar($avatar,$user_id,"custom_avatar");
				}
			}
	}
}
if ( is_admin() ) {
    function add_post_enctype() {
        echo "<script type='text/javascript'>
                  jQuery(document).ready(function(){
                      jQuery('#your-profile').attr('enctype','multipart/form-data');
                  });
              </script>";
    }
    add_action('admin_head', 'add_post_enctype');
	
}
function set_custom_avatar( $avatar, $user_id, $alt ) {	
		if ( $custom_avatar = get_user_meta( $user_id, 'cr_photo',true ) ) {
			return "<img alt='{$alt}' src='{$custom_avatar}' class='avatar custom-avatar photo' />";
		}
	return $avatar;
}

add_filter( 'get_avatar', 'set_custom_avatar', 1, 5 );

// adding extra lead column in backend listing for user
function nerdster_modify_user_table( $column ) {
    $column['cr_lead_fee'] = 'Discount(%)';
	$column['cr_retailer_type'] = 'Retailer type';
    return $column;
}

function nerdster_modify_user_table_row( $val, $column_name, $user_id ) {
    switch ($column_name) {
        case 'cr_lead_fee' :
            return get_the_author_meta( 'cr_lead_fee', $user_id );
            break;
		case 'cr_retailer_type' :
            return ucfirst(get_the_author_meta( 'cr_retailer_type', $user_id ));
            break;
        default:
    }
    return $val;
}
if(isset($_GET['role']) && ($_GET['role']=="retailer-account")){
	add_filter( 'manage_users_custom_column', 'nerdster_modify_user_table_row', 14, 3 );
	add_filter( 'manage_users_columns', 'nerdster_modify_user_table' );
}

// show verified column for buyer
function nerdster_show_column_buyer( $column ) {
    $column['cr_status'] = 'Status';	
    return $column;
}

function nerdster_show_user_table_row_buyer( $val, $column_name, $user_id ) {
	$social_login = get_the_author_meta( 'wsl_current_provider', $user_id );
    switch ($column_name) {
        case 'cr_status' :
            return (get_the_author_meta( 'cr_status', $user_id )==""?$social_login:ucfirst(get_the_author_meta( 'cr_status', $user_id )));
            break;
        
    }
    return $val;
}
function my_sortable_status_column( $columns ) {
    $columns['cr_status'] = 'cr_status'; 
    return $columns;
}
add_filter( 'manage_users_sortable_columns', 'my_sortable_status_column' );
/*function user_status_column_orderby( $vars ) {
	if ( isset( $vars['orderby'] ) && 'cr_status' == $vars['orderby'] ) {
		$vars = array_merge( $vars, array(
			'meta_key' => 'cr_status',
			'orderby' => 'meta_value',
			'order'     => 'asc'
		) );
	}
	return $vars;
}
add_filter( 'pre_get_posts', 'user_status_column_orderby' );*/
add_action('pre_user_query', 'status_column_orderby');
function status_column_orderby($userquery){
	if('cr_status'==$userquery->query_vars['orderby']) {
		global $wpdb;
		$userquery->query_from .= " LEFT OUTER JOIN $wpdb->usermeta AS alias ON ($wpdb->users.ID = alias.user_id) ";//note use of alias
		$userquery->query_where .= " AND alias.meta_key = 'cr_status' ";//which meta are we sorting with?
		$userquery->query_orderby = " ORDER BY alias.meta_value ".($userquery->query_vars["order"] == "ASC" ? "asc " : "desc ");//set sort order
	}
}

if(isset($_GET['role']) && ($_GET['role']=="buyer-account")){
	add_filter( 'manage_users_custom_column', 'nerdster_show_user_table_row_buyer', 14, 3 );
	add_filter( 'manage_users_columns', 'nerdster_show_column_buyer' );
	
}

//Customize password change notification to admin
if ( !function_exists('wp_password_change_notification') ) :
/**
 * Notify the blog admin of a user changing password, normally via email.
 *
 * @since 2.7.0
 *
 * @param WP_User $user User object.
 */
function wp_password_change_notification( $user ) {
	// send a copy of password change notification to the admin
	// but check to see if it's the admin whose password we're changing, and skip this
	if ( 0 !== strcasecmp( $user->user_email, get_option( 'admin_email' ) ) ) {
		//$message = sprintf(__('Password Lost and Changed for user: %s'), $user->user_login) . "\r\n";
		// The blogname option is escaped with esc_html on the way into the database in sanitize_option
		// we want to reverse this for the plain text arena of emails.
		$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
		
		//Nerdster customization
		$message = "<div style=\"width: 767px; float: none; margin: 0 auto; background-color: #e6e6e6; overflow: hidden; padding: 20px;\">
		  <div style=\"background-color: #fff; float: none; width: 650px; margin: 0 auto; overflow: hidden; font-family: 'Open Sans', Calibri, Verdana, Arial; font-size: 15px; line-height: 21px; padding: 0;\"><!-- Template Header Starts Here -->
			<div style=\"width: 100%; float: left; margin: 0;\">
			  <ul style=\"list-style-type: none; width: 100%; padding: 0; margin: 0;\">
				<li style=\"background-color: #fff; width: 100%; text-align: left; list-style: none; padding: 6px 0; margin: 0;\"><a title=\"{site_url} - {site_tag}\" href=\"{site_url}\"> <img src=\"{media_url}/email-logo.jpg\" alt=\"{site_url} - {site_tag}\" /> </a></li>
				<li style=\"background-color: #000000; width: 100%; text-align: left; list-style: outside none none; margin: 0px; max-height: 181px;\"><img src=\"{media_url}/email-banner.jpg\" alt=\"Find the Best Price on your Product\" /></li>
				<li style=\"width: 100%; background-color: #ff7801; float: left; text-align: center; color: #fff; padding: 20px 0; text-transform: uppercase; font-weight: bold; font-size: 18px; margin: 0;\">{email_heading}</li>
			  </ul>
			</div>
			<!-- Template Header Ends Here --> 
			<!-- Template Content Starts Here -->
			<div style=\"background-color: #fff; min-height: 50px; padding: 10px 0; float: left; width: 100%;\">
			  <ul style=\"width: 96%; float: left; padding: 2%; margin: 0; font-size:16px\">
				  <li style=\"width: 100%; float: left; list-style: none; margin: 0; color: #000; font-size:16px; text-align:center\">".sprintf(__('Password Lost and Changed for user:<br /> %s'), $user->user_login)."</li>
			  </ul>
			  
			</div>
			<!-- Template Content Ends Here --> 
			<!-- Template Footer Starts Here -->
			<div style=\"width: 100%; background-color: #000; float: left; margin: 0;\">
			  <div style=\"width: 100%; float: left; list-style: none; background-color: #191919; margin: 0;\">
				<ul style=\"width: 90%; float: left; list-style: none; padding: 15px 5%; margin: 0;\">
				  <li style=\"width: 50%; float: left; list-style: none; padding: 10px 0; color: #717171; font-size: 12px; margin: 0;\">Thanks,<br /> 
					{site_name} Team <br /><span style=\"float: left; color: #717171;\"><strong>ABN:</strong> {site_abn}</span></li>
				  <li style=\"width: 50%; float: right; list-style: none; padding: 10px 0; color: #717171; font-size: 12px; margin: 0;\"><strong>Contact Us</strong><br /> 
				  <span style=\"background: #191919 url('{media_url}/email-icon-email.png') no-repeat scroll left center; padding-left: 18px;\"> <a style=\"color: #717171; text-decoration: none;\"  href=\"mailto:{admin_email}\">{admin_email}</a> </span><br />
				   <span style=\"background: #191919 url('{media_url}/ph-icon-email.png') no-repeat scroll left center; padding-left: 18px; float: left;\">{site_phone}</span></li>
				</ul>
			  </div>
			  <div style=\"width: 100%; float: left; list-style: none; background-color: #000;\">
				<ul style=\"width: 90%; float: left; list-style: none; padding: 6px 5%; margin: 0;\">
				  <li style=\"width: 50%; float: left; list-style: none; padding: 0; color: #4f4e4e; font-size: 12px; margin: 0;\">© copyright {site_name}. All Rights Reserved.</li>
				  <li style=\"width: 50%; float: right; list-style: none; padding: 0; color: #4f4e4e; font-size: 12px; text-align: left; margin: 0;\">
					<ul style=\"float: right; line-height: 16px; margin: 0px; padding: 0px; width: 103px;\">
					  <li style=\"list-style: outside none none; padding: 0px; margin: 0px; float: left; width: 80px; line-height: 26px;\">Follow Us On:</li>
					  <li style=\"list-style: outside none none; padding: 0px; margin: 0px; float: left; width: 20px;\"><a title=\"Follow Us On\" href=\"{fb_url}\"> <img src=\"{media_url}/email-fb-icon.png\" alt=\"Follow Us On\" /> </a></li>
					</ul>
				  </li>
				</ul>
			  </div>
			</div>
			<!-- Template Footer Ends Here --> 
			
		  </div>
		</div>";
		$media_url = get_stylesheet_directory_uri()."/img";
		
		//======== Token Replacement =================//
		$mail_content = str_replace("{site_name}",get_bloginfo( 'name' ),$message);				
		$mail_content = str_replace("{site_url}",home_url(),$mail_content);
		$mail_content = str_replace("{site_tag}",get_bloginfo( 'description' ),$mail_content);
		$mail_content = str_replace("{media_url}",$media_url,$mail_content);	
		$mail_content = str_replace("{email_heading}","Password Lost/Changed",$mail_content);
		
		$mail_content = str_replace("{login_url}",home_url("my-account"),$mail_content);
		
		$mail_content = str_replace("{admin_email}",get_option( 'admin_email' ),$mail_content);
		$mail_content = str_replace("{site_abn}",get_option( 'site_abn' ),$mail_content);
		$mail_content = str_replace("{site_phone}",get_option( 'site_phone' ),$mail_content);
		$mail_content = str_replace("{fb_url}",get_option( 'fb_url' ),$mail_content);
		//======== Token Replacement =================//
		
		wp_mail(get_option('admin_email'), sprintf(__('[%s] Password Lost/Changed'), $blogname), $mail_content);
	}
}
endif;