<?php
/**
 * Plugin Name: Nerdster Site Options 
 * Description: This Plugins add the optional site settings.
 * Author: Nerdster
 * Author URI: http://nerdster.com.au
 * Version: 0.1
 *
 *
 * @package     
 * @author      Nerdster
 * @category    Plugin
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */
 
 // Adding admin control for jobs\
//menu items
add_action('admin_menu','site_options_dashboard');
function site_options_dashboard() {
//this is the main item for the menu
	add_menu_page('Site Options', //page title
	'Site Options', //menu title
	'manage_options', //capabilities
	'site_options', //menu slug
	'site_options', //function
	 plugins_url( 'nerdster-site-options/images/options.png' )
	);
}

function site_options(){
		if($_POST['save_option']){
			$table_option  = 	$wpdb->prefix . 'options';			
			$site_abn = $_POST['abn'];
			$site_phone = $_POST['phone'];
			$site_location = $_POST['location'];
			$con_email = $_POST['con_email'];
			$fb_url  = $_POST['fb_url'];
			if(get_option('site_abn')){
				update_option( 'site_abn', $site_abn );
			}else{
				add_option( 'site_abn', $site_abn, '', 'yes' );		
			}
			
			if(get_option('site_phone')){
				update_option( 'site_phone', $site_phone );
			}else{
				add_option( 'site_phone', $site_phone, '', 'yes' );		
			}
			
			if(get_option('site_location')){
				update_option( 'site_location', $site_location );
			}else{
				add_option( 'site_location', $site_location, '', 'yes' );		
			}
			if(get_option('con_email')){
				update_option( 'con_email', $con_email );
			}else{
				add_option( 'con_email', $con_email, '', 'yes' );		
			}
			if(get_option('fb_url')){
				update_option( 'fb_url', $fb_url );
			}else{
				add_option( 'fb_url', $fb_url, '', 'yes' );		
			}
		}
		?>
        <h2>Site Options</h2>
        <hr/>
        <form method="post" action="">
        <table class="widefat" width="100%" border="0" cellspacing="5" cellpadding="5">            
              <tr>
                <td><strong>ABN</strong></td><td><input type="text" name="abn" value="<?=get_option('site_abn')?>"/></td>
              </tr>
              <tr>
                <td><strong>Phone</strong></td><td><input type="text" name="phone" value="<?=get_option('site_phone')?>"/></td>
              </tr>
              <tr>
                <td><strong>Location</strong></td><td><textarea name="location"><?=get_option('site_location')?></textarea></td>
              </tr>
              <tr>
                <td><strong>Contact Email</strong></td><td><input type="text" name="con_email" value="<?=get_option('con_email')?>"/></td>
              </tr>  
              <tr>
                <td><strong>Facebook Page Url</strong></td><td><input type="text" name="fb_url" value="<?=get_option('fb_url')?>"/></td>
              </tr>         
        </table>
        <input type="submit" value="Save" name="save_option" class="button button-primary"/>
        </form>
        <?php
}

//widgets for contact info at footer
class contact_info_widget extends WP_Widget {
 
	function __construct() {
		parent::__construct(
		// Base ID of your widget
		'contact_info_widget',
		 
		// Widget name will appear in UI
		__('Contact Info Widget', 'wpb_widget_domain'),
		 
		// Widget description
		array( 'description' => __( 'This widget displays the site information on the footer.', 'contact_info_widget_domain' ), )
		);
	}
 
	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		
		$title = apply_filters( 'widget_title', $instance['title'] );	// block title
					
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
				 
		// This is where you run the code and display the output
		?>
        <?php if($title!="") { ?>
            <h2 class="bottom-footer-title">                
                <?=$title?>
            </h2>                
        <?php } ?>        
        <div class="contact-details">
            <div title="location" class="genericon genericon-location"><?=get_option('site_location')?></div>
            <div title="mail" class="genericon genericon-mail" ><?=get_option('con_email')?></div>
            <!--<div title="Call" class="genericon genericon-handset" ><?php //get_option('site_phone')?></div>-->            
            <div title="abn" class="genericon genericon-abn" ><?=get_option('site_abn')?></div>
            <div class="footer-social">
           follow us on facebook<a href="https://www.facebook.com/hagglefree/" target="_blank" class="class name: genericon-facebook-alt">&nbsp;</a>
</div>
        </div>        
		
		<?php
		echo $args['after_widget'];
	}
		 
	// Widget Backend
	public function form($instance) {
	
		if (isset( $instance['title'])) {
			$title = $instance['title'];		
		}			
	
		// Widget admin form
		?>
		<p>
		  <label for="<?php echo $this->get_field_id( 'title' ); ?>">
			<?php _e( 'Title:' ); ?>
		  </label>
		  <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>		
		<?php
	}
	 
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';		
		return $instance;
	}
} 

// Register and load the widget
function contact_info_load_widget() {
	register_widget( 'contact_info_widget' );
}
add_action( 'widgets_init', 'contact_info_load_widget' );
?>