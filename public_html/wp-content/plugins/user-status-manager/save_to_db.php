<?php
	$headers[] = 'Content-Type: text/html; charset=UTF-8';
	$headers[] = 'From: Hagglefree Team <info@hagglefree.com.au> ' . "\r\n";
	$url = get_base_url();
	$file_url = $url."/wp-config.php";
	require($file_url);
	
	global $wpdb;
	$table = $wpdb->prefix . 'user_status_manager';
	$table_user = $wpdb->prefix . 'users';	
	
	$user_id   = $_POST['user_id'];
	$from_date = $_POST['from_date'];
	$to_date   = $_POST['to_date'];
	
	if(isset($_POST['user_id']) && !empty($_POST['user_id'])){
		$user_id_arr = explode(",",$_POST['user_id']);
	}
	
	$from_date_arr = explode(",",$_POST['from_date']);
	$to_date_arr   = explode(",",$_POST['to_date']);
	$status   = explode(",",$_POST['status']);
	
	$user_id_count = count($user_id_arr);
	
	for($i=0;$i<$user_id_count;$i++){
		$result = $wpdb->get_row('select id from '.$table.' where user_id = '.$user_id_arr[$i]);
		$value = $wpdb->get_row('select ID,user_login,user_email from '.$table_user.' where ID='.$user_id_arr[$i]);
		//$meta_value = $wpdb->get_row('select hagg_capabilities from '.$table_usermeta.' where ID='.$user_id_arr[$i]);
		$user = new WP_User( $value->ID );

		if ( !empty( $user->roles ) && is_array( $user->roles ) ) {
			foreach ( $user->roles as $role )
				$user_role = $role;
		}
		
		if(count($result)>0){
			//update
			
			$update_val = array(
									'user_id' 	  => $value->ID,
									'user_name'   => $value->user_login,
									'user_email'  => $value->user_email,
									'status_from' => $from_date_arr[$i],
									'status_to'   => $to_date_arr[$i],
									'status'	  => $status[$i],
									'role'		=> $user_role
								);
			$where = array(
								'user_id' 	  => $value->ID
							);
			$stat = ($status[$i]=='0'?"active":"inactive");				
			update_user_meta($value->ID, 'cr_status', $stat);
			if($wpdb->update($table,$update_val,$where)){
			//mail customer there password	
				if($status[$i]=='0'){			
				$psswd = random_password(8);
				wp_set_password( $psswd, $value->ID );
				$template_name = "retailer_notification_for_account_activation";
				$template_detail = get_email_template($template_name);
				$mail_content = stripslashes($template_detail['email_template']);
				$media_url = get_stylesheet_directory_uri()."/img";
				//Codes: {site_url}, {site_tag}, {media_url}, {email_heading}, {site_name}, {login_url}, {retailer_email}, {retailer_password}, {admin_email}, {site_abn}, {site_phone}, {fb_url}
				
				//======== Token Replacement =================//
				$mail_content = str_replace("{site_name}",get_bloginfo( 'name' ),$mail_content);
				$mail_content = str_replace("{site_url}",home_url(),$mail_content);
				$mail_content = str_replace("{site_tag}",get_bloginfo( 'description' ),$mail_content);
				$mail_content = str_replace("{media_url}",$media_url,$mail_content);	
				$mail_content = str_replace("{email_heading}",stripslashes($template_detail['email_heading']),$mail_content);
				
				$mail_content = str_replace("{login_url}",home_url('my-account'),$mail_content);
				$mail_content = str_replace("{retailer_email}",$value->user_email,$mail_content);
				$mail_content = str_replace("{retailer_password}",$psswd,$mail_content);
	
				$mail_content = str_replace("{admin_email}",get_option( 'admin_email' ),$mail_content);
				$mail_content = str_replace("{site_abn}",get_option( 'site_abn' ),$mail_content);
				$mail_content = str_replace("{site_phone}",get_option( 'site_phone' ),$mail_content);
				$mail_content = str_replace("{fb_url}",get_option( 'fb_url' ),$mail_content);
				
				//======== Token Replacement =================//
				
				wp_mail( $value->user_email, $template_detail['subject'],$mail_content,$headers);
				}
				echo "yes";
			}else{
				echo "no";
			}
		}else{
			//insert
			$update_val = array(
									'user_id' 	  => $value->ID,
									'user_name'   => $value->user_login,
									'user_email'  => $value->user_email,
									'status_from' => $from_date_arr[$i],
									'status_to'   => $to_date_arr[$i],
									'status'	  => 0,
									'role'		=> 'retailer-account'
								);
			if($wpdb->insert( $table, $update_val)){		
				update_user_meta($value->ID, 'cr_status', 'active');
				echo "yes";
			}else{
				echo "no";
			}
		}
	}
	function random_password( $length = 8 ) {
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
		$password = substr( str_shuffle( $chars ), 0, $length );
		return $password;
	}	
	
	function get_base_url(){
	$root_dir = $_SERVER['DOCUMENT_ROOT'];
	
        /* returns /myproject/index.php */
        $path = $_SERVER['PHP_SELF'];

        /*
         * returns an array with:
         * Array (
         *  [dirname] => /myproject/
         *  [basename] => index.php
         *  [extension] => php
         *  [filename] => index
         * )
         */
        $path_parts = pathinfo($path);
        $directory = $path_parts['dirname'];
        /*
         * If we are visiting a page off the base URL, the dirname would just be a "/",
         * If it is, we would want to remove this
         */
		$dir =explode('/',$directory);
        $directory = ($directory == "/") ? "" : $dir;

        /* Returns localhost OR mysite.com */
        //$host = $_SERVER['HTTP_HOST'];
		$directory[1] = str_replace("wp-content", "", $directory[1]);
        return  $root_dir ."/".$directory[1];
	}
?>