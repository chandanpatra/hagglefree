<?php
/**
 * The template for the content bottom widget areas on posts and pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

if ( ! is_active_sidebar( 'secondry_footer' ) ) {
	return;
}

// If we get this far, we have widgets. Let's do this.
?>
<div class="container">
    	<div class="row"> 
			<?php if ( is_active_sidebar( 'secondry_footer' ) ) : ?>
                <div class="widget-area">
                    <?php dynamic_sidebar( 'secondry_footer' ); ?>
                </div><!-- .widget-area -->
            <?php endif; ?>
       </div>
</div><!-- .content-bottom-widgets -->
