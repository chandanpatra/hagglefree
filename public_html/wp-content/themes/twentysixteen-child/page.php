<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<section class="main-content-block">
    <div id="content" class="site-content container">
        <div id="primary" class="content-area row">
        <div class="site-main col-lg-12 col-md-12 col-sm-12 col-xs-12" role="main">
            <?php
            // Start the loop.
            while ( have_posts() ) : the_post();
    
                // Include the page content template.
                get_template_part( 'template-parts/content', 'page' );
    
                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) {
                    comments_template();
                }
    
                // End of the loop.
            endwhile;
            ?>
    
        </div><!-- .site-main -->
    </div><!-- .content-area -->
    </div>
</section>   
<section class="bottom-footer">	       	
     <?php get_sidebar( 'content-bottom' ); ?>        
</section> 
<?php get_footer(); ?>