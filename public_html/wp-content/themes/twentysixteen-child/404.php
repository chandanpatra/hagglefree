<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<section class="main-content-block error-404 not-found">
	<div id="content" class="site-content container">
	<div id="primary" class="content-area row">
		<div id="main" class="site-main col-lg-12 col-md-12 col-sm-12 col-xs-12" role="main">

			
				<header class="page-header">
					<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'twentysixteen' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php _e( 'It looks like nothing was found at this location. Maybe try a <span class="orange">search</span>?', 'twentysixteen' ); ?></p>
					
				</div><!-- .page-content -->
			

		</div><!-- .site-main -->	

	</div><!-- .content-area -->
    </div><!-- .site-content -->
</section>
<section class="bottom-footer">	       	
     <?php get_sidebar( 'content-bottom' ); ?>        
</section> 
<?php get_footer(); ?>
