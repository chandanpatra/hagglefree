<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
<section class="footer">
    <div class="container">
        <div class="row">
            <?php if ( is_active_sidebar( 'primary_footer' ) ) : ?>                        
                    <?php dynamic_sidebar( 'primary_footer' ); ?>                        
            <?php endif; ?>					        
        </div>
    </div>
</section>
</div><!-- .site-inner -->
</div><!-- .site -->

<?php wp_footer(); ?>
  <script src="<?=get_stylesheet_directory_uri()?>/js/jquery.touchwipe.min.js"></script>
  <script language="javascript"> 
   jQuery(document).ready(function() {
	  jQuery(".arconix-faq-accordion-wrap :first").trigger("click");
	  jQuery("#haggle-accordion").accordion({heightStyle: "content"});
	});
  </script>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-81042733-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- livezilla.net code (PLEASE PLACE IN BODY TAG) -->
<div id="livezilla_tracking" style="display:none"></div><script type="text/javascript">
var script = document.createElement("script");script.async=true;script.type="text/javascript";var src = "http://hagglefree.com.au/livezilla/server.php?a=662ab&rqst=track&output=jcrpt&intgroup=c3VwcG9ydA__&intid=YWRtaW5pc3RyYXRvcg__&pref=Z3JvdXA_&el=ZW4_&ovlc=I2ZmNzgwMA__&eca=MQ__&ecfe=I0ZGRTU4Qg__&echc=I0ZGNzgwMA__&ecsgs=I0ZGNzgwMA__&ecsge=I0ZGNzgwMA__&ecsp=MQ__&nse="+Math.random();setTimeout("script.src=src;document.getElementById('livezilla_tracking').appendChild(script)",1);</script><noscript><img src="http://hagglefree.com.au/livezilla/server.php?a=662ab&amp;rqst=track&amp;output=nojcrpt" width="0" height="0" style="visibility:hidden;" alt=""></noscript>
<!-- http://www.livezilla.net -->
</body>
</html>
<?php
//save search result
if(isset($_GET['s']) && $_GET['s']!="" && isset($_GET['post_type'])){
	global $wpdb;
	$table_cat = $wpdb->prefix . 'search_result_log';
	$search_term = $_GET['s'];
	$user_ip = $_SERVER['REMOTE_ADDR'];
	//$search_sql = "SELECT `search_term`,`user_ip` FROM $table_cat WHERE `search_term`=$search_term AND `user_ip`=$user_ip";
	//$search_rs = $wpdb->get_results($search_sql);
	
	
	$search_date = "SELECT NOW() AS times";
	$result = $wpdb->get_row($search_date);
	//insert
	$insert_val = array(
					'search_term' 	  => 	$search_term,
					'user_ip'   		  => 	$user_ip,
					'search_date'      => 	$result->times									
				);
	
	$insert = $wpdb->insert( $table_cat, $insert_val);
}
?>