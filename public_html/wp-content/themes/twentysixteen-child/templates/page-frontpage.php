<?php
/*
	Template Name: Home Template
*/

?>
<?php get_header(); 
$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
if($feat_image){
	?>
    <style type="text/css">
    	.search-block {background:url('<?=$feat_image?>') no-repeat center top}
	</style>
    <?php
}
?>
<section class="search-block">
    <div class="container">
        <div class="row">
            <h2 class="search-heading">Find the <span>Best Price</span> on your <span>Product</span></h2>
            <?php if ( is_active_sidebar( 'advanced_search' ) ) : ?>	
                    <?php dynamic_sidebar( 'advanced_search' ); ?>
            <?php endif; ?>
            <div class="browse-category">
            	<span class="genericon-menu">Browse by CATEGORY</span>            	
                <?php echo get_product_categorylist('category-menu','yes');?>
            </div>
        </div>
    </div>
</section>
<section class="howitworks-block">
	<h2 class="section-heading">How it <span>works</span></h2>
    <span class="slide-it">&nbsp;</span>
	<div class="container inner-wrap">
		<div class="row">
			<?php if ( is_active_sidebar( 'how_it_work' ) ) : ?>	
                    <?php dynamic_sidebar( 'how_it_work' ); ?>
            <?php endif; ?>
        </div>
    </div>
</section>
<section class="popular-category-block">
	<h2 class="section-heading">Popular <span>Categories</span></h2>
	<div class="container inner-wrap">
		<div class="row">
	        <?php echo do_shortcode('[popular_category_slider]');?>
        </div>
    </div>
</section>  
<section class="homepage-content-main">
	<h1 class="section-heading">Haggle <span>Free</span></h1>
	<div class="container inner-wrap">
        <div class="row homepage-content">
        	<?php the_content();?>
        </div>
    </div>    
</section>  
<section class="why-haggle-block">
	<h2 class="section-heading">Why source your product using <span>hagglefree<sup>TM</sup></span></h2>
	<div class="container inner-wrap">
		<div class="row">
			<?php if ( is_active_sidebar( 'why_haggle' ) ) : ?>	
                    <?php dynamic_sidebar( 'why_haggle' ); ?>
            <?php endif; ?>
        </div>
    </div>
</section>
<section class="testimonial-block">
	<h2 class="section-heading">What people say <span>About us</span></h2>	
    <?php echo do_shortcode('[testimonials-slider]');?>
</section>
<section class="bottom-footer">	       	
     <?php get_sidebar( 'content-bottom' ); ?>        
</section>

<script type="text/javascript">
	jQuery(document).ready(function(){
		var mq = window.matchMedia( "(max-width:767px)" );
		
		if (mq.matches) {					
			jQuery( ".howitworks-block > span.slide-it" ).click(function() {
			  jQuery( ".howitworks-block  .inner-wrap" ).slideToggle( "slow", function() {
				// Animation complete.
			  });
			});
		}
	});
	
</script>       
<?php get_footer(); ?>