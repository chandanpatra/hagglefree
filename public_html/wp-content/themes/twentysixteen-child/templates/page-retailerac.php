<?php
/*
	Template Name: Retailer-Account Template
*/

?>
<?php
	global $wpdb;
	$table = $wpdb->prefix . 'pmpro_membership_orders';	
	$user_ID = get_current_user_id();	
	$region = get_user_meta( $user_ID, 'cr_region', 'true' );// contains id
	// get m
	
	
	$result = $wpdb->get_row('select `membership_id`,`timestamp` from '.$table.' where user_id = '.$user_ID.' AND `status` = "success" order by `timestamp` desc limit 0,1');
		
	if(count($result)>0){
		$timestamp = $result->timestamp;
		$current_timestamp = "SELECT DATEDIFF(now() ,'$timestamp') AS days";
		$result1 = $wpdb->get_row($current_timestamp);
		$tot_days = $result1->days;
		$membership = $result->membership_id;
		if($tot_days>365){
			//redirect to specific level checkout page
			wp_redirect( home_url('retailer-checkout/?level='.$membership), 301 ); exit;
			
		}
		
		
		
	}else{
		// redirect to retailer retailer-checkout/?level=2 
		wp_redirect( home_url('retailer-checkout/?level='.$region), 301 ); exit;
	}
?>
<?php get_header(); ?>
<section class="main-content-block">
    <div id="content" class="site-content container">
        <div id="primary" class="content-area row">
            <div class="site-main col-lg-12 col-md-12 col-sm-12 col-xs-12" role="main">
                <?php
                // Start the loop.
                while ( have_posts() ) : the_post();
        
                    // Include the page content template.
                    get_template_part( 'template-parts/content', 'page' );
        
                    // If comments are open or we have at least one comment, load up the comment template.
                    if ( comments_open() || get_comments_number() ) {
                        comments_template();
                    }
        
                    // End of the loop.
                endwhile;
                ?>
        
            </div><!-- .site-main -->
        </div><!-- .content-area -->
    </div><!-- .site-content -->
</section>   
<section class="bottom-footer">	       	
     <?php get_sidebar( 'content-bottom' ); ?>        
</section> 
<?php get_footer(); ?>