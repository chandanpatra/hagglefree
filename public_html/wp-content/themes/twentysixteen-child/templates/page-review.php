<?php
/*
	Template Name: Review Template
*/
get_header(); ?>
<section class="main-content-block">
   <div id="content" class="site-content container">
        <div id="primary" class="content-area row">
            <div id="main" class="site-main col-lg-12 col-md-12 col-sm-12 col-xs-12" role="main">
                <?php
                // Start the loop.
                while ( have_posts() ) : the_post();
        
                    // Include the page content template.
                    get_template_part( 'template-parts/content', 'review' );
        
                    // If comments are open or we have at least one comment, load up the comment template.
                    if ( comments_open() || get_comments_number() ) {
                        comments_template();
                    }
        
                    // End of the loop.
                endwhile;
                ?>
        
            </div><!-- .site-main -->
        </div><!-- .content-area -->
    </div><!-- .site-content -->
</section> 
<section class="bottom-footer">	       	
     <?php get_sidebar( 'content-bottom' ); ?>        
</section> 
<?php get_footer(); ?>