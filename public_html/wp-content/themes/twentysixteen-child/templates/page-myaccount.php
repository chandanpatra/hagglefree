<?php
/*
	Template Name: My-account Template
*/
	//check retailer payment
	if(is_user_logged_in()) {
		$user_ID = get_current_user_id();
		$redirect_url = check_retailer_payment($user_ID);
		
		if($redirect_url!= '0'){
		?>
        <script type="text/javascript">			
			window.location.href="<?=$redirect_url?>";
		</script>
        <?php
		}	
		$responsive_classes = 'col-lg-9 col-md-9 col-sm-12 col-xs-12';
		
	}else{
		$responsive_classes = 'col-lg-12 col-md-12 col-sm-12 col-xs-12';
	}
?>

<?php get_header(); ?>
<section class="main-content-block">
    <div id="content" class="site-content container">
        <div id="primary" class="content-area row">
        	<?php 
				//role wise sidebar for retailer and buuyer
				if(is_user_logged_in()) {
					global $role;
					$user_ID = get_current_user_id();
					$user = new WP_User( $user_ID );
					$role = $user->roles[0];
					if($role == "buyer-account"){
						get_sidebar( 'buyer-sidebar' );
					}
					if($role == "retailer-account"){
						get_sidebar( 'retailer-sidebar' );
					}
				}
			?>
            <div class="member-main <?=$responsive_classes?>" role="main">
                <?php
                // Start the loop.
                while ( have_posts() ) : the_post();
        
                    // Include the page content template.
                    get_template_part( 'template-parts/content', 'myaccount' );
        
                    // If comments are open or we have at least one comment, load up the comment template.
                    if ( comments_open() || get_comments_number() ) {
                        comments_template();
                    }
        
                    // End of the loop.
                endwhile;
                ?>
        
            </div><!-- .site-main -->
            
        </div><!-- .content-area -->
    </div><!-- .site-content -->
</section>   
<section class="bottom-footer">	       	
     <?php get_sidebar( 'content-bottom' ); ?>        
</section> 
<?php get_footer(); ?>