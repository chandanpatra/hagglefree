<?php
error_reporting(0);
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 //check wether retailer have made payment
	/*if(is_user_logged_in()) {
		$user_ID = get_current_user_id();
		check_retailer_payment($user_ID);
	}*/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta name='robots' content='noindex,follow' /> 
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
    <?php //if(is_product()){?>
    <!--<meta name="description" content="<?php //echo html_entity_decode(get_post_meta( get_the_ID(), 'meta_description', true ));?> " />-->
    <?php //}?>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body <?php body_class(); ?>>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=355651184598780";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="page" class="site">
	<div class="site-inner">
		<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentysixteen' ); ?></a>
		<section id="masthead" class="site-header" role="banner">
			<div class="container site-header-main">
            	<div class="row">
                    <div class="site-branding col-xs-12 col-lg-3 col-md-3 col-sm-3">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="HAGGLEFREE - We will take all the fun out of haggling"><img src="<?php echo get_stylesheet_directory_uri(); ?>/logo.jpg" width="250px" height="59px" alt="HAGGLEFREE - We will take all the fun out of haggling"></a>
                        <?php
                        $description = get_bloginfo( 'description', 'display' );
                        if ( $description || is_customize_preview() ) : ?>
                            <p class="site-description"><?php echo $description; ?></p>
                        <?php endif; ?>
                    </div>
                    <!-- .site-branding -->
                   <!-- <div class="social-like"><?php //echo do_shortcode('[fbls url="http://www.facebook.com.au/hagglefree" width="400" faces="false" verb="like" scheme="dark" ]'); ?></div>-->
					<button id="menu-toggle" class="menu-toggle"><?php _e( 'Menu', 'twentysixteen' ); ?></button>
					<?php if ( has_nav_menu( 'primary' ) ) : ?>
					<div id="site-header-menu" class="site-header-menu col-xs-12 col-lg-9 col-md-9 col-sm-9">                   
                    <div class="follow-us" style="background-color:#fff !important;"><div class="fb-like" data-href="https://www.facebook.com/hagglefree/" data-width="108px" data-layout="button_count" data-action="like" data-size="large" data-show-faces="false" data-share="false"></div></div>                  	
						<?php if ( has_nav_menu( 'primary' ) ) : ?>
							<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
								<?php
									wp_nav_menu( array(
										'theme_location' => 'primary',
										'menu_class'     => 'primary-menu',
									 ) );
								?>
							</nav><!-- .main-navigation -->
						<?php endif; ?>                        
						</div>  
                        <div class="follow-us-mob"><div class="fb-like" data-href="https://www.facebook.com/hagglefree/" data-width="108px" data-layout="button_count" data-action="like" data-size="large" data-show-faces="false" data-share="false"></div></div>                      
					</div><!-- .site-header-menu -->
				<?php endif; ?>
			</div><!-- .site-header-main -->			
		</div><!-- .site-header -->
        <?php
        $feat_image = wp_get_attachment_url( get_post_thumbnail_id(218) );
		if($feat_image){
			?>
			<style type="text/css">
				.inner-browse-block {background:url('<?=$feat_image?>') no-repeat center top}
			</style>
			<?php
		}
		if ( !is_front_page() ) {
			?>	
			<section class='inner-browse-block'>
                <div class='container'>
                    <div class='row'>                   	
						<h2 class="search-heading">Find the <span>Best Price</span> on your <span>Product</span></h2>
                        <?php if ( is_active_sidebar( 'advanced_search' ) ) : ?>	
								<?php dynamic_sidebar( 'advanced_search' ); ?>
                        <?php endif; ?>                              
                        <div class='browse-category'>
                            <span class='genericon-menu'>Browse by CATEGORY</span><?php echo get_product_categorylist('category-menu','yes')?>
                        </div>
                       <!-- <div class='search-product'><?php //echo do_shortcode('[products_search]');?>
                        </div>-->
                    </div>
                </div>
          </section>
		<?php			
			}		
		?>
        