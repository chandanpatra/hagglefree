<?php
	/**
	 * Twenty Sixteen Child Widgets functionality
	 *
	 * @package Nerdster
	 * @subpackage Twenty_Sixteen
	 * @since Twenty Sixteen 1.0
	 */


// Creating the widget for how it works
class how_it_works_widget extends WP_Widget {
 
	function __construct() {
		parent::__construct(
		// Base ID of your widget
		'how_it_works_widget',
		 
		// Widget name will appear in UI
		__('How it Works Widget', 'wpb_widget_domain'),
		 
		// Widget description
		array( 'description' => __( 'This widget displays the how it works section on the home page.', 'how_it_works_widget_domain' ), )
		);
	}
 
	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		
		$title = apply_filters( 'widget_title', $instance['title'] );	// block title	
		$block_icon = apply_filters( 'widget_text', $instance['block_icon'] );	//block icon	
		$block_content = apply_filters( 'block_content', $instance['block_content'] ); //block content
		
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
				 
		// This is where you run the code and display the output
		?>
		<div class="widget-how-it-work">
        		<?php if($block_icon!=""){ 						
					?>
                	<span><img src="<?=$block_icon?>" alt="<?=$title?>" title="<?=$title?>" /></span>
                <?php } ?> 
            	<?php if($title!="") { ?>
                    <h5 class="hiw-title">                
                        <?=$title?>
                    </h5>                
                <?php } ?>               
                <div class="textwidget hiw-content"><?php echo $block_content?></div>		  
		</div>
		<?php
		echo $args['after_widget'];
	}
		 
	// Widget Backend
	public function form($instance) {
	
		if (isset( $instance['title'])) {
			$title = $instance['title'];		
		}	
		if (isset( $instance['block_icon'])) {
			$block_icon = $instance['block_icon'];		
		}
		else {
			$block_icon = __( 'Block icon', 'how_it_works_widget_domain' );
		}
		if (isset( $instance['block_content'])) {
			$block_content = $instance['block_content'];		
		}
		else {
			$block_content = __( 'Block Contnet', 'how_it_works_widget_domain' );
		}
	
		// Widget admin form
		?>
		<p>
		  <label for="<?php echo $this->get_field_id( 'title' ); ?>">
			<?php _e( 'Title:' ); ?>
		  </label>
		  <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>	
		<p>
		  <label for="<?php echo $this->get_field_id( 'block_icon' ); ?>">
			<?php _e( 'Block icon:' ); ?>
		  </label>
		  <input class="widefat" id="<?php echo $this->get_field_id( 'block_icon' ); ?>" name="<?php echo $this->get_field_name( 'block_icon' ); ?>" type="text" value="<?php echo esc_attr( $block_icon ); ?>" />
		</p>	
		<p>
		  <label for="<?php echo $this->get_field_id( 'block_content' ); ?>">
			<?php _e( 'Block Contnet:' ); ?>
		  </label>
          <textarea class="widefat" id="<?php echo $this->get_field_id( 'block_content' ); ?>" name="<?php echo $this->get_field_name( 'block_content' ); ?>"><?php echo esc_attr( $block_content ); ?></textarea>		  
		</p>
		
		<?php
	}
	 
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['block_icon'] = ( ! empty( $new_instance['block_icon'] ) ) ? strip_tags( $new_instance['block_icon'] ) : '';
		$instance['block_content'] = ( ! empty( $new_instance['block_content'] ) ) ? strip_tags( $new_instance['block_content'] ) : '';
		return $instance;
	}
} // Class how_it_works_widget ends here

// Register and load the widget
function how_it_works_load_widget() {
	register_widget( 'how_it_works_widget' );
}
add_action( 'widgets_init', 'how_it_works_load_widget' );

// Creating the widget for Why Haggle
class why_haggle_widget extends WP_Widget {
 
	function __construct() {
		parent::__construct(
		// Base ID of your widget
		'why_haggle_widget',
		 
		// Widget name will appear in UI
		__('Why Hagglefree Widget', 'wpb_widget_domain'),
		 
		// Widget description
		array( 'description' => __( 'This widget displays the why hagglefree section on the home page.', 'why_haggle_widget_domain' ), )
		);
	}
 
	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		
		$title = apply_filters( 'widget_title', $instance['title'] );	// block title	
		$block_content = apply_filters( 'block_content', $instance['block_content'] ); //block content
		
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
				 
		// This is where you run the code and display the output
		?>
        <?php if($title!="") { ?>
        	<h2 class="section-heading"><?=$title?></h2>
         <?php } ?> 
		<div class="widget-why-haggle">        		 
            	<?php if($block_content!="") { ?>                   
                    <div class="textwidget hiw-content"><?=$block_content?></div>                
                <?php } ?>
		</div>
		<?php
		echo $args['after_widget'];
	}
		 
	// Widget Backend
	public function form($instance) {
	
		if (isset( $instance['title'])) {
			$title = $instance['title'];		
		}			
		if (isset( $instance['block_content'])) {
			$block_content = $instance['block_content'];		
		}
		else {
			$block_content = __( 'Block Contnet', 'how_it_works_widget_domain' );
		}
	
		// Widget admin form
		?>
		<p>
		  <label for="<?php echo $this->get_field_id( 'title' ); ?>">
			<?php _e( 'Title:' ); ?>
		  </label>
		  <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>			
		<p>
		  <label for="<?php echo $this->get_field_id( 'block_content' ); ?>">
			<?php _e( 'Block Contnet:' ); ?>
		  </label>
          <textarea class="widefat" id="<?php echo $this->get_field_id( 'block_content' ); ?>" name="<?php echo $this->get_field_name( 'block_content' ); ?>"><?php echo esc_attr( $block_content ); ?></textarea>		  
		</p>
		
		<?php
	}
	 
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ?  $new_instance['title'] : '';		
		$instance['block_content'] = ( ! empty( $new_instance['block_content'] ) ) ? $new_instance['block_content'] : '';
		return $instance;
	}
} // Class how_it_works_widget ends here

// Register and load the widget
function why_haggle_load_widget() {
	register_widget( 'why_haggle_widget' );
}
add_action( 'widgets_init', 'why_haggle_load_widget' );