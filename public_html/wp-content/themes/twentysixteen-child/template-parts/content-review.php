<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php $title = explode(' ',get_the_title());			
			$rest_title = '';
			for($i=0;$i<count($title)-1;$i++){
				$rest_title .= ' '.$title[$i];
			}
			echo "<h1 class=\"entry-title\">$rest_title<span>".$title[count($title)-1]."</span></h1>";  ?>
	</header><!-- .entry-header -->

	<?php twentysixteen_post_thumbnail(); ?>

	<div class="entry-content">    	
        
		<?php
		// modification for storing review in db
		if(isset($_GET['job-id']) && isset($_GET['user-email']) && isset($_GET['review'])){
			global $wpdb;			
			$table_review = $wpdb->prefix."customer_rating";
			$review_date = "SELECT NOW() AS times";
			$result1 = $wpdb->get_row($review_date);	
			$review_time = $result1->times;
			
			$review_exist_query = "SELECT * FROM $table_review WHERE jobId = ".$_GET['job-id'];
			$review_exist_rs = $wpdb->get_results($review_exist_query);
			if(count($review_exist_rs)>0){
				//if entry exist for that db then update
				 $update_val = array(												
						'review'             => $_GET['review'],
						'reviewDate' 		 => $review_time	
					);
				 $where = array('jobId'	  => $_GET['job-id']);	
				 $update = $wpdb->update( $table_review, $update_val,$where);
			}else{		
				// insert the data if doesnt exist
				$review_val = array(
									'jobId' 	  		  => $_GET['job-id'],
									'userEmail'   		  => $_GET['user-email'],
									'review'    		 => $_GET['review'],
									'reviewDate' 		 => $review_time
								);
				$wpdb->insert( $table_review, $review_val);
			}
		}
		
		the_content();
		
		wp_link_pages( array(
			'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
			'after'       => '</div>',
			'link_before' => '<span>',
			'link_after'  => '</span>',
			'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
			'separator'   => '<span class="screen-reader-text">, </span>',
		) );
		?>
	</div><!-- .entry-content -->

	<?php
		edit_post_link(
			sprintf(
				/* translators: %s: Name of current post */
				__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
				get_the_title()
			),
			'<footer class="entry-footer"><span class="edit-link">',
			'</span></footer><!-- .entry-footer -->'
		);
	?>

</article><!-- #post-## -->