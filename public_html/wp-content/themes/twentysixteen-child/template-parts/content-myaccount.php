<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php $title = explode(' ',get_the_title());			
			$rest_title = '';
			for($i=0;$i<count($title)-1;$i++){
				$rest_title .= ' '.$title[$i];
			}
			echo "<h1 class=\"entry-title\">$rest_title<span>".$title[count($title)-1]."</span></h1>";  ?>
	</header>
	<?php twentysixteen_post_thumbnail(); ?>
	<div class="entry-content">    	 	
		<?php
		global $role;		
			the_content();		
		?>                
	</div><!-- .entry-content -->

	<?php
		edit_post_link(
			sprintf(
				/* translators: %s: Name of current post */
				__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
				get_the_title()
			),
			'<footer class="entry-footer"><span class="edit-link">',
			'</span></footer><!-- .entry-footer -->'
		);
	?>

</article><!-- #post-## -->