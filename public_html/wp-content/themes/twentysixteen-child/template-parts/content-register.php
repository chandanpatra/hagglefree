<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header register-header">
		<?php 
			$title = explode(' ',get_the_title());			
			$rest_title = '';
			for($i=0;$i<count($title)-1;$i++){
				$rest_title .= ' '.$title[$i];
			}
		echo "<h1 class=\"entry-title\">$rest_title<span>".$title[count($title)-1]."</span></h1>"; 
		 ?>
	</header>
	<?php twentysixteen_post_thumbnail(); ?>
	<div class="entry-content"> 
    	<div class="messages">&nbsp;</div>   	
		<?php
		the_content();
		?>
        <div class="login-social-form" style="display:none;">
			<span class="form-close">X</span>
            <div class="social-login"><?php echo do_shortcode("[wordpress_social_login]");?></div>
			<?php 
				$args = array(
					'echo'           => true,
					'remember'       => true,
					'redirect'       => ( is_ssl() ? 'https://' : 'http://' ) . site_url( '/my-account/ ' ),					
					'label_username' => __( 'Username' ),
					'label_password' => __( 'Password' ),
					'label_remember' => __( 'Remember Me' ),
					'label_log_in'   => __( 'Log In' ),
					'value_username' => '',
					'value_remember' => false
				);
				wp_login_form($args); 
			?>
        </div>        
	</div><!-- .entry-content -->

	<?php
		edit_post_link(
			sprintf(
				/* translators: %s: Name of current post */
				__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
				get_the_title()
			),
			'<footer class="entry-footer"><span class="edit-link">',
			'</span></footer><!-- .entry-footer -->'
		);
	?>

</article><!-- #post-## -->
<script type="text/javascript">
jQuery(document).ready(function() {
  jQuery("#cregister").click(function(){
  jQuery(".register-form").show("slow");
  jQuery(".register-wrap").hide("slow");  
 }); 
 jQuery(".form-close").click(function(){
  jQuery(".register-form").hide("slow");
  jQuery(".register-wrap").show("slow");  
 });  
 //login form
 /*jQuery(".login-here").click(function(){
  jQuery(".login-social-form").show("slow");
  jQuery(".register-wrap").hide("slow");  
 }); 
 jQuery(".form-close").click(function(){
  jQuery(".login-social-form").hide("slow");
  jQuery(".register-wrap").show("slow");  
 });  */
});
</script>