<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php $title = explode(' ',get_the_title());			
			$rest_title = '';
			for($i=0;$i<count($title)-1;$i++){
				$rest_title .= ' '.$title[$i];
			}
			echo "<h1 class=\"entry-title\">$rest_title<span>".$title[count($title)-1]."</span></h1>";  ?>
	</header><!-- .entry-header -->

	<?php twentysixteen_post_thumbnail(); ?>

	<div class="entry-content">    	
        <div class="register-form">
        	<?php 
				if(isset($_GET['verify-email']) && !is_user_logged_in()){					
					$args = array(
					'blog_id'      => 1,					
					'meta_key'     => 'cr_verify',
					'meta_value'   => $_GET['verify-email']					
				 ); 
				$blogusers = get_users( $args );
				foreach ( $blogusers as $user ) {
					if($user->user_email){
						$email =  $user->user_email;
						$id    =  $user->ID;
						break;
					}					
				}				
				if($email){
					echo do_shortcode('[verify_email email='.$email.' id='.$id.']');
				}else{
					echo '<div class="password-update-form">
							<p>verification process already completed.<br />Please <a class="alink" href="'.home_url('my-account').'">login</a></p>
						 </div>';
				}
				
				}else{
					if(is_user_logged_in()){
						echo "<div class=\"password-update-form\">
								<p>Sorry you are already logged in. Account verification is not completed. Please logout first.</p>
						  </div>";	
					}else{
					echo "<div class=\"password-update-form\">
								<p>Invalid verification code.</p>
						  </div>";
					}
				}
			?>        	
        </div>
		<?php
		

		wp_link_pages( array(
			'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
			'after'       => '</div>',
			'link_before' => '<span>',
			'link_after'  => '</span>',
			'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
			'separator'   => '<span class="screen-reader-text">, </span>',
		) );
		?>
	</div><!-- .entry-content -->

	<?php
		edit_post_link(
			sprintf(
				/* translators: %s: Name of current post */
				__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
				get_the_title()
			),
			'<footer class="entry-footer"><span class="edit-link">',
			'</span></footer><!-- .entry-footer -->'
		);
	?>

</article><!-- #post-## -->
<script type="text/javascript">
jQuery(document).ready(function() {
  
});
</script>