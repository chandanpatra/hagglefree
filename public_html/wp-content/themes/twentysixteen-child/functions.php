<?php
//customizer modifications
//require get_stylesheet_directory_uri() . '/inc/customizer.php';
require_once($_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/twentysixteen-child/inc/customizer.php'); 
//custom widgets
require_once($_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/twentysixteen-child/inc/custom_widgets.php'); 

//custom shortcode
require_once($_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/twentysixteen-child/inc/custom_shortcode.php');

//short code for site url
function my_relative_post_image_link() {
 $linky = get_bloginfo('wpurl');
 return $linky;
 }
 add_shortcode('site_url', 'my_relative_post_image_link');

add_filter('widget_text', 'do_shortcode');

add_filter( 'wp_nav_menu_items', 'hagg_add_loginout_link', 10, 2 );
function hagg_add_loginout_link( $items, $args ) {
    if (is_user_logged_in() && $args->theme_location == 'primary') {
		$items .= '<li><a href="'. site_url('my-account') .'">My Account</a></li>';
        $items .= '<li><a href="'. wp_logout_url(site_url()) .'">Log Out</a></li>';
    }
    elseif (!is_user_logged_in() && $args->theme_location == 'primary') {
       $items .= '<li><a href="'. site_url('register') .'">Retailers</a></li>'; 
	   $items .= '<li><a href="'. site_url('my-account') .'">Log In</a></li>';		
    }
    return $items;
}

// Custom redirect for users after logging in
add_filter('woocommerce_login_redirect', 'hagg_login_redirect');
function hagg_login_redirect( $redirect ) {
     $redirect = site_url('my-account');
     return $redirect;
}

//woocomerce extra field for editing 
add_action( 'woocommerce_edit_account_form', 'my_woocommerce_edit_account_form' );
add_action( 'woocommerce_save_account_details', 'my_woocommerce_save_account_details' );
 
function my_woocommerce_edit_account_form() {
 
  $user_id = get_current_user_id();
  $user = get_userdata( $user_id );
 
  if ( !$user )
    return;
 
  $businessname = get_user_meta( $user_id, 'cr_company', true );
  $abn = get_user_meta( $user_id, 'cr_abn', true );
  $cr_phone = get_user_meta( $user_id, 'cr_phone', true );
   
  ?>
 
  <fieldset>
    <legend>Additional information</legend>    
    <p class="form-row form-row-thirds">
      <label for="businessname">Businessname:</label>
      <input type="text" name="businessname" value="<?php echo esc_attr( $businessname ); ?>" class="input-text" />
    </p>
    <p class="form-row form-row-thirds">
      <label for="abn">ABN:</label>
      <input type="text" name="abn" value="<?php echo esc_attr( $abn ); ?>" class="input-text" />
    </p>
    <p class="form-row form-row-thirds">
      <label for="cr_phone">Number to contact:</label>
      <input type="text" name="cr_phone" value="<?php echo esc_attr( $cr_phone ); ?>" class="input-text" />
    </p>
  </fieldset>
  
  <?php
 
}
 
function my_woocommerce_save_account_details( $user_id ) {
 
  update_user_meta( $user_id, 'cr_company', htmlentities( $_POST[ 'businessname' ] ) );
  update_user_meta( $user_id, 'abn', htmlentities( $_POST[ 'abn' ] ) );
  update_user_meta( $user_id, 'cr_phone', htmlentities( $_POST[ 'cr_phone' ] ) );
   
}
// PMPRO essentials functions

function pmpro_level_name($id)
{
	global $wpdb;
	$table_level = $wpdb->prefix . 'pmpro_membership_levels';
	$result = $wpdb->get_row('select `name` from '.$table_level.' where id = '.$id);
	return $result->name;
}
function pmpro_level_id($name)
{
	global $wpdb;
	$table_level = $wpdb->prefix . 'pmpro_membership_levels';
	$result = $wpdb->get_row('select `id` from '.$table_level.' where name = '.$name);
	return $result->id;
}
function pmpro_level_list()
{
	global $wpdb;
	$table_level = $wpdb->prefix . 'pmpro_membership_levels';
	$result = $wpdb->get_results('select `id`,`name` from '.$table_level);
	return $result;
}
// Search widget
 register_sidebar( array(
		'name' => __( 'Search Widget', 'twentysixteen' ),
		'id' => 'advanced_search',
		'description' => __( 'This widget area is located on the Homes Page.', 'codeex_theme_name' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s advanced-search">',
		'after_widget' => "</div>",
		'before_title' => '<h5>',
		'after_title' => '</h5>',
) );
// How it works widget
 register_sidebar( array(
		'name' => __( 'How it works Widget', 'twentysixteen' ),
		'id' => 'how_it_work',
		'description' => __( 'This widget area is located on the Homes Page.', 'codeex_theme_name' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s how-it-work col-xs-12 col-lg-4 col-md-4 col-sm-4">',
		'after_widget' => "</div>",
		'before_title' => '<h5 class="hiw-title">',
		'after_title' => '</h5>',
) );
// Why haggle widget
 register_sidebar( array(
		'name' => __( 'Why Haggle Widget', 'twentysixteen' ),
		'id' => 'why_haggle',
		'description' => __( 'This widget area is located on the Homes Page.', 'codeex_theme_name' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s why-haggle col-lg-12 col-md-12 col-sm-12 col-xs-12">',
		'after_widget' => "</div>",
		'before_title' => '<h5 class="wh-title">',
		'after_title' => '</h5>',
) );
//Secondary Footer Widget
register_sidebar( array(
		'name'          => __( 'Secondry Footer', 'twentysixteen' ),
		'id'            => 'secondry_footer',
		'description'   => __( 'Appears at the footer of the content on posts and pages.', 'twentysixteen' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s footer-bottom col-lg-4 col-md-4 col-sm-6 col-xs-12">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="bottom-footer-title">',
		'after_title'   => '</h2>',
	) );
//Primary Footer Widget
register_sidebar( array(
		'name'          => __( 'Primary Footer', 'twentysixteen' ),
		'id'            => 'primary_footer',
		'description'   => __( 'Appears at the footer of the content on posts and pages.', 'twentysixteen' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s primary-footer col-lg-6 col-md-6 col-sm-6 col-xs-12">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="primary-footer-title">',
		'after_title'   => '</h2>',
	) );	
	
//Retailer Sidebar Widget
register_sidebar( array(
		'name'          => __( 'Retailer Sidebar', 'twentysixteen' ),
		'id'            => 'retailer_sidebar',
		'description'   => __( 'Appears at the sidebar for retailer on posts and pages.', 'twentysixteen' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s retailer-sidebar">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="retailer-sidebar-title">',
		'after_title'   => '</h2>',
	) );	
	
//Buyer Sidebar Widget
register_sidebar( array(
		'name'          => __( 'Buyer Sidebar', 'twentysixteen' ),
		'id'            => 'buyer_sidebar',
		'description'   => __( 'Appears at the sidebar for retailer on posts and pages.', 'twentysixteen' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s buyer-sidebar">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="buyer-sidebar-title">',
		'after_title'   => '</h2>',
	) );			

function distance($lat1, $lon1, $lat2, $lon2, $unit) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
    return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
        return $miles;
      }
}

add_filter('woocommerce_login_redirect', 'ras_login_redirect');

function ras_login_redirect( $redirect_to ) {
	 session_start();
	 if($_SESSION["loginredirect"] && $_SESSION["loginredirect"]!=""){
     $redirect_to = $_SESSION["loginredirect"];
	 $_SESSION["loginredirect"] == "";
	 }
	 else
	 $redirect_to =  $_SERVER['REQUEST_URI'];
     return $redirect_to;
}

// Renaming woocomerce tabs
add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
function woo_rename_tabs( $tabs ) {
	
	$tabs['additional_information']['title'] = __( 'Specifications' );	// Rename the additional information tab

	return $tabs;
}

// remove password strength on lost password 
function nerdster_remove_password_strength() {
	if ( wp_script_is( 'wc-password-strength-meter', 'enqueued' ) ) {
		wp_dequeue_script( 'wc-password-strength-meter' );
	}
}
add_action( 'wp_print_scripts', 'nerdster_remove_password_strength', 100 );

//show empty category
add_filter('woocommerce_product_subcategories_hide_empty', 'woocommerce_show_empty_categories', 10, 1);

function woocommerce_show_empty_categories($show_empty){
    return true;
}

/////////////// Brand Image in product details page//////////////////////////

//add_action( 'woocommerce_single_product_summary', 'wc_brand_image_single_product',19 );
function wc_brand_image_single_product() {
  global $post;
	$brands = wp_get_post_terms( $post->ID, 'product_brand' );
	if ( $brands )
		$brand = $brands[0];
	if ( ! empty( $brand ) ) {
		//$thumbnail = get_brand_thumbnail_url( $brand->term_id );
		$url = get_term_link( $brand->slug, 'product_brand' );
		$thumbnail_id 	= absint( get_woocommerce_term_meta( $brand->term_id, 'thumbnail_id', true ) );
		$thumbnail = wp_get_attachment_thumb_url( $thumbnail_id );
		if($thumbnail)
		echo '<a href="' . $url . '"><img class="woocommerce-brand-image-single" src="'. $thumbnail . '"/></a>';
	}
}

// custom title for product pages
/*if( ! function_exists('nerdster_modify_wp_title'))
{
	function nerdster_modify_wp_title($title, $seperator)
	{
		if( ! is_product()) return $title;

		$title = html_entity_decode(get_post_meta( get_the_ID(), 'meta_title', true));

		return $title;		
	}
	add_filter('wp_title', 'nerdster_modify_wp_title', 10, 2);
}*/

// add additional scripts and css
function hagglefree_custom_scripts() {
		$scriptsrc = get_stylesheet_directory_uri() . '/js/';
		wp_register_script( 'hagg_jquery_ui', $scriptsrc . 'jquery-ui.js', array('jquery'), '1.0', true );
		wp_enqueue_script( 'hagg_jquery_ui' );

}
add_action( 'wp_enqueue_scripts', 'hagglefree_custom_scripts' );

