<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

/*if ( ! $product->is_purchasable() ) {
	return;
}*/

?>

<?php
	// Availability
	$availability      = $product->get_availability();
	$availability_html = empty( $availability['availability'] ) ? '' : '<p class="stock ' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</p>';

	echo apply_filters( 'woocommerce_stock_html', $availability_html, $availability['availability'], $product );
	
if(is_user_logged_in()){
	$user_ID = get_current_user_id();
	$user = new WP_User( $user_ID );
	$role = $user->roles[0];
}	
?>

<?php if ( $product->is_in_stock() ) : ?>

<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>
<div  style="display:none">
<div id="jobDetails" class="jobDetails">
  <h3 class="bestQuotes"><?=(isset($_POST['job_form_save'])?"Almost done!":"Find Me A Deal")?></h3>
<?php
wp_enqueue_style( 'smaillipop_css', get_stylesheet_directory_uri().'/css/jquery.smallipop.css');
?>
 <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
 <script type="text/javascript" src="<?=get_stylesheet_directory_uri()?>/js/modernizr.js"></script>
 <script type="text/javascript" src="<?=get_stylesheet_directory_uri()?>/js/jquery.smallipop.js"></script>
 <?php
if (isset($_POST['job_form_save'])) {
	global $wpdb,$quote_date;
	$email = $_POST['email'];
	$get_option = get_option('cutoff_time');
 $activeMessage = "<p>Thank you for requesting a quote, we will shortly contact a number of businesses near by and try to get you a fantastic deal. You can expect a response back via email within $get_option business hour. Keep in mind that we deal with actual people when haggling for the best deals so we can only do that during business hours, please be patient, we promise its well worth the wait.</p>";
 if(get_option('hold_job_on_verification')=="1")
 	$inactiveMessage = "<p><strong>Verification email has been sent to $email. </strong></p>";
else	
 	$inactiveMessage = "<p><strong>Verification email has been sent to $email. Once this is verified your request for quote will be sent to retailers.</strong></p>";	
$track = 0;	
	if ( !is_user_logged_in() ) {							
		$user = get_user_by( 'email', $email );
		$userId = $user->id;
		$user_status = get_user_meta( $userId, 'cr_status', true);
		$social_user = get_user_meta( $userId, 'wsl_current_provider', true);
		
		if($social_user!=""){
			 echo $activeMessage;
		}else{
			if($user_status == "inactive"){
			 echo  $inactiveMessage;
			 $track = 1;
			}
		    else
			 echo $activeMessage;
		}
	}
	else{
			 echo $activeMessage;
	}

$table_subs = $wpdb->prefix."subscribe";
$sql = "SELECT * FROM $table_subs WHERE `email` = '$email'";
$result = $wpdb->get_row($sql);

$subs = array();
if($result){
 $subs = explode(",",$result->hotDeals);
}

?>

<form id="job_subscription_form" name="job_subscription_form" class="cart" action="<?php echo get_stylesheet_directory_uri(); ?>/woocommerce/single-product/add-to-cart/add_subscrib.php" method="post" enctype="multipart/form-data">
<?php if($track == 0){?>
<p><b>If you haven't received our quote <?=$quote_date['format']?>, Check your junk mail folder or log back into your account to see your quotes.</b></p>
<?php } ?>
<p>Notify me via email when there are HOT deals on:</p>
<ul>
    <li><input type="checkbox" class="subscribs" name="subscribs[]" value="White Goods"<?=(in_array("White Goods",$subs)?" checked=\"checked\"":"")?> />White Goods</li>
    <li><input type="checkbox" class="subscribs" name="subscribs[]" value="Television & Audio equipment"<?=(in_array("Television & Audio equipment",$subs)?" checked=\"checked\"":"")?> />Television & Audio equipment</li>
    <li><input type="checkbox" class="subscribs" name="subscribs[]" value="Cooling & Heating"<?=(in_array("Cooling & Heating",$subs)?" checked=\"checked\"":"")?> />Cooling & Heating</li>
    <li><input type="checkbox" class="subscribs" name="subscribs[]" value="Bathroom Fixtures"<?=(in_array("Bathroom Fixtures",$subs)?" checked=\"checked\"":"")?> />Bathroom Fixtures</li>
    <li><input type="checkbox" class="subscribs" name="subscribs[]" value="Hot Water Systems"<?=(in_array("Hot Water Systems",$subs)?" checked=\"checked\"":"")?> />Hot Water Systems</li>
    <li><input type="checkbox" class="subscribs" name="subscribs[]" value="BBQ"<?=(in_array("BBQ",$subs)?" checked=\"checked\"":"")?> />BBQ</li>
</ul>
<div id="response" class="response"></div>
<p><input type="button" name="job_subscription_save" value="Save Changes" onclick="job_subscription()" />
<input type="hidden" name="subs_email" id="subs_email" value="<?=$_POST['email']?>" />
</p>
</form>
<script type="text/javascript">
function job_subscription(){ 
    jQuery(".response").html("Loading....");
	var obj = { Page: jQuery(document).find("title").text(), Url: '<?php echo str_replace("&success_select_marketing=1","", $_SERVER['REQUEST_URI'])."&success_final=1"; ?>' };
    history.pushState(obj, obj.Page, obj.Url);
    var subscribs = jQuery('.subscribs:checked').serialize();
	
  	var email = jQuery("#subs_email").val();
  	var url = jQuery("#job_subscription_form").attr( "action" );
  	var posting = jQuery.post( url, { subscribs: subscribs,email:email } );
  	posting.done(function(data) {    
    	jQuery(".response").html(data);
		//jQuery.colorbox.close();
		setTimeout(parent.jQuery.colorbox.close,2000);
  	});
}
</script>
<?php
	}else{
		
  ?>
  
	<form class="cart" id="findaDealValidation" method="post" enctype='multipart/form-data' action="<?php echo $_SERVER['REQUEST_URI']."&success_select_marketing=1"; ?>">
	 	<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
		<table cellspacing="0" class="wccpf_fields_table">
           <tbody>
            <tr>
             <td class="wccpf_label"><label for="is_commercial"><span class="custom-label">Do you qualify for trade/Commercial pricing?
             <?php if(get_option('comment_trade_price')!=""){?>
                	<span class="smallipop haggle-hint" style="display:inline-block;">
                        &nbsp;
                        <span class="smallipop-hint">
                            <?=get_option('comment_trade_price')?>
                        </span>
                    </span>
                    <?php }?>
             </span> 
			 		</label></td>
             <td class="wccpf_value">              
              <span class="radio-wrap">
              <input type="radio" id="commercial" value="1" name="is_commercial" onclick="javascript:checkOrder(1)" class="custom-radio"/>     
              <label for="commercial" class="radio-custom-label"><?php _e( 'Yes', 'woocommerce' ); ?></label>  
              </span>
              <span class="radio-wrap">              
              <input type="radio" id="private" value="0" name="is_commercial" onclick="javascript:checkOrder(0)" class="custom-radio" checked="checked"/> 
              <label for="private" class="radio-custom-label"><?php _e( 'No', 'woocommerce' ); ?></label>  
              </span>
             </td>
            </tr>
             </tbody>
          </table>
          <div class="businessNameId1" style="display:none">
          <table cellspacing="0" class="wccpf_fields_table" id="businessNameId">
           <tbody>
            <tr class="commercial_block">
                 <td class="wccpf_label"><label for="businessName">Business Name (if applicable)</label></td>
                 <td class="wccpf_value"><input type="text" wccpf-mandatory="no" value="" name="businessName" id="businessName" class="input-text wccpf-field"/></td>
            </tr>
            <tr class="commercial_block">
                 <td class="wccpf_label"><label for="abn_no">ABN No (if applicable) </label></td>
                 <td class="wccpf_value"><input type="text" wccpf-mandatory="no" value="" name="abn_no" id="abn_no" class="input-text wccpf-field"/></td>
            </tr>
			</tbody>
          </table>          
          </div>
           <script type="text/javascript">
			   function checkOrder(status){
				   if(status=="1"){
					 jQuery(".businessNameId1").show(); 
					// jQuery("#businessName" ).addClass("required");
					// jQuery("#abn_no" ).addClass("required");
				   }else{
					 jQuery(".businessNameId1").hide();
					// jQuery("#businessName").removeClass("required");
					// jQuery("#abn_no").removeClass("required");
				   }
			   }
			</script>
            <?php if( get_field('is_extended_warranty') ): ?>
            	<?php 
					$warranty = get_field('is_extended_warranty');					
					if($warranty=='yes'){ ?>
                	<table cellspacing="0" class="wccpf_fields_table">
                       <tbody>
                        <tr>
                         <td class="wccpf_label"><label for="is_extended_warranty">2Yrs Extended Warranty :</label></td>
                         <td class="wccpf_value"> 
                         <span class="radio-wrap">             
                          <input type="radio" id="extended-yes" value="yes" name="is_extended_warranty" class="custom-radio" checked=""/>   
                           <label for="extended-yes" class="radio-custom-label"><?php _e( 'Yes', 'woocommerce' ); ?></label>  
              			 </span>	
                         <span class="radio-wrap">
                          <input type="radio" id="extended-no" value="no" name="is_extended_warranty" class="custom-radio"/>    
                          <label for="extended-no" class="radio-custom-label"><?php _e( 'No', 'woocommerce' ); ?></label>  
              			 </span>
                         </td>
                        </tr>
                         </tbody>
                      </table>
                <?php }?>
         <?php endif; ?>
	 	<?php
			if($role != 'retailer-account'){
	 		if ( ! $product->is_sold_individually() ) {
	 			woocommerce_quantity_input( array(
	 				'min_value'   => apply_filters( 'woocommerce_quantity_input_min', 1, $product ),
	 				'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product ),
	 				'input_value' => ( isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : 1 )
	 			) );
	 		}
			}
	 	?>

<input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->id ); ?>" />
                 
<div class="user-infos" id="user-infos">
  <ul class="form-list">
    <li id="user-info-form">
        <ul>
          <li class="fields">
            <div class="customer-name-middlename">
              <div class="field name-firstname">
                <label class="required" for="name">
                	<em>*</em><span class="custom-label">Your Name</span>
                    <?php if(get_option('comment_jobpost_name')!=""){?>
                	<span class="smallipop haggle-hint" style="display:inline-block;">
                        &nbsp;
                        <span class="smallipop-hint">
                            <?=get_option('comment_jobpost_name')?>
                        </span>
                    </span>
                    <?php }?>
                </label>
                <div class="input-box">
                  <input type="text" class="input-text required-entry required" maxlength="255" title="Name" value="<?=get_user_meta( $user_ID, 'first_name', true )?> <?=get_user_meta( $user_ID, 'last_name', true )?>" name="first_name" id="first_name">
                </div>
              </div>              
            </div>
          </li>
          <li class="wide">
            <label class="required" for="email"><em>*</em><span class="custom-label">Email Address</span>
            	<?php if(get_option('comment_jobpost_email')!=""){?>
                	<span class="smallipop haggle-hint" style="display:inline-block;">
                        &nbsp;
                        <span class="smallipop-hint">
                            <?=get_option('comment_jobpost_email')?>
                        </span>
                    </span>
                    <?php }?>
            </label>
            <div class="input-box">
              <input class="input-text validate-email required-entry required" title="Email Address" <?php if ( is_user_logged_in()){?>readonly="readonly" <?php }?>value="<?=$user->user_email?>" id="email" name="email" type="email" />
            </div>
          </li>
          <li class="fields">
            <div class="field">
              <label class="required" for="postcode"><em>*</em><span class="custom-label">Postcode</span>
              	<?php if(get_option('comment_jobpost_postcode')!=""){?>
                	<span class="smallipop haggle-hint" style="display:inline-block;">
                        &nbsp;
                        <span class="smallipop-hint">
                            <?=get_option('comment_jobpost_postcode')?>
                        </span>
                    </span>
                    <?php }?>
              </label>
              <div class="input-box">
                <input type="text" maxlength="4" class="input-text validate-zip-international required-entry required" title="Postcode" value="<?=get_user_meta( $user_ID, 'cr_pcode', true )?>" id="cr_pcode" name="cr_pcode" />
              </div>
            </div>            
          </li>  
          <?php if( get_option('show_phone_job_form')!="1"){?>        
          <li class="fields">
            <div class="field">
              <label class="required" for="telephone"><em>*</em><span class="custom-label">Best Number to contact you on</span>
              	<?php //if(get_option('comment_jobpost_phone')!=""){?>
                	<span class="smallipop haggle-hint" style="display:inline-block;">
                        &nbsp;
                        <span class="smallipop-hint">
                            <?= get_option('comment_jobpost_phone')?>
                        </span>
                    </span>
                    <?php // }?>
              </label>
              <div class="input-box">
                <input type="text" maxlength="12" class="input-text required-entry required" title="Telephone" value="<?=get_user_meta( $user_ID, 'cr_phone', true )?>" id="cr_phone" name="cr_phone"/>
              </div>
            </div>
          </li>
          <?php } ?>
          <li class="fields">
            <div class="field terms">              
              <label>By clicking Request a quote, you agree to the Hagglefree <a href="<?php echo home_url('terms-conditions')?>" target="_blank">Terms and conditions</a>.</label>              
            </div>
          </li>
        </ul>
    </li>
  </ul> 
</div>
	 	<input name="job_form_save" id="submit_job" type="submit" class="single_add_to_cart_button button alt" value="<?php echo esc_html( $product->single_add_to_cart_text() ); ?>"/>
		
		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
        
	</form>
    <script type="text/javascript">
		jQuery(".smallipop").smallipop();
	</script>
    <script>                 
	 jQuery(document).ready(function() {
		/*jQuery("#cr_pcode").keydown(function (e) {
			// Allow: backspace, delete, tab, escape, enter and .
			if (jQuery.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
				 // Allow: Ctrl+A
				(e.keyCode == 65 && e.ctrlKey === true) ||
				 // Allow: Ctrl+C
				(e.keyCode == 67 && e.ctrlKey === true) ||
				 // Allow: Ctrl+X
				(e.keyCode == 88 && e.ctrlKey === true) ||
				 // Allow: home, end, left, right
				(e.keyCode >= 35 && e.keyCode <= 39)) {
					 // let it happen, don't do anything
					 return;
			}
			// Ensure that it is a number and stop the keypress
			if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
			}
		});*/
		
		/*jQuery("#cr_pcode").blur(function(){
			var postcode = jQuery("#cr_pcode").val();
			if(postcode.length == 4){
			if(jQuery.isNumeric( postcode )){
			   jQuery("#cr_pcode").removeClass("error");	
			   jQuery("#submit_job").prop("disabled", false);
			   jQuery( ".invalid_pcode" ).remove();
			   jQuery( ".errorpostcode" ).removeClass("errorpostcode");
			}
			else{
			   jQuery("#cr_pcode").addClass("error");
			   jQuery( ".invalid_pcode" ).remove();
			   jQuery( "#cr_pcode" ).wrapInner( '<label for="cr_pcode" class="error invalid_pcode">Invalid Postcode</label>' );
			   jQuery("#submit_job").prop("disabled", true);
			   jQuery( "#cr_pcode" ).wrap( '<div class="errorpostcode"></div>' );
			}
			}else{
			 jQuery("#cr_pcode").addClass("error");	
			 jQuery("#submit_job").prop("disabled", true);
			 jQuery( ".invalid_pcode" ).remove();
			 jQuery( "#cr_pcode" ).wrapInner( '<label for="cr_pcode" class="error invalid_pcode">Invalid Postcode</label>' );
			 jQuery( "#cr_pcode" ).wrap( '<div class="errorpostcode"></div>' );
			}
		});*/
		$('#cr_pcode').keypress(function(event){
            //console.log(event.which);
        if(event.which != 8 && event.which != 0  && isNaN(String.fromCharCode(event.which))){
            event.preventDefault();
        }});
		/* check valid phone number */
		/*$('#cr_phone').keypress(function(event){
            //console.log(event.which);
        if(event.which != 8 && event.which != 0  ){
            event.preventDefault();
        }});*/
		/*jQuery("#cr_phone").blur(function(){
			var phone = jQuery("#cr_phone").val();
			if(phone.length <= 12){
			if(jQuery.isNumeric( phone )){
			   jQuery("#cr_phone").removeClass("error");	
			   jQuery("#submit_job").prop("disabled", false);
			   jQuery( ".invalid_phone" ).remove();
			   jQuery( ".errorphone" ).removeClass("errorphone");
			}
			else{
			   jQuery("#cr_phone").addClass("error");
			   jQuery( ".invalid_phone" ).remove();
			   jQuery( "#cr_phone" ).wrapInner( '<label for="cr_phone" class="error invalid_phone">Invalid Phone</label>' );
			   jQuery("#submit_job").prop("disabled", true);
			   jQuery( "#cr_phone" ).wrap( '<div class="errorphone"></div>' );
			}
			}else{
			 jQuery("#cr_phone").addClass("error");	
			 jQuery("#submit_job").prop("disabled", true);
			 jQuery( ".invalid_phone" ).remove();
			 jQuery( "#cr_phone" ).wrapInner( '<label for="cr_phone" class="error invalid_phone">Invalid Phone</label>' );
			 jQuery( "#cr_phone" ).wrap( '<div class="errorphone"></div>' );
			}
		});*/
       /*jQuery("#cr_pcode").focus(function(){		  
		  var postcode1 = jQuery("#cr_pcode").val();
		  if(postcode1.length == 4){
			if(jQuery.isNumeric( postcode1 )){
			   jQuery("#cr_pcode").removeClass("error");	
			   jQuery("#submit_job").prop("disabled", false);
			   jQuery( ".invalid_pcode" ).remove();
			   jQuery( ".errorpostcode" ).removeClass("errorpostcode");
			}else{
			   jQuery("#cr_pcode").addClass("error");
			   jQuery( ".invalid_pcode" ).remove();
			   jQuery( "#cr_pcode" ).wrapInner( '<label for="cr_pcode" class="error invalid_pcode">Invalid Postcode</label>' );
			   jQuery("#submit_job").prop("disabled", true);
			   jQuery( "#cr_pcode" ).wrap( '<div class="errorpostcode"></div>' );
		    }
		  }
	   });*/
	});
	</script>
  <?php }?>     
 </div> 
</div>
    <!-- Job submit form for all users except retailer ends-->    
<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>
<?php endif; ?>
<script language="javascript">
   jQuery(document).ready(function(){
	   <?php
	   $visibility = get_post_meta($product->get_id(),"_visibility",true);
       if($visibility != "hidden"){
	   if(isset($_GET['get_quote'])){
	   ?>
	    if(jQuery(window).width() > 767)
        jQuery("#product_form_link").colorbox({inline:true, width:"50%"});
		else
        jQuery("#product_form_link").colorbox({inline:true, width:"100%"});
		<?php
	   }
	   }
	   ?>
		jQuery("#findaDealValidation").validate();
		jQuery(".wccpf-field").addClass("required");
		// remove reuired class from non mandatory fields
		jQuery('.wccpf_fields_table tr').each(function() {
        	var columns = jQuery(this).find('td.wccpf_value');      
			columns.each(function() {         
				var box = jQuery(this).find(':input');  
				 if(box.attr('wccpf-mandatory')=="no"){
				   jQuery(".wccpf-field").removeClass("required");
				 } 
			});
  		});
   });
  </script>