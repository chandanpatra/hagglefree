<?php
/**
 * Single product short description
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/short-description.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post,$product;

if ( ! $post->post_excerpt ) {
	return;
}

if(is_product()){
$attributes = $product->get_attributes();
foreach($attributes as $attribute){
	if($attribute['name'] == "pa_product_dimensions"){
		if ( $attribute['is_taxonomy'] ) {

					$values = wc_get_product_terms( $product->id, $attribute['name'], array( 'fields' => 'names' ) );
					$dimension =  apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values );

				} else {

					// Convert pipes to commas and display values
					$values = array_map( 'trim', explode( WC_DELIMITER, $attribute['value'] ) );
					$dimension = apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values );

				}	
	}
	if($attribute['name'] == "pa_warranty"){
		if ( $attribute['is_taxonomy'] ) {

					$values = wc_get_product_terms( $product->id, $attribute['name'], array( 'fields' => 'names' ) );
					$warranty =  apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values );

				} else {

					// Convert pipes to commas and display values
					$values = array_map( 'trim', explode( WC_DELIMITER, $attribute['value'] ) );
					$warranty = apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values );

				}	
	}
	if($attribute['name'] == "pa_waterrating"){
		if ( $attribute['is_taxonomy'] ) {

					$values = wc_get_product_terms( $product->id, $attribute['name'], array( 'fields' => 'names' ) );
					$waterrating =  apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values );

				} else {

					// Convert pipes to commas and display values
					$values = array_map( 'trim', explode( WC_DELIMITER, $attribute['value'] ) );
					$waterrating = apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values );

				}	
	}
	if($attribute['name'] == "pa_energyrating"){
		if ( $attribute['is_taxonomy'] ) {

					$values = wc_get_product_terms( $product->id, $attribute['name'], array( 'fields' => 'names' ) );
					$energyrating =  apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values );

				} else {

					// Convert pipes to commas and display values
					$values = array_map( 'trim', explode( WC_DELIMITER, $attribute['value'] ) );
					$energyrating = apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values );

				}	
	}
}
  
?>
<div itemprop="description" class="product-description-wrap">
	<h3>Quick Overview</h3>
    <div class="product-sku"><span>SKU : </span><?php echo $product->get_sku(); ?></div>    
    <?php if($dimension){ ?>
    <span class="product-dimension"><span>Dimension : </span><?php echo $dimension; ?></span>
     <?php } ?>
     <?php if($warranty){ ?>
    <span class="product-warranty"><span>Warranty : </span><?php echo $warranty; ?></span>
     <?php } ?>
     <?php if($waterrating){ ?>
    <span class="product-warranty"><span>Water Rating : </span><span class="water-rating-<?php echo str_replace(".","-",strip_tags($waterrating)); ?>"></span></span>
     <?php } ?>
     <?php if($energyrating){ ?>
    <span class="product-warranty"><span>Energy Rating : </span><span class="energy-rating-<?php echo str_replace(".","-",strip_tags($energyrating)); ?>"></span></span>
     <?php } ?>
    <?php  echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?>    
</div>
<?php }?>
