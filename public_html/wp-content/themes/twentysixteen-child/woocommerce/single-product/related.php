<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

if ( empty( $product ) || ! $product->exists() ) {
	return;
}

$related = $product->get_related( $posts_per_page );
//print_r($related);
if ( sizeof( $related ) === 0 ) return;

$args = apply_filters( 'woocommerce_related_products_args', array(
	'post_type'            => 'product',
	'ignore_sticky_posts'  => 1,
	'no_found_rows'        => 1,
	'posts_per_page'       => $posts_per_page,
	'orderby'              => $orderby,
	'post__in'             => $related,
	'post__not_in'         => array( $product->id )
) );

$products = new WP_Query( $args );

$woocommerce_loop['columns'] = $columns;
if(is_product()){
if ( $products->have_posts() ) : ?>
       		
        <section class="related-product-section">
        	<h2 class="section-heading">Related<span>Products</span></h2>
        	<div class="container">
            	<div class="row">
                    <div id="related-products1" class="owl-carousel">
                        <?php //print_r($products);?>
                        <?php //woocommerce_product_loop_start(); ?>
                
                            <?php while ( $products->have_posts() ) : $products->the_post(); ?>
                                <div class="item">
                                	<?php $image_data = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_id() )); ?>
                                    <?php $image_url = $image_data[0]; ?>
                                    <a href="<?=get_permalink()?>">
                                    	<img width="<?=$image_data[1]?>" height="<?=$image_data[2]?>" alt="<?=get_the_title()?>" class="attachment-shop_catalog size-shop_catalog wp-post-image" src="<?=$image_url?>">
                                    </a>
                                    <div class="product-brand"><?=nerdster_show_brand_single()?></div>
                                    <div class="product-title"><h3><?=get_the_title()?></h3></div>
                                    <div class="related-buttonwrap"> 
                                    	<a class="button getquotesbutton" href="<?=get_permalink()?>?get_quote=1">Find Me A Deal</a>&nbsp;
                                        <a class="button viewdetailbutton" href="<?=get_permalink()?>">View Details</a> 
                                    </div>
                                </div>
                                <?php //wc_get_template_part( 'content', 'product' ); ?>
                
                            <?php endwhile; // end of the loop. ?>
                
                        <?php //woocommerce_product_loop_end(); ?>        
                    </div><!-- #related-products -->
                 </div>
             </div>
        </section>
    <script>
    jQuery(document).ready(function() {
      jQuery("#related-products1").owlCarousel({
        items : 4,        
        nav : true,
		pagination : false,
		responsiveClass:true,
		responsive:{
			320:{ items:1 }, // Mobile portrait
			600:{ items:2 }, // Small tablet portrait
			768:{ items:3 }, // Tablet portrait
			979:{ items:4 }  // Desktop
		}
      });
    });
    </script>
<?php endif;
}

wp_reset_postdata();
