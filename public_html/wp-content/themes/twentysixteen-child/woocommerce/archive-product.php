<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $woocommerce,$woocommerce_loop,$wp_query,$query_string;
get_header( 'shop' ); ?>
	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

			<h1 class="page-title"><?php woocommerce_page_title(); ?></h1>

		<?php endif; ?>

		<?php
			/**
			 * woocommerce_archive_description hook.
			 *
			 * @hooked woocommerce_taxonomy_archive_description - 10
			 * @hooked woocommerce_product_archive_description - 10
			 */
			do_action( 'woocommerce_archive_description' );
		?>
		<?php  	
		//$queried_object = get_queried_object();
		//echo '<pre>'; var_dump( $queried_object ); echo '</pre>';
	    //$query_status = array('key' => 'pa_status', 'value' => 1);
		//add_query_arg($query_status);
		//set_query_var( 'meta_query', $query_status );
	    //$meta_query = $query_status;
	    //$args1['meta_query'] = $meta_query;
		//echo '<pre>'; print_r($wp_query); echo '</pre>';
	
		foreach ( $wp_query->tax_query->queries as $q ) {
      		$tax_query[] = $q;
   		 }
		
		foreach ( $wp_query->meta_query->queries as $q ) {
      		$meta_query[] = $q;
   		 }
		 $paged = $wp_query->query_vars['paged'];
		$args=array('meta_query'=>$meta_query,'tax_query'=>$tax_query,'posts_per_page' => 6,'post_type' => 'product','orderby'=>$orderby,'order'=>$order ,'paged'=>$paged,'product_brand'=>$_GET['filter_product_brand']);
	   // echo '<pre>'; print_r($tax_query); echo '</pre>';
		 	// echo '<pre>'; print_r($meta_query); echo '</pre>';
		//if(!isset($_GET['filter_product_brand'])){	
		//query_posts($query_string.'&pa_status=1');
		// }
		//query_posts($args);
		?>
		<?php 
		//$products = new WP_Query( $args );
		if ( have_posts() ) : ?>
        <?php
		
			$cate = get_queried_object();
			$cateID = $cate->term_id;
        	$children = get_term_children($cateID, "product_cat");
			
			if( empty( $children ) || $_GET['filter_product_brand']!="" ) {
				// has no children
			
		?>
        
        <div class="product-navigation-wrap">

			<?php
				/**
				 * woocommerce_before_shop_loop hook.
				 *
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 * @hooked woocommerce_pagination - 25
				 */
				do_action( 'woocommerce_before_shop_loop' );
			?>
            </div>
			<?php 
			}
			?>
			<?php woocommerce_product_loop_start(); ?>
				<div class="haggle-product-category">
				<?php woocommerce_product_subcategories(); ?>
				</div>
                <div class="haggle-product">
                <ul>
				<?php while ( have_posts() ) : the_post(); ?>
					<?	$productColor = get_the_terms($product->ID,'pa_status');
					//echo 'Status-'.$productColor[0]->name.'<br />';?>
					<?php 
					/*if($productColor[0]->name =='2'){
						continue;
					}else{*/
					wc_get_template_part( 'content', 'product' ); 
					// }
					?>
				<?php endwhile; // end of the loop. ?>
                </ul>
				</div>
			<?php woocommerce_product_loop_end(); ?>
 			<div class="product-navigation-wrap">
			<?php
				/**
				 * woocommerce_after_shop_loop hook.
				 *
				 * @nerdster hooked woocommerce_result_count - 8
				 * @nerdster hooked woocommerce_catalog_ordering - 30
				 * @hooked woocommerce_pagination - 10
				 */
				do_action( 'woocommerce_after_shop_loop' );
			?>
			</div>
		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php wc_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif;
        

		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

	<?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action( 'woocommerce_sidebar' );
	?>

<?php get_footer( 'shop' ); ?>
