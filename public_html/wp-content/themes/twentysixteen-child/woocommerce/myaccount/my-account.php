<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 
 ============Status of Jobs================
 1 ====> New job
 2 ====> Quotation send to cutomer
 3 ====> Job closed
 
 10 ====> Quotation not available
 
 
 ============Status of Quotations================
 0 ====> Pending Quotation
 1 ====> New job Quotation
 2 ====> Quotations selected to send to cutomer
 3 ====> cutomer selected quotation
 
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

wc_print_notices(); 

?>
<div class="woocomerce-inner woocomerce-account">
	<div class="my-account-fieldset">
        
            <?php
			//extract logged in user role
			$user_id = get_current_user_id();
			$user = new WP_User( $user_id );
			$role = $user->roles[0];
			$limit =3;
			if($role == 'retailer-account'){
				////  Retailer Dashboard ///
				?>
                	<div class="retailer-dashboard">
                    	<!--// Pending Quotation //-->                        
                        <div class="retailer-info-section">
                            <div class="fieldset-title">Pending Quotations</div>
                            <div class="fieldset-content"><?php echo get_retailer_jobs($user_id,1,0,$limit);//retailer_id,job_status,quote_status,limit?></div>
                            
                        </div>
                    	<!--// Submitted Quotation //-->                        
                        <div class="retailer-info-section">
                            <div class="fieldset-title">Submitted Quotations</div>
                            <div class="fieldset-content"><?php echo get_retailer_jobs($user_id,2,1,$limit);?></div>
                            
                        </div>
                        <!--// Selected Quotation //-->                        
                        <div class="retailer-info-section">
                            <div class="fieldset-title">Selected Quotations</div>
                            <div class="fieldset-content"><?php echo get_retailer_jobs($user_id,2,2,$limit);?></div>                            
                        </div>
                    </div>
                <?php
			}elseif($role == 'buyer-account'){
				////  Customer Dashboard ///
				?>
                	<div class="buyer-dashboard">
                    	<!--// Waiting Quotation //-->                        
                        <div class="buyer-info-section">
                            <div class="fieldset-title">New Jobs</div>
                            <div class="fieldset-content"><?php echo get_buyer_jobs($user_id,1,$limit);?></div>
                            
                        </div>
                    	<!--// Submitted Quotation //-->                        
                        <div class="buyer-info-section">
                            <div class="fieldset-title">Quoted Jobs</div>
                            <div class="fieldset-content"><?php echo get_buyer_jobs($user_id,2,$limit);?></div>
                            
                        </div>
                        <!--// Selected Quotation //-->                        
                        <div class="buyer-info-section">
                            <div class="fieldset-title">Selected Jobs</div>
                            <div class="fieldset-content"><?php echo get_buyer_jobs($user_id,3,$limit);?></div>                            
                        </div>
                    </div>
                <?php
			}else{
			
			
            printf(
                __( 'Hello <strong>%1$s</strong> <br /> (not %1$s? <a href="%2$s">Sign out</a>).', 'woocommerce' ) . ' ',
                $current_user->display_name,
                wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) )
            );
        
            printf( __( '<br /><br />From your account dashboard you can view your recent deals, manage your shipping and billing addresses and <a href="%s">edit your password and account details</a>.', 'woocommerce' ),
                wc_customer_edit_account_url()
            );
			}
            ?>        
    </div>

	<?php do_action( 'woocommerce_before_my_account' ); ?>
    
    <?php //wc_get_template( 'myaccount/my-downloads.php' ); ?>
    
    <?php //wc_get_template( 'myaccount/my-orders.php', array( 'order_count' => $order_count ) ); ?>
    
    <?php //wc_get_template( 'myaccount/my-address.php' ); ?>
    
    <?php do_action( 'woocommerce_after_my_account' ); ?>
</div>