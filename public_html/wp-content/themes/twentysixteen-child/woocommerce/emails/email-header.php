<?php
error_reporting(0);
/**
 * Email Header
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-header.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates/Emails
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$stylsheet_url = get_stylesheet_directory_uri();
?>

<!DOCTYPE html>
<html dir="<?php echo is_rtl() ? 'rtl' : 'ltr'?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo get_bloginfo( 'name', 'display' ); ?></title>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800,300italic' rel='stylesheet' type='text/css'>
        <style type="text/css" media="screen">
        [style*='Open Sans'] {font-family: 'Open Sans', Arial, sans-serif !important}
        img {color:#ff5400;text-decoration:none;font-weight:bold;text-transform:uppercase}
        </style>
    </head>
	<body <?php echo is_rtl() ? 'rightmargin' : 'leftmargin'; ?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">   
    <div style="width:767px; float:none; margin:0 auto; background-color:#e6e6e6; overflow:hidden; padding:20px">
	<div style="background-color:#fff; float:none; width:100%; width:650px; margin:0 auto; overflow:hidden; font-family: 'Open Sans', Calibri, Verdana, Arial; font-size: 15px; line-height: 21px; padding:0">
    <!-- Template Header Starts Here -->
    <div style="width:100%; float:left; margin:0">
        <ul style="list-style-type:none; width:100%; padding:0; margin:0">
            <li style="background-color:#fff; width:100%; text-align:left; list-style:none; padding:6px 0; margin:0">
            	<a title="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>" href="<?=home_url()?>"><img src="<?=$stylsheet_url?>/img/email-logo.jpg" alt="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>" /></a>
            </li>
            <li style="background-color: rgb(0, 0, 0); width: 100%; text-align: left; list-style: outside none none; margin: 0px; max-height: 181px;">
            	<img src="<?=$stylsheet_url?>/img/email-banner.jpg" alt="Find the Best Price on your Product" />
           	</li>
            <li style="width:100%; background-color:#ff7801; float:left; text-align:center; color:#fff; padding:20px 0; text-transform:uppercase; font-weight:bold; font-size:18px; margin:0"><?php echo $email_heading; ?></li>
        </ul>
    </div>
    <!-- Template Header Ends Here -->
  