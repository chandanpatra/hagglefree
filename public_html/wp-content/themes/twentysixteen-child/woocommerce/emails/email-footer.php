<?php
/**
 * Email Footer
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-footer.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<!-- Template Footer Starts Here -->
    <div style="width:100%; background-color:#000; float:left; margin:0">
        	<div style="width:100%; float:left; list-style:none; background-color:#191919; margin:0">
            	<ul style="width:90%; float:left; list-style:none; padding:15px 5%; margin:0">
                	<li style="width:50%; float:left; list-style:none; padding:10px 0; color:#717171; font-size:12px; margin:0">
                    	Thanks,<br /><?php bloginfo( 'name' ); ?> Team<br />
                        <span style="float:left"><strong>ABN:</strong> <?=get_option( 'site_abn' )?></span>
                </li>
                <li style="width:50%; float:right; list-style:none; padding:10px 0; color:#717171; font-size:12px; margin:0"><strong>Contact Us</strong><br />
                  <span style="background: #191919 url('<?=get_stylesheet_directory_uri()?>/img/email-icon-email.png') no-repeat scroll left center; padding-left:18px">
                  <a href="mailto:<?=get_option( 'admin_email' )?>" title="" style="color:#717171; text-decoration:none"><?=get_option( 'admin_email' )?></a>
                  </span><br />
                  <span style="background: #191919 url('<?=get_stylesheet_directory_uri()?>/img/ph-icon-email.png') no-repeat scroll left center; padding-left:18px; float:left"> <?=get_option( 'site_phone' )?></span>                  
                </li>
                </ul>
</div>
            <div style="width:100%; float:left; list-style:none; background-color:#000">
<ul style="width:90%; float:left; list-style:none; padding:6px 5%; margin:0">
                	<li style="width:50%; float:left; list-style:none; padding:0; color:#4f4e4e; font-size:12px; margin:0">
                    	&copy; copyright <?php bloginfo( 'name' ); ?>. All Rights Reserved.
                </li>
                <li style="width:50%; float:right; list-style:none; padding:0; color:#4f4e4e; font-size:12px; text-align:left; margin:0">
                    	<ul style="float: right; line-height: 16px; margin: 0px; padding: 0px; width: 103px;">
                            <li style="list-style: outside none none; padding: 0px; margin: 0px; float: left; width: 80px; line-height: 26px;">Follow Us On:</li>
                            <li style="list-style: outside none none; padding: 0px; margin: 0px; float: left; width: 20px;"><a title="Follow Us On" href="<?=get_option( 'fb_url' )?>"><img src="<?=get_stylesheet_directory_uri()?>/img/email-fb-icon.png" alt="Follow Us On" /></a></li>
                        </ul>
                </li>
                </ul>
      </div>
        </div>
        <!-- Template Footer Ends Here -->
        </div>
        </div>
    </body>
</html>
