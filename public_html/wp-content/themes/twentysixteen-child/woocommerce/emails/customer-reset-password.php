<?php
/**
 * Customer Reset Password email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-reset-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

 <!-- Template Content Starts Here --> 		
        <div style="background: #ffffff url('<?=get_stylesheet_directory_uri()?>/img/password-reset-bg.png') no-repeat scroll right bottom; min-height: 240px; padding:10px 0; float:left; width:100%">
        <ul style="width:60%; float:left; padding:0 20px; margin:0">
        	<li style="width:100%; float:left; list-style:none; margin:0; color:#000">
			<?php _e( 'Someone requested that the password be reset for the following account', 'woocommerce' ); ?>
            <span style="color: #ff5400;"><?php _e('Username: ','woocommerce');?><?php printf( __( '<strong>%s</strong>', 'woocommerce' ), esc_html( $user_login )); ?></span><br />
            <br /><?php _e( 'If this was a mistake, just ignore this email and nothing will happen.', 'woocommerce' ); ?><br />
       	    <br /><?php _e( 'To reset your password, visit the following address:', 'woocommerce' ); ?></li>
            <li style="width:100%; float:left; list-style:none; padding-top:24px; margin:0">
            <a title="Activate your Account" href="<?php echo esc_url( add_query_arg( array( 'key' => $reset_key, 'login' => rawurlencode( $user_login ) ), wc_get_endpoint_url( 'lost-password', '', wc_get_page_permalink( 'myaccount' ) ) ) ); ?>">
            <img src="<?=get_stylesheet_directory_uri()?>/img/reset-password-btn.png" alt="Activate your Account" /></a></li>
        </ul>
</div>
        <!-- Template Content Ends Here -->
<?php do_action( 'woocommerce_email_footer', $email ); ?>
