<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

	<?php
	global $product;
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

		<?php while ( have_posts() ) : the_post(); ?>

			<?php wc_get_template_part( 'content', 'single-product' ); ?>

		<?php endwhile; // end of the loop. ?>

	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

<?php
	/**
	 * woocommerce_sidebar hook.
	 *
	 * @hooked woocommerce_get_sidebar - 10
	 */
	do_action( 'woocommerce_sidebar' );
?>

<?php get_footer( 'shop' ); ?>
<?php 
	if(isset($_GET['get_quote']) || isset($_POST['job_form_save'])){
?>
<script type="text/javascript">
jQuery(document).ready(function(){
	<!--jQuery.prettyPhoto.open('#jobDetails');-->
	<?php
	   $visibility = get_post_meta($product->get_id(),"_visibility",true);
       if($visibility != "hidden"){
	?>
			if(jQuery(window).width() > 1024)
			jQuery.colorbox({inline:true, width:"40%", href:"#jobDetails"});
			else
			jQuery.colorbox({inline:true, width:"80%", href:"#jobDetails"});
	<?php
	   }
	?>
	
	jQuery('.commercial').click(function(){
			  var val=jQuery(this).val();
			  if(val==1){
				jQuery('.commercial_block').show();
			  }else{
				jQuery('.commercial_block').hide();
			  }
			});
}); 
</script>
<?php
	}
?>