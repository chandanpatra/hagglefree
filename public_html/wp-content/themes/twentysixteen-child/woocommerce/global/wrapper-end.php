<?php
/**
 * Content wrappers
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/wrapper-end.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$template = get_option( 'template' );
ob_start();
?>
<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
 <ul id="sidebar">
  <?php dynamic_sidebar( 'sidebar-1' ); ?>
 </ul>
<?php endif; ?>
<?php
$left_sidevar = str_replace(")","",str_replace("(","",ob_get_contents()));
ob_end_clean();
switch( $template ) {
	case 'twentyeleven' :
		echo '</div></div>';
		break;
	case 'twentytwelve' :
		echo '</div></div>';
		break;
	case 'twentythirteen' :
		echo '</div></div>';
		break;
	case 'twentyfourteen' :
		echo '</div></div></div>';
		get_sidebar( 'content' );
		break;
	case 'twentyfifteen' :
		echo '</div></div>';
		break;
	case 'twentysixteen' :
		if(is_single()){
			echo '</div><!-- main --></div><!-- primary --></div><!-- content --></section>';
		}else{
			echo '</div><!-- main --><div id="secondary" class="shop-sidebar cl-lg-3 col-md-3 col-sm-12 col-xs-12">'.$left_sidevar.'</div><!-- secondry --></div><!-- primary --></div><!-- content --></section>';
		}
		
		break;
	default :
		echo '</div></div>';
		break;
}
