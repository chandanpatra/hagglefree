<?
// Add a footer/copyright information section.
add_action('customize_register','my_customize_register');
function my_customize_register( $wp_customize ) {
$wp_customize->add_section( 'footer' , array(
  'title' => __( 'Footer', 'themename' ),
  'priority' => 90, // Before Navigation.
) );
}