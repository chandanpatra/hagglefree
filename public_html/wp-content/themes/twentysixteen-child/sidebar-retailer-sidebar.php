<?php
/**
 * The template for the content bottom widget areas on posts and pages
 *
 * @package Nerdster
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

if ( ! is_active_sidebar( 'retailer_sidebar' ) ) {
	return;
}

// If we get this far, we have widgets. Let's do this.
?>
<div class="member-sidebar cl-lg-3 col-md-3 col-sm-12 col-xs-12">    	
			<?php if ( is_active_sidebar( 'retailer_sidebar' ) ) : ?>
                <div class="widget-area">
                    <?php dynamic_sidebar( 'retailer_sidebar' ); ?>
                </div><!-- .widget-area -->
            <?php endif; ?>
</div><!-- .member-sidebar -->
