<?php
/**
 * WordPress Cron Implementation for retailer
 * @package WordPress
 */
ignore_user_abort(true);

/**
 * Tell WordPress we are doing the CRON task.
 *
 * @var bool
 */
/*ini_set('max_execution_time',0);
ini_set('max_input_time',0);*/
ini_set('memory_limit','1024M');

if ( !defined('ABSPATH') ) {
	/** Set up WordPress environment */
	require_once( dirname( __FILE__ ) . '/wp-load.php' );
}

global $wpdb;

/*$tableJobs = $wpdb->prefix . 'jobs';
$sql = "SELECT *, NOW() as corTstmp FROM $tableJobs WHERE `status` = '1'";
$results = $wpdb->get_results($sql);*/
$tableFeed = $wpdb->prefix . 'feedRetalProducts';

// get retailers feed url
$args = array(	
	'role'         => 'retailer-account',	
	'meta_key'     => 'auto_quote',
	'meta_value'   => 'yes'
 ); 
 
$blogusers = get_users( $args );


// Array of WP_User objects.
foreach ( $blogusers as $user ) {    
	$feed_url = get_user_meta($user->ID,'feed_url',true);
	if($feed_url!=""){		
		//$homepage = curl_get_contents($feed_url);
		$homepage = trim(file_get_contents($feed_url));
		$data = explode("\n",$homepage);
		$retailerId = $user->ID;
		$count=0;
		if(count($data) > 0){
		foreach($data as $rec){
			//if($retailerId == 220)
			//continue;
			if(trim($rec)!="")
			{   
			    $count++;
				$fsku = "";
				$price = "";
				$stockQ = "";
				$condition = "";
				$url = "";
				$mprice = "";
								
				$pdata = explode("|",$rec);
				
				if(trim($pdata[0])!=""){
					$fsku = trim($pdata[0]);
				}
				$price = (float)trim($pdata[3]);
				$stockQ = trim($pdata[4]);
				$condition = trim($pdata[5]);
				$url = trim($pdata[6]);
				if($pdata[7]){				
					$mprice = (float)trim($pdata[7]);
				}else{
					$mprice = 0;
				}
				
				
				 $sql = "SELECT * FROM $tableFeed WHERE `retailerId` = '$retailerId' AND `sku` = '$fsku'";
				 $result = $wpdb->get_row($sql);
				 if($result){
					$sqlupdate = "UPDATE $tableFeed SET `price` = '$price',`mprice` = '$mprice',`qty`='$stockQ', `productCondition` = '$condition',`productUrl` = '$url',`status` = '2' WHERE `retailerId` = '$retailerId' AND `sku` = '$fsku'"; 
					$wpdb->query($sqlupdate);
				 }else{
					$sqlInsert = "INSERT INTO $tableFeed SET `price` = '$price',`mprice` = '$mprice',`qty`='$stockQ',`retailerId` = '$retailerId',`sku` = '$fsku', `productCondition` = '$condition',`productUrl` = '$url',`status` = '2'"; 
					$wpdb->query($sqlInsert);
				 }
				unset($pdata);
			}
			   
		}
		if(count($data) == $count){
			$sqlDelete = "DELETE FROM $tableFeed WHERE `retailerId` = '$retailerId' AND `status` = '1'";
			$wpdb->query($sqlDelete);
		}
		$sqlUpdate = "UPDATE $tableFeed SET `status` = '1' WHERE `retailerId` = '$retailerId' AND `status` = '2'";
		$wpdb->query($sqlUpdate);
	  }
	}
	$fopen = fopen("cron_run_status.txt","a");
	fwrite($fopen,"\n Retailer: ".$retailerId." Total Data:".count($data)." Iterration Count :".$count." Time:".date("d-m-Y H:i:s"));
	fclose($fopen);	
}

function curl_get_contents($url)
{
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  $data = curl_exec($ch);
  curl_close($ch);
  return $data;
}

//$homepage = file_get_contents('http://new.buysmarte.com.au/hagglefeed/hfmyshopping.php');

?>