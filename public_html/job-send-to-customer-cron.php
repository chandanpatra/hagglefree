<?php
/**

 * WordPress Cron Implementation for retailer

 * @package WordPress

 */
error_reporting(1);
ignore_user_abort(true);

/**
 * Tell WordPress we are doing the CRON task.
 *
 * @var bool
 */

if ( !defined('ABSPATH') ) {
	/** Set up WordPress environment */
	require_once( dirname( __FILE__ ) . '/wp-load.php' );
}

global $wpdb;
$cutoff_time = (int)get_option('cutoff_time');
$tableJobs = $wpdb->prefix . 'jobs';
$tableQuotation = $wpdb->prefix . 'jobquotation';

$best3_quotations_email_template = "best_3_quotations_for_buyer";
$best3_template_detail = get_email_template($best3_quotations_email_template);	

$my_quotation_selected_template = "my_quotation_selected";
$my_quotation_selected = get_email_template($my_quotation_selected_template);	

$sql = "SELECT *, NOW() as corTstmp, (NOW() + INTERVAL 1 HOUR) as daylightTstmp  FROM $tableJobs WHERE `status` = '1'";
//$sql = "SELECT *, NOW() as corTstmp FROM $tableJobs WHERE `id` = '417'";
$results = $wpdb->get_results($sql);

//=============Job Config Data===============
$job_no_of_quotes = (int)get_option('job_no_of_quotes');
$leadFee = (float)get_option('lead_fee');
/*

============Status of Jobs================

 0 ====> New job account not verified.
 1 ====> New job
 2 ====> Quotation send to cutomer 
 10 ====> Quotation not available

 ============Status of Quotations================

 0 ====> Request for Quotation
 1 ====> Quotation submited
 2 ====> Quotations selected to send to cutomer

*/

$media_url = get_stylesheet_directory_uri()."/img";

 //==================Daylight saving start date and end date====================================//
	$dlsStartDate = strtotime(date('Y-m-d 00:00:00', strtotime('First Sunday Of October '.date('Y'))));
	$dlsEndDate = strtotime(date('Y-m-d 00:00:00', strtotime('First Sunday Of April '.(date('Y')+1))));	 
// ==================== Daylight Saving period=================================//

foreach($results as $result){

   // Product id and it's price rrp//
	$productId = $result->productId;
	$rrp = (float)get_post_meta($productId,'_price', true);

	$quotationSendDate = strtotime($result->quotationSendDate);	
	$corTstmp = strtotime($result->corTstmp);
	$offerExp = $result->offerExp;
	
if(strtotime($result->jobDate) > $dlsStartDate && strtotime($result->jobDate) < $dlsEndDate){
		$cr_pcode = $result->Postcode;
		switch($cr_pcode){
			//======== South Australia ================//			
			case $cr_pcode>=5000 && $cr_pcode<=5799:
			case $cr_pcode>=5800 && $cr_pcode<=5999:
				$corTstmp = strtotime($result->daylightTstmp."+30 minutes");
				break;
				//======== NSW ================//
			case $cr_pcode>=1000 && $cr_pcode<=1999:
			case $cr_pcode>=2000 && $cr_pcode<=2599: 
			case $cr_pcode>=2620 && $cr_pcode<=2899:
			case $cr_pcode>=2921 && $cr_pcode<=2999: 
			    //======= ACT ================//
			case $cr_pcode>=200 && $cr_pcode<=299:
			case $cr_pcode>=2600 && $cr_pcode<=2619:
			case $cr_pcode>=2900 && $cr_pcode<=2920:
			
			  //====== VIC =====================//	
			  		
			case $cr_pcode>=3000 && $cr_pcode<=3999:  
			case $cr_pcode>=8000 && $cr_pcode<=8999: 
						
			  // ==== Tasmania ================//
			case $cr_pcode>=7000 && $cr_pcode<=7799: 
			case $cr_pcode>=7800 && $cr_pcode<=7999: 
			    $corTstmp = strtotime($result->daylightTstmp);
				break;
	 }
}
	
	if($corTstmp >= $quotationSendDate){
	   $bestThree = array();
	   $noBestThree = array();

	   $sqlQuotations = "SELECT * FROM `$tableQuotation` WHERE `jobId` = '".$result->id."' AND `quotationAmount` > 0  AND status > 0";
	   $quotationResults = $wpdb->get_results($sqlQuotations);

	  // echo "<pre>";print_r($quotationResults);echo "</pre>";

	   // ================= Email Token Variables ======================= 

	   $product = wc_get_product( $result->productId );
	   $usage_type = ($result->is_commercial==1?'Commercial':'Private');

	   $product_details =" <div style=\"width:100%; float:left; margin:0; padding:0\">

            <div style=\"width:96%; float:left; padding:10px 2%; margin:0; background-color: rgb(255, 142, 42); border-radius:4px 4px 0 0; text-align:left; color:#fff; font-weight:bold; font-size:16px\"> ".esc_html( $product->get_title() )." </div>

            <div style=\"width:96%; float:left; padding:10px 2% 20px; margin:0; background-color: rgb(255, 255, 255); border-radius:0 0 4px 4px; text-align:left; color:#000; font-weight:normal; font-size:14px\">

              <div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">Job id: <span style=\"color: rgb(255, 84, 0)\">".$result->id."</span></div>

              <div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">SKU: <span style=\"color: rgb(255, 84, 0);\">".$product->get_sku()."</span></div>
			  
			  <div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">Product Name: <span style=\"color: rgb(255, 84, 0);\">".$product->get_title()."</span></div>


              <div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">Quantity required: <span style=\"color: rgb(255, 84, 0);\">".$result->quantity."</span></div>

              <div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">Usage type: <span style=\"color: rgb(255, 84, 0);\">".$usage_type."</span></div>
			  <p></p>
				{dynamic_attributes}
            </div>
          </div>";

	  // Get Customer details

      $getJob = get_job("",$result->id);

      $customer_details = "<div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">Customer: <b>".$getJob->firstname."</b></div>";

      $customer_details .= "<div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">Email: <b>".$getJob->email."</b></div>";

	  $customer_details .= "<div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">Post Code: <b>".$getJob->Postcode."</b></div>";

	  if(get_option('show_phone_register_form')=="")

      	$customer_details .= "<div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">Phone: <b>".$getJob->telephone."</b></div>";

	  $customer_requirements = "<ul style=\"width:100%; float:left; margin:0 0; padding:0\">

                        <li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#000; border-radius:4px 4px 0 0; text-align:left; color:#fff; font-weight:normal; font-size:14px\">Customer's Requirement(s):</li>

                        <li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#fff; border-radius:0 0 4px 4px; text-align:left; color:#000; font-weight:normal; font-size:14px\">";

		///get dynamic attributes ////

		$das = get_dynamic_job_attributes($result->id);

		if(count($das)>0){

			$cr_da ="";
            $dynamic_attributes = "";
			$special_attribute = "<p></p>";
			foreach($das['key'] as $key => $value){

				if($value == "wccpf_when_do_you_need_the_product")
					$cr_da .= "<div style=\"width:100%;\">".$das['label'][$value]." : <span style=\"color: rgb(255, 84, 0);\">".product_delivery_switch($das['customer_choice'][$value])."</span></div>";
				else
					$cr_da .= "<div style=\"width:100%;\">".$das['label'][$value]." : <span style=\"color: rgb(255, 84, 0);\">".ucfirst($das['customer_choice'][$value])."</span></div>";

			//==================== For product details ========================//
			 if($value == "wccpf_deliverypickup"){
			  $special_attribute .= "<div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">".$das['label'][$value].": <span style=\"color: rgb(255, 84, 0);\">".ucfirst($das['customer_choice'][$value])."</span></div>";
			 }
			 else if($value == "wccpf_when_do_you_need_the_product"){
			  $special_attribute .= "<div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">Required by: <span style=\"color: rgb(255, 84, 0);\">".product_delivery_switch($das['customer_choice'][$value])."</span></div>";
			 }
			  else if($value == "wccpf_removal_of_old_appliance"){
			  $special_attribute .= "<div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">".$das['label'][$value].": <span style=\"color: rgb(255, 84, 0);\">".ucfirst($das['customer_choice'][$value])."</span></div>";
			 }
			 else if($value == "wccpf_installation"){
			  $special_attribute .= "<div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">".$das['label'][$value].": <span style=\"color: rgb(255, 84, 0);\">".ucfirst($das['customer_choice'][$value])."</span></div>";
			 }
			 else if($value == "wccpf_installation_required"){
			  $special_attribute .= "<div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">".$das['label'][$value].": <span style=\"color: rgb(255, 84, 0);\">".ucfirst($das['customer_choice'][$value])."</span></div>";
			 }
			 else{
			  $dynamic_attributes .= "<div style=\"padding-top:0; padding-bottom:0; line-height:21px; margin-top:0; margin-bottom:0; width:100%\">".$das['label'][$value].": <span style=\"color: rgb(255, 84, 0);\">".ucfirst($das['customer_choice'][$value])."</span></div>";
			 }
			 //========================== End ==============================//
			}

			$customer_requirements .=$cr_da;
			
			$product_details = str_replace("{dynamic_attributes}",$dynamic_attributes.$special_attribute,$product_details);

		}

		$customer_requirements .="
						<!-- <div style=\"width:100%;\">Usage type: <span style=\"color: rgb(255, 84, 0);\">".($result->is_commercial=='1'?'Commercial':'Private')."</span></div> -->
					</li>
				</ul>"; 

	   $buyer_details = "<ul style=\"width:100%; float:left; margin:0 0; padding:0\">

                        <li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#000; border-radius:4px 4px 0 0; text-align:left; color:#fff; font-weight:normal; font-size:14px\">Buyer Details:</li>

                        <li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#fff; border-radius:0 0 4px 4px; text-align:left; color:#000; font-weight:normal; font-size:14px\">

                            <div style=\"width:100%;\">Customer: <span style=\"color: rgb(255, 84, 0);\">".$getJob->firstname." ".$getJob->lastname."</span></div>

							<div style=\"width:100%;\">Email: <span style=\"color: rgb(255, 84, 0);\">".$getJob->email."</span></div>

							<div style=\"width:100%;\">Postcode: <span style=\"color: rgb(255, 84, 0);\">".$getJob->Postcode."</span></div>";

		if(get_option('show_phone_register_form')=="")

			$buyer_details .= "<div style=\"width:100%;\">Phone: <span style=\"color: rgb(255, 84, 0);\">".$getJob->telephone."</span></div>";

		$buyer_details .="
                        </li>
                    </ul>";

		$buyer_name = $getJob->firstname." ".$getJob->lastname;


	// ================= Email Token Variables ======================= 						

	   if(count($quotationResults)>0){

		$i = 0;
		$ii=0;

		foreach($quotationResults as $quotationResult){

		 $Yes = 0;
		 $No  = 0;
		 $finalQuotationAmount  = 0; 
		 $da_quote = get_dynamic_quote_attributes($quotationResult->id);

		 if(count($da_quote) > 0){
			foreach($da_quote as $key => $value){
				$$key = $value;
			}
		}

		 

		 			$meet_timeframe 		= $quotationResult->meet_timeframe;			
					$quotation_amount 	  = $quotationResult->quotationAmount;
					$warranty_price 		= $quotationResult->warranty_price*$result->quantity;
							

					//get dynamic prices

					if(count($das['key']) > 0){
						$priceMetaDataArray = array();
						foreach($das['key'] as $key => $val){
							$variable = "price_".$val;
							if($$variable==0)
							continue;
							$priceMetaDataArray[$variable]=$$variable*$result->quantity;
						}
					}

					// best three criteria selection";
					if(count($das['key']) > 0){
						foreach($das['key'] as $key => $val){
							$meet_variable = "meet_".$val;
							if(isset($$meet_variable) && ($das['customer_choice'][$val] != $$meet_variable)){
								if($$meet_variable == "no"){
									$No++;
								}
								else{
									$Yes++;
								}
							}else{
								if(!$$meet_variable){
								 	continue;
								}
								if($das['customer_choice'][$val] == $$meet_variable){
									$Yes++;
								}
								else{
									$No++;
								}
							}
						}
					}
					

		 if($No > 0 ){

			$noBestThree[$ii]['quotationId'] = $quotationResult->id;
			$noBestThree[$ii]['retailerId'] = $quotationResult->retailerId;
			$noBestThree[$ii]['contact_person'] =  $quotationResult->contact_person;
			$noBestThree[$ii]['contact_details'] =  $quotationResult->contact_details;
			$noBestThree[$ii]['quotationAmount'] =  $quotationResult->quotationAmount;
            $noBestThree[$ii]['ticketAmount'] =  $quotationResult->ticketAmount;
			$noBestThree[$ii]['quotationDate'] =  $quotationResult->quotationDate;
			$noBestThree[$ii]['comments'] =  $quotationResult->comments;
			$noBestThree[$ii]['product_condition'] =  $quotationResult->product_condition;
			$noBestThree[$ii]['product_warranty'] =  $quotationResult->product_warranty;//comments on product condition
			$noBestThree[$ii]['product_comment'] =  $quotationResult->product_comment;
			$finalQuotationAmount = $quotationResult->quotationAmount*$result->quantity;

			if(count($das['key']) > 0){

				$priceMetaDataArray = array();

				foreach($das['key'] as $key => $val){

					$price_variable = "price_".$val;
					$meet_variable = "meet_".$val;
					$comment_variable = "comment_".$val;			
					$noBestThree[$ii][$price_variable] =  $$price_variable;
					$noBestThree[$ii][$meet_variable] =  $$meet_variable;
					$noBestThree[$ii][$comment_variable] =  $$comment_variable;
					$finalQuotationAmount += $$price_variable*$result->quantity;
					unset($$price_variable);unset($$meet_variable);unset($$comment_variable);
				}
			}

			

			$noBestThree[$ii]['warranty_price'] =  $quotationResult->warranty_price*$result->quantity;	
			if($quotationResult->warranty_price)
				$finalQuotationAmount += $quotationResult->warranty_price*$result->quantity;	
			$noBestThree[$ii]['finalQuotationAmount'] = $finalQuotationAmount;
			$noBestThree[$ii]['terms_and_conditions'] =  $quotationResult->terms_and_conditions;
			$ii++;

		 }else{

			$bestThree[$i]['quotationId'] = $quotationResult->id;
			$bestThree[$i]['retailerId'] = $quotationResult->retailerId; 
			$bestThree[$i]['contact_person'] =  $quotationResult->contact_person;
			$bestThree[$i]['contact_details'] =  $quotationResult->contact_details;
			$bestThree[$i]['quotationAmount'] =  $quotationResult->quotationAmount;
			$bestThree[$i]['ticketAmount'] =  $quotationResult->ticketAmount;
			$bestThree[$i]['quotationDate'] =  $quotationResult->quotationDate;
			$bestThree[$i]['comments'] 		=  $quotationResult->comments;
			$bestThree[$i]['product_condition'] 		=  $quotationResult->product_condition;
			$bestThree[$i]['product_warranty'] =  $quotationResult->product_warranty;//comments on product condition
			$bestThree[$i]['product_comment'] 		=  $quotationResult->product_comment;
			$finalQuotationAmount = $quotationResult->quotationAmount*$result->quantity;

			if(count($das['key']) > 0){
				$priceMetaDataArray = array();
				foreach($das['key'] as $key => $val){
					$price_variable = "price_".$val;
					$meet_variable = "meet_".$val;
					$comment_variable = "comment_".$val;					
					$bestThree[$i][$price_variable] =  $$price_variable;
					$bestThree[$i][$meet_variable] =  $$meet_variable;
					$bestThree[$i][$comment_variable] =  $$comment_variable;
					$finalQuotationAmount += $$price_variable*$result->quantity;
					unset($$price_variable);unset($$meet_variable);unset($$comment_variable);
				}
			}

			$bestThree[$i]['warranty_price'] =  $quotationResult->warranty_price*$result->quantity;
			if($quotationResult->warranty_price)
			$finalQuotationAmount += $quotationResult->warranty_price*$result->quantity;
			$bestThree[$i]['finalQuotationAmount'] = $finalQuotationAmount;
			$bestThree[$i]['terms_and_conditions'] =  $quotationResult->terms_and_conditions;
		 $i++;	
		 }						 
		}

		/*echo "<pre>";

		print_r($bestThree);

		echo "===============>";

		print_r($noBestThree);

		echo "</pre>";*/

		

		if(count($noBestThree)>0)
		     $noBestThree = record_sort($noBestThree,'finalQuotationAmount');	
		if(count($bestThree)>0)
			 $bestThree = record_sort($bestThree,'finalQuotationAmount');

		if(count($bestThree) >= $job_no_of_quotes)
		  $limit = $job_no_of_quotes;
		else
		  $limit = count($bestThree);	
		  $quotation_detail_list = "";

		 for($k = 0; $k < $limit; $k++){			 
			$company_name = get_user_meta($bestThree[$k]['retailerId'],'cr_company',true);
			$retaler_info = get_userdata($bestThree[$k]['retailerId']);
			$quotation_amount = $bestThree[$k]['quotationAmount'];
			//print_r($retaler_info);

			$retalerDisplayName = $retaler_info->display_name;
			if($company_name == ""){
				if($retalerDisplayName!="")
				$company_name = ucfirst($retalerDisplayName);
				else
				$company_name = ucfirst($retaler_info->first_name);
			 }

			$product_condition = product_condition($bestThree[$k]['product_condition']);
			$isgst = ($result->is_commercial==1?'(Inc GST)':'(Inc GST)');

			      //======================== For retailer =========================//

			$quotation_details ="<ul style=\"width:100%; float:left; margin:0 0; padding:0\">
            <li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#000; border-radius:4px 4px 0 0; text-align:left; color:#fff; font-weight:normal; font-size:14px\">Your Quotation Details:</li>
             <li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#fff; border-radius:0 0 4px 4px; text-align:left; color:#000; font-weight:normal; font-size:14px\">
              <div style=\"width:100%;\">Quotation Amount: <span style=\"color: rgb(255, 84, 0);\">$".$quotation_amount." per item</span></div>";

			if($bestThree[$k]['comments']!="")					  
			 $quotation_details .= "<div style=\"width:100%;\">Comments on Quoted Price:<span style=\"color: rgb(255, 84, 0);\">".stripslashes($bestThree[$k]['comments'])."</span> </div>";
           $quotation_details .="<div style=\"width:100%;\">Quotation Date: <span style=\"color: rgb(255, 84, 0);\">".date("l, F j, Y",strtotime($bestThree[$k]['quotationDate']))."</span></div>";

			     //======================== For customer =========================//

			$quotation_detail_list .="<div style=\"width:100%; margin:0 auto; padding:0 0 5px; background-color: rgb(255, 255, 255); border-radius:0 0 4px 4px\">
              <div style=\"width:100%; overflow:hidden; padding:9px 0; margin:0 auto; background-color: rgb(0, 0, 0); border-radius:4px 4px 0 0; text-align:left; color:#fff; font-weight:normal; font-size:14px; color:rgb(255, 84, 0); font-weight:bold; text-transform:uppercase\">
                <div style=\"width:95%; margin:0 auto;\">Quotation No :<span style=\"border-radius: 50%; background-color: rgb(255, 84, 0); color: rgb(255, 255, 255); float: right; height: 24px; width: 24px; text-align: center; font-weight: bold; line-height: 24px;\">".($k+1)."</span></div>
              </div>              

				<!-- retailer info starts -->             
				  <div style=\"width:100%; float:left; padding:0; margin:0 auto; text-align:center\">";
				  if(get_user_meta( $bestThree[$k]['retailerId'], 'cr_photo', true )){
						$quotation_detail_list .="<div style=\"float:left; width:40%; padding:10px 5px; margin:0; min-width:200px\">					
							<img src=\"".get_user_meta( $bestThree[$k]['retailerId'], 'cr_photo', true )."\" alt=\"\" style=\"float:left; margin:0 auto; display:inline-block; text-align:center; max-width:98%\">
						</div>";
				  }
					$quotationdAmount =  number_format($bestThree[$k]['ticketAmount']*$result->quantity,2);
					
					$quotation_detail_list .="<div style=\"width:".(get_user_meta( $bestThree[$k]['retailerId'], 'cr_photo', true )!=""?"55%":"100%")."; overflow:hidden; padding:10px 5px; margin:0 auto 15px; text-align:left; color:#000; font-weight:normal; font-size:14px; float:left\">
						<div style=\"width:100%;\">Retailer Name: <span style=\"color: rgb(255, 84, 0);\">".$company_name."</span></div>
						<div style=\"width:100%;\">Contact Name: <span style=\"color: rgb(255, 84, 0);\"> ".$bestThree[$k]['contact_person']."</span></div>
						<div style=\"width:100%;\">Contact Details: <span style=\"color: rgb(255, 84, 0);\">".$bestThree[$k]['contact_details']."</span></div>
					  </div>
				  </div>
				  <!-- retailer info ends -->           
			  <div style=\"width:95%; margin:0 auto 5px; padding:4px 5px; border:1px solid #dedede; overflow:hidden\">

              <!-- quotation amount starts -->
                <div style=\"width:100%; float:left; margin:0; padding:0\">
                  <div style=\"width:96%; float:left; margin:0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; \">
                    <div style=\"width:100%\">Quotation Amount $isgst:<span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">$".number_format($bestThree[$k]['ticketAmount'],2)." X ".$result->quantity." = $".$quotationdAmount."</span></div>
                  </div>
                ";

			if($bestThree[$k]['comments']!="")							  
				$quotation_detail_list .= "<div style=\"width:96%; float:left; margin:0; padding:6px 2%; float:left; font-size:13px; color:rgb(126, 126, 126); line-height:18px\">
                    <div style=\"width:100%;\"><span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments on Quoted Price:</span> <span style=\"width:100%; float:left\">".stripslashes($bestThree[$k]['comments'])."</span></div>
                  </div>";

				$quotation_detail_list .= "</div><!-- quotation amount ends -->";
				$quotation_detail_list .="<!-- product condition ends -->
				<div style=\"width:100%; float:left; margin:0\">
                  <div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238)\">
                    <div style=\"width:100%\">Product Condition:<span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".$product_condition."</span></div>
                  </div>                  
                ";				



			if($bestThree[$k]['product_warranty']!="")
				$quotation_detail_list .= "<div style=\"width:96%; float:left; margin:0; padding:6px 2%; float:left; font-size:13px; color:rgb(126, 126, 126); line-height:18px\">
                    <div style=\"width:100%;\"><span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments on Product Condition:</span> <span style=\"width:100%; float:left\">".stripslashes($bestThree[$k]['product_warranty'])."</span></div>
                  </div>";
				$quotation_detail_list .= "</div><!-- product condition ends -->";
				//// get dynamic data  starts ///////

		$da_quote = get_dynamic_quote_attributes($bestThree[$k]['quotationId']);
		 if(count($da_quote) > 0){
			foreach($da_quote as $key => $value){
				$$key = $value;
			}
		}				

				if(count($das['key']) > 0){
					$dynamic_quote ="";
					$quotation_detail_list.="{timeframe}";
					foreach($das['key'] as $key => $val){
						$price_variable = "price_".$val;
						$meet_variable = "meet_".$val;
						$comment_variable = "comment_".$val;

						if($val == "wccpf_when_do_you_need_the_product" || $comment_variable == "comment_wccpf_when_do_you_need_the_product"){
							if($val == "wccpf_when_do_you_need_the_product")
								/*$when_do_you_need_the_product = "
								<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; text-indec:1%\">
                    <div style=\"width:100%;\">Timeframe: <span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".product_delivery_switch($das['customer_choice'][$val])."</span> </div>
                  </div>
							  ";*/
							  $when_do_you_need_the_product = "";
							  if($bestThree[$k][$meet_variable]!=""){
									$when_do_you_need_the_product .="
									<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; \">
										<div style=\"width:100%\">Can you meet ".product_delivery_switch($das['customer_choice'][$val])." requirements? : <span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".ucfirst($bestThree[$k][$meet_variable])."</span> </div>
									</div>
									";
									$dynamic_quote .= "<div style=\"width:100%;\">Can you meet ".product_delivery_switch($das['customer_choice'][$val])." requirements? <span style=\"color: rgb(255, 84, 0);\">".ucfirst($bestThree[$k][$meet_variable])."</span></div>";
								}
							 if($bestThree[$k][$comment_variable]!=""){
								$when_do_you_need_the_product .="
								<div style=\"width:96%; float:left; margin:0; padding:6px 2%; float:left; font-size:13px; color:rgb(126, 126, 126); line-height:18px\">
									<div style=\"width:100%;\">
									<span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments:</span>
									<span style=\"width:100%; float:left\">".stripslashes($bestThree[$k][$comment_variable])."</span> </div>
								  </div>";								
							 }
							 if($$comment_variable != "")
							 $dynamic_quote .= "<div style=\"width:100%;\">Timeframe Comment  :<span style=\"color: rgb(255, 84, 0);\">".stripslashes($$comment_variable)."</span></div>";
							}
							
						elseif($val == "wccpf_deliverypickup" || $comment_variable == "comment_wccpf_deliverypickup"){
							$when_do_you_need_the_product2 = "";
						if($val == "wccpf_deliverypickup"){	    
								//$when_do_you_need_the_product2 ="<li style=\"width:98%; float:left; margin:5px 0 0 0; padding:6px 1%; float:left; list-style:none; background-color:#eee\">".ucfirst($das['customer_choice'][$val])." :<span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">Yes</span></li>";
								
								/*$when_do_you_need_the_product2 .="<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; \"><div style=\"width:100%\">".$das['label'][$val]." : <span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".ucfirst($das['customer_choice'][$val])."</span> </div></div>";

								$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." <span style=\"color: rgb(255, 84, 0);\">".ucfirst($das['customer_choice'][$val])."</span></div>";*/									
								
								
						}
						if($bestThree[$k][$meet_variable]!=""){
							$when_do_you_need_the_product2 .="
							<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; \">
								<div style=\"width:100%\">Can you meet ".$das['label'][$val]." requirements? : <span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".ucfirst($bestThree[$k][$meet_variable])."</span> </div>
							</div>
							";
							$dynamic_quote .= "<div style=\"width:100%;\">Can you meet ".$das['label'][$val]." requirements? <span style=\"color: rgb(255, 84, 0);\">".ucfirst($bestThree[$k][$meet_variable])."</span></div>";
						}
						
						
							if($bestThree[$k][$price_variable]!=""){
								$when_do_you_need_the_product2 .="
						<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff;\">";				
								$when_do_you_need_the_product2 .="
								<div style=\"width:100%; float:left; margin:3px 0\">".$das['label'][$val]." Price $isgst: <span style=\"color:#000; padding-left:3px; color:rgb(255, 84, 0)\">".($bestThree[$k][$price_variable]==0?"FREE":"$".$bestThree[$k][$price_variable])."</span> </div>
								";
								$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." Price <span style=\"color: rgb(255, 84, 0);\">$".$$price_variable." per item</span></div>";
							    $when_do_you_need_the_product2 .="</div>";
							}
							
							
							if($bestThree[$k][$comment_variable]!=""){
							    $when_do_you_need_the_product2 .="
								<div style=\"width:96%; font-size:13px; color:rgb(126, 126, 126); margin:0; padding:6px 2%; float:left; background-color: rgb(255, 255, 255)\">";		
								
								$when_do_you_need_the_product2 .="
								<div style=\"width:100%\"> <span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments:</span> <span style=\"width:100%; float:left\">".stripslashes($bestThree[$k][$comment_variable])."</span> </div>
								";
								$dynamic_quote .= "<div style=\"width:100%;\"> Commnets on ".$das['label'][$val]." <span style=\"color: rgb(255, 84, 0);\">".stripslashes($bestThree[$k][$comment_variable])."</span></div>";
								
								$when_do_you_need_the_product2 .="</div>";
							}
						
						
						
						
								
						}
						elseif($val == "wccpf_removal_of_old_appliance"){
							$when_do_you_need_the_product3 = "";
							if($das['customer_choice'][$val] == "no")//job requirements is no, no need to show it in form and quote template
								continue;
							else{
								if($bestThree[$k][$meet_variable]!=""){
									$when_do_you_need_the_product3 .="
									<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; \">
										<div style=\"width:100%\">".$das['label'][$val]." : <span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".ucfirst($bestThree[$k][$meet_variable])."</span> </div>
									</div>
									";
									$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." <span style=\"color: rgb(255, 84, 0);\">".ucfirst($bestThree[$k][$meet_variable])."</span></div>";
								}
								
										
										if($bestThree[$k][$price_variable]!=""){
											$when_do_you_need_the_product3 .="
									<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff;\">";				
											$when_do_you_need_the_product3 .="
											<div style=\"width:100%; float:left; margin:3px 0\">".$das['label'][$val]." Price $isgst: <span style=\"color:#000; padding-left:3px; color:rgb(255, 84, 0)\">".($bestThree[$k][$price_variable]==0?"FREE":"$".$bestThree[$k][$price_variable])."</span> </div>
											";
											$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." Price <span style=\"color: rgb(255, 84, 0);\">$".$$price_variable." per item</span></div>";
											$when_do_you_need_the_product3 .="</div>";
										}
										if($bestThree[$k][$comment_variable]!=""){
											$when_do_you_need_the_product3 .="
								<div style=\"width:96%; font-size:13px; color:rgb(126, 126, 126); margin:0; padding:6px 2%; float:left; background-color: rgb(255, 255, 255)\">";		
											$when_do_you_need_the_product3 .="
											<div style=\"width:100%\"> <span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments:</span> <span style=\"width:100%; float:left\">".stripslashes($bestThree[$k][$comment_variable])."</span> </div>
											";
											$dynamic_quote .= "<div style=\"width:100%;\">Comments on ".$das['label'][$val]." <span style=\"color: rgb(255, 84, 0);\">".stripslashes($bestThree[$k][$comment_variable])."</span></div>";
											$when_do_you_need_the_product3 .="</div>";
										}
									
							}
						}
						else{

							
							
							/*$when_do_you_need_the_product .= "
								<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; text-indec:1%\">
                    <div style=\"width:100%;\">".$das['label'][$val].": <span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".$das['customer_choice'][$val]."</span> </div>
                  </div>";*/

						if($bestThree[$k][$meet_variable]!=""){

								$quotation_detail_list .="<!-- dynamic meet requirement starts --><div style=\"width:100%; float:left; margin:0; padding:0\">";

								if($val =="wccpf_when_do_you_need_the_product"){

									$dynamic_quote .= "<div style=\"width:100%;\">Can you meet ".product_delivery_switch($das['customer_choice'][$val])." Timeframe? <span style=\"color: rgb(255, 84, 0);\">".ucfirst($$meet_variable)."</span></div>";

									if($$comment_variable != "")

									$dynamic_quote .= "<div style=\"width:100%;\">Timeframe Comment  :<span style=\"color: rgb(255, 84, 0);\">".stripslashes($$comment_variable)."</span></div>";

									continue;

								}else{

									if($val != "wccpf_deliverypickup"){

									  $dynamic_quote .= "<div style=\"width:100%;\">Can you meet ".$das['label'][$val]."? <span style=\"color: rgb(255, 84, 0);\">".ucfirst($$meet_variable)."</span></div>";

								    }

									/*$quotation_detail_list .="

									<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; background-color: rgb(238, 238, 238)\">

                    					<div style=\"width:100%\"> 

											<span style=\"float:left\">".$das['label'][$val]." :</span> 

											<span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".ucfirst($das['customer_choice'][$val])."</span> 

										</div>

									</div>

									";*/

								}

							$quotation_detail_list .= "<!-- requirements div starts --><div style=\"width:96%; float:left; margin:0; padding:6px 2%; font-size:13px; color:rgb(126, 126, 126); line-height:18px\">";

							$quotation_detail_list .=" 

							<div style=\"width:100%; float:left; margin:3px 0\"> <span style=\"float:left\">Can you meet the customers requirement?:</span> <span style=\"color:#000; padding-left:3px\">".ucfirst($bestThree[$k][$meet_variable])."</span> </div>								

								";

							if($bestThree[$k][$price_variable]!=""){				
								$quotation_detail_list .="
								<div style=\"width:100%; float:left; margin:3px 0\"> <span style=\"float:left\">".$das['label'][$val]." Price $isgst:</span> <span style=\"color:#000; padding-left:3px; color:rgb(255, 84, 0)\">".($bestThree[$k][$price_variable]==0?"FREE":"$".$bestThree[$k][$price_variable])."</span> </div>
								";							

								$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." Price <span style=\"color: rgb(255, 84, 0);\">$".$$price_variable." per item</span></div>";

							}

							if($bestThree[$k][$comment_variable]!=""){
								$quotation_detail_list .="
								<div style=\"width:100%\"> <span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments:</span> <span style=\"width:100%; float:left\">".stripslashes($bestThree[$k][$comment_variable])."</span> </div>
								";

								$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." Comment  <span style=\"color: rgb(255, 84, 0);\">".stripslashes($$comment_variable)."</span></div>";

							}

							$quotation_detail_list .="</div><!-- requirements div ends -->";
							$quotation_detail_list .="</div><!-- dynamic meet requirement ends -->";		
						}
						}
					}

					

					$when_do_you_need_the_product = "<div style=\"width:100%; float:left; margin:0\">".$when_do_you_need_the_product.$when_do_you_need_the_product2.$when_do_you_need_the_product3."</div>";
					$quotation_detail_list = str_replace("{timeframe}",$when_do_you_need_the_product,$quotation_detail_list);

				}

				

				$quotation_details .= $dynamic_quote;			
				$quotation_details .="<div style=\"width:100%;\">Contact Person: <span style=\"color: rgb(255, 84, 0);\">".$bestThree[$k]['contact_person']."</span></div>
				<div style=\"width:100%;\">Contact Details: <span style=\"color: rgb(255, 84, 0);\">".$bestThree[$k]['contact_details']."</span></div>						
				<div style=\"width:100%;\">Product Condition: <span style=\"color: rgb(255, 84, 0);\">".product_condition($bestThree[$k]['product_condition'])."</span></div>";

				if($bestThree[$k]['product_warranty']!="")

					$quotation_details .="<div style=\"width:100%;\">Comments on Product Condition: <span style=\"color: rgb(255, 84, 0);\">".stripslashes($bestThree[$k]['product_warranty'])."</span></div>";
				if($bestThree[$k]['terms_and_conditions']!="")	
					$quotation_details .="<div style=\"width:100%;\">Offer Comments: <span style=\"color: rgb(255, 84, 0);\">".stripslashes($bestThree[$k]['terms_and_conditions'])."</span></div>";
					
				$quotation_details .="<div style=\"width:100%;\">Grand Quotation Total: <span style=\"color: rgb(255, 84, 0);\">$".$bestThree[$k]['finalQuotationAmount']."</span></div>";
				$quotation_details .="</li></ul>";

			//// get dynamic data  ends ///////

			if($bestThree[$k]['warranty_price']!="")				
				$quotation_detail_list .="<div style=\"width:100%; float:left; margin:3px 0\">Warranty Price:<span style=\"color:#000; padding-left:3px; color:rgb(255, 84, 0)\">".($bestThree[$k]['warranty_price']==0?"FREE":"$".$bestThree[$k]['warranty_price'])."</span></div>";
				

			if($bestThree[$k]['terms_and_conditions']!="")
			$quotation_detail_list .= "<div style=\"width:100%; float:left; margin:0; padding:0; border-top:1px solid #dedede\">
                  <div style=\"width:96%; float:left; margin:0; padding:6px 2%; float:left; font-size:13px; color:rgb(126, 126, 126); line-height:18px\">
                    <div style=\"width:100%\"> <span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Offer Comments:</span> <span style=\"width:100%; float:left\">".$bestThree[$k]['terms_and_conditions']."</span> </div>
                  </div>

                </div><!-- offer comments -->";

			$quotation_detail_list .="
				<div style=\"width:100%; float:left; margin:0; padding:0; border-top:1px solid #dedede\">
                  <div style=\"width:96%; float:left; margin:9px 0 0; padding:6px 2%; float:left; font-size:21px; color:rgb(255, 84, 0); line-height:21px;\">"; 
				  
				  //=============bonus section=============//
				  
			   $grandTotal = $bestThree[$k]['finalQuotationAmount'];
			   $quotedAmount = ($grandTotal - ($bestThree[$k]['quotationAmount']*$result->quantity)) + ($bestThree[$k]['ticketAmount']*$result->quantity);
			   
			   $quotation_detail_list .="<div style=\"width:100%;\">		
				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
				<tr>
				<td align=\"left\" valign=\"top\"><table width=\"100%\" border=\"0\" cellspacing=\"6\" cellpadding=\"6\">
				  <tr>
					<td width=\"40%\" rowspan=\"3\" align=\"center\" valign=\"middle\">";
					  if(((float)$bestThree[$k]['ticketAmount']-(float)$bestThree[$k]['quotationAmount'])>0){					  
					  	$quotation_detail_list .="<img src=\"".home_url()."/images/bonus-cash-icon.jpg\" width=\"150\" height=\"150\" alt=\"\" />";
					  }else{
						$quotation_detail_list .="&nbsp;&nbsp;";
					  }
					$quotation_detail_list .="</td>
					<td width=\"\" align=\"right\" valign=\"middle\" style=\"color:rgb(255,120,0); font-weight:bold;  font-size:16px\">Quoted Price: $</td>
					<td width=\"18%\" align=\"center\" valign=\"middle\"><span style=\"background-color:rgb(0,0,0); color:rgb(255,255,255); font-weight:bold;  font-size:16px; height:25px; width:100%; padding:6px 9px; display:block; line-height:25px\">".number_format($quotedAmount,2)."</span></td>
				  </tr>";
				  
				  if(((float)$bestThree[$k]['ticketAmount']-(float)$bestThree[$k]['quotationAmount'])>0){
					  
					  
				  $quotation_detail_list .="<tr>
					<td align=\"right\" valign=\"middle\" style=\"color:rgb(255,0,0); font-weight:bold;  font-size:16px\">HAGGLEFREE Bonus: $<br /><span style=\"color:rgb(0,0,0); font-size:10px\">(Bonus Expires after ".date("g:i a d/m/Y",strtotime($offerExp)).")</span></td>
					<td align=\"center\" valign=\"middle\"><span style=\"background-color:rgb(0,0,0); color:rgb(255,255,255); font-weight:bold;  font-size:16px; height:25px; width:100%; padding:6px 9px; display:block; line-height:25px\">".number_format(((float)$quotedAmount-(float)$grandTotal),2)."</span></td>
				  </tr>
				  <tr>
					<td align=\"right\" valign=\"middle\" style=\"color:rgb(255,120,0); font-weight:bold;  font-size:16px\">HAGGLEFREE Price: $</td>
					<td align=\"center\" valign=\"middle\"><span style=\"background-color:rgb(0,0,0); color:rgb(255,255,255); font-weight:bold;  font-size:16px; height:25px; width:100%; padding:6px 9px; display:block; line-height:25px\">".number_format($grandTotal,2)."</span></td>					
				  </tr>";
				  }
				  
				$quotation_detail_list .="</table></td>
				</tr>
				</table>
				</div>";
				
				  //================end bomus section =============//
				  
				  
				$quotation_detail_list .="</div></div>
			 </div>
			</div>";				

			//===============Update quotation status================//

				$update_val = array('status' => 2);

				$where = array('id' => $bestThree[$k]['quotationId']);	

				$wpdb->update($tableQuotation,$update_val,$where);


		    //==========Calculate lead fee and insert in database===============//
			$userDiscount = (float)get_user_meta($retaler_info->ID, 'cr_lead_fee', true);

			if($userDiscount > 0){			
			   $calLeadFee = ($rrp*$leadFee)/100;
			   $calLeadFee = ($calLeadFee - (($calLeadFee*$userDiscount)/100));
			}
			else{
			   $calLeadFee = ($rrp*$leadFee)/100;
			}

			//===============Update lead fee================//

				$update_val = array('rrp' => $rrp,'leadFee' => $leadFee,'leadDiscount' => $userDiscount,'totalLeadFee' => $calLeadFee);

				$where = array('id' => $bestThree[$k]['quotationId']);	

				$wpdb->update($tableQuotation,$update_val,$where);			 

			    if($calLeadFee > 0){

					$leadFeeText = "<div style=\"width:100%;\"><b> $".round($calLeadFee,2)."</b></div>";

				}else{

					$leadFeeText = "<div style=\"width:100%;\"><b>No Lead fee applicable.</b></div>";

				}


		//echo "//================ send email to retailer selected quote==========2====================// 			 ";

			 	

			$headers[] = 'Content-Type: text/html; charset=UTF-8';

			$headers[] = 'From: '.get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'> ' . "\r\n";			

			$mail_content = stripslashes($my_quotation_selected['email_template']);

			

			//======== Token Replacement =================//

			$mail_content = str_replace("{site_name}",get_bloginfo( 'name' ),$mail_content);

			$mail_content = str_replace("{site_url}",home_url(),$mail_content);

			$mail_content = str_replace("{site_tag}",get_bloginfo( 'description' ),$mail_content);

			$mail_content = str_replace("{media_url}",$media_url,$mail_content);	

			$mail_content = str_replace("{email_heading}",stripslashes($my_quotation_selected['email_heading']),$mail_content);

			

			$mail_content = str_replace("{company_name}",$company_name,$mail_content);

			$mail_content = str_replace("{quotation_amount}",$bestThree[$k]['finalQuotationAmount'],$mail_content);

			$mail_content = str_replace("{product_details}",$product_details,$mail_content);

			$mail_content = str_replace("{buyer_details}",$buyer_details,$mail_content);

			$mail_content = str_replace("{customer_requirements}",$customer_requirements,$mail_content);

			$mail_content = str_replace("{quotation_details}",$quotation_details,$mail_content);

			$mail_content = str_replace("{lead_fee}",$leadFeeText,$mail_content);	



			$mail_content = str_replace("{admin_email}",get_option( 'admin_email' ),$mail_content);

			$mail_content = str_replace("{site_abn}",get_option( 'site_abn' ),$mail_content);

			$mail_content = str_replace("{site_phone}",get_option( 'site_phone' ),$mail_content);

			$mail_content = str_replace("{fb_url}",get_option( 'fb_url' ),$mail_content);

			

			$subject = $my_quotation_selected['subject'];

			$subject = str_replace("{job_id}",$result->id,$subject);

			$subject = str_replace("{sku}",$product->get_sku(),$subject);
			
			$subject = str_replace("{postcode}",$result->Postcode,$subject);
			$subject = str_replace("{qty}",$result->quantity,$subject);
			$subject = str_replace("{job_type}",($result->is_commercial=='1'?'Commercial':'Private'),$subject);


			//======== Token Replacement =================//

			

			wp_mail( $retaler_info->user_email, $subject, $mail_content,$headers);

			//echo $mail_content."=====".$subject."=======";			

		 }

		 

		 if(count($noBestThree) > 0){

		

		   $limit2 = ($job_no_of_quotes-$limit);

		   if($limit2 > count($noBestThree))

		   $limit2 = count($noBestThree);

		   

		   for($m = 0; $m < ($limit2); $m++){	 

			 

			$retaler_info = get_userdata($noBestThree[$m]['retailerId']);

			$company_name = get_user_meta($noBestThree[$m]['retailerId'],'cr_company',true);

			$quotation_amount = $noBestThree[$m]['quotationAmount'];

			//print_r($retaler_info);

			$retalerDisplayName = $retaler_info->display_name;

			 if($company_name == ""){

				if($retalerDisplayName!="")

				$company_name = ucfirst($retalerDisplayName);

				else

				$company_name = ucfirst($retaler_info->first_name);

			 }

			

			$product_condition = product_condition($noBestThree[$m]['product_condition']);

			$isgst = ($result->is_commercial==1?'(Inc GST)':'(Inc GST)');

			

			    //======================== For retailer =========================//

			$quotation_details ="<ul style=\"width:100%; float:left; margin:0 0; padding:0\">

            <li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#000; border-radius:4px 4px 0 0; text-align:left; color:#fff; font-weight:normal; font-size:14px\">Your Quotation Details:</li>

                    <li style=\"width:96%; float:left; padding:10px 2%; margin:0; list-style:none; background-color:#fff; border-radius:0 0 4px 4px; text-align:left; color:#000; font-weight:normal; font-size:14px\">

                        <div style=\"width:100%;\">Quotation Amount: <span style=\"color: rgb(255, 84, 0);\">$".$quotation_amount." per item</span></div>";

			if($noBestThree[$m]['comments']!="")							  

				$quotation_details .= "<div style=\"width:100%;\">Comments on Quoted Price:<span style=\"color: rgb(255, 84, 0);\">".stripslashes($noBestThree[$m]['comments'])."</span> </div>";

				

				$quotation_details .= "<div style=\"width:100%;\">Quotation Date: <span style=\"color: rgb(255, 84, 0);\">".date("l, F j, Y",strtotime($noBestThree[$m]['quotationDate']))."</span></div>";

			

			



			//======================== quotation detail list for customer =========================//

			$quotation_detail_list .="<div style=\"width:100%; margin:0 auto; padding:0 0 5px; background-color: rgb(255, 255, 255); border-radius:0 0 4px 4px\">

            

              <div style=\"width:100%; overflow:hidden; padding:9px 0; margin:0 auto; background-color: rgb(0, 0, 0); border-radius:4px 4px 0 0; text-align:left; color:#fff; font-weight:normal; font-size:14px; color:rgb(255, 84, 0); font-weight:bold; text-transform:uppercase\">

                <div style=\"width:95%; margin:0 auto;\">Quotation No :<span style=\"border-radius: 50%; background-color: rgb(255, 84, 0); color: rgb(255, 255, 255); float: right; height: 24px; width: 24px; text-align: center; font-weight: bold; line-height: 24px;\">".($k+$m+1)."</span></div>

              </div>              
				
				<!-- retailer info starts -->             
				  <div style=\"width:100%; float:left; padding:0; margin:0 auto; text-align:center\">";
				  if(get_user_meta( $noBestThree[$m]['retailerId'], 'cr_photo', true )){
						$quotation_detail_list .="<div style=\"float:left; width:40%; padding:10px 5px; margin:0; min-width:200px\">					
							<img src=\"".get_user_meta( $noBestThree[$m]['retailerId'], 'cr_photo', true )."\" alt=\"\" style=\"float:left; margin:0 auto; display:inline-block; text-align:center; max-width:98%\">
						</div>";
				  }
				  
				    $quotationdAmount =  number_format($noBestThree[$m]['ticketAmount']*$result->quantity,2);
				  
					$quotation_detail_list .="<div style=\"width:".(get_user_meta( $noBestThree[$m]['retailerId'], 'cr_photo', true )!=""?"55%":"100%")."; overflow:hidden; padding:10px 5px; margin:0 auto 15px; text-align:left; color:#000; font-weight:normal; font-size:14px; float:left\">
						<div style=\"width:100%;\">Retailer Name: <span style=\"color: rgb(255, 84, 0);\">".$company_name."</span></div>
						<div style=\"width:100%;\">Contact Name: <span style=\"color: rgb(255, 84, 0);\"> ".$noBestThree[$m]['contact_person']."</span></div>
						<div style=\"width:100%;\">Contact Details: <span style=\"color: rgb(255, 84, 0);\">".$noBestThree[$m]['contact_details']."</span></div>
					  </div>
				  </div>
				  <!-- retailer info ends --> 
			  

			  <div style=\"width:95%; margin:0 auto 5px; padding:4px 5px; border:1px solid #dedede; overflow:hidden\">

              <!-- quotation amount starts -->

                <div style=\"width:100%; float:left; margin:0; padding:0\">

                  <div style=\"width:96%; float:left; margin:0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; \">

                    <div style=\"width:100%\">Quotation Amount $isgst:<span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">$".number_format($noBestThree[$m]['ticketAmount'],2)." X ".$result->quantity." = $".$quotationdAmount."</span></div>

                  </div>

                ";

							  

			if($noBestThree[$m]['comments']!="")							  

				$quotation_detail_list .= "<div style=\"width:96%; float:left; margin:0; padding:6px 2%; float:left; font-size:13px; color:rgb(126, 126, 126); line-height:18px\">

                    <div style=\"width:100%;\"><span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments on Quoted Price:</span> <span style=\"width:100%; float:left\">".stripslashes($noBestThree[$m]['comments'])."</span></div>

                  </div>";

				

				$quotation_detail_list .= "</div><!-- quotation amount ends -->";

				

				$quotation_detail_list .="<!-- product condition ends -->

				<div style=\"width:100%; float:left; margin:0\">

                  <div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238)\">

                    <div style=\"width:100%\">Product Condition:<span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".$product_condition."</span></div>

                  </div>                  

                ";				



			if($noBestThree[$m]['product_warranty']!="")

				$quotation_detail_list .= "<div style=\"width:96%; float:left; margin:0; padding:6px 2%; float:left; font-size:13px; color:rgb(126, 126, 126); line-height:18px\">

                    <div style=\"width:100%;\"><span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments on Product Condition:</span> <span style=\"width:100%; float:left\">".stripslashes($noBestThree[$m]['product_warranty'])."</span></div>

                  </div>";

				$quotation_detail_list .= "</div><!-- product condition ends -->";			

				// dynamic quote detail 

		$da_quote = get_dynamic_quote_attributes($noBestThree[$m]['quotationId']);

		 if(count($da_quote) > 0){

			foreach($da_quote as $key => $value){

				$$key = $value;

			}

		}				


				if(count($das['key']) > 0){

					$dynamic_quote ="";

					$quotation_detail_list.="{timeframe}";

					foreach($das['key'] as $key => $val){

						$price_variable = "price_".$val;

						$meet_variable = "meet_".$val;

						$comment_variable = "comment_".$val;

						if($val == "wccpf_when_do_you_need_the_product" || $comment_variable == "comment_wccpf_when_do_you_need_the_product"){
							if($val == "wccpf_when_do_you_need_the_product")
							     $when_do_you_need_the_product = "";
								/*$when_do_you_need_the_product = "
								<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; text-indec:1%\">
                    <div style=\"width:100%;\">Timeframe: <span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".product_delivery_switch($das['customer_choice'][$val])."</span> </div>
                  </div>
							  ";*/
							 if($noBestThree[$m][$meet_variable]!=""){
									$when_do_you_need_the_product .="
									<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; \">
										<div style=\"width:100%\">Can you meet ".product_delivery_switch($das['customer_choice'][$val])." requirements? : <span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".ucfirst($noBestThree[$m][$meet_variable])."</span> </div>
									</div>
									";
									$dynamic_quote .= "<div style=\"width:100%;\">Can you meet ".product_delivery_switch($das['customer_choice'][$val])." requirements? <span style=\"color: rgb(255, 84, 0);\">".ucfirst($noBestThree[$m][$meet_variable])."</span></div>";
								} 
							 if($noBestThree[$m][$comment_variable]!=""){
								
								$when_do_you_need_the_product .="
								<div style=\"width:96%; float:left; margin:0; padding:6px 2%; float:left; font-size:13px; color:rgb(126, 126, 126); line-height:18px\">
									<div style=\"width:100%;\">
									<span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments:</span>
									<span style=\"width:100%; float:left\">".stripslashes($noBestThree[$m][$comment_variable])."</span> </div>
								  </div>";
							 }
							 if($$comment_variable != "")
							 $dynamic_quote .= "<div style=\"width:100%;\">Timeframe Comment :<span style=\"color: rgb(255, 84, 0);\">".stripslashes($$comment_variable)."</span></div>";
							}
							
						elseif($val == "wccpf_deliverypickup" || $comment_variable == "comment_wccpf_deliverypickup"){
							$when_do_you_need_the_product2 = "";
						if($val == "wccpf_deliverypickup"){	    
								//$when_do_you_need_the_product2 ="<li style=\"width:98%; float:left; margin:5px 0 0 0; padding:6px 1%; float:left; list-style:none; background-color:#eee\">".ucfirst($das['customer_choice'][$val])." :<span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">Yes</span></li>";
								/*$when_do_you_need_the_product2 .="
								<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; \">
                    <div style=\"width:100%\">".$das['label'][$val]." : <span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".ucfirst($das['customer_choice'][$val])."</span> </div>
                  </div>";

								$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." <span style=\"color: rgb(255, 84, 0);\">".ucfirst($das['customer_choice'][$val])."</span></div>";	*/								
								
								
						}
						if($noBestThree[$m][$meet_variable]!=""){
									$when_do_you_need_the_product2 .="
									<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; \">
										<div style=\"width:100%\">Can you meet ".$das['label'][$val]." requirements? : <span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".ucfirst($noBestThree[$m][$meet_variable])."</span> </div>
									</div>
									";
									$dynamic_quote .= "<div style=\"width:100%;\">Can you meet ".$das['label'][$val]." requirements? <span style=\"color: rgb(255, 84, 0);\">".ucfirst($noBestThree[$m][$meet_variable])."</span></div>";
						}
						$when_do_you_need_the_product2 .="
						<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff;\">";
						
							if($noBestThree[$m][$price_variable]!=""){				
								$when_do_you_need_the_product2 .="
								<div style=\"width:100%; float:left; margin:3px 0\">".$das['label'][$val]." Price $isgst: <span style=\"color:#000; padding-left:3px; color:rgb(255, 84, 0)\">".($noBestThree[$m][$price_variable]==0?"FREE":"$".$noBestThree[$m][$price_variable])."</span> </div>
								";
								$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." Price <span style=\"color: rgb(255, 84, 0);\">$".$$price_variable." per item</span></div>";
							}
							if($noBestThree[$m][$comment_variable]!=""){
								$when_do_you_need_the_product2 .="
								<div style=\"width:100%\"> <span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments:</span> <span style=\"width:100%; float:left\">".stripslashes($noBestThree[$m][$comment_variable])."</span> </div>
								";
								$dynamic_quote .= "<div style=\"width:100%;\">Commetns on ".$das['label'][$val]." <span style=\"color: rgb(255, 84, 0);\">".stripslashes($noBestThree[$m][$comment_variable])."</span></div>";
							}
							
						$when_do_you_need_the_product2 .="</div>";
								
						}
						elseif($val == "wccpf_removal_of_old_appliance"){
							$when_do_you_need_the_product3 = "";
							if($das['customer_choice'][$val] == "no")//job requirements is no, no need to show it in form and quote template
								continue;
							else{
								if($noBestThree[$m][$meet_variable]!=""){
									/*$when_do_you_need_the_product3 .="
									<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; \">
										<div style=\"width:100%\">".$das['label'][$val]." : <span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".ucfirst($noBestThree[$m][$meet_variable])."</span> </div>
									</div>
									";
									$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." <span style=\"color: rgb(255, 84, 0);\">".ucfirst($noBestThree[$m][$meet_variable])."</span></div>";*/
								}
								
										
										if($noBestThree[$m][$price_variable]!=""){
										
										   $when_do_you_need_the_product3 .="
									       <div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; float:left; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff;\">";	
															
											$when_do_you_need_the_product3 .="
											<div style=\"width:100%; float:left; margin:3px 0\">".$das['label'][$val]." Price $isgst: <span style=\"color:#000; padding-left:3px; color:rgb(255, 84, 0)\">".($noBestThree[$m][$price_variable]==0?"FREE":"$".$noBestThree[$m][$price_variable])."</span> </div>
											";
											$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." Price <span style=\"color: rgb(255, 84, 0);\">$".$$price_variable." per item</span></div>";
										    $when_do_you_need_the_product3 .="</div>";
										
										}
										
										
										
										if($noBestThree[$m][$comment_variable]!=""){
										
										    $when_do_you_need_the_product3 .="
									        <div style=\"width:96%; font-size:13px; color:rgb(126, 126, 126); margin:0; padding:6px 2%; float:left; background-color: rgb(255, 255, 255)\">";	
											
											$when_do_you_need_the_product3 .="
											<div style=\"width:100%\"> <span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments:</span> <span style=\"width:100%; float:left\">".stripslashes($noBestThree[$m][$comment_variable])."</span> </div>
											";
											$dynamic_quote .= "<div style=\"width:100%;\">Comments on ".$das['label'][$val]." <span style=\"color: rgb(255, 84, 0);\">".stripslashes($noBestThree[$m][$comment_variable])."</span></div>";
											$when_do_you_need_the_product3 .="</div>";
										}
										
									
							}
						}

						else{

							/*$when_do_you_need_the_product .= "

								<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; background-color: rgb(238, 238, 238); border-bottom:2px solid #fff; text-indec:1%\">

                    <div style=\"width:100%;\">".$das['label'][$val].": <span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".$das['customer_choice'][$val]."</span> </div>

                  </div>

							  ";*/	

						if($noBestThree[$m][$meet_variable]!=""){

								$quotation_detail_list .="<!-- dynamic meet requirement starts --><div style=\"width:100%; float:left; margin:0; padding:0\">";

								if($val =="wccpf_when_do_you_need_the_product"){

									$dynamic_quote .= "<div style=\"width:100%;\">Can you meet ".product_delivery_switch($das['customer_choice'][$val])." Timeframe? <span style=\"color: rgb(255, 84, 0);\">".ucfirst($$meet_variable)."</span></div>";

									if($$comment_variable != "")

									$dynamic_quote .= "<div style=\"width:100%;\">Timeframe Comment  :<span style=\"color: rgb(255, 84, 0);\">".stripslashes($$comment_variable)."</span></div>";

									continue;

								}else{

									if($val != "wccpf_deliverypickup"){

									  $dynamic_quote .= "<div style=\"width:100%;\">Can you meet ".$das['label'][$val]."? <span style=\"color: rgb(255, 84, 0);\">".ucfirst($$meet_variable)."</span></div>";

								    }

							        

									/*$quotation_detail_list .="

									<div style=\"width:96%; float:left; margin:4px 0 0 0; padding:6px 2%; background-color: rgb(238, 238, 238)\">

                    					<div style=\"width:100%\"> 

											<span style=\"float:left\">".$das['label'][$val]." :</span> 

											<span style=\"color: rgb(255, 84, 0); padding:0 0 0 3px\">".ucfirst($das['customer_choice'][$val])."</span> 

										</div>

									</div>

									";
*/
								}

							$quotation_detail_list .= "<!-- requirements div starts --><div style=\"width:96%; float:left; margin:0; padding:6px 2%; font-size:13px; color:rgb(126, 126, 126); line-height:18px\">";

							$quotation_detail_list .=" 

							<div style=\"width:100%; float:left; margin:3px 0\"> <span style=\"float:left\">Can you meet the customers requirement?:</span> <span style=\"color:#000; padding-left:3px\">".ucfirst($noBestThree[$m][$meet_variable])."</span> </div>";

							if($noBestThree[$m][$price_variable]!=""){				

								$quotation_detail_list .="

								<div style=\"width:100%; float:left; margin:3px 0\"> <span style=\"float:left\">".$das['label'][$val]." Price $isgst:</span> <span style=\"color:#000; padding-left:3px; color:rgb(255, 84, 0)\">".($noBestThree[$m][$price_variable]==0?"FREE":"$".$noBestThree[$m][$price_variable])."</span> </div>

								";							

								$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." Price <span style=\"color: rgb(255, 84, 0);\">$".$$price_variable." per item</span></div>";

							}

							if($noBestThree[$m][$comment_variable]!=""){

								$quotation_detail_list .="

								<div style=\"width:100%\"> <span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Comments:</span> <span style=\"width:100%; float:left\">".stripslashes($noBestThree[$m][$comment_variable])."</span> </div>

								";

								$dynamic_quote .= "<div style=\"width:100%;\">".$das['label'][$val]." Comment  <span style=\"color: rgb(255, 84, 0);\">".stripslashes($$comment_variable)."</span></div>";

							}

							$quotation_detail_list .="</div><!-- requirements div ends -->";

							$quotation_detail_list .="</div><!-- dynamic meet requirement ends -->";		
						}
					  }
					}

					$when_do_you_need_the_product = "<div style=\"width:100%; float:left; margin:0\">".$when_do_you_need_the_product.$when_do_you_need_the_product2.$when_do_you_need_the_product3."</div>";

					$quotation_detail_list = str_replace("{timeframe}",$when_do_you_need_the_product,$quotation_detail_list);

				}

			        $quotation_details .= $dynamic_quote;			

					$quotation_details .="<div style=\"width:100%;\">Contact Person: <span style=\"color: rgb(255, 84, 0);\">".$noBestThree[$m]['contact_person']."</span></div>

					<div style=\"width:100%;\">Contact Details: <span style=\"color: rgb(255, 84, 0);\">".$noBestThree[$m]['contact_details']."</span></div>						

					<div style=\"width:100%;\">Product Condition: <span style=\"color: rgb(255, 84, 0);\">".product_condition($noBestThree[$m]['product_condition'])."</span></div>";

					if($noBestThree[$m]['product_warranty']!="")

						$quotation_details .="<div style=\"width:100%;\">Comments on Product Condition: <span style=\"color: rgb(255, 84, 0);\">".stripslashes($noBestThree[$m]['product_warranty'])."</span></div>";
						
					if($noBestThree[$m]['terms_and_conditions']!="")	
					$quotation_details .="<div style=\"width:100%;\">Offer Comments:<span style=\"color: rgb(255, 84, 0);\">".stripslashes($noBestThree[$m]['terms_and_conditions'])."</span></div>";
					
					$quotation_details .="<div style=\"width:100%;\">Grand Quotation Total: <span style=\"color: rgb(255, 84, 0);\">$".$noBestThree[$m]['finalQuotationAmount']."</span></div>";

                    $quotation_details .="</li></ul>";				


			if($noBestThree[$m]['warranty_price']!="")				

				$quotation_detail_list .="<div style=\"width:100%; float:left; margin:3px 0\">Warranty Price:<span style=\"color:#000; padding-left:3px; color:rgb(255, 84, 0)\">".($noBestThree[$m]['warranty_price']==0?"FREE":"$".$noBestThree[$m]['warranty_price'])."</span></div>";


			if($noBestThree[$m]['terms_and_conditions']!="")

			$quotation_detail_list .= "<div style=\"width:100%; float:left; margin:0; padding:0; border-top:1px solid #dedede\">

                  <div style=\"width:96%; float:left; margin:0; padding:6px 2%; float:left; font-size:13px; color:rgb(126, 126, 126); line-height:18px\">

                    <div style=\"width:100%\"> <span style=\"color: rgb(255, 84, 0); text-transform:uppercase; font-size:11px; width:100%; float:left\">Offer Comments:</span> <span style=\"width:100%; float:left\">".stripslashes($noBestThree[$m]['terms_and_conditions'])."</span> </div>

                  </div>

                </div><!-- offer comments -->";

			$quotation_detail_list .="
				<div style=\"width:100%; float:left; margin:0; padding:0; border-top:1px solid #dedede\">
                  <div style=\"width:96%; float:left; margin:9px 0 0; padding:6px 2%; float:left; font-size:21px; color:rgb(255, 84, 0); line-height:21px;\">"; 
				  
				   //=============bonus section=============//
			   $grandTotal = $noBestThree[$m]['finalQuotationAmount'];
			   $quotedAmount = ($grandTotal - ($noBestThree[$m]['quotationAmount']*$result->quantity)) + ($noBestThree[$m]['ticketAmount']*$result->quantity);
			  
			   $quotation_detail_list .="<div style=\"width:100%;\">	
				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
				<tr>
				<td align=\"right\" valign=\"top\" width=\"40%\">";
					  if(((float)$noBestThree[$m]['ticketAmount']-(float)$noBestThree[$m]['quotationAmount'])>0){
					  	$quotation_detail_list .="<img src=\"".home_url()."/images/bonus-cash-icon.jpg\" width=\"150\" height=\"150\" alt=\"\" />";
					  }else{
						$quotation_detail_list .="&nbsp;&nbsp;";
					  }
					$quotation_detail_list .="</td>
				<td valign=\"top\" width=\"60%\">
				<table width=\"100%\" border=\"0\" cellspacing=\"6\" cellpadding=\"6\">
				  <tr>					
					<td width=\"82%\" align=\"right\" valign=\"middle\" style=\"color:rgb(255,120,0); font-weight:bold;  font-size:16px\">Quoted Price: $</td>
					<td width=\"18%\" align=\"center\" valign=\"middle\"><span style=\"background-color:rgb(0,0,0); color:rgb(255,255,255); font-weight:bold;  font-size:16px; height:25px; width:100%; padding:6px 9px; display:block; line-height:25px\">".number_format($quotedAmount,2)."</span></td>
				  </tr>";
				  
				  if(((float)$noBestThree[$m]['ticketAmount']-(float)$noBestThree[$m]['quotationAmount'])>0){
				  $quotation_detail_list .="<tr>
					<td align=\"right\" valign=\"middle\" style=\"color:rgb(255,0,0); font-weight:bold;  font-size:16px\">HAGGLEFREE Bonus: $<br /><span style=\"color:rgb(0,0,0); font-size:10px\">(Bonus Expires after ".date("g:i a d/m/Y",strtotime($offerExp)).")</span></td>
					<td align=\"center\" valign=\"middle\"><span style=\"background-color:rgb(0,0,0); color:rgb(255,255,255); font-weight:bold;  font-size:16px; height:25px; width:100%; padding:6px 9px; display:block; line-height:25px\">".number_format(((float)$quotedAmount-(float)$grandTotal),2)."</span></td>
				  </tr>
				  <tr>
					<td align=\"right\" valign=\"middle\" style=\"color:rgb(255,120,0); font-weight:bold;  font-size:16px\">HAGGLEFREE Price: $</td>
					<td align=\"center\" valign=\"middle\"><span style=\"background-color:rgb(0,0,0); color:rgb(255,255,255); font-weight:bold;  font-size:16px; height:25px; width:100%; padding:6px 9px; display:block; line-height:25px\">".number_format($grandTotal,2)."</span></td>					
				  </tr>";
				  }
				  
				$quotation_detail_list .="</table>
				   </td>
				  </tr>
				 </table>
				 </td>
				</div>";
				
				  //================end bomus section =============//
				  
				  $quotation_detail_list .="</div></div>
			 
			 </div>
			</div>";			

			//================== quotation detail list ================================//

			//===============Update quotation status================//

				$update_val = array('status' => 2);

				$where = array('id' => $noBestThree[$m]['quotationId']);	

				$wpdb->update($tableQuotation,$update_val,$where);

				

		    //==========Calculate lead fee and insert in database===============//

			

			$userDiscount = (float)get_user_meta($retaler_info->ID, 'cr_lead_fee', true);

			if($userDiscount > 0){			

			   $calLeadFee = ($rrp*$leadFee)/100;

			   $calLeadFee = ($calLeadFee - (($calLeadFee*$userDiscount)/100));

			}

			else{

			   $calLeadFee = ($rrp*$leadFee)/100;

			}

			//===============Update lead fee================//

				$update_val = array('rrp' => $rrp,'leadFee' => $leadFee,'leadDiscount' => $userDiscount,'totalLeadFee' => $calLeadFee);

				$where = array('id' => $bestThree[$k]['quotationId']);	

				$wpdb->update($tableQuotation,$update_val,$where);			 

			

			if($calLeadFee > 0){

				$leadFeeText = "<div style=\"width:100%;\"><b> $".round($calLeadFee,2)."</b></div>";

			}else{

				$leadFeeText = "<div style=\"width:100%;\"><b>No Lead fee applicable.</b></div>";

			}

			

			//echo " //================ send email to retailer selected quote===========1===================//";

			 	

			$headers[] = 'Content-Type: text/html; charset=UTF-8';

			$headers[] = 'From: '.get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'> ' . "\r\n";			

			$mail_content = stripslashes($my_quotation_selected['email_template']);

			

			//======== Token Replacement =================//

			$mail_content = str_replace("{site_name}",get_bloginfo( 'name' ),$mail_content);

			$mail_content = str_replace("{site_url}",home_url(),$mail_content);

			$mail_content = str_replace("{site_tag}",get_bloginfo( 'description' ),$mail_content);

			$mail_content = str_replace("{media_url}",$media_url,$mail_content);	

			$mail_content = str_replace("{email_heading}",stripslashes($my_quotation_selected['email_heading']),$mail_content);

			

			$mail_content = str_replace("{company_name}",$company_name,$mail_content);

			$mail_content = str_replace("{quotation_amount}",$noBestThree[$m]['finalQuotationAmount'],$mail_content);

			$mail_content = str_replace("{product_details}",$product_details,$mail_content);

			$mail_content = str_replace("{buyer_details}",$buyer_details,$mail_content);

			$mail_content = str_replace("{customer_requirements}",$customer_requirements,$mail_content);

			$mail_content = str_replace("{quotation_details}",$quotation_details,$mail_content);

			$mail_content = str_replace("{lead_fee}",$leadFeeText,$mail_content);	

					

			$mail_content = str_replace("{admin_email}",get_option( 'admin_email' ),$mail_content);

			$mail_content = str_replace("{site_abn}",get_option( 'site_abn' ),$mail_content);

			$mail_content = str_replace("{site_phone}",get_option( 'site_phone' ),$mail_content);

			$mail_content = str_replace("{fb_url}",get_option( 'fb_url' ),$mail_content);

			

			$subject = $my_quotation_selected['subject'];

			$subject = str_replace("{job_id}",$result->id,$subject);

			$subject = str_replace("{sku}",$product->get_sku(),$subject);
			
			$subject = str_replace("{postcode}",$result->Postcode,$subject);
			$subject = str_replace("{qty}",$result->quantity,$subject);
			$subject = str_replace("{job_type}",($result->is_commercial=='1'?'Commercial':'Private'),$subject);


			//======== Token Replacement =================//

			wp_mail( $retaler_info->user_email, $subject, $mail_content,$headers);

			//echo $mail_content."=====".$subject."=======";


		   }

		 }


		//echo " //================ send email to customer===============best-3===============//";

		 

			$headers[] = 'Content-Type: text/html; charset=UTF-8';

			$headers[] = 'From: '.get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'> ' . "\r\n";

			

			//======== Token Replacement =================//

			$mail_content = str_replace("{site_name}",get_bloginfo( 'name' ),stripslashes($best3_template_detail['email_template']));
			
			$mail_content = str_replace("<img","<img style=\"max-width:100%\" ",$mail_content);

			$mail_content = str_replace("{site_url}",home_url(),$mail_content);

			$mail_content = str_replace("{site_tag}",get_bloginfo( 'description' ),$mail_content);

			$mail_content = str_replace("{media_url}",$media_url,$mail_content);	

			$mail_content = str_replace("{email_heading}",stripslashes($best3_template_detail['email_heading']),$mail_content);

			

			

			$mail_content = str_replace("{buyer_name}",$buyer_name,$mail_content);

			$mail_content = str_replace("{product_details}",$product_details,$mail_content);

			$mail_content = str_replace("{quotation_detail_list}",$quotation_detail_list,$mail_content);
			
			//========= review changes ============
			$mail_content = str_replace("{job_id}",$result->id,$mail_content);
			$mail_content = str_replace("{email}",$result->email,$mail_content);


			$mail_content = str_replace("{admin_email}",get_option( 'admin_email' ),$mail_content);

			$mail_content = str_replace("{site_abn}",get_option( 'site_abn' ),$mail_content);

			$mail_content = str_replace("{site_phone}",get_option( 'site_phone' ),$mail_content);

			$mail_content = str_replace("{fb_url}",get_option( 'fb_url' ),$mail_content);

			

			$subject = $best3_template_detail['subject'];

			$subject = str_replace("{job_id}",$result->id,$subject);

			$subject = str_replace("{sku}",$product->get_sku(),$subject);
			
			$subject = str_replace("{postcode}",$result->Postcode,$subject);
			$subject = str_replace("{qty}",$result->quantity,$subject);
			$subject = str_replace("{job_type}",($result->is_commercial=='1'?'Commercial':'Private'),$subject);

			//======== Token Replacement =================//

			$mail_content = '<!DOCTYPE html>

							<html lang="en">

								<head>

									<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

									<title>'.$subject.'</title>

								</head>					

								<body>'.$mail_content.'</body>

							</html>';
           
			

			wp_mail( $result->email,$subject, $mail_content,$headers);

		    wp_mail( get_option('admin_monitor_email'), "Notification for best quotation #".$result->id." / ".$product->get_sku(), $mail_content,$headers);

			//wp_mail( "chandan.k@nerdster.com.au", "Notification for best quotation #".$result->id." / ".$product->get_sku(), $mail_content,$headers);			

	      // echo $mail_content;

	      //================ Update job status to send quotation==============================//   	 

				$update_val = array('status' => 2);
				$where = array('id' => $result->id);
				$wpdb->update($tableJobs,$update_val,$where); 

	   }else{

		//echo "//================ Mail send to customer on no quotation found against a job==============================//   ";

			$headers[] = 'Content-Type: text/html; charset=UTF-8';

			$headers[] = 'From: '.get_bloginfo( 'name' ).' <'.get_option( 'admin_email' ).'> ' . "\r\n";

			$template_name = "no_quotations_for_buyer";

			$media_url = get_stylesheet_directory_uri()."/img";

			//Codes: {site_url}, {site_tag}, {media_url}, {email_heading}, {site_name}, {buyer_name}, {post_date}, {product_details}, {buyer_details}, {customer_requirements}, {admin_email}, {site_abn}, {site_phone}, {fb_url}

			$template_detail = get_email_template($template_name);

	   		$post_date = "<div style=\"width:100%\"><span style=\"color: rgb(255, 84, 0);\">".date("Y-m-d",strtotime($getJob->jobDate))."</span> <span style=\"color: #787878;\">".date("H:i:s",strtotime($getJob->jobDate))."</span></div>";

			$mail_content = stripslashes($template_detail['email_template']);

			//======== Token Replacement =================//

			$mail_content = str_replace("{site_name}",get_bloginfo( 'name' ),$mail_content);

			$mail_content = str_replace("{site_url}",home_url(),$mail_content);

			$mail_content = str_replace("{site_tag}",get_bloginfo( 'description' ),$mail_content);

			$mail_content = str_replace("{media_url}",$media_url,$mail_content);	

			$mail_content = str_replace("{email_heading}",stripslashes($template_detail['email_heading']),$mail_content);


			$mail_content = str_replace("{buyer_name}",$buyer_name,$mail_content);

			$mail_content = str_replace("{post_date}",$post_date,$mail_content);

			$mail_content = str_replace("{product_details}",$product_details,$mail_content);

			$mail_content = str_replace("{buyer_details}",$buyer_details,$mail_content);

			$mail_content = str_replace("{customer_requirements}",$customer_requirements,$mail_content);

			$mail_content = str_replace("{admin_email}",get_option( 'admin_email' ),$mail_content);

			$mail_content = str_replace("{site_abn}",get_option( 'site_abn' ),$mail_content);

			$mail_content = str_replace("{site_phone}",get_option( 'site_phone' ),$mail_content);

			$mail_content = str_replace("{fb_url}",get_option( 'fb_url' ),$mail_content);

			$subject = $template_detail['subject'];

			$subject = str_replace("{job_id}",$result->id,$subject);

			$subject = str_replace("{sku}",$product->get_sku(),$subject);
			
			$subject = str_replace("{postcode}",$result->Postcode,$subject);
			$subject = str_replace("{qty}",$result->quantity,$subject);
			$subject = str_replace("{job_type}",($result->is_commercial=='1'?'Commercial':'Private'),$subject);

			//======== Token Replacement =================//

			wp_mail( $result->email, $subject, $mail_content,$headers);	

			wp_mail( get_option('admin_monitor_email'), "Notification for no quotation #".$result->id." / ".$product->get_sku(), $mail_content,$headers);

			//echo $mail_content;


			$update_val = array('status' => 10);

			$where = array('id' => $result->id);	

			$wpdb->update($tableJobs,$update_val,$where);	   

		}

		//===================================================End===================================================
	}
	// ================ Unset dynamic product attribute variables ===================
	
	   if(count($da_quote) > 0){
			foreach($da_quote as $key => $value){
				unset($$key);
			}
		}
}
$fopen = fopen("job-send-to-customer-cron.txt", "a");
fwrite($fopen,"\nYes it is calling at ". date("l, F j, Y"));
fclose($fopen);
?>