<?php
/**
 * WordPress Cron Implementation for retailer
 * @package WordPress
 */

ignore_user_abort(true);

/**
 * Tell WordPress we are doing the CRON task.
 *
 * @var bool
 */

if ( !defined('ABSPATH') ) {
	/** Set up WordPress environment */
	require_once( dirname( __FILE__ ) . '/wp-load.php' );
}

global $wpdb;

$query = new WP_Query( array( 'post_type' => array( 'product') ,'posts_per_page' => '-1','orderby' => 'title','order'   => 'ASC',) );
$cnt =0;
?>
<table class="widefat" width="80%" border="1" cellspacing="0" cellpadding="10" align="center" bordercolor="#000000">
	<thead>
      <tr bgcolor="#999999">    	
        <th height="25"><strong>Name</strong></th>
        <th><strong>SKU</strong></th>
        <th><strong>RRP</strong></th>
        <th><strong>Stock Status</strong></th>
      </tr>
  </thead>  
  <tbody>
<?php
while ( $query->have_posts() ) : $query->the_post();
	$product = wc_get_product( get_the_ID() );	
	$stock = $product->is_in_stock();
	?>
    <tr p_id="<?= get_the_ID()?>">
        <td align="center"><?= $product->get_title() ?></td>
        <td align="center"><?= $product->get_sku()?></td>
        <td align="center"><?= $product->get_regular_price();?></td>
        <td align="center"><?= ($stock?"<p style=\"color:green\">In Stock</p>":"<p style=\"color:red\">Out of Stock</p>")?></td>
    </tr>
    <?php
	$cnt++;
endwhile;
?>
	</tbody>
</table>
<?php
echo "Total Products : $cnt";
?>